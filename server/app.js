/* serving angular files */
var express = require('express');
var app = express();
var path = require('path');

app.use(express.static(path.join(__dirname, '../dist')));

app.get('/*', function(req, res) {
  res.sendFile(path.join(__dirname, '../dist/index.html'));
});

app.listen(3350, function () {
  console.log('App started at 3350');
});