import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditOriginateComponent } from './edit-originate.component';

describe('EditOriginateComponent', () => {
  let component: EditOriginateComponent;
  let fixture: ComponentFixture<EditOriginateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditOriginateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditOriginateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
