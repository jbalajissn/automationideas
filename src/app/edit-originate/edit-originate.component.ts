import { Component, OnInit } from '@angular/core';
import { OriginateService } from '../show-originate/originateDetails';
import { MatDialog } from '@angular/material';
import { FormControl, Validators, FormGroup, FormGroupDirective } from '@angular/forms';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
import { HttpCOnfig } from '../utils/httpConfig';
import { CheckUserStatus } from '../utils/checkUserStatus';
import { OriginateDetailsComponent } from '../originate-details/originate-details.component';
import { OriginateCommentsViewComponent } from '../originate-comments-view/originate-comments-view.component';
import { isNull } from 'util';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-originate',
  templateUrl: './edit-originate.component.html',
  styleUrls: ['./edit-originate.component.css']
})
export class EditOriginateComponent implements OnInit {
  public user: string;
  originateTowerType: any[] = [];
  originateCategoryType: any[] = [];
  dialogRef: any;
  OriginateForm: FormGroup;
  public showOriginateDialogRef: any;
  public originateList: Array<any> = [];
  public isAdmin = false;
  searchOriginateForm = new FormGroup({
    searchField: new FormControl('', Validators.required)
  });

  constructor(
    public userinfo: CheckUserStatus,
    public showOriginateService: OriginateService,
    public matDialog: MatDialog,
    public router: Router,
    public httpConfig: HttpCOnfig) {
    this.getOriginateList();
    this.getUserInfo();
  }

  ngOnInit() {
  }

  // Check user admin or not
  getUserInfo() {
    if (isNull(JSON.parse(this.userinfo.getUserDetails()))) {
      this.router.navigateByUrl('/login');
    } else {
    this.user = JSON.parse(this.userinfo.getUserDetails())[0].username;
    }
  }

  getOriginateList() {
    this.showDialog();
    const originateGetAll: string = this.httpConfig.getOriginateDetails();
    this.showOriginateService.originateDetails(originateGetAll).subscribe((result) => {
      this.hideDialog();
      console.log(result);
      if (result.status) {
        console.log(result);
        this.originateList = [];
        this.originateList = result.data;
      }
    }, (error) => {
      console.log(error);
      this.hideDialog();
    });
  }

  // showing the dialog
  showDialog() {
    this.dialogRef = this.matDialog.open(ProgressDialogComponent, {data: {code: 1}});
  }

  // hiding the dialog
  hideDialog() {
    this.dialogRef.close();
  }

 getimgsrc(status: string) {
   if (status === 'Completed') {
   return ('../../assets/completed.png');
  } else if (status === 'New') {
   return ('../../assets/new.png');
  } else if (status === 'Deferred') {
   return ('../../assets/imageedit_13_8227496638.png');
  } else if ( status === 'Cancelled') {
   return ('../../assets/cancel.png');
  } else if ( status === 'On going') {
   return ('../../assets/inprogress.png');
  }
 }

  onSearchSubmit() {
    this.showDialog();
    let strStringFetch = this.searchOriginateForm.get('searchField').value;
    strStringFetch = '%' + strStringFetch + '%';
    console.log(strStringFetch);
    const originateSearch: string = this.httpConfig.getSearchOriginate();
    this.showOriginateService.searchoriginateDetails(originateSearch, {strSearch : strStringFetch}).subscribe((result) => {
      this.hideDialog();
      if (result.status) {
        console.log(result);
        if (result.data.length > 0) {
          this.originateList = [];
          this.originateList = result.data;
        } else {
          this.dialogRef = this.matDialog.open(ProgressDialogComponent,
            {data: {code: 6, message: 'Your Search did not return any match'}});
        }
      }
    }, (error) => {
      this.hideDialog();
      console.log(error);
    });
  }

  onOriginateClick (i) {
    // showing idea in dialog
    console.log(this.originateList[i]);
     this.showOriginateDialogRef = this.matDialog.open(OriginateDetailsComponent, {
        data: this.originateList[i]
      });
  }


  onOriginatComments(i) {
    console.log(this.originateList[i].OriginateID);
    const originateComments: string = this.httpConfig.commentsOriginate();
    this.showOriginateService.searchoriginateDetails(originateComments, {strSearch : this.originateList[i].OriginateID})
    .subscribe((result) => {
      if (result.status) {
        console.log(result);
        if (result.data.length > 0) {
          this.dialogRef = this.matDialog.open(OriginateCommentsViewComponent,
            {data: result.data});
        }
      }
    }, (error) => {
      this.hideDialog();
      console.log(error);
    });
  }
}
