import { ShowIdeaComponent } from './../show-idea/show-idea.component';
import { Component, OnInit } from '@angular/core';
import { IdeasServices } from './ideas-services';
import { MatDialog } from '@angular/material';
import { FormControl, Validators, FormGroup, FormGroupDirective } from '@angular/forms';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';

@Component({
  selector: 'app-ideas',
  templateUrl: './ideas.component.html',
  styleUrls: ['./ideas.component.css']
})
export class IdeasComponent implements OnInit {

  public dialogRef: any;
  public showIdeaDialogRef: any;
  public ideasList: Array<any> = [];
 
  constructor(
    public ideaServices: IdeasServices,
    public dialog: MatDialog
  ) {
    this.getIdeas();
  }

  searchIdeaForm = new FormGroup({
    searchField: new FormControl('', Validators.required)
  });

  ngOnInit() {
  }

  // getting ideas from server 
  getIdeas(){
    this.showDialog();
    this.ideaServices.getAllIdeas({}).subscribe((result)=>{
      this.hideDialog();
      if(result.status) {
        this.ideasList = result.data;
      }
    },(error)=>{
      console.log(error);
      this.hideDialog();
    })
    
  }

  // when user clicks onidea 
  onIdeaClick(i){
    // showing idea in dialog 
    // get id of selected idea
    // getting likes count 
    this.showDialog();
   
    this.ideaServices.getIdeasCount({id : this.ideasList[i].id}).subscribe((result)=>{
      this.hideDialog();
      if(result.status){
        this.showIdeaDialogRef = this.dialog.open(ShowIdeaComponent,
          {data: result.data[0], panelClass: 'show-idea'});
      }
    },(error)=>{
      this.hideDialog();
      console.log(error);
    })
   
  }



  // showing the dialog
  showDialog() {
    this.dialogRef = this.dialog.open(ProgressDialogComponent,{data:{code: 1}})
  }

  // hiding the dialog
  hideDialog() {
    this.dialogRef.close();
  }

  onSearchSubmit() {
    this.showDialog();
    let strStringFetch = this.searchIdeaForm.get('searchField').value;
    strStringFetch = '%' + strStringFetch + '%';
    this.ideaServices.getSearchIdea({strSearch : strStringFetch}).subscribe((result) => {
      this.hideDialog();
      if (result.status) {
        console.log(result);
        if (result.data.length > 0) {
          this.ideasList = [];
          this.ideasList = result.data;
        } else {
          this.dialogRef = this.dialog.open(ProgressDialogComponent,
            {data: {code: 5, message: 'Your Search did not return any match'}});
        }
      }
    }, (error) => {
      this.hideDialog();
      console.log(error);
    });
  }
}
