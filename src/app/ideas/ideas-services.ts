import { HttpCOnfig } from './../utils/httpConfig';
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

// this class helps us in making ideas http calls
@Injectable()
export class IdeasServices{
    constructor(private httpClinet: HttpClient, private httpConfig: HttpCOnfig){}

    // getting all the ideas from server 
    getAllIdeas(data){
        return this.httpClinet.post<any>(this.httpConfig.getIdeas(),data);
    }

    // get ideas count 
    getIdeasCount(data){
        
        return this.httpClinet.post<any>(this.httpConfig.getLikesCount(),data);
    }

    // getSearchIdea
    getSearchIdea(data) {
        return this.httpClinet.post<any>(this.httpConfig.searchIdeaDetails(), data);
    }
}
