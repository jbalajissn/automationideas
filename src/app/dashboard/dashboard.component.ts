import { Component, OnInit } from '@angular/core';
import { NavService } from '../utils/navServices';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  private navStatus: boolean = true;

  constructor(private navService: NavService) { }

  ngOnInit() {
    this.subScribeToNavService();
    this.navService.showNavStatus();
  }

  subScribeToNavService(){
    this.navService.getNavStatus().subscribe((status)=>{
      this.navStatus = status;
    })  
  }
  

}
