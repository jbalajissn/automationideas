import { ExcelService } from './../utils/excelService';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpCOnfig } from '../utils/httpConfig';
//import {single,multi} from './data';
import {innovationDashBoardService} from './innovationDashboardService';
import { count } from '@swimlane/ngx-charts';
declare let jsPDF;

@Component({
  selector: 'app-innovation-dashboard',
  templateUrl: './innovation-dashboard.component.html',
  styleUrls: ['./innovation-dashboard.component.css']
})
export class InnovationDashboardComponent implements OnInit {
  pdf = new jsPDF('l', 'mm', 'a4');
  rowTower1: any[];
  single: any[];
  count:any;
  test:any;
  
  BAview: any[] = [480, 350];
  multi: any[];
  finance: any[];
  tech:any[];
  view: any[] = [450,350];
  elements = ['Towerdata', 'StatusData', 'TechData'];
  elCount = -1;
  colorScheme = {
   // domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
   domain: ['#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886']
  };

  statColorcheme = {
    // domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
    domain: ['#A10A28', '#D3342D', '#EF6D49', '#FAAD67', '#FDDE90', '#DBED91', '#A9D770', '#6CBA67', '#2C9653', '#146738']
   };
   TechColorcheme = {
    // domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
    domain: ['#042e40', '#075170', '#53868b', '#b5d5e2', '#008b8b', '#bdb76b', '#3a90b3', '#6cacc6', '#9dc7d9', '#b5d5e2']
   };
   verStacScheme = {
    domain: ['#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886']
  };
 
  BAlegendTitle = 'Efforts';
  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Ideas';
  showYAxisLabel = true;
  yAxisLabel = 'Status';

   // line, area
   autoScale = true;

   constructor(
    //public dialogRef: MatDialogRef<DashboardDialogComponent>,
    private excelService : ExcelService,private httpConfig: HttpCOnfig,private dashboardService:innovationDashBoardService,
    //@Inject(MAT_DIALOG_DATA) public data: any
  ) {
      
    }

    async ngOnInit() {      
      this.getTowerCount();
      this.getStatusCount();
      this.getTechCount();
      this.getFinanceHourCount();  
  }

  
  getTowerCount(){
    return new Promise((resolve, reject) => {
      const innovationDashBoardDetailsURL: string = this.httpConfig.getIdeasCountByTower();
      this.dashboardService.innovationDashboard(innovationDashBoardDetailsURL).subscribe((result) => {
        if (result.status) {
         //(result.status ? resolve(result.data) : reject(result));
          if (result.data.length > 0) {
          // this.single = [];
          this.single = result.data;          
          }
        }
      }, (error) => {
        reject(error);
      });
    });
  }

  getTechCount(){
    return new Promise((resolve, reject) => {
      const innovationDashBoardDetailsURL: string = this.httpConfig.getIdeasCountByTech();
      this.dashboardService.innovationDashboard(innovationDashBoardDetailsURL).subscribe((result) => {
        if (result.status) {
          if (result.data.length > 0) {
            this.tech = [];
            this.tech = result.data;
          }
        }
      }, (error) => {
        reject(error);
      });
    });
  }

  getStatusCount(){
    return new Promise((resolve, reject) => {
      const innovationDashBoardDetailsURL: string = this.httpConfig.getIdeasCountByStatus();
      this.dashboardService.innovationDashboard(innovationDashBoardDetailsURL).subscribe((result) => {
        if (result.status) {
          //(result.status ? resolve(result.data) : reject(result));
          if (result.data.length > 0) {
            this.multi = [];
            this.multi = result.data;
            console.log(this.multi);
          }
        }
      }, (error) => {
        reject(error);
      });
    });
  }

  getFinanceHourCount(){
    console.log("Hello");
    return new Promise((resolve, reject) => {
      const innovationDashBoardDetailsURL: string = this.httpConfig.getIdeasCountByQuarter();
      console.log(innovationDashBoardDetailsURL);
      this.dashboardService.innovationDashboard(innovationDashBoardDetailsURL).subscribe((result) => {
        if (result.status) {
          //(result.status ? resolve(result.data) : reject(result));
          if (result.data.length > 0) {
            this.finance = [];
            this.finance = result.data;
            console.log(this.finance);
          }
        }
      }, (error) => {
        reject(error);
      });
    });
  }
  
  
  exportDataAspdf(){

    this.pdf.setFontSize(22);
    this.pdf.setFont('times');
    this.pdf.setFontType('bolditalic');
    this.pdf.text('Automation Report', 150, 10, 'center');

  }

  recursiveAddHtml(): any {
    this.elCount++;
    if (this.elCount < this.elements.length) {
      this.pdf.setFontSize(16);
      this.pdf.setFont('helvetica');
      this.pdf.setFontType('italic');
      this.pdf.text(this.elements[this.elCount] , 10 , 30);
      this.pdf.addHTML(document.getElementById(this.elements[this.elCount]), 80, 40 , () => {
        switch (this.elements[this.elCount]) {
          case 'Towerdata':
                this.printTowerData(this.single);
                break;
          case 'StatusData':
              //  this.printStatusData();
                break;
          case 'TechData':
                //this.printBenefitData();
                break;
          
        }
        this.pdf.setLineWidth(0.5);
        this.pdf.line(10, 150 , 150, 150);
        this.pdf.addPage();
        this.recursiveAddHtml();
      });
    } else {
     // this.printDelayedData();
      //this.pdf.save('Automation_Dashboard.pdf');
    }
  }

  printTowerData(data:any){
    
    const rowsTower: any[]  = [];
    const rowsStatus: any[]  = [];
    const columnsTower = [
      { title: 'ID', dataKey: 'id' },
      { title: 'Tower', dataKey: 'tower' },
      { title: 'Count', dataKey: 'count' }];
    this.count=0;
   
   console.log("testb");
   console.log(data);
    data.forEach(element => {
      rowsTower.push({
        id: ++this.count,
        tower: element.name,
        count: element.value
      })
  })
  console.log("Hi")
  console.log(rowsTower)
    }
  // exporting data as excel 
  exportDataAsExcel(){
    this.excelService.exportAsExcelFile(this.single,'tempFile');
  }

}
