import { DashboardDialogComponent } from './../dashboard-dialog/dashboard-dialog.component';
import { CheckUserStatus } from './../utils/checkUserStatus';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-updated-tool-bar',
  templateUrl: './updated-tool-bar.component.html',
  styleUrls: ['./updated-tool-bar.component.css']
})
export class UpdatedToolBarComponent implements OnInit {

  // loggin in status 
  public isLoggedIn : boolean = false;

  constructor(
    public checkUserStatus: CheckUserStatus,
    public router: Router,
    public dialog: MatDialog
  ) { 
      // subscribe to logging status 
      this.subScibeToLogin();
    }

  ngOnInit() {
  }

  // subscribe to logging status 
  subScibeToLogin(){
    this.checkUserStatus.getUserLoggedStatus().subscribe((status)=>{
      this.isLoggedIn = status;
    })
    // check if user active or not 
    this.checkUserStatus.setUserLoggedStatus(this.checkUserStatus.isUserLoggedIn());
  }

  // whether user clicks on logout button 
  onLoggedOut(){
    // clear user data 
    this.checkUserStatus.clearUserInfo();
    this.checkUserStatus.setUserLoggedStatus(false);
    this.router.navigateByUrl('/login');
  }

  // open dashboard dialog 
  showDashboard(){
    let dialogRef = this.dialog.open(DashboardDialogComponent, {
      panelClass: 'graphs-dialog',
      data: { name: 'name', animal: 'this.animal' }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
 
}
