import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatedToolBarComponent } from './updated-tool-bar.component';

describe('UpdatedToolBarComponent', () => {
  let component: UpdatedToolBarComponent;
  let fixture: ComponentFixture<UpdatedToolBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatedToolBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatedToolBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
