import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomationhistorydialogComponent } from './automationhistorydialog.component';

describe('AutomationhistorydialogComponent', () => {
  let component: AutomationhistorydialogComponent;
  let fixture: ComponentFixture<AutomationhistorydialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutomationhistorydialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomationhistorydialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
