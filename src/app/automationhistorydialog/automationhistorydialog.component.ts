import { Component, OnInit, Inject, Injectable } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import { MatTableModule, MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-automationhistorydialog',
  templateUrl: './automationhistorydialog.component.html',
  styleUrls: ['./automationhistorydialog.component.css']
})
export class AutomationhistorydialogComponent implements OnInit {

  private selectTowerWisedata: any;
  private automationdata: AutomationDetails[] = [];
  automationDatasource: any;
  displayedColumns: any;

  constructor(
    private dialogRef: MatDialogRef<AutomationhistorydialogComponent>,
    private loginDialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) private data: any) {
     this.selectTowerWisedata = data;
   }

  ngOnInit() {
    this.BuildDialog(this.selectTowerWisedata);
  }

  BuildDialog(data: any) {
    let i = 0;
    data.forEach(element => {
      i = i + 1;
      this.automationdata.push({
        No : i ,
        COMMENT: element.autodetailsComments,
        USER: element.autodetailsUser,
        DATE: element.autodetailsCreatedDate
      });
    });
    console.log(this.automationdata);
    this.displayedColumns = ['No', 'COMMENT', 'USER', 'DATE'];
    this.automationDatasource = new MatTableDataSource<AutomationDetails>(this.automationdata);
  }
}

export interface AutomationDetails {
  No: number;
  COMMENT: string;
  USER: string;
  DATE: string;
}
