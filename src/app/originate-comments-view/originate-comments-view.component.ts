import { Component, OnInit, Inject, Injectable } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import { MatTableModule, MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-originate-comments-view',
  templateUrl: './originate-comments-view.component.html',
  styleUrls: ['./originate-comments-view.component.css']
})
export class OriginateCommentsViewComponent implements OnInit {

  private selectecOriginateDate: any;
  private originateComments: CommentsDetails[] = [];
  originateCommentsDataSource: any;
  displayedColumns: any;

  constructor(
    private dialogRef: MatDialogRef<OriginateCommentsViewComponent>,
    private loginDialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) private data: any) {
      console.log(data);
     this.selectecOriginateDate = data;
   }

  ngOnInit() {
    this.BuildDialog(this.selectecOriginateDate);
  }

  BuildDialog(data: any) {
    let i = 0;
    data.forEach(element => {
      i = i + 1;
      this.originateComments.push({
        No : i ,
        COMMENT: element.Originatecomments,
        USER: element.OriginateCommentsBy,
        DATE: element.OriginateCommentsDate
      });
    });
    console.log(this.originateComments);
    this.displayedColumns = ['No', 'COMMENT', 'USER', 'DATE'];
    this.originateCommentsDataSource = new MatTableDataSource<CommentsDetails>(this.originateComments);
  }
}

export interface CommentsDetails {
  No: number;
  COMMENT: string;
  USER: string;
  DATE: string;
}

