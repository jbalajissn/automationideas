import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OriginateCommentsViewComponent } from './originate-comments-view.component';

describe('OriginateCommentsViewComponent', () => {
  let component: OriginateCommentsViewComponent;
  let fixture: ComponentFixture<OriginateCommentsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OriginateCommentsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OriginateCommentsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
