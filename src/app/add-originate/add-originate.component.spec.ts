import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AddOriginateComponent } from './add-originate.component';

describe('AddOriginateComponent', () => {
  let component: AddOriginateComponent;
  let fixture: ComponentFixture<AddOriginateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOriginateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOriginateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
