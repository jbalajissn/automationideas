import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CheckUserStatus } from './../utils/checkUserStatus';
import { FormGroup, FormControl, Validators, FormBuilder, FormGroupDirective, AbstractControl } from '@angular/forms';
import { HttpCOnfig } from '../utils/httpConfig';
import { MatDialog } from '@angular/material';
import { FormArray } from '@angular/forms/src/model';
import { forEach } from '@angular/router/src/utils/collection';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
import { AddOriginateService } from './addoriginate';
import { isNull } from 'util';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { Location, DatePipe } from '@angular/common';

@Component({
  selector: 'app-add-originate',
  templateUrl: './add-originate.component.html',
  styleUrls: ['./add-originate.component.css']
})

export class AddOriginateComponent implements OnInit {
  public user: string;
  originateTowerType: any[] = [];
  originateCategoryType: any[] = [];
  originateStatusType: any[] = [];
  originateTechonologyType: any[] = [];
  originateApprover: any[] = [];
  dialogRef: any;
  OriginateForm: FormGroup;

  @ViewChild(FormGroupDirective) myform;
  get formArray(): AbstractControl | null { return this.OriginateForm.get('formArray'); }
  // OriginateForm

   constructor(private _formBuilder: FormBuilder,
    private httpConfig: HttpCOnfig,
    public userinfo: CheckUserStatus,
    public addOriginateService: AddOriginateService,
    public router: Router,
    public dialog: MatDialog ) {
      this.getUserInfo();
    }

    myFilter = (d: Date): boolean => {
      const day = d.getDay();
      const year = d.getFullYear();
      const currentDate = new Date();
      const currentyear = currentDate.getFullYear();
      // Prevent Saturday and Sunday from being selected.
      return day !== 0 && day !== 6 && year > currentyear - 2 && year < currentyear + 2;
    }
    getUserInfo() {
      if (isNull(JSON.parse(this.userinfo.getUserDetails()))) {
        this.router.navigateByUrl('/login');
      } else {
      this.user = JSON.parse(this.userinfo.getUserDetails())[0].username;
      }
    }

    async ngOnInit() {
      this.OriginateForm = this._formBuilder.group({
      formArray: this._formBuilder.array([
        this._formBuilder.group({
        originateTitle: new FormControl('', Validators.compose([Validators.minLength(15)])),
        originateDescription: new FormControl('', Validators.compose([Validators.minLength(20)])),
        originateTower: new FormControl('', Validators.required),
        originateCategory: new FormControl('', Validators.required),
        originateACNStakeholder: new FormControl('', Validators.required),
        originateCLNStakeholder: new FormControl('', Validators.required),
        originateTechnology: new FormControl('', Validators.minLength(1))
        }),
        this._formBuilder.group({
          originateStatus: new FormControl('', Validators.required),
          originateETCDate: new FormControl('', Validators.required),
          proposedCCI: new FormControl('' , Validators.required),
          approvedCCI: new FormControl('', Validators.required),
          originateProbability: new FormControl('', Validators.compose([Validators.required, Validators.max(100)])),
          originateROMEstimate: new FormControl('', Validators.required),
          originateRevenue: new FormControl('', Validators.required),
          originateApprover: new FormControl(),
          originateComments: new FormControl('', Validators.compose([Validators.minLength(20)]))
        })
      ])
    });

      let originateEntityData: any = '';
      originateEntityData = await this.FetchOriginateEntity();
      this.PopulateDropdowns(originateEntityData);
    }

    FetchOriginateEntity() {
      return new Promise((resolve, reject) => {
        const originateDetailsURL: string = this.httpConfig.getOriginateEntity();
        this.addOriginateService.originateEntity(originateDetailsURL).subscribe((result) => {
          if (result.status) {
            (result.status ? resolve(result.data) : reject(result));
          }
        }, (error) => {
          reject(error);
        });
      });
    }

    PopulateDropdowns(dropdownResults: any) {
      console.log(dropdownResults);
      const serverData: any[] = [];
      dropdownResults.forEach(element => {
        serverData.push({
            item: element.originateentityID,
            value: element.originateentityDesc,
            Type: element.originateentityTitle
        });
      });
      serverData.forEach(element => {
        if  (element.Type === 'originateTower') {
          this.originateTowerType.push(element);
        }
        if  (element.Type === 'originateStatusType') {
          this.originateStatusType.push(element);
        }
        if  (element.Type === 'originateCategoryType') {
          this.originateCategoryType.push(element);
        }
        if  (element.Type === 'originateTechnology') {
          this.originateTechonologyType.push(element);
        }
        if  (element.Type === 'originateApprover') {
          this.originateApprover.push(element);
        }
      });
    }

    openDIalog(code) {
      this.dialogRef = this.dialog.open(ProgressDialogComponent, {data: {code: code}});
    }

    closeDialog() {
      this.dialogRef.close();
    }

    successGif() {
      // first reset form data
      this.closeDialog();
      this.resetForm();
      this.openDIalog(2);
      setTimeout(() => {
        this.closeDialog();
      }, 5000);
    }

    resetForm() {
      this.myform.resetForm();
    }

    onAddOriginateButton() {
      // login as admin if login type is admin or else as user
      this.openDIalog(1);
      const frmArry: any = this.OriginateForm.get('formArray') as FormArray;
      const frmArryValues: any = frmArry.value as Array<any>;
      const datePipe   = new DatePipe('en-US');
      const originateData = {
        OriginateTower : frmArryValues[0].originateTower,
        OriginateCategoryType  : frmArryValues[0].originateCategory,
        OriginateTitle  : frmArryValues[0].originateTitle,
        OriginateDesc  : frmArryValues[0].originateDescription,
        OriginateACNStakeholder  : frmArryValues[0].originateACNStakeholder,
        OriginateCLNStakeholder : frmArryValues[0].originateCLNStakeholder,
        OriginateTechnology : frmArryValues[0].originateTechnology,
        OriginateETCDate : datePipe.transform(frmArryValues[0].originateETCDate, 'dd/MM/yyyy'),
        OriginateStatusType : frmArryValues[1].originateStatus,
        ProposedCCI : frmArryValues[1].proposedCCI,
        ApprovedCCI : frmArryValues[1].approvedCCI,
        OriginateProbability : frmArryValues[1].originateProbability,
        OriginateEstimate  : frmArryValues[1].originateROMEstimate,
        OriginateRevenue   : frmArryValues[1].originateRevenue,
        OriginateApprover : frmArryValues[1].originateApprover,
        OriginateComments : frmArryValues[1].originateComments,
        OriginateCreatedBy   : this.user
    };
      console.log(originateData);
      const automationSaveURL: string = this.httpConfig.saveOriginateDetails();
      this.addOriginateService.saveoriginate(automationSaveURL, {data: originateData}).subscribe((result) => {
        if (result.status) {
          // erase form data and show success gif
          console.log(result.status);
          this.successGif();
          
        } else {
          console.log(result.message);
        }
      }, (error) => {
        console.log('error', error);
      });
    }
}
