import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, HttpModule } from '@angular/http';
/*
Fetching Automation Details through Http Calls
*/
@Injectable()
export class AddOriginateService {
    constructor(private httpClient: HttpClient) {
    }

    // Fetch Automation Dashboard Details
    originateEntity(URL: string) {
        return this.httpClient.get<any>(URL, {});
    }

    saveoriginate(URL: string, data: any) {
        return this.httpClient.post<any>(URL, data);
    }
    updateoriginate(URL: string, data: any) {
        return this.httpClient.post<any>(URL, data);
    }
}
