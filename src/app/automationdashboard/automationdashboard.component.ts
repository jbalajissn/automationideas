import { Router } from '@angular/router';
import { CheckUserStatus } from './../utils/checkUserStatus';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { HttpCOnfig } from '../utils/httpConfig';
import { AutomationService } from './automationService';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AutomationdashdialogComponent } from '../automationdashdialog/automationdashdialog.component';
import { AddAutomationService } from '../add-automation/addautomationservice';
import { TS } from 'typescript-linq';
import { fakeAsync } from '@angular/core/testing';
import { ExcelService } from './../utils/excelService';
declare let jsPDF;
declare let linq;

@Component({
  selector: 'app-automationdashboard',
  templateUrl: './automationdashboard.component.html',
  styleUrls: ['./automationdashboard.component.css']
})

export class AutomationdashboardComponent implements OnInit {

  BAview: any[] = [480, 350];
  VTTview: any[] = [480, 350];
  view: any[] = [880, 350];
  searchAutomationForm: FormGroup;
  towerWiseServerdata: any[] = [];
  selectDialogData: any = [];
  
  clientTowerTypes: any[] = [];
  initiativeCat: any[] = [];
  timeperiod: any[] = [];
  elements = ['TowerArea', 'StatusArea', 'BenefitArea', 'WorkType'];
  elCount = -1;
  pdf = new jsPDF('l', 'mm', 'a4');
  
  towerName: any = '';
  initiativeCatName: any = '';
  createdDate: any = '';
  sample = new TS.Collections.List<any>(false);
  pieChartdata: any[] = [];
  pieChartStatusdata: any[] = [];
  horTypeToolData: any[] = [];
  benefitAreaData: any[] = [];
  delayedPieChart: any[] = [];

  towerWisedata: any[] = [];
  statusWisedata: any[] = [];
  benefitWisedata: any[] = [];
  typeToolData: any[] = [];
  delayedData: any[] = [];

  gradient = false;
  BAlegendTitle = 'Initiative';
  WTlegentTitle = 'Tool';
  count = 0;

  verStacScheme = {
    domain: ['#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886']
  };
  colorscheme = {
    domain: ['#647c8a', '#3f51b5', '#2196f3', '#00b862', '#afdf0a', '#a7b61a', '#f3e562', '#ff9800', '#ff5722', '#ff4514']
  };
  towerScheme = {
    domain: ['#A10A28', '#D3342D', '#EF6D49', '#FAAD67', '#FDDE90', '#DBED91', '#A9D770', '#6CBA67', '#2C9653', '#146738']
  };
  WorkToolScheme = {
    domain: ['#55C22D', '#C1F33D', '#3CC099', '#AFFFFF', '#8CFC9D', '#76CFFA', '#BA60FB', '#EE6490', '#C42A1C', '#FC9F32']
  };

  constructor(
    private httpConfig: HttpCOnfig,
    private automationService: AutomationService,
    private addAutomationService: AddAutomationService,
    private _formBuilder: FormBuilder,
    private excelService : ExcelService,
    public dialog: MatDialog
  ) { }

  async ngOnInit() {
    let serverResult: any = '';

    this.searchAutomationForm = this._formBuilder.group({
      initiative: new FormControl(),
      clientTowerType: new FormControl(),
      timePeriod: new FormControl()
    });

    let automationEntityData: any = '';
    automationEntityData = await this.FetchAutomationEntity();
    this.PopulateDropdowns(automationEntityData);

    let automationTowerData: any = '';
    automationTowerData = await this.FetchAutomationTowers();
    this.PopulateTowers(automationTowerData);

    serverResult = await this.FetchDetailsServer();
    this.PopulateDashboard(serverResult);
    this.pieChartdata = this.towerWisedata;
    this.pieChartStatusdata = this.statusWisedata;
    this.benefitAreaData = this.benefitWisedata;
    this.horTypeToolData = this.typeToolData;
    this.delayedPieChart = this.delayedData;
  }

  FetchAutomationTowers() {
    return new Promise((resolve, reject) => {
      const automationDetailsURL: string = this.httpConfig.getAutomationTower();
      this.addAutomationService.automationTower(automationDetailsURL).subscribe((result) => {
        if (result.status) {
          (result.status ? resolve(result.data) : reject(result));
        }
      }, (error) => {
        reject(error);
      });
    });
  }

  FetchAutomationEntity() {
    return new Promise((resolve, reject) => {
      const automationDetailsURL: string = this.httpConfig.getautomationEntity();
      this.addAutomationService.automationEntity(automationDetailsURL).subscribe((result) => {
        if (result.status) {
          (result.status ? resolve(result.data) : reject(result));
        }
      }, (error) => {
        reject(error);
      });
    });
  }

  PopulateTowers(dropdownResults: any) {
    const serverData: any[] = [];
    dropdownResults.forEach(element => {
      serverData.push({
        item: element.ID,
        value: element.Value
      });
    });
    this.clientTowerTypes = serverData;
  }

  PopulateDropdowns(dropdownResults: any) {
    const serverData: any[] = [];
    dropdownResults.forEach(element => {
      serverData.push({
        item: element.ID,
        value: element.Value,
        Type: element.Type
      });
    });
    serverData.forEach(element => {
      if (element.Type === 'automationInitiativeCat') {
        this.initiativeCat.push(element);
      }
      if (element.Type === 'TimePeriod') {
        this.timeperiod.push(element);
      }
    });
  }

  FetchDetailsServer() {
    return new Promise((resolve, reject) => {
      const automationDetailsURL: string = this.httpConfig.getAutomationDetails();
      this.automationService.automationDashboard(automationDetailsURL).subscribe((result) => {
        if (result.status) {
          (result.status ? resolve(result.data) : reject(result));
        }
      }, (error) => {
        reject(error);
      });
    });
  }

  PopulateTowerData(data: any) {
    data.forEach(element => {
      if (!this.towerWisedata) {
        this.towerWisedata.push({
          name: element.tower,
          value: 1
        });
      } else {
        const findTower: any = this.towerWisedata.findIndex(ele => ele.name === element.tower);
        if (findTower === -1) {
          this.towerWisedata.push({
            name: element.tower,
            value: 1
          });
        } else {
          this.towerWisedata.find(ele => ele.name === element.tower).value++;
        }
      }
    });
  }

  PopulateStatusData(data: any) {
    // console.log(data);
    data.forEach(element => {
      if (!this.statusWisedata) {
        this.statusWisedata.push({
          name: element.status,
          value: 1
        });
      } else {
        const findStatus: any = this.statusWisedata.findIndex(ele => ele.name === element.status);
        if (findStatus === -1) {
          this.statusWisedata.push({
            name: element.status,
            value: 1
          });
        } else {
          this.statusWisedata.find(ele => ele.name === element.status).value++;
        }
      }
    });
  }

  PopulateDelayedData(element: any): any {
    // console.log(data);
    let status: any = '';
    const dtDate = new Date();
    const dtCurrentDate = new Date(dtDate).valueOf();
    if ((element.STATUS === 'New' || element.STATUS === 'In Progress' )&&element.automationDate != null) {
      const dtETCDate = new Date(element.automationDate).valueOf();
      //console.log(dtCurrentDate, " / ", dtETCDate, " / ", element.status);
      if (dtCurrentDate > dtETCDate) {
        status = 'Delayed';
      } else {
        status = element.STATUS;
      }
    } else {
      status = element.STATUS;
    }
    // console.log(dtCurrentDate, " / ", dtETCDate, " / ", element.status , " / " , status);
    return status;
  }

  PopulateBenefitData(data: any) {
   
    data.forEach(element => {
      
      if (!this.benefitWisedata) {
        this.benefitWisedata.push({
          name: element.BenefitArea,
          series: [{
            name: element.Initiative,
            value: 1
          }]
        });
        
      } else {
        const findBenefitArea: any = this.benefitWisedata.findIndex(ele => ele.name === element.BenefitArea);
       
        if (findBenefitArea === -1) {
          this.benefitWisedata.push({
            name: element.BenefitArea,
            series: [{
              name: element.Initiative,
              value: 1
            }]
          });
         
        } else {
          const findIncentive: any = this.benefitWisedata[findBenefitArea].series.findIndex(ele => ele.name === element.Initiative);
         
          if (findIncentive === -1) {
            this.benefitWisedata[findBenefitArea].series.push({
              name: element.Initiative,
              value: 1
            });
          } else {
            this.benefitWisedata[findBenefitArea].series[findIncentive].value++;
           
          }
        }
      }
    });
  }

  PopulateTypeToolData(data: any) {
    // console.log(data);
    data.forEach(element => {
      if (!this.typeToolData) {
        this.typeToolData.push({
          name: element.WorkType,
          series: [{
            name: element.Tool,
            value: 1
          }]
        });
      } else {
        const findTypeArea: any = this.typeToolData.findIndex(ele => ele.name === element.WorkType);
        if (findTypeArea === -1) {
          this.typeToolData.push({
            name: element.WorkType,
            series: [{
              name: element.Tool,
              value: 1
            }]
          });
        } else {
          const findTool: any = this.typeToolData[findTypeArea].series.findIndex(ele => ele.name === element.Tool);
          if (findTool === -1) {
            this.typeToolData[findTypeArea].series.push({
              name: element.Tool,
              value: 1
            });
          } else {
            this.typeToolData[findTypeArea].series[findTool].value++;
          }
        }
      }
    });
  }

  PopulateDashboard(data: any) {
    this.towerWiseServerdata = [];
    // console.log(data);
    data.forEach(element => {
      const statusData = this.PopulateDelayedData(element);
      this.towerWiseServerdata.push({
        id: element.automationID,
        tower: element.automationTower,
        application: element.automationApplication,
        title: element.automationTitle,
        description: element.automationDescription,
        efforts : element.automationEfforts,
        implementation : element.automationImplementation,
        FTE : element.automationFTE,
        costperday: element.automationcostperday,
        costsaving: element.automationnetcostsaving,
        ticket: element.automationticket,
        date: element.automationDate,
        status: statusData,
        WorkType: element.WorkType,
        BenefitArea: element.BenefitArea,
        Tool: element.Tool,
        Initiative: element.Initiative,
        AutomationCreationDate: element.automationCreationDate
      });
    });
    this.PopulateTowerData(this.towerWiseServerdata);
    this.PopulateStatusData(this.towerWiseServerdata);
    this.PopulateBenefitData(this.towerWiseServerdata);
    this.PopulateTypeToolData(this.towerWiseServerdata);
    this.PopulateDelayedData(this.towerWiseServerdata);
  }

  OnTowerChange(event: any) {
    if (event !== -1) {
      const selectTowerWisedata: any = [];
      if (this.initiativeCatName === '' && this.createdDate === '') {
        this.towerWiseServerdata.forEach(element => {
          if (element.tower === event) {
            selectTowerWisedata.push(element);
          }
        });
      } else {
        if (this.createdDate === '') {
          this.towerWiseServerdata.forEach(element => {
            if (element.tower === event && element.Initiative === this.initiativeCatName) {
              selectTowerWisedata.push(element);
            }
          });
        } else
          if (this.initiativeCatName === '') {
            this.towerWiseServerdata.forEach(element => {
              const eventEndTime = new Date(element.AutomationCreationDate);
              const eventStartTime = new Date(this.createdDate);
              if (element.tower === event && eventEndTime.valueOf() > eventStartTime.valueOf()) {
                selectTowerWisedata.push(element);
              }
            });
          } else {
            this.towerWiseServerdata.forEach(element => {
              const eventEndTime = new Date(element.AutomationCreationDate);
              const eventStartTime = new Date(this.createdDate);
              if (element.tower === event && element.Initiative === this.initiativeCatName
                && eventEndTime.valueOf() > eventStartTime.valueOf()) {
                selectTowerWisedata.push(element);
              }
            });
          }
      }
      this.pieChartStatusdata = [];
      this.statusWisedata = [];
      this.benefitAreaData = [];
      this.benefitWisedata = [];
      this.typeToolData = [];
      this.towerWisedata = [];

      this.towerName = event;

      this.PopulateTowerData(selectTowerWisedata);
      this.PopulateStatusData(selectTowerWisedata);
      this.PopulateBenefitData(selectTowerWisedata);
      this.PopulateTypeToolData(selectTowerWisedata);
      this.pieChartdata = this.towerWisedata;
      this.pieChartStatusdata = this.statusWisedata;
      this.benefitAreaData = this.benefitWisedata;
      this.horTypeToolData = this.typeToolData;
    }
  }

  OnInitiativeCatChange(event: any) {
    if (event !== -1) {
      const selectTowerWisedata: any = [];
      if (this.towerName === '' && this.createdDate === '') {
        this.towerWiseServerdata.forEach(element => {
          if (element.Initiative === event) {
            selectTowerWisedata.push(element);
          }
        });
      } else {
        if (this.createdDate === '') {
          this.towerWiseServerdata.forEach(element => {
            if (element.Initiative === event && element.tower === this.towerName) {
              selectTowerWisedata.push(element);
            }
          });
        } else {
          if (this.towerName === '') {
            this.towerWiseServerdata.forEach(element => {
              const eventEndTime = new Date(element.AutomationCreationDate);
              const eventStartTime = new Date(this.createdDate);
              if (eventEndTime.valueOf() > eventStartTime.valueOf()) {
                selectTowerWisedata.push(element);
              }
            });
          } else {
            this.towerWiseServerdata.forEach(element => {
              const eventEndTime = new Date(element.AutomationCreationDate);
              const eventStartTime = new Date(this.createdDate);
              if (element.Initiative === event && element.tower === this.towerName
                && eventEndTime.valueOf() > eventStartTime.valueOf()) {
                selectTowerWisedata.push(element);
              }
            });
          }
        }
      }
      this.pieChartStatusdata = [];
      this.statusWisedata = [];
      this.benefitAreaData = [];
      this.benefitWisedata = [];
      this.typeToolData = [];
      this.towerWisedata = [];

      this.initiativeCatName = event;

      this.PopulateTowerData(selectTowerWisedata);
      this.PopulateStatusData(selectTowerWisedata);
      this.PopulateBenefitData(selectTowerWisedata);
      this.PopulateTypeToolData(selectTowerWisedata);
      this.pieChartdata = this.towerWisedata;
      this.pieChartStatusdata = this.statusWisedata;
      this.benefitAreaData = this.benefitWisedata;
      this.horTypeToolData = this.typeToolData;
    }
  }

  onSelectAll(event) {
    this.pieChartdata = [];
    this.towerWisedata = [];
    this.pieChartStatusdata = [];
    this.statusWisedata = [];
    this.benefitAreaData = [];
    this.benefitWisedata = [];
    this.typeToolData = [];
    this.towerName = '';
    this.initiativeCatName = '';
    this.createdDate = '';
    this.PopulateTowerData(this.towerWiseServerdata);
    this.PopulateStatusData(this.towerWiseServerdata);
    this.PopulateBenefitData(this.towerWiseServerdata);
    this.PopulateTypeToolData(this.towerWiseServerdata);
    this.pieChartdata = this.towerWisedata;
    this.pieChartStatusdata = this.statusWisedata;
    this.benefitAreaData = this.benefitWisedata;
    this.horTypeToolData = this.typeToolData;

    this.searchAutomationForm.controls['initiative'].setValue(-1);
    this.searchAutomationForm.controls['clientTowerType'].setValue(-1);
    this.searchAutomationForm.controls['timePeriod'].setValue(-1);
  }

  OnPeriodChange(event) {
    if (event !== -1) {
      const selectTowerWisedata: any = [];
      const dtDate = new Date();
      if (event === 'Last 3 months') {
        dtDate.setMonth(-3);
      }
      if (event === 'Last 6 months') {
        dtDate.setMonth(-6);
      }
      if (event === 'Last 9 months') {
        dtDate.setMonth(-9);
      }
      if (event === 'Last 12 months') {
        dtDate.setMonth(-12);
      }
      this.createdDate = dtDate.valueOf();
      const eventStartTime = new Date(this.createdDate);
      if (this.towerName === '' && this.initiativeCatName === '') {
        this.towerWiseServerdata.forEach(element => {
          const eventEndTime = new Date(element.AutomationCreationDate);
          if (eventEndTime.valueOf() > eventStartTime.valueOf()) {
            selectTowerWisedata.push(element);
          }
        });
      } else {
        if (this.initiativeCatName === '') {
          this.towerWiseServerdata.forEach(element => {
            const eventEndTime = new Date(element.AutomationCreationDate);
            if (eventEndTime.valueOf() > eventStartTime.valueOf() && element.tower === this.towerName) {
              selectTowerWisedata.push(element);
            }
          });
        } else {
          if (this.towerName === '') {
            this.towerWiseServerdata.forEach(element => {
              const eventEndTime = new Date(element.AutomationCreationDate);
              if (eventEndTime.valueOf() > eventStartTime.valueOf() && element.Initiative === this.initiativeCatName) {
                selectTowerWisedata.push(element);
              }
            });
          } else {
            this.towerWiseServerdata.forEach(element => {
              const eventEndTime = new Date(element.AutomationCreationDate);
              //console.log('Sample');
              //console.log(element.tower, ' And ', this.towerName);
              //console.log(element.Initiative, ' And ', this.initiativeCatName);
              //console.log(eventEndTime.valueOf(), ' And ', eventStartTime.valueOf());
              if (element.tower === this.towerName && element.Initiative === this.initiativeCatName
                && eventEndTime.valueOf() > eventStartTime.valueOf()) {
                selectTowerWisedata.push(element);
              }
            });
          }
        }
      }
      this.pieChartStatusdata = [];
      this.statusWisedata = [];
      this.benefitAreaData = [];
      this.benefitWisedata = [];
      this.typeToolData = [];
      this.towerWisedata = [];
      this.PopulateTowerData(selectTowerWisedata);
      this.PopulateStatusData(selectTowerWisedata);
      this.PopulateBenefitData(selectTowerWisedata);
      this.PopulateTypeToolData(selectTowerWisedata);
      this.pieChartdata = this.towerWisedata;
      this.pieChartStatusdata = this.statusWisedata;
      this.benefitAreaData = this.benefitWisedata;
      this.horTypeToolData = this.typeToolData;
    }
  }


  onStatusSelect(event) {
    // console.log(event.value);
    // console.log(event.name);
    // console.log(this.towerWiseServerdata);
    const selectDialogData: any = [];
    if (this.towerName === '' && this.initiativeCatName === '') {
      this.towerWiseServerdata.forEach(element => {
        if (element.status === event.name) {
          selectDialogData.push(element);
        }
      });
    } else {
      if (this.towerName !== '' && this.initiativeCatName === '') {
        this.towerWiseServerdata.forEach(element => {
          if (element.status === event.name && element.tower === this.towerName) {
            selectDialogData.push(element);
          }
        });
      } else {
        this.towerWiseServerdata.forEach(element => {
          if (element.status === event.name && element.tower === this.towerName &&
            element.Initiative === this.initiativeCatName) {
            selectDialogData.push(element);
          }
        });
      }
    }
    this.dialog.open(AutomationdashdialogComponent, {
      data: { detailsData: selectDialogData, type: 'Status' }, panelClass: 'show-idea'
    });
  }

  onBASelect(event) {
    const selectDialogData: any = [];
    console.log(event);
    if (this.towerName === '' && this.initiativeCatName === '') {
      this.towerWiseServerdata.forEach(element => {
        if (element.Initiative === event.name && element.BenefitArea === event.series) {
          selectDialogData.push(element);
        }
      });
    } else {
      if (this.towerName !== '' && this.initiativeCatName === '') {
        this.towerWiseServerdata.forEach(element => {
          if (element.tower === this.towerName && element.Initiative === event.name
            && element.BenefitArea === event.series) {
            selectDialogData.push(element);
          }
        });
      } else {
        this.towerWiseServerdata.forEach(element => {
          if (element.tower === this.towerName && element.Initiative === event.name
            && element.BenefitArea === event.series && element.Initiative === this.initiativeCatName) {
            selectDialogData.push(element);
          }
        });
      }
    }
    this.dialog.open(AutomationdashdialogComponent, {
      data: { detailsData: selectDialogData, type: 'BenefitArea' }, panelClass: 'show-idea'
    });
  }

  onWTSelect(event) {
    const selectDialogData: any = [];
    if (this.towerName === '') {
      this.towerWiseServerdata.forEach(element => {
        if (element.Tool === event.name && element.WorkType === event.series) {
          selectDialogData.push(element);
        }
      });
    } else {
      this.towerWiseServerdata.forEach(element => {
        if (element.Tool === event.name && element.WorkType === event.series &&
          element.tower === this.towerName) {
          selectDialogData.push(element);
        }
      });
    }
    this.dialog.open(AutomationdashdialogComponent, {
      data: { detailsData: selectDialogData, type: 'WorkType' }, panelClass: 'show-idea'
    });
  }

  ClickPDF() {

    // All units are in the set measurement for the document
    // This can be changed to 'pt' (points), 'mm' (Default), 'cm', 'in'
    this.sample = new TS.Collections.List<any>(false);
    this.towerWiseServerdata.forEach(element => {
      this.sample.add(element);
    });
    this.pdf = new jsPDF('l', 'mm', 'a4');
    this.elCount = -1;
    this.pdf.setFontSize(22);
    this.pdf.setFont('times');
    this.pdf.setFontType('bolditalic');
    this.pdf.text('Automation Report', 150, 10, 'center');
    this.recursiveAddHtml();
  }

  recursiveAddHtml(): any {
    this.elCount++;
    if (this.elCount < this.elements.length) {
      this.pdf.setFontSize(16);
      this.pdf.setFont('helvetica');
      this.pdf.setFontType('italic');
      switch (this.elements[this.elCount]) {
        case 'TowerArea':
              this.pdf.text("Tower Wise View" , 10 , 30);
              break;
        case 'StatusArea':
              this.pdf.text("Status Wise View" , 10 , 30);
              break;
        case 'BenefitArea':
              this.pdf.text("Benefit Vs Initiative Type" , 10 , 30);
              break;
        case 'WorkType':
              this.pdf.text("Work Type Vs Tool Used" , 10 , 30);
              break;
      }
      //this.pdf.text(this.elements[this.elCount] , 10 , 30);
      this.pdf.addHTML(document.getElementById(this.elements[this.elCount]), 80, 40 , () => {
        switch (this.elements[this.elCount]) {
          case 'TowerArea':
                this.printTowerData();
                break;
          case 'StatusArea':
                this.printStatusData();
                break;
          case 'BenefitArea':
                this.printBenefitData();
                break;
          case 'WorkType':
                this.printToolData();
                break;
        }
        this.pdf.setLineWidth(0.5);
        this.pdf.line(10, 150 , 150, 150);
        this.pdf.addPage();
        this.recursiveAddHtml();
      });
    } else {
      this.printDelayedData();
      this.pdf.save('Automation_Dashboard_' + new Date().getTime() + '.pdf');
    }
  }

  printTowerData(): any {
    const rowsTower: any[]  = [];
    const rowsStatus: any[]  = [];
    let resultTower = new TS.Collections.List<any>(false);
    resultTower = this.sample.groupBy(ele =>  (ele.tower)).toList();
    const columnsTower = [
      { title: 'ID', dataKey: 'id' },
      { title: 'Tower', dataKey: 'tower' },
      { title: 'Count', dataKey: 'count' }];
      this.count = 0;
      resultTower.forEach(element => {
      rowsTower.push({
        id: ++this.count,
        tower: element.key,
        count: this.sample.count(ele => ele.tower === element.key)
      });
    });
    
    this.pdf.autoTable(columnsTower, rowsTower, {
      margin: { vertical: 40 },
      styles: { overflow: 'linebreak', columnWidth: 'wrap' },
      columnStyles: { text: { columnWidth: 'auto' } }
    });
  }

  printStatusData(): any {
    const rowsStatus: any[]  = [];
    let resultStatus = new TS.Collections.List<any>(false);
    resultStatus = this.sample.groupBy(ele =>  (ele.status)).toList();
    const columnsStatus = [
        { title: 'ID', dataKey: 'id' },
        { title: 'Status', dataKey: 'status' },
        { title: 'Count', dataKey: 'count' }];
    this.count = 0;
    resultStatus.forEach(element => {
      rowsStatus.push({
        id: ++this.count,
        status: element.key,
        count: this.sample.count(ele => ele.status === element.key)
      });
    });
    this.pdf.autoTable(columnsStatus, rowsStatus, {
      margin: { vertical : 40 },
      styles: { overflow: 'linebreak', columnWidth: 'wrap' },
      columnStyles: { text: { columnWidth: 'auto' } }
    });
  }

  printBenefitData(): any {
    const rowsBArea: any[]  = [];
    let resultBArea = new TS.Collections.List<any>(false);
    resultBArea = this.sample.groupBy(ele =>  (ele.BenefitArea)).toList();
    // console.log(resultBArea);
    const columnsBArea = [
        { title: 'ID', dataKey: 'id' },
        { title: 'Benefit Area', dataKey: 'benArea' },
        { title: 'Count', dataKey: 'count' }];
    this.count = 0;
    resultBArea.forEach(element => {
      rowsBArea.push({
        id: ++this.count,
        benArea: element.key,
        count: this.sample.count(ele => ele.BenefitArea === element.key)
      });

    });
    this.pdf.autoTable(columnsBArea, rowsBArea, {
      margin: { vertical : 40 },
      styles: { overflow: 'linebreak', columnWidth: 'wrap' },
      columnStyles: { text: { columnWidth: 'auto' } }
    });
  }

  printToolData(): any {
    const rowsBArea: any[]  = [];
    let resultBArea = new TS.Collections.List<any>(false);
    resultBArea = this.sample.groupBy(ele =>  (ele.Tool)).toList();
    const columnsBArea = [
        { title: 'ID', dataKey: 'id' },
        { title: 'Tool', dataKey: 'tool' },
        { title: 'Count', dataKey: 'count' }];
    this.count = 0;
    resultBArea.forEach(element => {
      rowsBArea.push({
        id: ++this.count,
        tool: element.key,
        count: this.sample.count(ele => ele.Tool === element.key)
      });
    });
    this.pdf.autoTable(columnsBArea, rowsBArea, {
      margin: { vertical : 40 },
      styles: { overflow: 'linebreak', columnWidth: 'wrap' },
      columnStyles: { text: { columnWidth: 'auto' } }
    });
  }

  printDelayedData(): any {
    this.pdf.setFontSize(18);
    this.pdf.setFont('times');
    this.pdf.setFontType('bold');
    this.pdf.text('Delayed Automations', 40, 10);
    this.pdf.setFontSize(16);
    this.pdf.setFont('helvetica');
    const rowsDelayed: any[]  = [];
    let resultDelayed  = new TS.Collections.List<any>(false);
    resultDelayed = this.sample.where(ele =>  (ele.status === 'Delayed')).toList();
    const columnsDelayed = [
        { title: 'ID', dataKey: 'id' },
        { title: 'Tower', dataKey: 'tower' },
        { title: 'Application', dataKey: 'application' },
        { title: 'Title', dataKey: 'title'  },
        { title: 'ETC', dataKey: 'date'  },
        { title: 'BenefitArea', dataKey: 'BenefitArea'  }];
    this.count = 0;
    resultDelayed.forEach(element => {
      rowsDelayed.push({
        id: ++this.count,
        tower: element.tower,
        application: element.application,
        title: element.title,
        date: element.date,
        BenefitArea: element.BenefitArea
      });
    });
    this.pdf.autoTable(columnsDelayed, rowsDelayed, {
      margin: { vertical : 40 },
      styles: { overflow: 'linebreak', columnWidth: 'wrap' },
      columnStyles: { text: { columnWidth: 'auto' } }
    });
  }

  ClickExcel() {
    this.selectDialogData = [];
    console.log(this.towerName);
    console.log(this.initiativeCatName);
    if (this.towerName === '' && this.initiativeCatName === '') {
      this.towerWiseServerdata.forEach(element => {
         this.selectDialogData.push(element);        
      });
    } else {
      if (this.towerName !== '' && this.initiativeCatName === '') {
        this.towerWiseServerdata.forEach(element => {
          if (element.tower === this.towerName) {
            this.selectDialogData.push(element);
          }
        });
      }else if (this.towerName === '' && this.initiativeCatName !== ''){
        this.towerWiseServerdata.forEach(element => {
          if (element.Initiative === this.initiativeCatName) {
            this.selectDialogData.push(element);
          }
        });
      } else {
        this.towerWiseServerdata.forEach(element => {
          if (element.tower === this.towerName && element.Initiative === this.initiativeCatName) {
          this.selectDialogData.push(element);
          }
        });
      }
    }
    this.excelService.exportAsExcelFile(this.selectDialogData,'Automation_Dashboard');
  }
}
