import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomationdashboardComponent } from './automationdashboard.component';

describe('AutomationdashboardComponent', () => {
  let component: AutomationdashboardComponent;
  let fixture: ComponentFixture<AutomationdashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutomationdashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomationdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
