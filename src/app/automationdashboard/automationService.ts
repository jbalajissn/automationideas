import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http,HttpModule } from '@angular/http';
/*
Fetching Automation Details through Http Calls
*/
@Injectable()
export class AutomationService {
    constructor(private httpClient: HttpClient) {
    }

    // Fetch Automation Dashboard Details
    automationDashboard(URL: string) {
        return this.httpClient.get<any>(URL, {});
    }
}
