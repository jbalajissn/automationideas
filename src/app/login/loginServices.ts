import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
/*
this class helps us in making all the HTTP calls for login components 
*/
@Injectable()
export class LoginService {
    constructor(private httpClient: HttpClient){}

    // make login request to server 
    loginUser(URL: string, data: any){
        return this.httpClient.post<any>(URL,{email: data.email,password: data.password});
    }
}