import { Router } from '@angular/router';
import { CheckUserStatus } from './../utils/checkUserStatus';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpCOnfig } from '../utils/httpConfig';
import { MatDialog } from '@angular/material';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
import { LoginService } from './loginServices';

// email pattern 
const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  // type of loggin admin / normal user 
  loginTypes : Array<String> = ["Admin","Non-Admin"];
  dialogRef : any;

  // login form 
  loginForm  = new FormGroup({
    email: new FormControl('@accenture.com',Validators.compose([Validators.required,Validators.pattern(EMAIL_REGEX)])),
    password: new FormControl('',Validators.compose([Validators.minLength(5),Validators.maxLength(20)])),
    loginType: new FormControl('',Validators.required)
  });

  constructor(
    private httpConfig : HttpCOnfig,
    private dialog: MatDialog,
    private loginService : LoginService,
    private checkUserStatus: CheckUserStatus,
    private router: Router
  ) { 

    this.checkUserLoggedInStatus();

  }

  ngOnInit() {
  }

  // when user clicks on login button 
  onLoginButton(){
    // login as admin if login type is admin or else as user 
    const loginType : string = this.loginForm.get('loginType').value;
    let loginURL : string = '';
    const data = {
      email: this.loginForm.get('email').value,
      password: this.loginForm.get('password').value
    }
    if(loginType === "Admin"){
      loginURL = this.httpConfig.getAdminURL();
    }else{
      loginURL = this.httpConfig.getUserLoginURL();
    }
    this.loginUserFromServer(loginURL,data)
  }

  loginUserFromServer(URL: string, data: Object){
    this.showDIalog(1);
    this.loginService.loginUser(URL,data).subscribe((result)=>{
      this.hideDialog();
      if(result.status){
        // store user inforamtion 
        this.checkUserStatus.saveLoginInformation(JSON.stringify(result.data));
        this.checkUserStatus.setUserLoggedStatus(true);
        // now goto home page and hide login button 
        this.redirectOnSuccess();
      }else{
        this.showErrorMessage({code: 4, message: result.message});
      }
    },(error)=>{
      this.hideDialog();
      console.log(error);
    })
  }

  // showing dialog 
  showDIalog(code : number){
    this.dialogRef = this.dialog.open(ProgressDialogComponent,{data: {code: 1}})
  }

  // show error message
  showErrorMessage(data){
    this.dialogRef = this.dialog.open(ProgressDialogComponent,{data: data})
  }

  // hiding dialog 
  hideDialog(){
    this.dialogRef.close();
  }

  /* check whether user logged in or not 
  if logged in re direct to /ideas */
  checkUserLoggedInStatus(){
    if(this.checkUserStatus.isUserLoggedIn()){
      this.redirectOnSuccess();
    }
  }
  
  redirectOnSuccess(){
    this.router.navigateByUrl('/ideas');
  }

}
