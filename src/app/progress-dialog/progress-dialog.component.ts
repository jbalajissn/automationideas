import { Injectable } from '@angular/core';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';

@Component({
  selector: 'app-progress-dialog',
  templateUrl: './progress-dialog.component.html',
  styleUrls: ['./progress-dialog.component.css','./progress-dialog.scss']
})
export class ProgressDialogComponent implements OnInit {

  public code: number;
  public message = '';

  constructor(public dialogRef: MatDialogRef<ProgressDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      if (this.data.code === 4 || this.data.code === 5 || this.data.code === 6 || this.data.code === 7) {
        this.dialogRef.disableClose = false;
        this.message = this.data.message;
      } else {
        this.dialogRef.disableClose = true;
      }
      this.code = data.code;
  }
  // when we close dialog
  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }
}


