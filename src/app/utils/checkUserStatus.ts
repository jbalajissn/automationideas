import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

/*
this will check user status such as 
1. whether he is admin or not 
2. whether user logged in or not 
*/
@Injectable()
export class CheckUserStatus{

    private isLoggedIn : string = "isLoggedIn";
    private isAdmin : string = "isAdmin";
    private userDetails: string = "userDetails";
    private isLoggedBoolean: boolean = false;

    // creating a emitter to show/hide login/logout button 
    private isLoggedInEmitter : Subject<boolean>  = new Subject<boolean>();

    constructor(){};

    // checking user logged in or not 
    isUserLoggedIn() : boolean {
        return sessionStorage.getItem(this.isLoggedIn) != null;
    }

    // checking whther user admin or not here 1 is for admin
    isUserAdmin(): boolean{
       if(sessionStorage.getItem(this.userDetails) == null){
           return false;
       }
        return JSON.parse(sessionStorage.getItem(this.userDetails))[0].user_group_id_fk == 1;
    }

    // saving login information 
    saveLoginInformation(userDetails) : void {
        sessionStorage.setItem(this.isLoggedIn,"true");
        sessionStorage.setItem(this.userDetails,userDetails)
    }

    // getting user details 
    getUserDetails() : string {
        return sessionStorage.getItem(this.userDetails);
    }

    // remove localStorage
    clearUserInfo() : void {
        sessionStorage.clear();
    }

    // check whether user logged in or not, remember this is an emitter 
    getUserLoggedStatus() : Observable<boolean> {
        return this.isLoggedInEmitter.asObservable();
    }

    // save user logged status 
    setUserLoggedStatus(status : boolean) : void {
        this.isLoggedInEmitter.next(status)
    }

}