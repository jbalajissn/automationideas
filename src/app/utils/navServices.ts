import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { Observable } from "rxjs/Observable";

// this class helps us in hide/show navgation 
@Injectable()
export class NavService{
    private navStatus : boolean = false;
    private subject: Subject<boolean> = new Subject<boolean>();

    // this is for showing nav status 
    showNavStatus(){
        this.navStatus = true;
        this.subject.next(this.navStatus);
    }

    // this is for hiding nav status 
    hideNavStatsu(){
        this.navStatus = false;
        this.subject.next(this.navStatus);
    }

    // getting nav status 
    getNavStatus() : Observable<boolean>{
       return this.subject;
    }

}