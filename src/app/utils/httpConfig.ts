import { Injectable } from "@angular/core";

// will be storing all the http URLs here 
@Injectable()
export class HttpCOnfig{
    constructor(){}
    private BASE_URL: string = "http://localhost:3000/";
    
    // get URL for inserting ideas 
    getIdeasInsertURL():  string {
        return `${this.BASE_URL}ideas/insert`;
    }

    // get all the ideas 
    getIdeas(){
        return `${this.BASE_URL}ideas/getIdeas`;
    }

    // get admin login URL 
    getAdminURL(): string {
        return `${this.BASE_URL}auth/adminLogin`;
    }

    // get non-admin uers login URL 
    getUserLoginURL():  string {
        return `${this.BASE_URL}auth/userLogin`;
    }

    // update like count 
    getUpdateLikeURL(): string {
        return `${this.BASE_URL}ideas/updateLike`;
    }

    // get likes count URL 
    getLikesCount(): string {
        return `${this.BASE_URL}ideas/getLikesCount`;
    }

    // get approval history URL 
    getApprovalHistotyURL(): string {
        return `${this.BASE_URL}ideas/getApprovalHistory`;
    }

    // update approval data 
    updateApproveStatus(): string {
        return `${this.BASE_URL}ideas/updateApproval`;
    }

    // get feasibility history 
    getFeasiBulityHiURL(): string {
        return `${this.BASE_URL}ideas/getFeasibilityHistory`;
    }

    // update feas data 
    getFeasDataURL(): string {
        return `${this.BASE_URL}ideas/updateFeasibilityStatus`;
    }

    // getPhase data URL
    getPhasesDataURL(): string {
        return `${this.BASE_URL}ideas/getOverdueStatus`;
    }

    // getting team data URL 
    getTeamDataURL(): string {
        return `${this.BASE_URL}ideas/getTeamData`;
    }

    // adding team data on server 
    addTeamDataURL(): string{
        return `${this.BASE_URL}ideas/insertTeamData`;
    }

    // get build data URL 
    getBuilDataURL(): string{
        return `${this.BASE_URL}ideas/getBuildData`;
    }

    // isnert build data
    isnertBuildDataURL(): string{
        return `${this.BASE_URL}ideas/insertBuildData`;
    }

    // get production data URL
    getProductionDataURL(): string {
        return `${this.BASE_URL}ideas/getProductionData`;
    }

    insertProductionData(): string {
        return `${this.BASE_URL}ideas/insertProductionData`;
    }

    getAutomationDetails():  any {
        return `${this.BASE_URL}automation/getDetails`;
    }
    getAutomationDetail(): any{
        return `${this.BASE_URL}automation/getEditAutomation`;
    }
    getautomationEntity():  any {
        return `${this.BASE_URL}automation/getEntity`;
    }

    getAutomationTower():  any {
        return `${this.BASE_URL}automation/getTowers`;
    }
    getAutomationStatus():  any {
        return `${this.BASE_URL}automation/getStatus`;
    }

    updateAutomation(): any{
        return `${this.BASE_URL}automation/updateAutomationDetails`;
    }

    getAutomationAppNames():  any {
        return `${this.BASE_URL}automation/getApplications`;
    }

    saveAutomationDetails():  any {
        return `${this.BASE_URL}automation/insertAutomation`;
    }

    searchIdeaDetails():  any {
        return `${this.BASE_URL}ideas/searchIdeaDetails`;
    }

    getAllAutomationDetails():  any {
        return `${this.BASE_URL}automation/getAllAutomation`;
    }

    getSearchAutomation():  any {
        return `${this.BASE_URL}automation/getSearchAutomation`;
    }
    getIdeasCountByTower(): any{
        return `${this.BASE_URL}ideas/getTowerIdeacount`;
    }
    getIdeasCountByTech(): any{
        return `${this.BASE_URL}ideas/getTechIdeacount`;
    }
    getIdeasCountByQuarter(): any{
        return `${this.BASE_URL}ideas/getQuarterIdeacount`;
    }
    getIdeasCountByStatus(): any{
        return `${this.BASE_URL}ideas/getStatusIdeacount`;
    }

    getBusinessBenefitData():  any {
        return `${this.BASE_URL}ideas/getBenefitsdata`;
    }

    saveBusinessBenefitsData():  any {
        return `${this.BASE_URL}ideas/saveBenefitsdata`;
    }
	 //getquarterdata
    getQuarterData(): string {
        return `${this.BASE_URL}ideas/getQuarter`;
    }

    getquarthours(): string {
        return `${this.BASE_URL}ideas/gethours`;
    }
    savePeopleContributionDetails():  any {
        return `${this.BASE_URL}peopleContribution/insertPeopleContribution`;
    }
    TrainingEnrollment(): any{
        return `${this.BASE_URL}peopleContribution/insertPeopleTrainingEnrollment`;
    }
    getAllTrainings(): string {
    return `${this.BASE_URL}peopleContribution/getAllTrainings`;
    }
    getTrainingsUserCount(): string {
        return `${this.BASE_URL}peopleContribution/userEnrolledcountbyType`;
        }
    getRegisteredTraining(): string{
        return `${this.BASE_URL}peopleContribution/getregisteredTraining`;
    }
    getTrainingById(): string {
        return `${this.BASE_URL}peopleContribution/showTrainingById`;
        }
    updateTrainingById(): string {
        return `${this.BASE_URL}peopleContribution/updateTrainingDetails`;
    }
    getEnrolledUserCount(): string{
        return `${this.BASE_URL}peopleContribution/getTrainingCount`;
    }
    getEnrolledUserCountByMonth():  string{
        return `${this.BASE_URL}peopleContribution/getEnrolledUserCntbyMonth`;
    }
    getOriginateEntity():  string {
        return `${this.BASE_URL}originate/getOriginateEntity`;
    }
    getOriginateDetails():  string {
        return `${this.BASE_URL}originate/getOriginateDetails`;
    }
    saveOriginateDetails():  string {
        return `${this.BASE_URL}originate/saveOriginateDetails`;
    }
    getSearchOriginate():  any {
        return `${this.BASE_URL}originate/getSearchOriginate`;
    }
    updateoriginate(): any {
        return `${this.BASE_URL}originate/editOriginateDetails`;
    }
    ViewAutomationCommentsHistor(): any {
        return `${this.BASE_URL}automation/getCommentsHistory`;
    }
    commentsOriginate() : any {
        return `${this.BASE_URL}originate/getCommentsOriginate`;
    }
}
