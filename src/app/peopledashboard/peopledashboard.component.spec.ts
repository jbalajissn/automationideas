import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeopledashboardComponent } from './peopledashboard.component';

describe('PeopledashboardComponent', () => {
  let component: PeopledashboardComponent;
  let fixture: ComponentFixture<PeopledashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeopledashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeopledashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
