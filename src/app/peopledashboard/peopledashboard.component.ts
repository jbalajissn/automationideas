import { Component, OnInit,ViewChild } from '@angular/core';
import {DatePipe} from '@angular/common';
import { FormControl, Validators, FormGroup, FormGroupDirective,FormBuilder } from '@angular/forms';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
import { HttpCOnfig } from '../utils/httpConfig';
import {PeopleDashboardService} from './peopleDashBoardService';
import { TS } from 'typescript-linq';
import { element } from 'protractor';
declare let jsPDF;
declare let linq;


//import { element } from 'protractor';

@Component({
  selector: 'app-peopledashboard',
  templateUrl: './peopledashboard.component.html',
  styleUrls: ['./peopledashboard.component.css']
})
export class PeopledashboardComponent implements OnInit {
  elements = ['CompleteTraining', 'SkilledEmpCount', 'TrainingHistory', 'EnrolledUserCount'];
  pdf = new jsPDF('l', 'mm', 'a4');
  elCount = -1;
  count=0;
  sample = new TS.Collections.List<any>(false);
  view: any[] = [480, 350];
  BAview: any[] = [480, 350];
  BAlegendTitle = 'Initiative';
  PeopleDashboardForm: FormGroup;
  SkillTypeScheme = {
    domain: ['#647c8a', '#3f51b5', '#2196f3', '#00b862', '#afdf0a', '#a7b61a', '#f3e562', '#ff9800', '#ff5722', '#ff4514']
  };
  UserCountScheme = {
    domain: ['#A10A28', '#D3342D', '#EF6D49', '#FAAD67', '#FDDE90', '#DBED91', '#A9D770', '#6CBA67', '#2C9653', '#146738']
  };
  verStacScheme = {
    domain: ['#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886']
  };
  gradient = false;

  public trainingList: any;
  public trainingTech:Array<any>=["NewIT","Domain","Technical","SoftSkill"];
  public evntchange:any;
  public trainingTypeData:any[]=[];
  public FutureTrainingTypeData:any[]=[];
  public trainingUserCount:any[]=[];
  public trainingUserCount1:any[]=[];
  public PiechartTrainingType:any[]=[];
  public PieChartUserCount:any[]=[];
  public PieChartFutureTraining:any[]=[];
  public barChartTraining:any[]=[];
  public futTrainingDate:Array<any>=["name","series"]; 
  public futTrainingDate2:Array<any> = [];
  public futTrainingChartData:Array<any> = [];
  public stackChartdata:Array<any> = [];
  public usrCountByMonth:Array<any>=[];
  public barChartTrained:Array<any>=[];
  public UsertrainingCountbyMonth:any;
  //public mon:Array<any>=[];
  mon=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

  constructor(public peopleDashboardservice:PeopleDashboardService,private _formBuilder: FormBuilder,public httpConfig:HttpCOnfig) {

    
    
   }

   async ngOnInit() {
    this.PeopleDashboardForm = this._formBuilder.group({
      
      timePeriod: new FormControl()
    });
    
       this.trainingList=await this.getAllTrainings();  
   this.TrainingcountbyType(this.trainingList);
    this.PiechartTrainingType=this.trainingTypeData;
    console.log(this.PiechartTrainingType);
    let UsertrainingCount:any;
    UsertrainingCount=await this.getUserCount();
    console.log("Users");
    console.log(UsertrainingCount);
    this.TrainingUserCount(UsertrainingCount);
    this.PieChartUserCount=this.trainingUserCount1;
    this.futureTrainingList(this.trainingList);
    this.PieChartFutureTraining=this.FutureTrainingTypeData;
    this.pastTrainingList();
    this.stackChartdata=this.futTrainingDate2;
    console.log("Im here");
    console.log(this.stackChartdata);
    //let UsertrainingCountbyMonth:any;
    this.UsertrainingCountbyMonth=await this.getEnrolledUserCountbyMonth();
    console.log(this.UsertrainingCountbyMonth);
    this.UserCountByMonth(this.UsertrainingCountbyMonth);
    this.barChartTrained=this.usrCountByMonth
  }
 getAllTrainings(){
  return new Promise((resolve, reject) => {
  const getalltrainingList=this.httpConfig.getAllTrainings();
  console.log(getalltrainingList);
  this.peopleDashboardservice.getAlltrainings(getalltrainingList).subscribe((result)=>{
    if (result.status) {
      (result.status ? resolve(result.data) : reject(result));
    }
  }, (error) => {
    reject(error);
  });
});

 }

 TrainingcountbyType(data:any){
    if(data.length>0){
   this.trainingTech.forEach(element=>{
      const TypeCount:any[]=data.filter(train=>train.TrainingSkillType===element); 
       if(TypeCount.length>0)
       {
        this.trainingTypeData.push({
           name:element,
           value:TypeCount.length
         });
         
       }
       else{
        this.trainingTypeData.push({
          name:element,
          value:TypeCount.length
        }
        );

       }
          
   });
   
}
else{
  console.log("Training List is empty");
  
}}

getUserCount(){
  return new Promise((resolve, reject) => {
  const getUserCountTrainingType=this.httpConfig.getTrainingsUserCount();
  console.log(getUserCountTrainingType);
  this.peopleDashboardservice.getEnrolledUserCount(getUserCountTrainingType).subscribe((result)=>{
    if (result.status) {
      (result.status ? resolve(result.data) : reject(result));
    }
  }, (error) => {
    reject(error);
  });
});
}
TrainingUserCount(data:any)
{
  if(data.length>0)
  {
    data.forEach(element=>this.trainingUserCount1.push({
      name:element["TrainingSkillType"],
      value:element["EmployeeCount"]
    }));

  }else{
    console.log("Enrolled userlist is empty")
  }

}

futureTrainingList(data:any){
  
  
     if(data.length>0){
       
    let datepipe:DatePipe;
       var date=new Date();
       var mon=date.getMonth();
       console.log(date);
     let filtertrainingList:any;
       filtertrainingList=data.filter(function (item) {
        var itemTime = new Date(item.TrainingStartDate)
        return itemTime>date});

       console.log(filtertrainingList);
   this.trainingTech.forEach(element=>{
      console.log(element);
       const pastSkillTypeCount:any[]=filtertrainingList.filter(train=>train.TrainingSkillType==element);
       console.log("HI");
       console.log(pastSkillTypeCount);
       
       if(pastSkillTypeCount.length>0)
       {
        console.log("BeforePushingData");
        let result:any;
        console.log(this.FutureTrainingTypeData);
         this.FutureTrainingTypeData.push({
           name:element,
           value:pastSkillTypeCount.length
         });
         
       }
       else{
        this.FutureTrainingTypeData.push({
          name:element,
          value:pastSkillTypeCount.length
        });

       }
       console.log("AfterPushingData1");
       console.log(this.FutureTrainingTypeData);
     
   });
   
}
else{
  console.log("Training List is empty");
  
}

}

pastTrainingList(){
  let data:any;
  data=this.trainingList;
  console.log("I m in fn");
  const dt=new Date();
  let mn,i:any;
  i=3;
  
  
 
  if(data.length>0){
    
  //  mn=this.monthAdd(dt,-6);
    
    while( i>0){
      console.log(i);
       console.log("in while loop");
     let mnth:any;
     mnth=this.monthAdd(dt,-i);
     let filterbyMonth,pastSkillTypeCount1:any;
     let trngcount:any[]=[];
      filterbyMonth=data.filter(function (item){
       var itemTime=new Date(item.TrainingStartDate).getMonth();
       var itemYr=new Date(item.TrainingStartDate).getFullYear();
       return (itemTime===(mnth.getMonth()+1) && (itemYr===mnth.getFullYear()))});
            
       console.log(filterbyMonth);
       for(let tech of this.trainingTech){
        console.log("For Loop");
        console.log(tech);
        pastSkillTypeCount1=filterbyMonth.filter(train=>train.TrainingSkillType===tech);
        console.log(pastSkillTypeCount1.length);
        trngcount.push({
          name:tech,
          value:pastSkillTypeCount1.length
        });
        console.log(trngcount);
       }
       this.futTrainingDate2.push({
         name:this.mon[mnth.getMonth()+1],
         series:trngcount
       })
       console.log(this.futTrainingDate2);
       
       i--
      }
 
   
  }
  else{
    console.log("Training List is empty");
  }
}

 addToMonth( date, months ):any {
  var d = new Date( date || new Date() );
  d.setMonth( d.getMonth() + (months || 0), d.getDate());
  return d;      
}
monthAdd(date, month):Date {
  var temp = date;
  console.log("month1");
  console.log(month);
  temp = new Date(date.getFullYear(), date.getMonth(), 1);
  var test=temp.getMonth() + (month + 1);
  console.log("test1");
  console.log(test);
  temp.setMonth(temp.getMonth() + (month + 1));
  console.log("month");
  console.log(temp);
  temp.setDate(temp.getDate() - 1); 
  console.log("date");
  console.log(temp);
  console.log("compare");
  console.log(date.getDate());
  console.log(temp.getDate());
  if (date.getDate() < temp.getDate()) { 
      temp.setDate(date.getDate()); 
  }
  console.log("final");
  console.log(temp);
    return temp;    
}
getEnrolledUserCountbyMonth(){
  return new Promise((resolve, reject) => {
    const getUserCountByMonth=this.httpConfig.getEnrolledUserCountByMonth();
    console.log(getUserCountByMonth);
    this.peopleDashboardservice.getEnrolledUserCountbyMonth(getUserCountByMonth).subscribe((result)=>{
      if (result.status) {
        (result.status ? resolve(result.data) : reject(result));
      }
    }, (error) => {
      reject(error);
    });
  });
}
UserCountByMonth(data:any){
  const dt=new Date();
  let mn,i:any;
  i=3;
  if (data.length>0){
    while(i>0){
      
     let mnth:any;
     mnth=this.monthAdd(dt,-i);
     
     let cntbyMonth,skillcnt:any;
     let trngcount:any[]=[];
     cntbyMonth=data.filter(function (item){
       var itemTime=new Date(item.trngStrtDate).getMonth();
       var itemYr=new Date(item.trngStrtDate).getFullYear();
       return (itemTime===(mnth.getMonth()+1) && (itemYr===mnth.getFullYear()))});
      console.log(cntbyMonth);
      for(let tech of this.trainingTech){
        console.log("For Loop");
        console.log(tech);
        skillcnt=cntbyMonth.filter(train=>train.TrainingSkillType===tech);
        console.log(skillcnt);
        
        let cnt:any;
        if(skillcnt.length>0){
        cnt=skillcnt[0].empcount;
        }
        else{
          cnt=0;
        }
        trngcount.push({
          name:tech,
          value:cnt
        });
      }
      this.usrCountByMonth.push({
        name:this.mon[mnth.getMonth()+1],
        series:trngcount
      });
      console.log(this.usrCountByMonth);
      i--;
    }

  }
  else{
    console.log("Enrolled User count is empty");
  }

}

ClickPDF(){
  this.sample = new TS.Collections.List<any>(false);
  this.pdf = new jsPDF('l', 'mm', 'a4');
    this.elCount = -1;
    this.pdf.setFontSize(22);
    this.pdf.setFont('times');
    this.pdf.setFontType('bolditalic');
    this.pdf.text('People Training Report', 150, 10, 'center');
    this.recursiveAddHtml();

}
recursiveAddHtml():any{
  this.elCount++;
    if (this.elCount < this.elements.length) {
      this.pdf.setFontSize(16);
      this.pdf.setFont('helvetica');
      this.pdf.setFontType('italic');
      switch (this.elements[this.elCount]) {
        case 'CompleteTraining':
              this.pdf.text("Complete Training List by Skill Type" , 10 , 30);
              break;
        case 'SkilledEmpCount':
              this.pdf.text(" Resources Training Enrollment Count" , 10 , 30);
              break;
        case 'TrainingHistory':
              this.pdf.text("Training History" , 10 , 30);
              break;
        case 'EnrolledUserCount':
              this.pdf.text("Employees trained count in previous months" , 10 , 30);
              break;
      }
      //this.pdf.text(this.elements[this.elCount] , 10 , 30);
      this.pdf.addHTML(document.getElementById(this.elements[this.elCount]), 80, 40 , () => {
        switch (this.elements[this.elCount]) {
          case 'CompleteTraining':
                this.printoverallTraining();
                break;
          case 'SkilledEmpCount':
               this.printOverallTrainedEmployeeCount();
                break;
          case 'TrainingHistory':
               this.printTrainingHistory();
                break;
          case 'EnrolledUserCount':
               this.printEnrolledUserCount();
                break;
        }
        this.pdf.setLineWidth(0.5);
        this.pdf.line(10, 150 , 150, 150);
        this.pdf.addPage();
        this.recursiveAddHtml();
      });
    } else {
        this.pdf.save('Automation_Dashboard.pdf');
    }

}
printoverallTraining():any{
  const rowsArea:any[]=[];
  const columnsArea = [
    { title: 'ID', dataKey: 'id' },
    { title: 'TrainingType', dataKey: 'TrainingType' },
    { title: 'Count', dataKey: 'count' }];
    this.count=0;
    this.PiechartTrainingType.forEach(element=>{rowsArea.push({
      id:++this.count,
      TrainingType:element.name,
      count:element.value

    });
  });
  this.pdf.autoTable(columnsArea, rowsArea, {
    margin: { vertical : 40 },
    styles: { overflow: 'linebreak', columnWidth: 'wrap' },
    columnStyles: { text: { columnWidth: 'auto' } }
  });
}
printOverallTrainedEmployeeCount():any{
  const rowsArea:any[]=[];
  const columnsArea = [
    { title: 'ID', dataKey: 'id' },
    { title: 'TrainingType', dataKey: 'TrainingType' },
    { title: 'Count', dataKey: 'count' }];
    this.count=0;
    this.PieChartUserCount.forEach(element=>{rowsArea.push({
      id:++this.count,
      TrainingType:element.name,
      count:element.value

    });
  });
  this.pdf.autoTable(columnsArea, rowsArea, {
    margin: { vertical : 40 },
    styles: { overflow: 'linebreak', columnWidth: 'wrap' },
    columnStyles: { text: { columnWidth: 'auto' } }
  });

}
printTrainingHistory():any{

  let test1,i,j:any;
  j=40;
  if(this.futTrainingChartData.length>0){
    let rowsArea:any[]=[];
    let maincolumnsArea:any = [
     // { title: 'ID', dataKey: 'id' },
      { title: 'TrainingType', dataKey: 'TrainingType' },
      { title: 'Count', dataKey: 'count' }];
      let lcount:number=0;
    this.futTrainingChartData.forEach(element=>{       
   
    j=j+50;
    if(element.series.length>0){
  
    rowsArea.push({
   
       TrainingType:element.name,
   
     });
        
    element.series.forEach(ele=>{
     
      
      rowsArea.push({
       // id:++lcount,
        TrainingType:ele.name,
        count:ele.value
      });
     
   
    });
    let k:any=30;
    let l:any=40;
    console.log("Col Value");
    console.log(rowsArea);
    this.pdf.text(k,l,"");
    k=k+10;
    l=l+10;
    
   
  }
 
 });
 this.pdf.autoTable(maincolumnsArea,rowsArea,{
  margin: { vertical : 40 },
  styles: { overflow: 'linebreak', columnWidth: 'wrap' },
columnStyles: { text: { columnWidth: 'auto' } }
});


  }
  

}

printEnrolledUserCount(){
  let test1,i,j:any;
  j=40;
  if(this.barChartTrained.length>0){
    let rowsArea:any[]=[];
    let maincolumnsArea:any = [
     
      { title: 'TrainingType', dataKey: 'TrainingType' },
      { title: 'Count', dataKey: 'count' }];
      let lcount:number=0;
    this.barChartTrained.forEach(element=>{       
  
    j=j+50;
    if(element.series.length>0){
  
    rowsArea.push({
   
       TrainingType:element.name,
   
     });
        
    element.series.forEach(ele=>{
     
      
      rowsArea.push({
       // id:++lcount,
        TrainingType:ele.name,
        count:ele.value
      });
     
   
    });
    let k:any=30;
    let l:any=40;
    console.log("Col Value");
    console.log(rowsArea);
    this.pdf.text(k,l,"");
    k=k+10;
    l=l+10;
    
   
  }
 
 });
 this.pdf.autoTable(maincolumnsArea,rowsArea,{
  margin: { vertical : 40 },
  styles: { overflow: 'linebreak', columnWidth: 'wrap' },
columnStyles: { text: { columnWidth: 'auto' } }
});

  }
}

OnPeriodChange(event:any){
  let mn:any;
  let data,data1,cntbyMonth,skillcnt:any;
  let futTrainingDate3:Array<any> = [];
  
  data=this.trainingList;
  data1=this.UsertrainingCountbyMonth;
  console.log("inside evwent")
  console.log(event)
  if(event!==-1){
    
    if(event==6){
     this.evntchange=6
    }
    if(event==3){
      this.evntchange=3
     }
    if(event==-3){
      this.evntchange=-3
     }
  }
  const dt=new Date();
  /*training History for future date */
  if( this.evntchange>0){
        
  if(data.length>0){
    let cnt:any;
    cnt=this.evntchange;
        while( cnt>0){
          let cntbyMonth,skillcnt:any;
     let usrtrngcount:any[]=[];
      console.log(this.evntchange);
       console.log("in while loop");
     let mnth:any;
     mnth=this.monthAdd(dt,-cnt);
     let filterbyMonth,pastSkillTypeCount1:any;
     let trngcount:any[]=[];
      filterbyMonth=data.filter(function (item){
       var itemTime=new Date(item.TrainingStartDate).getMonth();
       var itemYr=new Date(item.TrainingStartDate).getFullYear();
       return (itemTime===(mnth.getMonth()+1) && (itemYr===mnth.getFullYear()))});
            
       console.log(filterbyMonth);
       /*for user count */
       cntbyMonth=data.filter(function (item){
        var itemTime=new Date(item.trngStrtDate).getMonth();
        var itemYr=new Date(item.trngStrtDate).getFullYear();
        return (itemTime===(mnth.getMonth()+1) && (itemYr===mnth.getFullYear()))});

       for(let tech of this.trainingTech){
        console.log("For Loop");
        console.log(tech);
        pastSkillTypeCount1=filterbyMonth.filter(train=>train.TrainingSkillType===tech);
        console.log(pastSkillTypeCount1.length);
        trngcount.push({
          name:tech,
          value:pastSkillTypeCount1.length
        });
        console.log(trngcount);
       }
       console.log("brfore push");
       console.log(trngcount);
       console.log(this.mon[mnth.getMonth()+1]);
       futTrainingDate3.push({
         name:this.mon[mnth.getMonth()+1],
         series:trngcount
       })
       console.log(futTrainingDate3);
       /*for user training count*/
       
       
       cnt--;
      }
      console.log(futTrainingDate3);
      this.futTrainingChartData=futTrainingDate3;
      this.stackChartdata=futTrainingDate3;
    //  this.barChartTrained=futuserTrainingDate3;
    }
    if(data1.length>0){
      let usrcnt1:any;
      let futuserTrainingDate3:Array<any> = [];
      usrcnt1=this.evntchange;
      console.log(data1.length);
      console.log(this.evntchange);
    //  mn=this.monthAdd(dt,-6);
    if(usrcnt1>0){
      while( usrcnt1>0){
        let mnth:any;
       mnth=this.monthAdd(dt,-usrcnt1);
        let cntbyMonth,skillcnt:any;
       let trngcount:any[]=[];
       cntbyMonth=data1.filter(function (item){
         var itemTime=new Date(item.trngStrtDate).getMonth();
         var itemYr=new Date(item.trngStrtDate).getFullYear();
         return (itemTime===(mnth.getMonth()+1) && (itemYr===mnth.getFullYear()))});
        console.log(cntbyMonth);
        for(let tech of this.trainingTech){
          console.log("For Loop");
          console.log(tech);
          skillcnt=cntbyMonth.filter(train=>train.TrainingSkillType===tech);
          console.log(skillcnt);
          
          let cnt:any;
          if(skillcnt.length>0){
          cnt=skillcnt[0].empcount;
          }
          else{
            cnt=0;
          }
          trngcount.push({
            name:tech,
            value:cnt
          });
        }
        futuserTrainingDate3.push({
          name:this.mon[mnth.getMonth()+1],
          series:trngcount
        });
        console.log(futuserTrainingDate3);
        usrcnt1--;
      }
      this.futTrainingChartData=
      this.barChartTrained=futuserTrainingDate3;
    }
    }
    else{
      console.log("Training is empty");
    }
  }

   /*training History for past  date */
    if(this.evntchange<0){

      if(data.length>0){
      var dummyval=0;
      this.evntchange=this.evntchange+1;
      while(dummyval>=this.evntchange ){
        console.log(dummyval);
         console.log("in while loop");
       let mnth:any;
       
       mnth=this.monthAdd(dt,-dummyval);
       console.log("add");
     console.log(mnth);
       let filterbyMonth,pastSkillTypeCount1:any;
       let trngcount:any[]=[];
        filterbyMonth=data.filter(function (item){
         var itemTime=new Date(item.TrainingStartDate).getMonth();
         var itemYr=new Date(item.TrainingStartDate).getFullYear();
         return (itemTime===(mnth.getMonth()) && (itemYr===mnth.getFullYear()))});
              
         console.log(filterbyMonth);
         for(let tech of this.trainingTech){
          console.log("For Loop");
          console.log(tech);
          pastSkillTypeCount1=filterbyMonth.filter(train=>train.TrainingSkillType===tech);
          console.log(pastSkillTypeCount1.length);
          trngcount.push({
            name:tech,
            value:pastSkillTypeCount1.length
          });
          console.log(trngcount);
         }
         futTrainingDate3.push({
           name:this.mon[mnth.getMonth()],
           series:trngcount
         })
         console.log(futTrainingDate3);
         
         dummyval--;
        }
        console.log(futTrainingDate3);
        this.futTrainingChartData=futTrainingDate3;
        this.stackChartdata=futTrainingDate3;

    }
    
    if(data1.length>0){
      let usrcnt1:any;
      let futuserTrainingDate3:Array<any> = [];
      var dummyval=0;
      usrcnt1=this.evntchange;
      console.log(data1.length);
      console.log(this.evntchange);
    //  mn=this.monthAdd(dt,-6);
          while( dummyval>=usrcnt1){
          let mnth:any;
          mnth=this.monthAdd(dt,-dummyval);
          let cntbyMonth,skillcnt:any;
          let trngcount:any[]=[];
          cntbyMonth=data1.filter(function (item){
          var itemTime=new Date(item.trngStrtDate).getMonth();
          var itemYr=new Date(item.trngStrtDate).getFullYear();
          return (itemTime===(mnth.getMonth()) && (itemYr===mnth.getFullYear()))});
         for(let tech of this.trainingTech){
          skillcnt=cntbyMonth.filter(train=>train.TrainingSkillType===tech);       
          let cnt:any;
          if(skillcnt.length>0){
          cnt=skillcnt[0].empcount;
          }
          else{
            cnt=0;
          }
          trngcount.push({
            name:tech,
            value:cnt
          });
        }
        futuserTrainingDate3.push({
          name:this.mon[mnth.getMonth()],
          series:trngcount
        });
        dummyval--;
      }
      this.barChartTrained=futuserTrainingDate3;

    }
  }
  
  }
  
  ClickExcel(){
    
  }

  

}


