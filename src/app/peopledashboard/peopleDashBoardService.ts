import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { HttpCOnfig } from './../utils/httpConfig';
import { Http, HttpModule,RequestOptions,ResponseContentType, Headers } from '@angular/http';
@Injectable()
export class PeopleDashboardService{

    constructor(private httpClient: HttpClient) {  
    }
    //Fetch all Training
    getAlltrainings(URL: string){
        return this.httpClient.get<any>(URL,{});
    }
    getEnrolledUserCount(URL: string){
        return this.httpClient.get<any>(URL,{});
    }
    getEnrolledUserCountbyMonth(URL: string){
        return this.httpClient.get<any>(URL,{});
    }
}