import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { NgForm } from '@angular/forms/src/directives/ng_form';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

@Component({
  selector: 'app-add-ideas',
  templateUrl: './add-ideas.component.html',
  styleUrls: ['./add-ideas.component.css']
})
export class AddIdeasComponent implements OnInit {

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(EMAIL_REGEX)]);

  constructor() { }

  ngOnInit() {
  }

  onIdeaSubmit(form : NgForm){
    console.log(form.value)
  }

}
