import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdeaLoggingComponent } from './idea-logging.component';

describe('IdeaLoggingComponent', () => {
  let component: IdeaLoggingComponent;
  let fixture: ComponentFixture<IdeaLoggingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdeaLoggingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdeaLoggingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
