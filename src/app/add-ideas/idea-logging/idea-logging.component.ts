
import { MatDialog } from '@angular/material';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { Component, OnInit, ViewChild} from '@angular/core';
import { FormControl, Validators, FormGroup, FormGroupDirective } from '@angular/forms';
import { NavService } from '../../utils/navServices';
import { IdeasLogginService } from './idea-logging-service';
import { ProgressDialogComponent } from '../../progress-dialog/progress-dialog.component';
import { HttpCOnfig } from '../../utils/httpConfig';
import { AddAutomationService } from '../../add-automation/addautomationservice';

// const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@accenture.com/;


@Component({
  selector: 'app-idea-logging',
  templateUrl: './idea-logging.component.html',
  styleUrls: ['./idea-logging.component.css']
})
export class IdeaLoggingComponent implements OnInit {

  dialogRef: any;
  typeOfIdeas: Array<any> = ['Innovate', 'Co-Innovate']
  clientTowerTypes: any[] = [];
  technology: any[] = [];

  @ViewChild(FormGroupDirective) myform;


  ideaForm = new FormGroup({
    email: new FormControl('', Validators.compose([Validators.pattern(EMAIL_REGEX)])),
    employeeID: new FormControl('', Validators.compose(
      [Validators.required,
        Validators.minLength(8),
        Validators.maxLength(10)])),
    title: new FormControl('', Validators.minLength(8)),
    techs: new FormControl('', Validators.minLength(1)),
    clientTowerType: new FormControl('', Validators.required),
    ideaType: new FormControl('', Validators.required),
    description: new FormControl('', Validators.compose([Validators.maxLength(256),Validators.minLength(10)]))
  });

  constructor(
    public navService: NavService,
    public ideadLogginService: IdeasLogginService,
    public dialog: MatDialog,
    public addAutomationService: AddAutomationService,
    public httpConfig: HttpCOnfig
  ) {
    this.navService.hideNavStatsu();
  }

  async ngOnInit() {
    let automationTowerData: any = '';
    automationTowerData = await this.FetchAutomationTowers();
    this.PopulateTowers(automationTowerData);

    let automationEntityData: any = '';
    automationEntityData = await this.FetchAutomationEntity();
    this.PopulateDropdowns(automationEntityData);
  }

  FetchAutomationTowers() {
    return new Promise((resolve, reject) => {
      const automationDetailsURL: string = this.httpConfig.getAutomationTower();
      this.addAutomationService.automationTower(automationDetailsURL).subscribe((result) => {
        if (result.status) {
          (result.status ? resolve(result.data) : reject(result));
        }
      }, (error) => {
        reject(error);
      });
    });
  }

  FetchAutomationEntity() {
    return new Promise((resolve, reject) => {
      const automationDetailsURL: string = this.httpConfig.getautomationEntity();
      this.addAutomationService.automationEntity(automationDetailsURL).subscribe((result) => {
        if (result.status) {
          (result.status ? resolve(result.data) : reject(result));
        }
      }, (error) => {
        reject(error);
      });
    });
  }

  PopulateDropdowns(dropdownResults: any) {
    console.log(dropdownResults);
    const serverData: any[] = [];
    dropdownResults.forEach(element => {
      serverData.push({
          item: element.ID,
          value: element.Value,
          Type: element.Type
      });
    });
    serverData.forEach(element => {
      if  (element.Type === 'InnovtionTechnology') {
        this.technology.push(element);
      }
    });
  }

  PopulateTowers(dropdownResults: any) {
    console.log(dropdownResults);
    const serverData: any[] = [];
    dropdownResults.forEach(element => {
      serverData.push({
          item: element.ID,
          value: element.Value
      });
    });
    this.clientTowerTypes = serverData;
  }

  onIdeaSubmit(){
    this.openDIalog(1);
    const data = {
      email: this.ideaForm.get('email').value,
      employeeID: this.ideaForm.get('employeeID').value,
      title: this.ideaForm.get('title').value,
      description: this.ideaForm.get('description').value,
      tower: this.ideaForm.get('clientTowerType').value,
      innovate_status: (this.ideaForm.get('ideaType').value === 'Innovate' ? 1 : 2),
      techs: this.ideaForm.get('techs').value
    }
    console.log(data);
    this.ideadLogginService.storeIdeas({data: data}).subscribe((result)=>{
      if(result.status){
        // erase form data and show success gif
        this.successGif();
      }else{
        this.closeDialog();
        console.log(result.message);
      }
    },(error)=>{
      this.closeDialog();
      console.log("error",error)
    })
  }

  openDIalog(code){
    this.dialogRef = this.dialog.open(ProgressDialogComponent,{data: {code: code}});
  }

  closeDialog(){
    this.dialogRef.close();
  }

  successGif(){
    // first reset form data
    this.closeDialog();
    this.resetForm();
    this.openDIalog(2);
    setTimeout(()=>{
      this.closeDialog();
    },2000);
  }

  // open animated gif 
  welcomeGif(){
    this.openDIalog(3);
    // close it after 2 seconds 
    setTimeout(()=>{this.closeDialog()},4000);
  }

  // resetting form
  resetForm(){
    this.myform.resetForm();
  }

}
