import { HttpCOnfig } from './../../utils/httpConfig';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// this is for making http calls 

@Injectable()
export class IdeasLogginService{
    constructor(
        private httpClient: HttpClient,
        private httpConfig: HttpCOnfig
    ){}
    
    // sending idea details to server 
    storeIdeas(data){
        return this.httpClient.post<any>(this.httpConfig.getIdeasInsertURL(),data);
    }
}