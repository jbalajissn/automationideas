import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdeaByMonthComponent } from './idea-by-month.component';

describe('IdeaByMonthComponent', () => {
  let component: IdeaByMonthComponent;
  let fixture: ComponentFixture<IdeaByMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdeaByMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdeaByMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
