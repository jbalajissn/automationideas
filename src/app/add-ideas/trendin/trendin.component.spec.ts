import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrendinComponent } from './trendin.component';

describe('TrendinComponent', () => {
  let component: TrendinComponent;
  let fixture: ComponentFixture<TrendinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrendinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrendinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
