import { Component, OnInit } from '@angular/core';
import {NgxChartsModule} from '@swimlane/ngx-charts';

@Component({
  selector: 'app-trendin',
  templateUrl: './trendin.component.html',
  styleUrls: ['./trendin.component.css']
})
export class TrendinComponent implements OnInit {

  view: any[] = [200, 200];
  single: any;
  single2: any;

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  constructor() { }

  ngOnInit() {
    this.single = [
      {
        "name": "Java",
        "value": 127
      },
      {
        "name": "JavaScript",
        "value": 3544
      },
      {
        "name": "SQL",
        "value": 654
      }
    ];
  
    this.single2 = [
      {
        "name": "SOS",
        "value": 322
      },
      {
        "name": "OPOM",
        "value": 165
      },
      {
        "name": "CMBS",
        "value": 345
      }
    ];
  }

  

  onSelect(event: any){
    console.log(event)
  }

}
