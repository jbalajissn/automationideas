export var single = [
    {
      "name": "JAVA",
      "value": 123
    },
    {
      "name": "SQL",
      "value": 143
    },
    {
      "name": "Node",
      "value": 222
    }
  ];
  
  export var multi = [
    {
      "name": "Approval",
      "series": [
        {
          "name": "Approved",
          "value": 21
        },
        {
          "name": "disApproved",
          "value": 222
        }
      ]
    },
  
    {
      "name": "Feasibility",
      "series": [
        {
          "name": "Approved",
          "value": 222
        },
        {
          "name": "Disapproved",
          "value": 123
        }
      ]
    },
  
    {
      "name": "Production",
      "series": [
        {
          "name": "Approved",
          "value": 33
        },
        {
          "name": "Disapproved",
          "value": 1
        }
      ]
    }
  ];
  