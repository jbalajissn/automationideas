import { ExcelService } from './../utils/excelService';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpCOnfig } from '../utils/httpConfig';
//import {single,multi} from './data';
import {DashBoardService} from './dashboardService';

@Component({
  selector: 'app-dashboard-dialog',
  templateUrl: './dashboard-dialog.component.html',
  styleUrls: ['./dashboard-dialog.component.css']
})
export class DashboardDialogComponent implements OnInit {

  single: any[];
  test:any[];
  multi: any[];
  view: any[] = [700, 400];
  colorScheme = {
   // domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
   domain: ['#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886']
  };

  statColorcheme = {
    // domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
    domain: ['#A10A28', '#D3342D', '#EF6D49', '#FAAD67', '#FDDE90', '#DBED91', '#A9D770', '#6CBA67', '#2C9653', '#146738']
   };
 
  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Ideas';
  showYAxisLabel = true;
  yAxisLabel = 'Status';

   // line, area
   autoScale = true;

  constructor(
    public dialogRef: MatDialogRef<DashboardDialogComponent>,
    private excelService : ExcelService,private httpConfig: HttpCOnfig,private dashboardService:DashBoardService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.getTowerCount();
      this.getStatusCount();
      
  //  console.log(this.getStatusCount());
    }
    ngOnInit() { 
    

   
   
    }

    getTowerCount(){
      

      return new Promise((resolve, reject) => {
        const innovationDashBoardDetailsURL: string = this.httpConfig.getIdeasCountByTower();
        this.dashboardService.innovationDashboard(innovationDashBoardDetailsURL).subscribe((result) => {
          
          if (result.status) {
           //(result.status ? resolve(result.data) : reject(result));
           if (result.data.length > 0) {
            this.single = [];
            this.single = result.data;
            console.log(this.single);
          }
           

          }
        }, (error) => {
          reject(error);
        });
      });
    }

    getStatusCount(){
      

      return new Promise((resolve, reject) => {
        const innovationDashBoardDetailsURL: string = this.httpConfig.getIdeasCountByStatus();
        this.dashboardService.innovationDashboard(innovationDashBoardDetailsURL).subscribe((result) => {
          
          if (result.status) {
           //(result.status ? resolve(result.data) : reject(result));
           if (result.data.length > 0) {
            this.multi = [];
            this.multi = result.data;
            console.log(this.multi);
          }
           

          }
        }, (error) => {
          reject(error);
        });
      });
    }
    onSelect(event) {
      console.log(event);
    }


    // exporting data as excel 
    exportDataAsExcel(){
      this.excelService.exportAsExcelFile(this.single,'tempFile');
    }

}
