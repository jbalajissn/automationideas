import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, convertToParamMap } from '@angular/router';
import { CheckUserStatus } from './../utils/checkUserStatus';
import { FormGroup, FormControl, Validators, FormBuilder, FormGroupDirective, AbstractControl } from '@angular/forms';
import { HttpCOnfig } from '../utils/httpConfig';
import { MatDialog } from '@angular/material';
import { FormArray } from '@angular/forms/src/model';
import { forEach } from '@angular/router/src/utils/collection';
import { AddAutomationService } from './addautomationservice';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import { isNull } from 'util';
import { FormatWidth, DatePipe } from '@angular/common';

@Component({
  selector: 'app-add-automation',
  templateUrl: './add-automation.component.html',
  styleUrls: ['./add-automation.component.css']
})

export class AddAutomationComponent implements OnInit {
 public user: string;
  applicationName: FormControl = new FormControl();
  clientTowerTypes: any[] = [];
  workTypes: any[] = [];
  benefitAreas: any[] = [];
  initiativeCat: any[] = [];
  methodologies: any[] = [];
  dialogRef: any;
  AutomationForm: FormGroup;
  automationBasicFormGroup: FormGroup;
  automationDetailFormGroup: FormGroup;
  automationAppData: any = [];
  applicationNames: any = [
    {
      item: 0,
      value: '-- Select Tower--'
    }];

  filteredOptions: Observable<string[]>;
  @ViewChild(FormGroupDirective) myform;

  get formArray(): AbstractControl | null { return this.AutomationForm.get('formArray'); }
  // AutomationBasicForm

  myFilter = (d: Date): boolean => {
    const day = d.getDay();
    const year = d.getFullYear();
    const currentDate = new Date();
    const currentyear = currentDate.getFullYear();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0 && day !== 6 && year > currentyear - 2 && year < currentyear + 2;
  }

    constructor(private _formBuilder: FormBuilder,
    private httpConfig: HttpCOnfig,
    public userinfo: CheckUserStatus,
    private addAutomationService: AddAutomationService,
    public dialog: MatDialog ) {
      this.getUserInfo();
      console.log(this.user);
    }

    getUserInfo(){
      this.user = JSON.parse(this.userinfo.getUserDetails())[0].username;
    }

  async ngOnInit() {
    this.AutomationForm = this._formBuilder.group({
      formArray: this._formBuilder.array([
        this._formBuilder.group({
          clientTowerType: new FormControl('', Validators.required),
          applicationName: new FormControl('', Validators.required),
          anchor: new FormControl('', Validators.required),
          date: new FormControl('', Validators.required),
          autoenddate:new FormControl('',Validators.required),
          assignedTo: new FormControl('', Validators.required),
          title: new FormControl('', Validators.compose([Validators.minLength(10)])),
          description: new FormControl('', Validators.compose([Validators.minLength(20)]))
        }),
        this._formBuilder.group({
          effort: new FormControl('', Validators.compose([Validators.minLength(2), Validators.maxLength(4),
          Validators.min(10), Validators.max(9999)])),
          implementation: new FormControl('', Validators.compose([Validators.minLength(2), Validators.maxLength(4),
          Validators.min(10), Validators.max(9999)])),
          ticket: new FormControl('',null),
          FTE: new FormControl('', Validators.required),
          costperday: new FormControl('', Validators.required),
          netcostsaving: new FormControl('', Validators.required),
          initiative: new FormControl('', Validators.required),
          methodology: new FormControl('', Validators.required),
          workType: new FormControl('', Validators.required),
          benefitArea: new FormControl('', Validators.required)
        }),
      ])
    });

    this.automationBasicFormGroup = this._formBuilder.group({
      clientTowerType: new FormControl('', Validators.required),
      applicationName: new FormControl('', Validators.required),
      anchor: new FormControl('', Validators.required),
      date: new FormControl('', Validators.required),
      autoenddate:new FormControl('',Validators.required),
      assignedTo: new FormControl('', Validators.required),
      title: new FormControl('', Validators.compose([Validators.minLength(10)])),
      description: new FormControl('', Validators.compose([Validators.minLength(20)]))
    });

    this.automationDetailFormGroup = this._formBuilder.group({
      effort: new FormControl('', Validators.compose([Validators.minLength(2), Validators.maxLength(4),
      Validators.min(10), Validators.max(9999)])),
      implementation: new FormControl('', Validators.compose([Validators.minLength(2), Validators.maxLength(4),
      Validators.min(10), Validators.max(9999)])),
      ticket: new FormControl('',null),
      FTE: new FormControl('', Validators.required),
      costperday: new FormControl('', Validators.required),
      netcostsaving: new FormControl('', Validators.required),
      initiative: new FormControl('', Validators.required),
      methodology: new FormControl('', Validators.required),
      workType: new FormControl('', Validators.required),
      benefitArea: new FormControl('', Validators.required)
    });

    let automationEntityData: any = '';
    automationEntityData = await this.FetchAutomationEntity();
    this.PopulateDropdowns(automationEntityData);

    let automationTowerData: any = '';
    automationTowerData = await this.FetchAutomationTowers();
    this.PopulateTowers(automationTowerData);

    this.automationAppData = await this.FetchAutomationApplicationName();
    this.filteredOptions  = this.applicationName.valueChanges
      .pipe(
        startWith(''),
        map(val => this.filter(val))
      );
  }

  filter(val: string): string[] {
    return this.applicationNames.filter(option =>
      option.value.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  FetchAutomationTowers() {
    return new Promise((resolve, reject) => {
      const automationDetailsURL: string = this.httpConfig.getAutomationTower();
      this.addAutomationService.automationTower(automationDetailsURL).subscribe((result) => {
        if (result.status) {
          (result.status ? resolve(result.data) : reject(result));
        }
      }, (error) => {
        reject(error);
      });
    });
  }

  FetchAutomationApplicationName() {
    return new Promise((resolve, reject) => {
      const automationDetailsURL: string = this.httpConfig.getAutomationAppNames();
      this.addAutomationService.automationApplication(automationDetailsURL).subscribe((result) => {
        if (result.status) {
          (result.status ? resolve(result.data) : reject(result));
        }
      }, (error) => {
        reject(error);
      });
    });
  }

  FetchAutomationEntity() {
    return new Promise((resolve, reject) => {
      const automationDetailsURL: string = this.httpConfig.getautomationEntity();
      this.addAutomationService.automationEntity(automationDetailsURL).subscribe((result) => {
        if (result.status) {
          (result.status ? resolve(result.data) : reject(result));
        }
      }, (error) => {
        reject(error);
      });
    });
  }

  PopulateTowers(dropdownResults: any) {
    console.log(dropdownResults);
    const serverData: any[] = [];
    dropdownResults.forEach(element => {
      serverData.push({
          item: element.ID,
          value: element.Value
      });
    });
    this.clientTowerTypes = serverData;
  }

  PopulateDropdowns(dropdownResults: any) {
    console.log(dropdownResults);
    const serverData: any[] = [];
    dropdownResults.forEach(element => {
      serverData.push({
          item: element.ID,
          value: element.Value,
          Type: element.Type
      });
    });
    serverData.forEach(element => {
      if  (element.Type === 'automationWorkType') {
        this.workTypes.push(element);
      }
      if  (element.Type === 'automationInitiativeCat') {
        this.initiativeCat.push(element);
      }
      if  (element.Type === 'automationTool') {
        this.methodologies.push(element);
      }
      if  (element.Type === 'automationBenefitArea') {
        this.benefitAreas.push(element);
      }
    });

  }
  /* Add Automation */
  onAddAutomationButton() {
    // login as admin if login type is admin or else as user
    this.openDIalog(1);
    const frmArry: any = this.AutomationForm.get('formArray') as FormArray;
    const frmArryValues: any = frmArry.value as Array<any>;
    const datePipe   = new DatePipe('en-US');
    const automationData = {
      automationTower : frmArryValues[0].clientTowerType,
      automationApplication: frmArryValues[0].applicationName,
      automationTitle : frmArryValues[0].title,
      automationDescription : frmArryValues[0].description,
      automationDate : datePipe.transform(frmArryValues[0].date, 'dd/MM/yyyy') ,
      automationActualCompletionDate: datePipe.transform(frmArryValues[0].autoenddate, 'dd/MM/yyyy'),
      automationLead : frmArryValues[0].anchor,
      automationResource  : frmArryValues[0].assignedTo,
      automationEfforts : frmArryValues[1].effort,
      automationImplementation : frmArryValues[1].implementation,
      automationFTE:frmArryValues[1].FTE,
      automationcostperday: frmArryValues[1].costperday,
      automationnetcostsaving : frmArryValues[1].netcostsaving,
      automationticket : isNull(frmArryValues[1].ticket) ? 0 :  frmArryValues[1].ticket,
      automationWorkType : frmArryValues[1].workType,
      automationBenefitArea: frmArryValues[1].benefitArea,
      automationInitiativeCat: frmArryValues[1].initiative,
      automationTool: frmArryValues[1].methodology,
      automationCreatedBy : this.user
    };

    console.log(automationData);
    const automationSaveURL: string = this.httpConfig.saveAutomationDetails();
    this.addAutomationService.saveAutomation(automationSaveURL, {data: automationData}).subscribe((result) => {
      if (result.status) {
        // erase form data and show success gif
        console.log(result.status);
        this.successGif();
      } else {
        console.log(result.message);
      }
    }, (error) => {
      console.log('error', error);
    });
  }

  arraytoString(toolvalues)  {
      let strTool = '';
      toolvalues.forEach(element => {
        strTool += element;
      });
      return strTool;
  }   

  openDIalog(code) {
    this.dialogRef = this.dialog.open(ProgressDialogComponent, {data: {code: code}});
  }

  closeDialog() {
    this.dialogRef.close();
  }

  successGif() {
    // first reset form data
    this.closeDialog();
    this.resetForm();
    this.openDIalog(2);
    setTimeout(() => {
      this.closeDialog();
    }, 2000);
  }

  OnTowerChange(event: any) {
    this.applicationNames = [];
    this.applicationName.reset();
    const applicationData: any[] = [];
    this.automationAppData.forEach(element => {
      if (element.TowID === event) {
          applicationData.push({
            item: element.ID,
            value: element.Value
          });
      }
    });
    this.applicationNames = applicationData;
    this.filteredOptions = this.applicationName.valueChanges
      .pipe(
        startWith(''),
        map(val => this.filter(val))
      );
  }

  resetForm() {
    this.myform.resetForm();
  }
}
