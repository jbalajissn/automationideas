import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, HttpModule } from '@angular/http';
/*
Fetching Automation Details through Http Calls
*/
@Injectable()
export class AddAutomationService {
    constructor(private httpClient: HttpClient) {
    }

    // Fetch Automation Dashboard Details
    automationEntity(URL: string) {
        return this.httpClient.get<any>(URL, {});
    }

    automationTower(URL: string) {
        return this.httpClient.get<any>(URL, {});
    }

    automationApplication(URL: string) {
        return this.httpClient.get<any>(URL, {});
    }

    saveAutomation(URL: string, data: any) {
        return this.httpClient.post<any>(URL, data);
    }
}
