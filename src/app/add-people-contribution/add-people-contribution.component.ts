import { Component,Inject, OnInit,ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatTableDataSource, MatDrawerToggleResult } from '@angular/material';
import { FormGroup, FormControl, Validators, FormBuilder, FormGroupDirective, AbstractControl } from '@angular/forms';
import { HttpCOnfig } from '../utils/httpConfig';
import { FormArray } from '@angular/forms/src/model';
import { CheckUserStatus } from './../utils/checkUserStatus';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
import { AddPeopleContributionService } from './addPeopleContributionService';
import { LandingPageComponent } from './../landing-page/landing-page.component';
@Component({
  selector: 'app-add-people-contribution',
  templateUrl: './add-people-contribution.component.html',
  styleUrls: ['./add-people-contribution.component.css']
})
export class AddPeopleContributionComponent implements OnInit {
  public user:string;
  public peopleContributiondata:any;
  PeopleContributionForm: FormGroup;
  PeopledetailsForm:FormGroup; 
  P2FormGroup:FormGroup;
  dialogRef: any;
  @ViewChild(FormGroupDirective) myform;
  myFilter = (d: Date): boolean => {
    const day = d.getDay();
    const year = d.getFullYear();
    const currentDate = new Date();
    const currentyear = currentDate.getFullYear();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0 && day !== 6 && year > currentyear - 2 && year < currentyear + 2;
  }
  constructor(
    
    private _formBuilder:FormBuilder, 
    public userinfo:CheckUserStatus,
    public dialog: MatDialog ,
    private addPeopleContributionService: AddPeopleContributionService,
    private httpConfig: HttpCOnfig) {
      this.getUserInfo();
    }
    getUserInfo()
    {
      this.user=JSON.parse(this.userinfo.getUserDetails())[0].username;
    }

    
    get formArray(): AbstractControl | null { return this.PeopleContributionForm.get('formArray'); }
    onCloseCancel()
      {

        
      }

   async ngOnInit() {
   
    this.PeopleContributionForm=new FormGroup({
     formArray: this._formBuilder.array([
       this._formBuilder.group({
       TitleControl:new FormControl('', Validators.required),
       Prerequisitecntrl:new FormControl('', Validators.required),
       Descriptioncntrl:new FormControl('', Validators.required),
       trngSkillType:new FormControl('', Validators.required),
       trngType:new FormControl('', Validators.required),
     
      }) ,     
      
      this._formBuilder.group({
        //trainer:new FormControl('',Validators.required), 
        trngLocation:new FormControl('', Validators.required),
        trngMode:new FormControl('', Validators.required),
        TargetAudience:new FormControl('', Validators.required),
        TrainingStartdate: new FormControl('', Validators.required),
        trainingEnddate  :new FormControl('',Validators.required),
        Autocomments:new FormControl({value:'',disabled:false},Validators.required)
      }) 
      ])
  
});

        
        this.P2FormGroup=this._formBuilder.group({
          Autocomments:new FormControl({value:''})
              });
     let automationStatus: any = ''; 
 
   
   }
  onAddPeopleContributionButton() {
    // login as admin if login type is admin or else as user
   this.openDIalog(1);
    const frmArry: any = this.PeopleContributionForm.get('formArray') as FormArray;
    const frmArryValues: any = frmArry.value as Array<any>;
    const peopleContributionData = {
      TrainingName : frmArryValues[0].TitleControl,
      TrainingDescription: frmArryValues[0].Descriptioncntrl,
      TrainingSkillType:frmArryValues[0].trngSkillType,
      TrainingMode:frmArryValues[1].trngMode,
      TrainingType:frmArryValues[0].trngType,
      Location:frmArryValues[1].trngLocation,
      TargetAudience : frmArryValues[1].TargetAudience,
      Prerequisite : frmArryValues[0].Prerequisitecntrl,
     // Contributor:frmArryValues[1].trainer,
     Contributor:this.user,
      TrainingComments : frmArryValues[1].Autocomments,
      TrainingStartdate: frmArryValues[1].TrainingStartdate,
      TrainingEnddate:frmArryValues[1].trainingEnddate,
    
    };
    console.log(peopleContributionData);
    const peopleContributionSaveURL: string = this.httpConfig.savePeopleContributionDetails();
    this.addPeopleContributionService.savePeopleContribution(peopleContributionSaveURL, {data: peopleContributionData}).subscribe((result) => {
      if (result.status) {
        // erase form data and show success gif
        console.log(result.status);
         this.successGif();
      } else {
        console.log(result.message);
      }
    }, (error) => {
      console.log('error', error);
    });
  }
  openDIalog(code) {
    this.dialogRef = this.dialog.open(ProgressDialogComponent, {data: {code: code}});
  }

  successGif() {
    // first reset form data
    this.closeDialog();
    this.resetForm();
    this.openDIalog(2);
    setTimeout(() => {
      this.closeDialog();
    }, 2000);
  }

  resetForm() {
    this.myform.resetForm();
  }

  closeDialog() {
    this.dialogRef.close();
  }
}