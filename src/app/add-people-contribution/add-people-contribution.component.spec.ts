import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPeopleContributionComponent } from './add-people-contribution.component';

describe('AddPeopleContributionComponent', () => {
  let component: AddPeopleContributionComponent;
  let fixture: ComponentFixture<AddPeopleContributionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPeopleContributionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPeopleContributionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
