import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, HttpModule } from '@angular/http';
/*
Fetching Automation Details through Http Calls
*/
@Injectable()
export class AddPeopleContributionService {
    constructor(private httpClient: HttpClient) {
    }

   
    savePeopleContribution(URL: string, data: any) {
        return this.httpClient.post<any>(URL, data);
    }
}
