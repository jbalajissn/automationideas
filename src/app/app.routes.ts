import { LandingPageComponent } from './landing-page/landing-page.component';
import { LoginComponent } from './login/login.component';
import { IdeaLoggingComponent } from './add-ideas/idea-logging/idea-logging.component';
import { AddIdeasComponent } from './add-ideas/add-ideas.component';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { TrendinComponent } from './add-ideas/trendin/trendin.component';
import { WelcomeComponentComponent } from './welcome-component/welcome-component.component';
import { IdeasComponent } from './ideas/ideas.component';
import { AddAutomationComponent } from './add-automation/add-automation.component';
import { AutomationdashboardComponent } from './automationdashboard/automationdashboard.component';
import { ShowAutomationComponent } from './show-automation/show-automation.component';
import { AddPeopleContributionComponent } from './add-people-contribution/add-people-contribution.component';
import { ShowPeopleComponent } from './show-people/show-people.component';
import { PeopledashboardComponent } from './peopledashboard/peopledashboard.component';
import { InnovationDashboardComponent } from './innovation-dashboard/innovation-dashboard.component';
import { AddOriginateComponent } from './add-originate/add-originate.component';
import { EditOriginateComponent } from './edit-originate/edit-originate.component';
import { ShowOriginateComponent } from './show-originate/show-originate.component';

const routes: Routes = [
    {
        path: '',
        component: LandingPageComponent
    },
    {
        path: 'ideaLogging',
        component: WelcomeComponentComponent
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path:'trending',
        component: TrendinComponent
    },
    {
        path: 'ideaLogging',
        component: IdeaLoggingComponent
    },
    {
        path: 'ideas',
        component: IdeasComponent
    },
    {
        path: '404',
        component: WelcomeComponentComponent

    },
    {
        path: 'addIdeas',
        component: AddIdeasComponent
    },
    {
        path: 'addAutomation',
        component: AddAutomationComponent
    },
    {
        path: 'automationDashboard',
        component: AutomationdashboardComponent
    },
    {
        path:'peopleDashboard',
        component:PeopledashboardComponent
    },
    {
        path: 'innovationDashboard',
        component: InnovationDashboardComponent
    },
    {
        path: 'showAutomation',
        component: ShowAutomationComponent
    },
    {
        path: 'addPeopleContribution',
        component: AddPeopleContributionComponent
    },
    {
        path: 'showPeopleContribution',
        component: ShowPeopleComponent
    },
    {
        path: 'originateLogging',
        component: AddOriginateComponent
    },
    {
        path: 'originate',
        component: EditOriginateComponent
    },
    {
        path: 'originateDashboard',
        component: ShowOriginateComponent
    },
    {
        path: '**',
        redirectTo: '/404'
    }
];

export const appRouteModule = RouterModule.forRoot(routes);
