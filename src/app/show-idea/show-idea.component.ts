import { MatTableDataService } from './dataService';
import { Subject } from 'rxjs/Subject';
import { FormGroup, Validators, FormControl, FormBuilder, FormGroupDirective } from '@angular/forms';
import { ShowIdeaService } from './showIdeaService';
import { ProgressDialogComponent } from './../progress-dialog/progress-dialog.component';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatTableDataSource, MatDrawerToggleResult } from '@angular/material';
import { Component, OnInit, Inject, Injectable, ViewChild } from '@angular/core';
import { CheckUserStatus } from '../utils/checkUserStatus';
import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk/collections';
import { BuildDataSource } from './buildDataSource';
import { ProductionDataSource } from './productionDataSource';
import { AddAutomationService } from '../add-automation/addautomationservice';
import { AddAutomationComponent } from '../add-automation/add-automation.component';
import { HttpCOnfig } from '../utils/httpConfig';

@Component({
  selector: 'app-show-idea',
  templateUrl: './show-idea.component.html',
  styleUrls: ['./show-idea.component.css']
})
export class ShowIdeaComponent implements OnInit {

  public ideaData: any;
  public ideaStatus: any;
  // this is like button text
  public likeButtonString = 'Like';
  public likeButtonStatus = false;
  public isAdmin = false;
  public isLikeButtonBusy = false;
  public likeCount: number;
  public isProgressVisible = false;
  public progressText = 'Show Progress';
  public benefits: any[] = [];
  // please login dialog
  public loginDialogRef: any;

  // idea phases
  public approvalText: string;
  public isApproved: boolean = false;
  public isApprovalHistory: boolean = false;
  public isApprovalButtonBusy: boolean = false;
  public feasHistoryButtonText: string = "show history";

  // phases history details
  public approvedHistoryData: Array<any> = [];
  public phasesData: any = [];
  //Quarter dropdown
  public quartsDropdown:any[]=[];
  
  // views for phases

  // form group for approval phase
  approValForm = new FormGroup({
    comments: new FormControl('', Validators.compose([Validators.required, Validators.minLength(10)]))
  })

  /* everything about feasibility */
  public isFeasibile: boolean = false;
  public isFeasHistory: boolean = false;
  public isFeasData: boolean = false;
  public feasData: any = [];
  public feasHistoryButtonStatus: boolean = false;
  feasibilityForm = new FormGroup({
    quartsValue: new FormControl('', Validators.compose([Validators.required])),
    comments: new FormControl('', Validators.compose([Validators.required, Validators.minLength(10)])),
    quarthours: new FormControl('', Validators.compose([Validators.required, Validators.minLength(2),, Validators.maxLength(3)])),
  });

  /* everything about team status */
  public isTeamAvailable: boolean = false;
  teamForm = new FormGroup({
    team: new FormControl('', Validators.compose([Validators.required, Validators.minLength(5)]))
  })
  displayedColumns = ["name", "action"]
  demoDataSource: DemoDataSource | null;

  /* everything about build status */
  minDate = new Date();
  public isBuild: boolean = false;
  teamDisplayColumns = ["title", "date", "start date", "end date"]
  buildDataSourceTable: BuildDataSourceTable;
  buildForm = new FormGroup({
    buildTitle: new FormControl('', Validators.compose([Validators.minLength(6)])),
    builStartDate: new FormControl('', Validators.compose([Validators.required])),
    builEndDate: new FormControl('', Validators.compose([Validators.required]))
  })
  @ViewChild(FormGroupDirective) myform;

  /* everyhting about production status */
  productionDisplayColumns = ["title", "date", "start date", "end date"]
  public isProduction: boolean = false;
  productionDataSourceTable: ProductionDataSourceTable;
  productionForm = new FormGroup({
    buildTitle: new FormControl('', Validators.compose([Validators.minLength(6)])),
    builStartDate: new FormControl('', Validators.compose([Validators.required])),
    builEndDate: new FormControl('', Validators.compose([Validators.required]))
  })

  /* everything about production Benefits */
  public busBenefits: any = [];
  public isBenefits = false;
  public isEffortReduction = false;
  public businessBenefits: any = '';
  public businessbenefitsClassification: any = '';
  public businessAppreciation: any = '';
  public benefitsHours: any = '';

  businessBenefitsform = new FormGroup({
    businessBenefits: new FormControl('', Validators.compose([Validators.required, Validators.minLength(10)])),
    businessbenefitsClassification: new FormControl('', Validators.compose([Validators.required])),
    businessAppreciation: new FormControl(),
    benefitsHours: new FormControl()
  });

  constructor(
    public userStatus: CheckUserStatus,
    public showIdeaService: ShowIdeaService,
    public dialogRef: MatDialogRef<ShowIdeaComponent>,
    public formBuilder: FormBuilder,
    public matDataService: MatTableDataService,
    public buildDataSource: BuildDataSource,
    public productionDataSource: ProductionDataSource,
    public addAutomationService: AddAutomationService,
    public loginDialog: MatDialog,
    public httpConfig: HttpCOnfig,
    @Inject(MAT_DIALOG_DATA) public data: any, ) {
    this.ideaData = data;
    console.log(data);
    this.ideaStatus = this.ideaStatusValue(data);
    this.likeCount = this.ideaData.likes;
    this.likeButtonString = `Like\t${this.likeCount}`;
    // check user admin or not
    this.checkUserAdminOrNot();
  }

  ideaStatusValue(data) {
    if(data.approval_status == 0) {
      return "Approval Pending";
    }else if(data.feasibility_status == 0) {
      return "Feasibility Pending";
    }else if(data.team_status == 0) {
      return "Team Creation Phase";
    }else if(data.build_status == 0) {
      return "Build Phase";
    }else if(data.production_status == 0){
      return "Production Phase";
    }else if(data.business_impact_status == 0){
      return "Deployed & Business Impact Analysis";
    }else{
      return "Idea Implemented";
    }
  }

  async ngOnInit() {
    // intializing all the form by step
    this.demoDataSource = new DemoDataSource(this.matDataService);
    this.buildDataSourceTable = new BuildDataSourceTable(this.buildDataSource);
    this.productionDataSourceTable = new ProductionDataSourceTable(this.productionDataSource)

    let automationEntityData: any = '';
    automationEntityData = await this.FetchAutomationEntity();
    this.PopulateDropdowns(automationEntityData);

    let quarts: any = '';
    quarts = await this.FetchQuarterEntity();
    this.PopulateQuarts(quarts);
  }

  FetchAutomationEntity() {
    return new Promise((resolve, reject) => {
      const automationDetailsURL: string = this.httpConfig.getautomationEntity();
      this.addAutomationService.automationEntity(automationDetailsURL).subscribe((result) => {
        if (result.status) {
          (result.status ? resolve(result.data) : reject(result));
        }
      }, (error) => {
        reject(error);
      });
    });
  }

  PopulateDropdowns(dropdownResults: any) {
    console.log(dropdownResults);
    const serverData: any[] = [];
    dropdownResults.forEach(element => {
      serverData.push({
          item: element.ID,
          value: element.Value,
          Type: element.Type
      });
    });
    serverData.forEach(element => {
      if  (element.Type === 'BusinessBenefits') {
        this.benefits.push(element);
      }
    });
  }

  // for quarter dropdown

  FetchQuarterEntity(){
    return new Promise((resolve, reject) => {
      const automationDetailsURL: string = this.httpConfig.getQuarterData();
      this.showIdeaService.quarts(automationDetailsURL).subscribe((result) => {
        if (result.status) {
          (result.status ? resolve(result.data) : reject(result));
        }
      }, (error) => {
        reject(error);
      });
    });
  }

    PopulateQuarts(dropdownResults: any) {
    console.log(dropdownResults);
    const serverData: any[] = [];
    dropdownResults.forEach(element => {
      serverData.push({
          item: element.automationEntityID,
          value: element.automationEntityValue
      });
    });
    this.quartsDropdown = serverData;
  }
  
  
  // when user clicks on like button 
  onLike() {
    if (this.checkUserLoggedInOrNot()) {
      // first disable the button before making server call 
      this.isLikeButtonBusy = true;
      const data = { count: this.getLikeCount(), id: this.ideaData.id };
      // now update like button count on server 
      this.showIdeaService.updateLikeCount(data).subscribe((result) => {
        this.isLikeButtonBusy = false;
        if (result.status) {
          // update the button status 
          this.changeLikeStatus();
        }
      }, (error) => {
        this.isLikeButtonBusy = false;
      })

    } else {
      // open please login dialog 
      this.loginDialogRef = this.loginDialog.open(ProgressDialogComponent, { data: { code: 4, message: 'Please Login to Like!' } })
    }
  }

  // check user logged in or not 
  checkUserLoggedInOrNot(): boolean {
    return this.userStatus.isUserLoggedIn();
  }

  // changing like buttin status and text 
  changeLikeStatus() {
    if (this.likeButtonStatus) {
      this.likeButtonStatus = false;
      if (this.likeCount >= 0) this.likeCount = this.likeCount - 1;
      this.likeButtonString = `Like\t${this.likeCount}`;
    } else {
      this.likeButtonStatus = true;
      this.likeCount = this.likeCount + 1;
      this.likeButtonString = `Liked\t${this.likeCount}`;
    }
  }

  // get like button count based on that liked or un liked 
  getLikeCount(): number {
    if (!this.likeButtonStatus) return this.likeCount + 1;
    else return this.likeCount - 1;
  }


  // check user admin or not 
  checkUserAdminOrNot() {
    this.isAdmin = this.userStatus.isUserAdmin();
  }

  // when user cliks on showProgress button 
  onProgress() {
    this.getApprovalHistory();
  }

  // get phases data
  getApprovalHistory() {
    if (!this.isProgressVisible) {
      this.progressText = "Getting data.."
      const data = { id: this.ideaData.id }
      this.showIdeaService.getPhaseData(data).subscribe((result) => {
        if (result.status) {
          this.isProgressVisible = !this.isProgressVisible;
          this.phasesData = result.data;
          this.showProgressDetails();
        } else {
          console.log(result.message);
        }
      })
    } else {
      this.isProgressVisible = !this.isProgressVisible;
      this.showProgressDetails();
    }
  }

  // showing progress detais; 
  showProgressDetails() {
    this.progressText = (this.isProgressVisible ? "Hide Progress" : "Show Progress");
    if (this.isProgressVisible) this.getProgressDetails();
  }

  // getting progress details 
  getProgressDetails() {
    this.approvalText = (this.ideaData.approval_status == 0 ? "Idea has not been Approved" : "Idea has been approved");
    this.ideaData.approval_status == 0 ? this.isApproved == false : this.isApproved = true;
    this.ideaData.feasibility_status == 0 ? this.isFeasibile == false : this.isFeasibile = true;

    /* chage the color of phases text based on overdue data */
    this.phasesStatus();
  }

  // checking phases status and date 
  phasesStatus() {
    // first lets change approval text color 

  }

  // when user cliks on approve button for approval phase 
  async onApprove(status: number) {
    this.isApprovalButtonBusy = true;
    const data = {
      id: this.ideaData.id,
      comments: this.approValForm.get('comments').value,
      status: status,
      created_by: JSON.parse(this.userStatus.getUserDetails())[0].id
    }
    const result = await this.onApprovalUpdate(data).catch((error) => {
      console.log(error);
    })
    // on successfull updation of approval 
    if (result) {
      if(status == 1)
      {
        this.ideaData.approval_status = 1;
        this.isApproved = true ;
      } 
      else
      {
        this.ideaData.approval_status = 0;
        this.isApproved = false ;
      } 
      this.approvedHistoryData.push(data);
      this.approValForm.reset();
      this.getApprovalStyle();
      this.isApprovalButtonBusy = false;
    }
  }


  // get user details from local storage 
  getUserDetails(): string {
    return this.userStatus.getUserDetails();
  }

  // when user clicks on get history for approved details 
  showApprovedHistory() {
    this.isApprovalHistory = !this.isApprovalHistory;
    if (this.isApprovalHistory && this.approvedHistoryData.length == 0) {
      // get approved details history from server for selected idea id
      this.showIdeaService.getApprovalHistory({ id: this.ideaData.id }).subscribe((result) => {
        if (result.status) {
          if(result.data.count > 0){
              result.data.forEach(element => {
                
              }); 
          }
          this.approvedHistoryData = result.data;
        }
      }, (error) => {
        console.log(error)
      })
    }
  }

  // updating approval status on server 
  onApprovalUpdate(data: any) {
    return new Promise((resolve, reject) => {
      this.showIdeaService.updateApproveStatus(data).subscribe((result) => {
        (result.status ? resolve(result.data) : reject(result))
      }, (error) => {
        reject(error);
      })
    })
  }
  //getting quarterhours in feasibility form
  getQuarterHours() {
    console.log(this.ideaData.id);
    this.showIdeaService.getQuarterHours({ id: this.ideaData.id }).subscribe((result) => {
      if (result.status) {
          console.log(result);
          // this.businessBenefits = result.data[0].businessBenefits;
          this.feasibilityForm.controls['quarthours'].setValue(result.data[0].quarthours);
      } else {
        console.log(result.message);
      }
    }, ( error) => {

    });
  }


  // showing feasibility history 
  showFeassibilityHistory() {
    console.log("hello");
    this.feasHistoryButtonStatus = !this.feasHistoryButtonStatus;
    // get the feasibility history from server
      this.feasData = [];
      this.showIdeaService.getFeasibilityHistpry({ id: this.ideaData.id }).subscribe((result) => {
        if (result.status) {
          this.feasData = result.data;
          console.log(this.feasData);
          console.log(this.isFeasData);
          this.feasData.length > 0 ? this.isFeasData = true : this.isFeasData = false;
          console.log(this.isFeasData);
        } else {
          console.log(result.message);
          
        }
      }, (error) => {

      })
  }

  // when user clicks on approve/disapprove button 
  onFeasible(status) {
    this.isApprovalButtonBusy = true;
    const data = {
      id: this.ideaData.id,
      comments: this.feasibilityForm.get('comments').value,
      quarter : this.feasibilityForm.get('quartsValue').value,
      quarterhours: this. feasibilityForm.get('quarthours').value,
      status: status,
      created_by: JSON.parse(this.userStatus.getUserDetails())[0].id
    }
    this.showIdeaService.updateFeasData(data).subscribe((result) => {
      if (result.status) {
        
        if(status == 1)
        {
          this.ideaData.feasibility_status = 1;
          this.isFeasibile = true ;
        } 
        else
        {
          this.ideaData.feasibility_status = 0;
          this.isFeasibile = false ;
        } 
        this.isApprovalButtonBusy = false;
        this.feasibilityForm.reset();
        this.getFeasibilityStyle();
        this.showFeassibilityHistory();
      } else {
        this.isApprovalButtonBusy = false;
      }

    })
  }

  /*  everything about teams*/
  // delete user from team 
  deleteUser(index) {
    let updateData = this.matDataService.getTeamDataAsArray();
    updateData = updateData.filter((items) => items.name != updateData[index].name)
    let teamString = "";
    updateData.map((item) => {
      (teamString.length == 0 ? (teamString = item.name.trim()) : teamString = `${teamString},${item.name.trim()}`)
    })
    // update team data on server
    if (teamString.trim().length > 0) {
      const postData = {
        id: this.ideaData.id,
        team_memebers: teamString,
        created_by: JSON.parse(this.userStatus.getUserDetails())[0].id
      }
      this.showIdeaService.addTeamDataOnServer(postData).subscribe((result) => {
        if (result.status) {
          this.matDataService.removeUSerFromTeam(index);
        }
      })
    }
  }

  // for adding new team members 
  AddTeam() {
    let teamString: string = "";
    const tempTeamData = this.teamForm.get('team').value.trim().split(",").filter((item) => item.trim().length > 0)
    const data = tempTeamData.map((item) => {
      (teamString.length == 0 ? (teamString = item.trim()) : teamString = `${teamString},${item.trim()}`)
      return {
        name: item,
        action: ''
      }
    })
    // update team data on server
    const postData = {
      id: this.ideaData.id,
      team_memebers: teamString,
      created_by: JSON.parse(this.userStatus.getUserDetails())[0].id
    }
    this.showIdeaService.addTeamDataOnServer(postData).subscribe((result) => {
      if (result.status) {
        this.matDataService.addTeamData(data);
        this.teamForm.reset();
        this.isTeamAvailable = true;
        this.getTeamStyle();
      } else {
        console.log(result.message);
      }
    }, (error) => {
      console.log(error);
    })

  }

  // getting the team data from server for given id 
  getTeamData() {
    if (this.matDataService.getTeamDataAsArray().length == 0) {
      this.showIdeaService.getTeamData({ id: this.ideaData.id }).subscribe((result) => {
        if (result.status) {
          // now add data to table service with desired format 
          if (result.data.length > 0) {
            let teamData = [];
            result.data[0].team_memebers.split(",").map((item) => {
              teamData.push({ name: item, action: '' })
            })
            this.matDataService.addTeamData(teamData);
          }
        } else {
          console.log(result.message)
        }
      }, (error) => {
        console.log(error);
      })
    }
  }

  /* everything about build status */

  // getting the build data 
  getBuildData() {
    if (this.buildDataSource.getTeamDataAsArray().length == 0) {
      this.showIdeaService.getBuildData({ id: this.ideaData.id }).subscribe((result) => {
        console.log("hey!", result);
        if (result.status) {
          this.buildDataSource.addTeamData(result.data);
        } else {
          console.log(result.message);
        }
      }, (error) => {

      })
    }

  }

  /* everything about production */
  getProductionData() {
    if (this.productionDataSource.getTeamDataAsArray().length == 0) {
      this.showIdeaService.getProductionData({ id: this.ideaData.id }).subscribe((result) => {
        console.log("hey!!", result);
        if (result.status) {
          this.productionDataSource.addTeamData(result.data);
        } else {
          console.log(result.message);
        }
      }, (error) => {

      })
    }
  }

  getBusinessBenefitsData() {
    console.log(this.ideaData.id);
    this.showIdeaService.getBusinessBenefitsData({ id: this.ideaData.id }).subscribe((result) => {
      if (result.status ) {
        if(result.data.length > 0){
          console.log(result);
          // this.businessBenefits = result.data[0].businessBenefits;
          this.businessBenefitsform.controls['businessBenefits'].setValue(result.data[0].businessBenefits);
          this.businessBenefitsform.controls['businessAppreciation'].setValue(result.data[0].businessAppreciation);
          this.businessBenefitsform.controls['businessbenefitsClassification'].setValue(result.data[0].businessbenefitsClassification);
          this.businessBenefitsform.controls['benefitsHours'].setValue(result.data[0].benefitsHours);
        }
      } else {
        console.log(result.message);
      }
    }, ( error) => {

    });
  }
  // insert production data 
  productionFormSubmit(form) {

    // check start date is before end data 
    if (!this.compareTwoDays(form.get('builEndDate').value, form.get('builStartDate').value)) {
      this.productionForm.controls['builEndDate'].setErrors({})
      return true;
    }
    const data = {
      id: this.ideaData.id,
      title: form.get('buildTitle').value,
      start_date: new Date(form.get('builStartDate').value),
      end_date: new Date(form.get('builEndDate').value),
      created_by: JSON.parse(this.userStatus.getUserDetails())[0].id,
      created_at: new Date(),
      status: 1
    }

    console.log("data ", data);

    this.showIdeaService.insertProductionData(data).subscribe((result) => {
      if (result.status) {
        this.productionDataSource.addTeamData([data]);
        this.productionForm.reset();
        this.isProduction = true;
        this.getProductionStyle();
        this.myform.reset();
      } else {
        console.log(result.message);
      }
    }, (error) => {

    })


  }

  // on build data submit 
  buildFormSubmit(form) {
    // check start date is before end data 
    if (!this.compareTwoDays(form.get('builEndDate').value, form.get('builStartDate').value)) {
      this.buildForm.controls['builEndDate'].setErrors({})
      return true;
    }
    const data = {
      id: this.ideaData.id,
      title: form.get('buildTitle').value,
      start_date: new Date(form.get('builStartDate').value),
      end_date: new Date(form.get('builEndDate').value),
      created_by: JSON.parse(this.userStatus.getUserDetails())[0].id,
      created_at: new Date(),
      status: 1
    }

    console.log("data ", data);

    this.showIdeaService.insertBuildData(data).subscribe((result) => {
      if (result.status) {
        this.buildDataSource.addTeamData([data]);
        this.buildForm.reset();
        this.myform.reset();
        this.isBuild = true;
        this.getBuildStyle();
      } else {
        console.log(result.message);
      }
    }, (error) => {

    });

  }
  // Save Business Benefits Data

  benefitsFormSubmit(form) {
    // check start date is before end data 
    const data = {
      id: this.ideaData.id,
      businessBenefits: form.get('businessBenefits').value,
      businessbenefitsClassification: form.get('businessbenefitsClassification').value,
      benefitsHours: form.get('benefitsHours').value,
      businessAppreciation: form.get('businessAppreciation').value,
      created_by: JSON.parse(this.userStatus.getUserDetails())[0].id,
      created_dt: new Date(),
      status : 1
    };

    console.log('data : ', data);

    this.showIdeaService.saveBusinessBenefitsData(data).subscribe((result) => {
      if (result.status) {
        //this.businessBenefitsform.reset();
        this.isBenefits = true;
        this.getBenefitsStyle();
      } else {
        console.log(result.message);
      }
      
    }, (error) => {

    });

  }



  /* everything about changing phases text color */
  getApprovalStyle() {
    let style = { 'color': 'white' };
    this.ideaData.approval_status == 1 ? style.color = 'green' : style.color = 'red';
    if (this.ideaData.approvalData) {
      if (this.getDayDifference(this.ideaData.approvalData.created_at) >= 10) {
        style.color = 'orange'
      }
    }
    return style;
  }

  getFeasibilityStyle() {
    let style = { 'color': 'white' };
    this.ideaData.feasibility_status == 1 ? style.color = 'green' : style.color = 'red'
    if (this.ideaData.feasibilityData ) {
      if (this.getDayDifference(this.ideaData.feasibilityData.created_at) >= 10) {
        style.color = 'orange'
      }
    }
    return style;
  }

  getTeamStyle() {
    let style = { 'color': 'white' };
    this.ideaData.team_status == 1 ? style.color = 'green' : ((this.isTeamAvailable == true) ? style.color = 'green' : style.color = 'red');
    //this.isTeamAvailable == true ? style.color = 'green' : style.color = 'red';
    return style;
  }

  // updating build status color 
  getBuildStyle() {
    let style = { 'color': 'white' };
    this.ideaData.build_status == 1 ? style.color = 'green' : ((this.isBuild == true) ? style.color = 'green' : style.color = 'red');
    return style;
  }

  // updating production status color 
  getProductionStyle() {
    let style = { 'color': 'white' };
    this.ideaData.production_status == 1 ? style.color = 'green' : ((this.isProduction == true) ? style.color = 'green' : style.color = 'red');
    return style;
  }

  // updating production status color 
  getBenefitsStyle() {
    let style = { 'color': 'white' };
    this.ideaData.business_impact_status == 1 ? style.color = 'green' : ((this.isBenefits == true) ? style.color = 'green' : style.color = 'red');
    return style;
  }

  // get days diff 
  getDayDifference(status_date): number {
    return ((new Date().valueOf() - new Date(status_date).valueOf()) / (1000 * 60 * 60 * 24));
  }

  // check start date before or not 
  compareTwoDays(end_date, start_date) {
    const difference = Math.round(new Date(end_date).getTime() - new Date(start_date).getTime()) / ((1000 * 60 * 60 * 24));
    return difference >= 0;
  }

}



export interface Element {
  name: string;
  action: string;
}


// export interface TeamData {
//   name : string,
//   action : string;
//   date : string;
// }

// demo class for getting data stream 
export class DemoDataSource extends DataSource<any>{
  constructor(public matTableDataSource: MatTableDataService) {
    super();
  }
  connect(): Observable<any> {
    return this.matTableDataSource.data$;
  }

  disconnect() {
    this.matTableDataSource.resetData();
  }
}

/* build data source class */
export class BuildDataSourceTable extends DataSource<any>{
  constructor(public teamDataSource: BuildDataSource) {
    super();
  }
  connect(): Observable<any> {
    return this.teamDataSource.data$;
  }
  disconnect() {
    this.teamDataSource.resetData();
  }
}

/* production data source class */
export class ProductionDataSourceTable extends DataSource<any>{
  constructor(public producionDatSource: ProductionDataSource) {
    super();
  }
  connect(): Observable<any> {
    return this.producionDatSource.data$;
  }
  disconnect() {
    this.producionDatSource.resetData();
  }
}