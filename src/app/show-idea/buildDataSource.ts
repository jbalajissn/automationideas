import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class BuildDataSource{

    datService$: BehaviorSubject<any> = new BehaviorSubject<any>([]);
    data = [];
    constructor(){}
    
    // getting the data
    get data$(): Observable<any> {
        return this.datService$.asObservable();
    }

     // add team 
     addTeamData(data) {
        this.data = this.data.concat(data);
        this.datService$.next(this.data);
    }

     // getting team data as Array 
     getTeamDataAsArray () : Array<any>{
        return this.data;
    }

    // get team data 
    getTeamData() : Observable<any>{
        return this.datService$;
    }

    // reset the data 
    resetData() : void {
        this.data = [];
        this.datService$.next(this.data);
    }

}