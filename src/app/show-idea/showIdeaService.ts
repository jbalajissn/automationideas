import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { HttpCOnfig } from "../utils/httpConfig";

/*
this is for making all the idea related http calls 
*/
@Injectable()
export class ShowIdeaService {
    constructor(
        private httpClient: HttpClient,
        private httpConfig: HttpCOnfig){
    }
    quarts(URL: string) {
        return this.httpClient.post<any>(URL, {});
    }

    // updating an idea like count 
    updateLikeCount(data) {
        return this.httpClient.post<any>(this.httpConfig.getUpdateLikeURL(),data);
    }

    // getting approval history 
    getApprovalHistory(data){
        return this.httpClient.post<any>(this.httpConfig.getApprovalHistotyURL(),data);
    }

    // update approval data 
    updateApproveStatus(data){
        return this.httpClient.post<any>(this.httpConfig.updateApproveStatus(),data);
    }

    // get feasibility history 
    getFeasibilityHistpry(data){
        return this.httpClient.post<any>(this.httpConfig.getFeasiBulityHiURL(),data);
    }

    // get quarterHours
    getQuarterHours(data){
        return this.httpClient.post<any>(this.httpConfig.getquarthours(),data);
    }
    // updateFesibilityData 
    updateFeasData(data){
        return this.httpClient.post<any>(this.httpConfig.getFeasDataURL(),data);
    }

    // getting phases data 
    getPhaseData(data){
        return this.httpClient.post<any>(this.httpConfig.getPhasesDataURL(),data);
    }

    // getting team data 
    getTeamData(data){
        return this.httpClient.post<any>(this.httpConfig.getTeamDataURL(),data);
    }

    // adding team data on server 
    addTeamDataOnServer(data){
        return this.httpClient.post<any>(this.httpConfig.addTeamDataURL(),data);
    }
    
    // getting build data 
    getBuildData(data){
        return this.httpClient.post<any>(this.httpConfig.getBuilDataURL(),data);
    }
    
    // insert  build data 
    insertBuildData(data){
        return this.httpClient.post<any>(this.httpConfig.isnertBuildDataURL(),data);
    }

    // get production Data 
    getProductionData(data){
        return this.httpClient.post<any>(this.httpConfig.getProductionDataURL(),data);
    }

    // insert production data 
    insertProductionData(data){
        return this.httpClient.post<any>(this.httpConfig.insertProductionData(),data);
    }

    // getBusinessBenefitsData
    getBusinessBenefitsData(data) {
        return this.httpClient.post<any>(this.httpConfig.getBusinessBenefitData(), data);
    }

    // saveBusinessBenefisData
    saveBusinessBenefitsData(data) {
        return this.httpClient.post<any>(this.httpConfig.saveBusinessBenefitsData(), data);
    }
}