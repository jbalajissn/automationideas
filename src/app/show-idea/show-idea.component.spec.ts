import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowIdeaComponent } from './show-idea.component';

describe('ShowIdeaComponent', () => {
  let component: ShowIdeaComponent;
  let fixture: ComponentFixture<ShowIdeaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowIdeaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowIdeaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
