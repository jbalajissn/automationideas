import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpCOnfig } from './../utils/httpConfig';
import { Http, HttpModule,RequestOptions,ResponseContentType, Headers } from '@angular/http';
/*
Fetching Automation Details through Http Calls
*/
@Injectable()
export class ShowPeopleService {
    constructor(private httpClient: HttpClient) {
        
        }
        getAlltrainings(URL: string){
            return this.httpClient.get<any>(URL,{});

    }

    getPeopleDetail(URL:string,data:any) {
        console.log(URL);
        console.log(data)
        return this.httpClient.post<any>(URL,data);
    }
    RegisterUser(URL: string, data: any) {
        return this.httpClient.post<any>(URL, data);
    }
    getRegisteredTraining(URL:string,data:any){
        console.log(URL);   
        console.log("data");
        console.log(data);
       console.log(this.httpClient.post<any>(URL,data)) ;
        return this.httpClient.post<any>(URL,data);
    }
    getEnrolledUserCount(URL:string){
        console.log(URL);
        
   return this.httpClient.get<any>(URL);
    }
}