import { Component, OnInit,ViewChild } from '@angular/core';
import {DatePipe} from '@angular/common';
import { MatDialog } from '@angular/material';
import { FormControl, Validators, FormGroup, FormGroupDirective,FormBuilder } from '@angular/forms';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
import { HttpCOnfig } from '../utils/httpConfig';
import { CheckUserStatus } from '../utils/checkUserStatus';
import {ShowPeopleService} from './showPeopleService';
import {TrainingDetailsComponent} from '../training-details/training-details.component';
import { error, element } from 'protractor';

@Component({
  selector: 'app-show-people',
  templateUrl: './show-people.component.html',
  styleUrls: ['./show-people.component.css']
})
export class ShowPeopleComponent implements OnInit {
  public dialogRef: any;
  public ShowPeopleDialogRef:any;
  public Filterdata:any
  public user:string;
  public userid:string;
  public isAdmin=false;
  PeopleSearchForm:FormGroup;
  public trngMode:any='';
  public trngskill:any='';
  public trngdate:any='';
  public trainingList: Array<any> = [];
  public RegiteredList:Array<any>=[];
  public userCnt: Array<any> = [];
 
  

  constructor(public datepipe:DatePipe, private _formBuilder: FormBuilder,public userinfo:CheckUserStatus,public dialog: MatDialog,public matDialog: MatDialog,public httpConfig: HttpCOnfig,public showPeopleService:ShowPeopleService) {
    this.userid=JSON.parse(this.userinfo.getUserDetails())[0].id;
    this.getContributionList();
    this.getRegisteredTraining();
    this.getUserInfo();
    this.getUsercount();  
       
   }


 ngOnInit() {
  this.PeopleSearchForm = this._formBuilder.group({
    searchField: new FormControl(),
    modeSearch: new FormControl(),
    SkillTypeSearch: new FormControl()
  });
   
  }

  getUserInfo()
  {
    this.user=JSON.parse(this.userinfo.getUserDetails())[0].username;
    this.userid=JSON.parse(this.userinfo.getUserDetails())[0].userid;
    this.isAdmin=this.userinfo.isUserAdmin();
  }
  @ViewChild(FormGroupDirective) myform;
  
  myFilter = (d: Date): boolean => {
    const day = d.getDay();
    const year = d.getFullYear();
    const currentDate = new Date();
    const currentyear = currentDate.getFullYear();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0 && day !== 6 && year > currentyear - 2 && year < currentyear + 2;
  }

  getContributionList(){
   // this.showDialog();
      this.dialogRef = this.matDialog.open(ProgressDialogComponent,{data:{code: 1}});
      const getalltrainingList=this.httpConfig.getAllTrainings();
      console.log(getalltrainingList);
      this.showPeopleService.getAlltrainings(getalltrainingList).subscribe((result)=>{
      this.hideDialog();
      if(result.status){
        console.log(result);
        this.trainingList=result.data;
        console.log(this.trainingList); 
      }
    },(error)=>{
      console.log(error);
      this.hideDialog();
    })
  }
  getList():any{
    this.dialogRef = this.matDialog.open(ProgressDialogComponent,{data:{code: 1}});
    const getalltrainingList=this.httpConfig.getAllTrainings();
    console.log(getalltrainingList);
    this.showPeopleService.getAlltrainings(getalltrainingList).subscribe((result)=>{
      this.hideDialog();
      if(result.status){
        console.log(result);
        this.Filterdata=result.data;
        //console.log(this.trainingList);
        return `this.Filterdata`;
      }
    },(error)=>{
      console.log(error);
      this.hideDialog();
    })
  }

  onTrainingClick(i){
    this.showDialog();
    const TrainingDetail: any = this.httpConfig.getTrainingById();
    this.showPeopleService.getPeopleDetail(TrainingDetail,{id:this.trainingList[i].TrainingId}).subscribe((result)=>{
      this.hideDialog();
      if(result.status){
        console.log("Test");
        console.log(result.data[0]);
      this.ShowPeopleDialogRef=this.matDialog.open(TrainingDetailsComponent,{width:'500px',height:'570px',
        data:result.data[0]
      });
    
      }

    },(error)=>{this.hideDialog();
      console.log(error);
    })
  }
  
  showDialog() {
      this.dialogRef = this.matDialog.open(ProgressDialogComponent,{data:{code: 1}})
    }
  hideDialog() {
    this.dialogRef.close();
  }
  onModeChange(event:any){
    if(event !== -1){
    if(this.trngskill===''&&this.trngdate==='')
    {
      const getalltrainingList=this.httpConfig.getAllTrainings();
      this.showPeopleService.getAlltrainings(getalltrainingList).subscribe((result)=>{
       this.hideDialog();
        if(result.status){
          this.trainingList=result.data;
          this.trainingList=this.trainingList.filter(train=>train.TrainingMode===event);
          }
      },(error)=>{
        console.log(error);
        this.hideDialog();
      })
    }else{
      if(this.trngskill==='' && this.trngdate!==''){
        const getalltrainingList=this.httpConfig.getAllTrainings();
        this.showPeopleService.getAlltrainings(getalltrainingList).subscribe((result)=>{  
        if(result.status){
          this.trainingList=result.data;
          this.trainingList=this.trainingList.filter(train=>train.TrainingStartDate>=this.trngdate);
          this.trainingList=this.trainingList.filter(train=>train.TrainingMode===event) ;
          }
    },(error)=>{
      console.log(error);
      }); 
    }
    if(this.trngskill!=='' && this.trngdate==''){
      const getalltrainingList=this.httpConfig.getAllTrainings();
      this.showPeopleService.getAlltrainings(getalltrainingList).subscribe((result)=>{
        if(result.status){
        this.trainingList=result.data;
        this.trainingList=this.trainingList.filter((train=>train.TrainingSkillType===this.trngskill));
        this.trainingList=this.trainingList.filter((train=>train.TrainingMode===event));
        }
  },(error)=>{
    console.log(error);
    
  })
      
  }
  if(this.trngskill!==''&&this.trngdate!==''){
    const getalltrainingList=this.httpConfig.getAllTrainings();
    this.showPeopleService.getAlltrainings(getalltrainingList).subscribe((result)=>{
      if(result.status){
        console.log(result);
        this.trainingList=result.data;
        console.log(event);
        this.trainingList=this.trainingList.filter(train=>train.TrainingStartDate>this.trngdate);
        this.trainingList=this.trainingList.filter (train=>train.TrainingSkillType=this.trngskill);
        this.trainingList=this.trainingList.filter(train=>train.TrainingMode===event); 
    }
  },(error)=>{
    console.log(error);
    
  })

  }

  }
  this.trngMode=event;
    

  }}
  onTypeChange(event){
       
if(event !== -1){
    if(this.trngMode===''&&this.trngdate==='')
    {
      const getalltrainingList=this.httpConfig.getAllTrainings();
      this.showPeopleService.getAlltrainings(getalltrainingList).subscribe((result)=>{
        this.hideDialog();
        if(result.status){
          this.trainingList=result.data;
          this.trainingList=this.trainingList.filter(train=>train.TrainingSkillType===event);
          
        }
      },(error)=>{
        console.log(error);
        this.hideDialog();
      })
    }else{
      if(this.trngMode==='' && this.trngdate!==''){
      const getalltrainingList=this.httpConfig.getAllTrainings();
      this.showPeopleService.getAlltrainings(getalltrainingList).subscribe((result)=>{
      if(result.status){
         this.trainingList=result.data;
          console.log(event);
          this.trainingList=this.trainingList.filter(train=>train.TrainingStartDate>=this.trngdate);
          this.trainingList=this.trainingList.filter(train=>train.TrainingSkillType===event);
      }
    },(error)=>{
      console.log(error);
      
    })
        
    }
    if(this.trngMode!=='' && this.trngdate==''){
    const getalltrainingList=this.httpConfig.getAllTrainings();
    this.showPeopleService.getAlltrainings(getalltrainingList).subscribe((result)=>{
    if(result.status){
        console.log(result);
        this.trainingList=result.data;
        console.log(event);
        this.trainingList=this.trainingList.filter(train=>train.TrainingMode===this.trngMode) ;
        this.trainingList=this.trainingList.filter(train=>train.TrainingSkillType===event);
    }
  },(error)=>{
    console.log(error);
    
  })
      
  }
  if(this.trngMode!==''&&this.trngdate!==''){
    const getalltrainingList=this.httpConfig.getAllTrainings();
    this.showPeopleService.getAlltrainings(getalltrainingList).subscribe((result)=>{
    if(result.status){
       this.trainingList=result.data;
        console.log(event);
        this.trainingList=this.trainingList.filter(train=>train.TrainingMode===this.trngMode) ;
        this.trainingList=this.trainingList.filter(train=>train.TrainingStartDate>=this.trngdate);
        this.trainingList=this.trainingList.filter(train=>train.TrainingSkillType==event);
    }
  },(error)=>{
    console.log(error);
    
  })}
  }
  this.trngskill=event;
}
  }

 onSearchSubmit(){
    this.getContributionList();
    
     }
  resetfrm(){
    this.PeopleSearchForm.controls["searchField"].setValue("");
    this.PeopleSearchForm.controls["modeSearch"].setValue("");
    this.PeopleSearchForm.controls["SkillTypeSearch"].setValue("");
  }
  onSelectAll(event){
    this.resetfrm();
    this.getContributionList();
    
  }

  onDateChange(event){
    event=this.datepipe.transform(event,'yyyy-MM-dd')
    if(event !== -1){
      if(this.trngMode===''&&this.trngskill==='')
      {
        const getalltrainingList=this.httpConfig.getAllTrainings();
        this.showPeopleService.getAlltrainings(getalltrainingList).subscribe((result)=>{
          this.hideDialog();
          if(result.status){
            this.trainingList=result.data;
            this.trainingList=this.trainingList.filter(train=>train.TrainingStartDate>=event);
           }
        },(error)=>{
          console.log(error);
          this.hideDialog();
        })
      }else{
        if(this.trngMode==='' && this.trngskill!==''){
        const getalltrainingList=this.httpConfig.getAllTrainings();
        console.log(getalltrainingList);
        this.showPeopleService.getAlltrainings(getalltrainingList).subscribe((result)=>{
        
          if(result.status){
            console.log(result);
            this.trainingList=result.data;
            console.log(event);
            this.trainingList=this.trainingList.filter(train=>train.TrainingSkillType===this.trngskill);
            this.trainingList=this.trainingList.filter(train=>train.TrainingStartDate>=event);
        }
      },(error)=>{
        console.log(error);
        
      })
          
      }
      if(this.trngMode!=='' && this.trngskill==''){
      const getalltrainingList=this.httpConfig.getAllTrainings();
      this.showPeopleService.getAlltrainings(getalltrainingList).subscribe((result)=>{
      
        if(result.status){
          console.log(result);
          this.trainingList=result.data;
          console.log(event);
          this.trainingList=this.trainingList.filter(train=>train.TrainingMode===this.trngMode) ;
          this.trainingList=this.trainingList.filter(train=>train.TrainingStartDate>=event);
      }
    },(error)=>{
      console.log(error);
      
    })
        
    }
    if(this.trngMode!==''&&this.trngskill!==''){
      const getalltrainingList=this.httpConfig.getAllTrainings();
      this.showPeopleService.getAlltrainings(getalltrainingList).subscribe((result)=>{
       if(result.status){
          this.trainingList=result.data;
          this.trainingList=this.trainingList.filter(train=>train.TrainingMode===this.trngMode) ;
          this.trainingList=this.trainingList.filter(train=>train.TrainingSkillType===this.trngskill);
          this.trainingList=this.trainingList.filter(train=>train.TrainingStartDate>=event);
      }
    },(error)=>{
      console.log(error);
      
    })
    }
    }
    this.trngdate=event;
    }
    }

    

  onRegisterClick(i){     
    let TrainingId:any;
    let usrCount:any;
    let status:any;
    status="Registered";
    let chkenroll:any[];
    /*gets the User count registered for that Training */
    usrCount=this.userCnt.filter(x=>x.TrainingId===this.trainingList[i].TrainingId);
    if(usrCount.length>0){
      usrCount=usrCount[0].usrCount;
    }
    else{
      usrCount=0;
    }
  /*Checks whether the user is already enrolled */ 
     chkenroll=(this.RegiteredList.filter(x=>x.TrainingId===this.trainingList[i].TrainingId)); 
     if(chkenroll.length===0){
      if( usrCount<this.trainingList[i].TargetAudience){
    console.log("you are eligible for registration");
    const userEnroll={
    TrainingId:this.trainingList[i].TrainingId,
    EmployeeId:JSON.parse(this.userinfo.getUserDetails())[0].id,
    EnrollmentStatus:"Registered"
      }  
 
    const enrollmentUrl: string = this.httpConfig.TrainingEnrollment();
    console.log(userEnroll);
    this.showPeopleService.RegisterUser(enrollmentUrl, {data: userEnroll}).subscribe((result) => {
      if (result.status) {
        // erase form data and show success gif
        console.log(result.status);
        this.successGif();} else {
        console.log(result.message);
      }
    }, (error) => {
      console.log('error', error);
    });
    this.getRegisteredTraining();
     this.getUsercount();
    }
  else{
    console.log("you have been in waiting list");
  }
}
else{
  console.log("You have been already registered for this");
}
 
  
  
  
  }

  openDIalog(code) {
    this.dialogRef = this.dialog.open(ProgressDialogComponent, {data: {code: code}});
  }

  successGif() {
    // first reset form data
    this.closeDialog();
  // this.resetForm();
    this.openDIalog(2);
    setTimeout(() => {
      this.closeDialog();
    }, 2000);
  }

  resetForm() {
    this.myform.resetForm();
  }

  closeDialog() {
    this.dialogRef.close();
  }
  /*get the list of trainings registered by the specific user */
  getRegisteredTraining(){
   let id:any;
   this.userid=JSON.parse(this.userinfo.getUserDetails())[0].id;
    id=this.userid;
    const getRegisteredTrainingList=this.httpConfig.getRegisteredTraining();
    this.showPeopleService.getRegisteredTraining(getRegisteredTrainingList,{id:this.userid}).subscribe((result)=>{
    if(result.status){
        this.RegiteredList=result.data;       
      }
    },(error)=>{
      console.log(error);
      this.hideDialog();
    })
  }
  /*check and set whether the user is already enrolled*/
  checkEnrolled(i:any){
    let chckEnroll:any;
    chckEnroll=(this.RegiteredList.filter(x=>x.TrainingId===this.trainingList[i].TrainingId));
    if(chckEnroll.length===0){
      return false;
    }
    else{
      return true;
    }
  }
  /*get the  enrolled usercount of all trainings*/
  getUsercount(){
    const userCountUrl=this.httpConfig.getEnrolledUserCount();
    this.showPeopleService.getEnrolledUserCount(userCountUrl).subscribe((result) => {
     if(result.status){
      this.userCnt=result.data;
      } else {
       console.log(result.message);
     }
   }, (error) => {
     console.log('error', error);
   });
    
  }

}
