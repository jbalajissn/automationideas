import { Component, OnInit, Inject, Injectable } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import {MatTableModule, MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-automationdashdialog',
  templateUrl: './automationdashdialog.component.html',
  styleUrls: ['./automationdashdialog.component.css']
})
export class AutomationdashdialogComponent implements OnInit {

  private selectTowerWisedata: any;
  private selectedType: any;
  private automationdata: AutomationDetails[] = [];
  automationDatasource: any;
  displayedColumns: any;

  constructor(
    private dialogRef: MatDialogRef<AutomationdashdialogComponent>,
    private loginDialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) private data: any) {
     this.selectTowerWisedata = data.detailsData;
     this.selectedType = data.type;
   }

  ngOnInit() {
    this.BuildDialog(this.selectTowerWisedata);
  }

  BuildDialog(data: any) {
    data.forEach(element => {
      this.automationdata.push({
        TOWER: element.tower,
        APPLICATION: element.application,
        TITLE: element.title,
        STATUS: element.status,
        WORKTYPE: element.WorkType,
        BENEFITAREA: element.BenefitArea,
        TOOL: element.Tool,
        INITIATIVE: element.Initiative
      });
    });
    console.log(this.automationdata);
    this.displayedColumns = ['TOWER', 'APPLICATION', 'TITLE', 'INITIATIVE', 'TOOL'];
    this.automationDatasource = new MatTableDataSource<AutomationDetails>(this.automationdata);
  }
}

export interface AutomationDetails {
  TOWER: string;
  APPLICATION: string;
  TITLE: string;
  STATUS: string;
  WORKTYPE: string;
  BENEFITAREA: string;
  TOOL: string;
  INITIATIVE: string;
}
