import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomationdashdialogComponent } from './automationdashdialog.component';

describe('AutomationdashdialogComponent', () => {
  let component: AutomationdashdialogComponent;
  let fixture: ComponentFixture<AutomationdashdialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutomationdashdialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomationdashdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
