import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpCOnfig } from './../utils/httpConfig';
import { Http, HttpModule,RequestOptions,ResponseContentType, Headers } from '@angular/http';
/*
Fetching Automation Details through Http Calls
*/
@Injectable()
export class ShowAutomatonService {
    constructor(private httpClient: HttpClient) {
    }

    // Fetch Automation Dashboard Details
    getAllAutomation(URL: string) {
        return this.httpClient.get<any>(URL, {});
    }

    getSearchAutomation(URL: string, data: any) {
        return this.httpClient.post<any>(URL, data);
    }
    getAutomationDetail(URL:string,data:any) {
        console.log(data)
        return this.httpClient.post<any>(URL,data);
    }
}
