import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowAutomationComponent } from './show-automation.component';

describe('ShowAutomationComponent', () => {
  let component: ShowAutomationComponent;
  let fixture: ComponentFixture<ShowAutomationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowAutomationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowAutomationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
