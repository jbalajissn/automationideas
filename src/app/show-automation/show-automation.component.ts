import { Component, OnInit } from '@angular/core';
import { ShowAutomatonService  } from './showAutomationService';
import { MatDialog , MatSlider } from '@angular/material';
import { FormControl, Validators, FormGroup, FormGroupDirective } from '@angular/forms';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
import { HttpCOnfig } from '../utils/httpConfig';
import { CheckUserStatus } from '../utils/checkUserStatus';
import {AutomationDetailsComponent} from '../automation-details/automation-details.component';
import { error } from 'protractor';
import { TS } from 'typescript-linq';
@Component({
  selector: 'app-show-automation',
  templateUrl: './show-automation.component.html',
  styleUrls: ['./show-automation.component.css']
})
export class ShowAutomationComponent implements OnInit {
  public dialogRef: any;
  public showAutomationDialogRef: any;
  public searchSelection = '';
  public searchparameters: any =
  [   {name: 'automationTower', value: 'Tower'},
      {name: 'automationApplication', value: 'Application'},
      {name: 'automationTitle', value: 'Title'},
      {name: 'STATUS', value: 'Status'} ];
  public automationlist: Array<any> = [];
  public automationlistInit: Array<any> = [];
  public isAdmin = false;
  searchAutomationForm = new FormGroup({
    searchby: new FormControl('', Validators.required),
    searchField: new FormControl('', Validators.required)
  });

  constructor(
    public userStatus: CheckUserStatus,
    public showAutomationService: ShowAutomatonService,
    public matDialog: MatDialog,
  public httpConfig: HttpCOnfig) {
    this.checkUserAdminOrNot();
    this.getAutomationList();
  }

  ngOnInit() {
  }

  // check user admin or not 
  checkUserAdminOrNot() {
    this.isAdmin = this.userStatus.isUserAdmin();
  }

  getAutomationList() {
    this.showDialog();
    const automationGetALL: string = this.httpConfig.getAllAutomationDetails();
    this.showAutomationService.getAllAutomation(automationGetALL).subscribe((result) => {
      this.hideDialog();
      console.log(result);
      if (result.status) {
        console.log(result);
        this.automationlist = [];
        this.automationlist = result.data;
        this.automationlistInit = [];
        this.automationlistInit = this.automationlist;
      }
    }, (error) => {
      console.log(error);
      this.hideDialog();
    });
  }

  // showing the dialog
  showDialog() {
    this.dialogRef = this.matDialog.open(ProgressDialogComponent,{data:{code: 1}})
  }

  // hiding the dialog
  hideDialog() {
    this.dialogRef.close();
  }

 getimgsrc(status: string) {
   if(status=="Completed") {
   return ('../../assets/completed.png')
  }
   if(status=="New")
   return ('../../assets/new.png')
   if(status=="Pending Approval")
   return ('../../assets/imageedit_13_8227496638.png')
   if(status=="Cancelled")
   return ('../../assets/cancel.png')
   if(status=="In Progress")
   return ('../../assets/inprogress.png')
 }

 OnSearchByChange(event: any) {
   console.log(event);
  if (event !== -1) {
    this.searchSelection = event;
    console.log(this.searchSelection);
  } else {
    this.searchSelection = 'automationTitle';
  }
}

OnTimeByChange(event: any) {
  let automationlistTime: Array<any> = [];
  if (event.value > 0){
  let additiondays = 30;
  additiondays = additiondays * event.value;
  const dtDate = new Date();
  const dtTd = new Date();
  const eventmonthTime = new Date(dtTd.setDate(dtDate.getDate() + additiondays));
  this.automationlistInit.forEach(element => {
    const eventEndTime = new Date(element.automationActualCompletionDate);
    if (eventEndTime > dtDate && eventEndTime < eventmonthTime) {
      automationlistTime.push(element);
    }
  });
  } else {
    automationlistTime = this.automationlistInit;
  }
  if (automationlistTime.length > 0) {
  this.automationlist = [];
  this.automationlist = automationlistTime;
  } else {
    this.dialogRef = this.matDialog.open(ProgressDialogComponent,
      {data: {code: 6, message: 'Your Search did not return any match'}});
  }
}

OnTimeByCompletedChange(event: any) {
  let automationlistTime: Array<any> = [];
  if (event.value > 0) {
  let additiondays = -30;
  additiondays = additiondays * event.value;
  const dtDate = new Date();
  const dtTd = new Date();
  const eventmonthTime = new Date(dtTd.setDate(dtDate.getDate() + additiondays));
  this.automationlistInit.forEach(element => {
    const eventEndTime = new Date(element.automationActualCompletionDate);
    if (eventEndTime < dtDate && eventEndTime > eventmonthTime) {
      automationlistTime.push(element);
    }
  });
  } else {
    automationlistTime = this.automationlistInit;
  }
  if (automationlistTime.length > 0) {
  this.automationlist = [];
  this.automationlist = automationlistTime;
  } else {
    this.dialogRef = this.matDialog.open(ProgressDialogComponent,
      {data: {code: 6, message: 'Your Search did not return any match'}});
  }
}
  onSearchSubmit() {
    this.showDialog();
    let strStringFetch = this.searchAutomationForm.get('searchField').value;
    strStringFetch = '%' + strStringFetch + '%';
    console.log(strStringFetch);
    const automationGetALL: string = this.httpConfig.getSearchAutomation();
    this.showAutomationService.getSearchAutomation(automationGetALL, {strSearch : strStringFetch,
      strfield : this.searchSelection}).subscribe((result) => {
      this.hideDialog();
      if (result.status) {
        console.log(result);
        if (result.data.length > 0) {
          this.automationlist = [];
          this.automationlist = result.data;
          this.automationlistInit = [];
          this.automationlistInit = this.automationlist;
        } else {
          this.dialogRef = this.matDialog.open(ProgressDialogComponent,
            {data: {code: 6, message: 'Your Search did not return any match'}});
        }
      }
    }, (error) => {
      this.hideDialog();
      console.log(error);
    });
  }

  onAutomationClick (i) {
    // showing idea in dialog
    this.showDialog();
    const automationdetail: any = this.httpConfig.getAutomationDetail();
    console.log(this.automationlist[i].automationID);
    this.showAutomationService.getAutomationDetail(automationdetail,
      {id: this.automationlist[i].automationID}).subscribe((result) => {
      this.hideDialog();
      if (result.status) {
        console.log(result.data[0]);
      this.showAutomationDialogRef = this.matDialog.open(AutomationDetailsComponent, {
        data: result.data[0]
      });
      }
    }, (error) => {
      this.hideDialog();
      console.log(error);
    });
    // get id of selected idea
    // getting likes count
    console.log(i);
  }
}
