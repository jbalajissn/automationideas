import { ShowIdeaService } from './show-idea/showIdeaService';
import { NavService } from './utils/navServices';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatSelectModule} from '@angular/material/select';
import {MatSliderModule} from '@angular/material/slider';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { appRouteModule } from './app.routes';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { HttpModule } from '@angular/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { TS } from 'typescript-linq';
// material imports
import {
  MatButtonModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatSidenavModule,
  MatListModule,
  MatInputModule,
  MatTabsModule,
  MatDialogModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatStepperModule,
  MatTooltipModule,
  MatTableModule,
  MatDatepickerModule,
  MatCheckboxModule,
  MatNativeDateModule
} from '@angular/material';

import { SideNavComponent } from './side-nav/side-nav.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AddIdeasComponent } from './add-ideas/add-ideas.component';

import {NgxChartsModule} from '@swimlane/ngx-charts';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { IdeaLoggingComponent } from './add-ideas/idea-logging/idea-logging.component';
import { IdeaByMonthComponent } from './add-ideas/idea-by-month/idea-by-month.component';
import { TrendinComponent } from './add-ideas/trendin/trendin.component';
import { DemoIdeaLogginComponent } from './demo-idea-loggin/demo-idea-loggin.component';
import { HttpCOnfig } from './utils/httpConfig';
import { IdeasLogginService } from './add-ideas/idea-logging/idea-logging-service';
import { DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ProgressDialogComponent } from './progress-dialog/progress-dialog.component';
import { UpdatedToolBarComponent } from './updated-tool-bar/updated-tool-bar.component';
import { WelcomeComponentComponent } from './welcome-component/welcome-component.component';
import { IdeasComponent } from './ideas/ideas.component';
import { IdeasServices } from './ideas/ideas-services';
import { ImageSliderComponent } from './image-slider/image-slider.component';
import { NgxCarouselModule } from 'ngx-carousel';
import 'hammerjs';
import { ShowIdeaComponent} from './show-idea/show-idea.component';
import { CheckUserStatus } from './utils/checkUserStatus';
import { LoginComponent } from './login/login.component';
import { LoginService } from './login/loginServices';
import { MatTableDataService } from './show-idea/dataService';
import { DashboardDialogComponent } from './dashboard-dialog/dashboard-dialog.component';
import { BuildDataSource } from './show-idea/buildDataSource';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { ProductionDataSource } from './show-idea/productionDataSource';
import { ExcelService } from './utils/excelService';
import { AddAutomationComponent } from './add-automation/add-automation.component';
import { AutomationdashboardComponent } from './automationdashboard/automationdashboard.component';
import { AutomationService } from './automationdashboard/automationService';
import { AutomationdashdialogComponent } from './automationdashdialog/automationdashdialog.component';
import { AddAutomationService } from './add-automation/addautomationservice';
import { AddOriginateService } from './add-originate/addoriginate';
import { OriginateService } from './show-originate/originateDetails';
import { AutomationDetailService } from './automation-details/automationdetailservice';
import { ShowAutomationComponent } from './show-automation/show-automation.component';
import { ShowAutomatonService } from './show-automation/showAutomationService';
import { ShowPeopleService } from './show-people/showPeopleService';
import {DashBoardService} from './dashboard-dialog/dashboardService';
import {innovationDashBoardService} from './innovation-dashboard/innovationDashboardService';
import { AddPeopleContributionComponent } from './add-people-contribution/add-people-contribution.component';
import { AutomationDetailsComponent } from './automation-details/automation-details.component';
import { AddPeopleContributionService } from './add-people-contribution/addPeopleContributionService';
import { InnovationDashboardComponent } from './innovation-dashboard/innovation-dashboard.component';
import { ShowPeopleComponent } from './show-people/show-people.component';
import { TrainingDetailsComponent } from './training-details/training-details.component';
import {TrainingDetailService} from './training-details/trainingDetailService';
import { PeopledashboardComponent } from './peopledashboard/peopledashboard.component';
import {PeopleDashboardService} from './peopledashboard/peopleDashBoardService';
import { AddOriginateComponent } from './add-originate/add-originate.component';
import { EditOriginateComponent } from './edit-originate/edit-originate.component';
import { ShowOriginateComponent } from './show-originate/show-originate.component';
import { OriginatedashdialogComponent } from './originatedashdialog/originatedashdialog.component';
import { OriginateDetailsComponent } from './originate-details/originate-details.component';
import { AutomationhistorydialogComponent } from './automationhistorydialog/automationhistorydialog.component';
import { OriginateCommentsViewComponent } from './originate-comments-view/originate-comments-view.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ToolbarComponent,
    SideNavComponent,
    NotFoundComponent,
    AddIdeasComponent,
    TrendinComponent,
    LandingPageComponent,
    IdeaLoggingComponent,
    IdeaByMonthComponent,
    DemoIdeaLogginComponent,
    ProgressDialogComponent,
    UpdatedToolBarComponent,
    WelcomeComponentComponent,
    IdeasComponent,
    ImageSliderComponent,
    ShowIdeaComponent,
    LoginComponent,
    DashboardDialogComponent,
    AddAutomationComponent,
    AutomationdashboardComponent,
    AutomationdashdialogComponent,
    ShowAutomationComponent,
    AddPeopleContributionComponent,
    AutomationDetailsComponent,
    InnovationDashboardComponent,
    ShowPeopleComponent,
    TrainingDetailsComponent,
    PeopledashboardComponent,
    AddOriginateComponent,
    EditOriginateComponent,
    ShowOriginateComponent,
    OriginatedashdialogComponent,
    OriginateDetailsComponent,
    AutomationhistorydialogComponent,
    OriginateCommentsViewComponent
  ],
  imports: [
    BrowserModule,
    appRouteModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatMenuModule,
    MatTableModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatSidenavModule,
    MatListModule,
    MatInputModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    NgxChartsModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatDialogModule,
    MatTabsModule,
    MatRadioModule,
    HttpClientModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    NgxCarouselModule,
    MatStepperModule,
    MatDatepickerModule,
    MatSelectModule,
    MatSliderModule,
    MatNativeDateModule
  ],
  entryComponents: [ProgressDialogComponent, ShowIdeaComponent,
    DashboardDialogComponent, AutomationdashdialogComponent, AutomationDetailsComponent,OriginatedashdialogComponent,
      TrainingDetailsComponent , OriginateDetailsComponent, AutomationhistorydialogComponent, OriginateCommentsViewComponent],
  providers: [ DatePipe, NavService, HttpCOnfig, IdeasLogginService, HttpClientModule, IdeasServices, AutomationService,
              CheckUserStatus, LoginService, ShowIdeaService, MatTableDataService, OriginateService,
              BuildDataSource, ProductionDataSource, ExcelService, AddAutomationService,
              ShowAutomatonService, AutomationDetailService, TrainingDetailService, AddOriginateService,
              AddPeopleContributionService, DashBoardService, innovationDashBoardService,
              ShowPeopleService, PeopleDashboardService],
  bootstrap: [AppComponent]
})
export class AppModule { }