import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoIdeaLogginComponent } from './demo-idea-loggin.component';

describe('DemoIdeaLogginComponent', () => {
  let component: DemoIdeaLogginComponent;
  let fixture: ComponentFixture<DemoIdeaLogginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoIdeaLogginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoIdeaLogginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
