import { Component, OnInit, Inject, ViewChild, OnChanges } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatTableDataSource, MatDrawerToggleResult } from '@angular/material';
import { FormControl, Validators, FormGroup, FormGroupDirective, FormBuilder, FormArray, 
  FormControlName, AbstractControl } from '@angular/forms';
import {ShowAutomationComponent} from '../show-automation/show-automation.component';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
import { AutomationhistorydialogComponent } from '../automationhistorydialog/automationhistorydialog.component';
import { DISABLED } from '@angular/forms/src/model';
import { CheckUserStatus } from './../utils/checkUserStatus';
import {AddAutomationService} from '../add-automation/addautomationservice';
import { HttpCOnfig } from '../utils/httpConfig';
import {AutomationDetailService} from './automationdetailservice';
import { isNull } from 'util';

import { error } from 'protractor';
@Component({
  selector: 'app-automation-details',
  templateUrl: './automation-details.component.html',
  styleUrls: ['./automation-details.component.css']
})
export class AutomationDetailsComponent implements OnInit,OnChanges {
  public automationdata:any;
  public isAdmin=false;
  public createUser=false;
  public cntrlenable=false;
  public btnenable:boolean;
  public user:string;
  public automateEntityService:AddAutomationService;
  statval:string;
  dialogRef: any;
  automationStatus: any[]=[];
  workTypes: any[] = [];
  benefitAreas: any[] = [];
  initiativeCat: any[] = [];
  methodologies: any[] = [];
  AutomationForm: FormGroup;
  automationBasicFormGroup: FormGroup;
  automationDetailFormGroup: FormGroup;

  constructor(
    private _formBuilder:FormBuilder,
    public userinfo:CheckUserStatus,
    public dialog: MatDialog ,
    private httpConfig: HttpCOnfig, 
    private automationdetailservice:AutomationDetailService, 
    @Inject(MAT_DIALOG_DATA) public data: any ) {
      this.getUserInfo();
      this.automationdata = data;
      this.checkuser();
      //this.statval=data.autoStatus;
      console.log(data);
    }
    get formArray(): AbstractControl | null { return this.AutomationForm.get('formArray'); }
    ngOnChanges(){
     
     }
     
     getUserInfo()
    {
      this.user=JSON.parse(this.userinfo.getUserDetails())[0].username;
    }
  /* rebuildForm()
     {
       this.AutomationForm.controls.['Autocomments'].valueChanges.subscribe((value) => {
        console.log(value);      
       });
      
     }
     */
    

    myFilter = (d: Date): boolean => {
      const day = d.getDay();
      const year = d.getFullYear();
      const currentDate = new Date();
      const currentyear = currentDate.getFullYear();
      // Prevent Saturday and Sunday from being selected.
      return day !== 0 && day !== 6 && year > currentyear - 2 && year < currentyear + 2;
    }

    async ngOnInit() {
   
   this.AutomationForm=new FormGroup({
    formArray: this._formBuilder.array([
      this._formBuilder.group({
      TowerControl:new FormControl({value:this.automationdata.automationTower,disabled:true}),
      AutoAppcntrl:new FormControl({value:this.automationdata.automationApplication,disabled:true}),
      AutoTitlecntrl:new FormControl({value:this.automationdata.automationTitle,disabled:true}),
      AutoDescriptioncntrl:new FormControl({value:this.automationdata.automationDescription,disabled:true}),
      automationStatus:new FormControl({value:this.automationdata.automationStatus}, Validators.required),
      AutoLeadCCntrl:new FormControl({value:this.automationdata.automationLead,disabled:!this.cntrlenable},Validators.required),
      AutoResourceCCntrl:new FormControl({value:this.automationdata.automationResource,disabled:!this.cntrlenable},Validators.required),
      Automationdatecntrl:new FormControl({value:this.automationdata.automationDate,disabled:true}),
      Automationexpecteddatecntrl:new FormControl({value:this.automationdata.CompletionDate,disabled:!this.cntrlenable},Validators.required),
     // Autocreatedatecntrl:new FormControl({value:'',disabled:true}),
      Autoownercntrl:new FormControl({value:this.automationdata.automationCreatedBy,disabled:true})}) ,     
          this._formBuilder.group({
            effort: new FormControl({value:this.automationdata.automationEfforts,disabled:!this.cntrlenable}, Validators.compose([Validators.minLength(1), Validators.maxLength(4),
              Validators.min(1), Validators.max(9999)])),
            implementation: new FormControl({value:this.automationdata.automationImplementation,disabled:!this.cntrlenable}, Validators.compose([Validators.minLength(1), Validators.maxLength(4),
              Validators.min(1), Validators.max(9999)])),
              ticket: new FormControl({value:this.automationdata.automationticket,disabled:!this.cntrlenable},null),
              FTE: new FormControl({value:this.automationdata.automationFTE,disabled:!this.cntrlenable}, Validators.required),
              costperday: new FormControl({value:this.automationdata.automationcostperday,disabled:!this.cntrlenable}, Validators.required),
              netcostsaving: new FormControl({value:this.automationdata.automationnetcostsaving,disabled:!this.cntrlenable}, Validators.required),
      Autocomments:new FormControl({value:this.automationdata.automationComments,disabled:!this.cntrlenable},Validators.required),
      
    
    // Automationdatecntrl:new FormControl((new Date()).toISOString()),
      methodology:new FormControl({value:this.automationdata.automationTool,disabled:!this.cntrlenable},Validators.required),
      initiative:new FormControl({value:this.automationdata.automationInitiativeCat,disabled:!this.cntrlenable},Validators.required),
      benefitArea:new FormControl({value:this.automationdata.automationBenefitArea,disabled:!this.cntrlenable},Validators.required),
     
      workType: new FormControl({value:this.automationdata.automationWorkType}, Validators.required),
       })]
          )
      
    });
      this.automationBasicFormGroup = this._formBuilder.group({
        TowerControl:new FormControl({value:'',disabled:true}),
        AutoAppcntrl:new FormControl({value:'',disabled:true}),
        AutoTitlecntrl:new FormControl({value:'',disabled:true}),
        AutoDescriptioncntrl:new FormControl({value:'',disabled:true}),
        Autocreatedatecntrl:new FormControl({value:'',disabled:true}),
        AutoLeadCCntrl:new FormControl({value:'',disabled:!this.cntrlenable}),
        AutoResourceCCntrl:new FormControl({value:'',disabled:!this.cntrlenable}),
      Automationdatecntrl:new FormControl({value:'',disabled:!this.cntrlenable}),
      Automationexpecteddatecntrl:new FormControl({value:'',disabled:!this.cntrlenable}),
        Autoownercntrl:new FormControl({value:'',disabled:true})});
    
    this.automationDetailFormGroup=  this._formBuilder.group({
    //  effort: new FormControl('', Validators.compose([Validators.minLength(2), Validators.maxLength(4),
      //  Validators.min(10), Validators.max(9999)])),
        // implementation: new FormControl('', Validators.compose([Validators.minLength(2), Validators.maxLength(4),
        // Validators.min(10), Validators.max(9999)])),
      automationDetailFormGroup:new FormControl({value:''}),
      
      workType: new FormControl('', Validators.required),
      methodology:new FormControl('', Validators.required),
      initiative:new FormControl('', Validators.required),
      benefitArea:new FormControl('', Validators.required)
      

    });
    let automationStatus: any = '';
    automationStatus = await this.FetchAutomationStatus();
    this.PopulateStatus(automationStatus);
    let automationEntityData: any = '';
    automationEntityData = await this.FetchAutomationEntity();
    console.log("Entity");
    console.log(automationEntityData);

    this.PopulateDropdowns(automationEntityData);

  
  }
  FetchAutomationEntity() {
    return new Promise((resolve, reject) => {
      const automationDetailsURL: string = this.httpConfig.getautomationEntity();
      console.log(automationDetailsURL);
      this.automationdetailservice.automationEntity(automationDetailsURL).subscribe((result) => {
        if (result.status) {
          (result.status ? resolve(result.data) : reject(result));
        }
      }, (error) => {
        reject(error);
      });
    });
  }

  PopulateDropdowns(dropdownResults: any) {
    console.log(dropdownResults);
    const serverData: any[] = [];
    dropdownResults.forEach(element => {
      serverData.push({
          item: element.ID,
          value: element.Value,
          Type: element.Type
      });
    });
    serverData.forEach(element => {
      if  (element.Type === 'automationWorkType') {
        this.workTypes.push(element);
      }
      if  (element.Type === 'automationInitiativeCat') {
        this.initiativeCat.push(element);
      }
      if  (element.Type === 'automationTool') {
        this.methodologies.push(element);
      }
      if  (element.Type === 'automationBenefitArea') {
        this.benefitAreas.push(element);
      }
    });

  }

  FetchAutomationStatus() {
    return new Promise((resolve, reject) => {
      const automationDetailsURL: string = this.httpConfig.getAutomationStatus();
      this.automationdetailservice.automationStatus(automationDetailsURL).subscribe((result) => {
        if (result.status) {
          (result.status ? resolve(result.data) : reject(result));
        }
      }, (error) => {
        reject(error);
      });
    });
  }

  PopulateStatus(dropdownResults: any) {
    console.log(dropdownResults);
    const serverData: any[] = [];
    dropdownResults.forEach(element => {
      serverData.push({
          item: element.ID,
          value: element.Value
      });
    });
    this.automationStatus = serverData;

  }

  onSaveAutomationButton(){
    this.openDIalog(1);
    const frmArry: any = this.AutomationForm.get('formArray') as FormArray;
    console.log(frmArry);
    const frmArryValues: any = frmArry.value as Array<any>;
    const automationupdatedData = {
      comment:frmArryValues[1].Autocomments,
      status:frmArryValues[0].automationStatus,
      lead:frmArryValues[0].AutoLeadCCntrl,
      resource:frmArryValues[0].AutoResourceCCntrl,
     creationdate:frmArryValues[0].Automationdatecntrl,
     completiondate:frmArryValues[0].Automationexpecteddatecntrl,
      automationWorkType : frmArryValues[1].workType,
      automationBenefitArea: frmArryValues[1].benefitArea,
      automationInitiativeCat: frmArryValues[1].initiative,
      automationTool: frmArryValues[1].methodology,
      automationEffort:frmArryValues[1].effort,
      automationImplementation:frmArryValues[1].implementation,
      automationFTE:frmArryValues[1].FTE,
      automationcostperday: frmArryValues[1].costperday,
      automationnetcostsaving : frmArryValues[1].netcostsaving,
      automationticket : isNull(frmArryValues[1].ticket) ? 0 :  frmArryValues[1].ticket,
      automationModifiedUser:this.user,
      id:this.automationdata.automationID
    };
    console.log(automationupdatedData);
    const automationsubmitUrl=this.httpConfig.updateAutomation();
    console.log(automationsubmitUrl);
    this.automationdetailservice.updateAutomation(automationsubmitUrl,{data:automationupdatedData}).subscribe((result)=>{
      if(result.status)
      {
        console.log(result.status);
        this.successGif();
      }else{
        console.log(result.message);
      }
    },error=>{
      console.log('error',error);
    })
  }

  OnShowHistory() {
    this.showDialog();
    const automationdetail: any = this.httpConfig.ViewAutomationCommentsHistor();
    this.automationdetailservice.getAutomationHistory(automationdetail,
      {id: this.automationdata.automationID}).subscribe((result) => {
      this.hideDialog();
      if (result.status) {
        console.log(result.data);
        if(result.data.length > 0) {
      this.dialogRef = this.dialog.open(AutomationhistorydialogComponent, {
        data: result.data , panelClass: 'show-automation'
      });
    } else {
        this.dialogRef = this.dialog.open(ProgressDialogComponent,
          {data: {code: 7, message: 'No Comments History Available'}});
    }
      }
    }, (error) => {
      this.hideDialog();
      console.log(error);
    });
  }

  showDialog() {
    this.dialogRef = this.dialog.open(ProgressDialogComponent, {data: {code: 1}});
  }

  hideDialog() {
    this.dialogRef.close();
  }

  openDIalog(code) {
    this.dialogRef = this.dialog.open(ProgressDialogComponent, {data: {code: code}});
  }
  successGif() {
    // first reset form data
    this.closeDialog();
    this.openDIalog(2);
    setTimeout(() => {
      this.closeDialog();
    }, 2000);
  }

  closeDialog() {
    this.dialogRef.close();
  }

  checkuser(){
    this.isAdmin=this.userinfo.isUserAdmin();
    if(this.automationdata.automationCreatedBy==this.user)
    {
      this.createUser=true;
    }
    this.cntrlenable=this.isAdmin||this.createUser;
    //this.cntrlenable=true;
    console.log(this.isAdmin);
    console.log(this.createUser);
    console.log(this.user);
    console.log(this.cntrlenable);

  }

  getDate(date):any{
    if (date) {
        let dateOld: Date = new Date(date);
        return dateOld;
  }

}
checkform(){
  if(!this.AutomationForm.dirty && !this.AutomationForm.valid){
    this.btnenable=true;
  }
  else{
    this.btnenable=false;
  }
}
}
