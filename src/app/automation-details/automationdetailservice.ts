import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, HttpModule } from '@angular/http';
/*
Fetching Automation Details through Http Calls
*/
@Injectable()
export class AutomationDetailService {
    constructor(private httpClient: HttpClient) {
    }
    automationStatus(URL: string) {
        return this.httpClient.get<any>(URL, {});
    }
    automationEntity(URL: string) {
        return this.httpClient.get<any>(URL, {});
    }
    updateAutomation(URL: string, data: any) {
        console.log('Hello',URL);
        console.log('Hello',data);
        return this.httpClient.post<any>(URL, data);
    }
    getAutomationHistory(URL: string, id:any) {
        return this.httpClient.post<any>(URL, id);
    }
}
