import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OriginateDetailsComponent } from './originate-details.component';

describe('OriginateDetailsComponent', () => {
  let component: OriginateDetailsComponent;
  let fixture: ComponentFixture<OriginateDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OriginateDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OriginateDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
