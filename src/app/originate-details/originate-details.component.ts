import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CheckUserStatus } from './../utils/checkUserStatus';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatTableDataSource, MatDrawerToggleResult } from '@angular/material';
import { FormGroup, FormControl, Validators, FormBuilder, FormGroupDirective, AbstractControl } from '@angular/forms';
import { HttpCOnfig } from '../utils/httpConfig';
import { FormArray } from '@angular/forms/src/model';
import { forEach } from '@angular/router/src/utils/collection';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
import { AddOriginateService } from '../add-originate/addoriginate';
import { isNull } from 'util';

@Component({
  selector: 'app-originate-details',
  templateUrl: './originate-details.component.html',
  styleUrls: ['./originate-details.component.css']
})
export class OriginateDetailsComponent implements OnInit {
  public user: string;
  public originateData: any;
  originateTowerType: any[] = [];
  originateTechonologyType: any[] = [];
  originateApprover: any[] = [];
  originateCategoryType: any[] = [];
  originateStatusType: any[] = [];
  dialogRef: any;
  OriginateForm: FormGroup;

  @ViewChild(FormGroupDirective) myform;
  get formArray(): AbstractControl | null { return this.OriginateForm.get('formArray'); }
  // OriginateForm

   constructor(private _formBuilder: FormBuilder,
    private httpConfig: HttpCOnfig,
    public dialog: MatDialog ,
    public userinfo: CheckUserStatus,
    public addOriginateService: AddOriginateService,
    public router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any ) {
      this.getUserInfo();
      console.log(data);
      this.originateData = data;
    }

    getUserInfo() {
      if (isNull(JSON.parse(this.userinfo.getUserDetails()))) {
        this.router.navigateByUrl('/login');
      } else {
      this.user = JSON.parse(this.userinfo.getUserDetails())[0].username;
      }
    }

    async ngOnInit() {
      this.OriginateForm = this._formBuilder.group({
        formArray: this._formBuilder.array([
          this._formBuilder.group({
          originateTitle: new FormControl({value: this.originateData.OriginateTitle , disabled : true},
            Validators.compose([Validators.minLength(15)])),
          originateDescription: new FormControl({ value: this.originateData.OriginateDesc, disabled : false}
             , Validators.compose([Validators.minLength(20)])),
          originateTower: new FormControl({value: this.originateData.OriginateTower , disabled : true } 
            , Validators.required),
          originateCategory: new FormControl({value: this.originateData.OriginateCategoryType , disabled : false }
            ,  Validators.required),
          originateACNStakeholder: new FormControl({value: this.originateData.OriginateACNStakeholder, disabled : false}
            , Validators.required),
          originateCLNStakeholder: new FormControl({value: this.originateData.OriginateCLNStakeholder, disabled : false },
             Validators.required),
          originateTechnology: new FormControl({value: this.originateData.OriginateTechnology , disabled : false }
            , Validators.minLength(1))
          }),
          this._formBuilder.group({
            originateStatus: new FormControl({value: this.originateData.OriginateStatusType, disabled: false},
               Validators.required),
            originateETCDate: new FormControl({value: this.originateData.OriginateETCDate, disabled: false},
               Validators.required),
            proposedCCI: new FormControl({value: this.originateData.ProposedCCI , disabled : false},
               Validators.required),
            approvedCCI: new FormControl({value: this.originateData.ApprovedCCI, disabled : false },
               Validators.required),
            originateProbability: new FormControl({value: this.originateData.OriginateProbability, disabled : false } ,
              Validators.compose([Validators.required, Validators.max(100)])),
            originateROMEstimate: new FormControl({value: this.originateData.OriginateEstimate, disabled : false }, 
               Validators.required),
            originateRevenue: new FormControl({value: this.originateData.OriginateRevenue, disabled : false},
               Validators.required),
            originateApprover: new FormControl(),
            originateComments: new FormControl({value: this.originateData.OriginateComments, disabled : false},
               Validators.compose([Validators.minLength(20)]))
          })
        ])
      });
      let originateEntityData: any = '';
      originateEntityData = await this.FetchOriginateEntity();
      this.PopulateDropdowns(originateEntityData);
    }

    FetchOriginateEntity() {
      return new Promise((resolve, reject) => {
        const originateDetailsURL: string = this.httpConfig.getOriginateEntity();
        this.addOriginateService.originateEntity(originateDetailsURL).subscribe((result) => {
          if (result.status) {
            (result.status ? resolve(result.data) : reject(result));
          }
        }, (error) => {
          reject(error);
        });
      });
    }

    myFilter = (d: Date): boolean => {
      const day = d.getDay();
      const year = d.getFullYear();
      const currentDate = new Date();
      const currentyear = currentDate.getFullYear();
      // Prevent Saturday and Sunday from being selected.
      return day !== 0 && day !== 6 && year > currentyear - 2 && year < currentyear + 2;
    }

    PopulateDropdowns(dropdownResults: any) {
      console.log(dropdownResults);
      const serverData: any[] = [];
      dropdownResults.forEach(element => {
        serverData.push({
            item: element.originateentityID,
            value: element.originateentityDesc,
            Type: element.originateentityTitle
        });
      });
      serverData.forEach(element => {
        if  (element.Type === 'originateTower') {
          this.originateTowerType.push(element);
        }
        if  (element.Type === 'originateStatusType') {
          this.originateStatusType.push(element);
        }
        if  (element.Type === 'originateCategoryType') {
          this.originateCategoryType.push(element);
        }
        if  (element.Type === 'originateTechnology') {
          this.originateTechonologyType.push(element);
        }
        if  (element.Type === 'originateApprover') {
          this.originateApprover.push(element);
        }
      });
    }

    openDIalog(code) {
      this.dialogRef = this.dialog.open(ProgressDialogComponent, {data: {code: code}});
    }

    closeDialog() {
      this.dialogRef.close();
    }

    successGif() {
      // first reset form data
      this.closeDialog();
      this.openDIalog(2);
      setTimeout(() => {this.closeDialog(); }, 2000);
    }

    resetForm() {
      this.myform.resetForm();
    }

    onAddOriginateButton() {
      // login as admin if login type is admin or else as user
      this.openDIalog(1);
      const frmArry: any = this.OriginateForm.get('formArray') as FormArray;
      const frmArryValues: any = frmArry.value as Array<any>;
      const originateDataUpdate = {
        OriginateID : this.originateData.OriginateID,
        OriginateCategoryType  : frmArryValues[0].originateCategory,
        OriginateDesc  : frmArryValues[0].originateDescription,
        OriginateACNStakeholder  : frmArryValues[0].originateACNStakeholder,
        OriginateCLNStakeholder : frmArryValues[0].originateCLNStakeholder,
        OriginateTechnology : frmArryValues[0].originateTechnology,
        OriginateETCDate : frmArryValues[1].originateETCDate,
        OriginateStatusType : frmArryValues[1].originateStatus,
        ProposedCCI : frmArryValues[1].proposedCCI,
        ApprovedCCI : frmArryValues[1].approvedCCI,
        OriginateProbability : frmArryValues[1].originateProbability,
        OriginateEstimate  : frmArryValues[1].originateROMEstimate,
        OriginateRevenue   : frmArryValues[1].originateRevenue,
        OriginateApprover : frmArryValues[1].originateApprover,
        OriginateComments : frmArryValues[1].originateComments,
        OriginateCreatedBy   : this.user
    };

      const originateUpdateURL = this.httpConfig.updateoriginate();
      console.log(originateDataUpdate);
      this.addOriginateService.updateoriginate(originateUpdateURL, {data: originateDataUpdate}).subscribe((result) => {
      if (result.status) {
        console.log(result.status);
        this.successGif();
        location.reload();
      }else {
        console.log(result.message);
      }
      }, error => {
      console.log('error', error);
    });
    }
}
