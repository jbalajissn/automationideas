import { Component, OnInit, Inject, Injectable } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import {MatTableModule, MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-originatedashdialog',
  templateUrl: './originatedashdialog.component.html',
  styleUrls: ['./originatedashdialog.component.css']
})

export class OriginatedashdialogComponent implements OnInit {

  private selectOriginatedata: any;
  private selectedType: any;
  private originatedata: OriginateDetails[] = [];
  originateDatasource: any;
  displayedColumns: any;

  constructor(
    private dialogRef: MatDialogRef<OriginatedashdialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any) {
     this.selectOriginatedata = data.detailsData;
     this.selectedType = data.type;
   }

  ngOnInit() {
    this.BuildDialog(this.selectOriginatedata);
  }

  BuildDialog(data: any) {
    data.forEach(element => {
      this.originatedata.push({
        tower: element.tower,
        category: element.category,
        status: element.status,
        title: element.title,
        description: element.description,
        stakeholder: element.stakeholder,
        probability: element.probability,
        estimate: element.estimate,
        revenue: element.revenue
      });
    });
    this.displayedColumns = ['TOWER', 'CATEGORY', 'STATUS', 'TITLE', 'DESCRIPTION', 'STAKEHOLDER'];
    this.originateDatasource = new MatTableDataSource<OriginateDetails>(this.originatedata);
  }
}

export interface OriginateDetails {
  tower: string;
  category: string;
  status: string;
  title: string;
  description: string;
  stakeholder: string;
  probability: string;
  estimate: string;
  revenue: string;
}

