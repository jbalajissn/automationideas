import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OriginatedashdialogComponent } from './originatedashdialog.component';

describe('OriginatedashdialogComponent', () => {
  let component: OriginatedashdialogComponent;
  let fixture: ComponentFixture<OriginatedashdialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OriginatedashdialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OriginatedashdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
