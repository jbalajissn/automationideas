import { Router } from '@angular/router';
import { CheckUserStatus } from './../utils/checkUserStatus';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { HttpCOnfig } from '../utils/httpConfig';
import { OriginatedashdialogComponent } from '../originatedashdialog/originatedashdialog.component';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { TS } from 'typescript-linq';
import { AddOriginateService } from '../add-originate/addoriginate';
import { OriginateService } from './originateDetails';
import { fakeAsync } from '@angular/core/testing';
import { ExcelService } from './../utils/excelService';
declare let jsPDF;
declare let linq;

@Component({
  selector: 'app-show-originate',
  templateUrl: './show-originate.component.html',
  styleUrls: ['./show-originate.component.css']
})
export class ShowOriginateComponent implements OnInit {

  BAview: any[] = [480, 350];
  VTTview: any[] = [480, 350];
  view: any[] = [480, 350];
  selectDialogData: any = [];
  elCount = 0;
  originateTower: any = [];
  originateCategory: any = [];
  //datasource: object ;
  elements = ['CategoryArea', 'CatStatProb', 'CatStatEst', 'CatStatRev'];
  pdf = new jsPDF('l', 'mm', 'a4');
  originateDetailsData: any[] = [];
  masterOriginateDetails: any[] = [];
  towerselected: any = '';
  categoryselected: any = '';
  originateCatData: any[] = [];
  origCatStatProb: any[] = [];
  origCatStatEst: any[] = [];
  origCatStatReve: any[] = [];
  searchOriginateForm: FormGroup;
  sample = new TS.Collections.List<any>(false);
  pieOriginCatData: any[] = [];
  origCatStatEstData: any[] = [];
  origCatStatReveData: any[] = [];
  origCatStatProbData: any[] = [];
  gradient = false;
  BAlegendTitle = 'Initiative';
  WTlegentTitle = 'Tool';
  count = 0;

  verStacScheme = {
    domain: ['#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886']
  };
  colorscheme = {
    domain: ['#647c8a', '#3f51b5', '#2196f3', '#00b862', '#afdf0a', '#a7b61a', '#f3e562', '#ff9800', '#ff5722', '#ff4514']
  };
  originatecategoryscheme = {
    domain: ['#A10A28', '#D3342D', '#EF6D49', '#FAAD67', '#FDDE90', '#DBED91', '#A9D770', '#6CBA67', '#2C9653', '#146738']
  };
  WorkToolScheme = {
    domain: ['#55C22D', '#C1F33D', '#3CC099', '#AFFFFF', '#8CFC9D', '#76CFFA', '#BA60FB', '#EE6490', '#C42A1C', '#FC9F32']
  };

  constructor(
    private httpConfig: HttpCOnfig,
    private _formBuilder: FormBuilder,
    private excelService: ExcelService,
    private originateService: OriginateService,
    public addOriginateService: AddOriginateService,
    public dialog: MatDialog
  ) {
  }

  async ngOnInit() {
    let serverResult: any = '';

    this.searchOriginateForm = this._formBuilder.group({
      originateTower: new FormControl(),
      originateCategory: new FormControl()
    });

    let originateEntityData: any = '';
    originateEntityData = await this.FetchOriginateEntity();
    this.PopulateDropdowns(originateEntityData);

    serverResult = await this.FetchDetailsServer();
    this.masterOriginateDetails = serverResult;
    this.PopulateDashboard(serverResult);
    // this.pieOriginCatData = {
    // 'chart': {
    //   'caption': 'Opportunity Category Split',
    //   'plottooltext': '<b>$percentValue</b> of opportunity are $label category',
    //   'showlegend': '0',
    //   'showpercentvalues': '1',
    //   'legendposition': 'bottom',
    //   'usedataplotcolorforlabels': '1',
    //   'theme': 'fusion'
    // }, 'data': this.originateCatData};
    console.log(this.originateCatData);
    this.pieOriginCatData = this.originateCatData;
    this.origCatStatEstData = this.origCatStatEst;
    this.origCatStatProbData = this.origCatStatProb;
    this.origCatStatReveData = this.origCatStatReve;
  }

  FetchOriginateEntity() {
    return new Promise((resolve, reject) => {
      const originateEntityURL: string = this.httpConfig.getOriginateEntity();
      this.addOriginateService.originateEntity(originateEntityURL).subscribe((result) => {
        if (result.status) {
          (result.status ? resolve(result.data) : reject(result));
        }
      }, (error) => {
        reject(error);
      });
    });
  }

  PopulateDropdowns(dropdownResults: any) {
    const serverData: any[] = [];
    dropdownResults.forEach(element => {
      serverData.push({
        item: element.originateentityID,
        value: element.originateentityDesc,
        Type: element.originateentityTitle
      });
    });
    serverData.forEach(element => {
      if (element.Type === 'originateTower') {
        this.originateTower.push(element);
      }
      if (element.Type === 'originateCategoryType') {
        this.originateCategory.push(element);
      }
    });
  }

  FetchDetailsServer() {
    return new Promise((resolve, reject) => {
      const automationDetailsURL: string = this.httpConfig.getOriginateDetails();
      this.originateService.originateDetails(automationDetailsURL).subscribe((result) => {
        if (result.status) {
          (result.status ? resolve(result.data) : reject(result));
        }
      }, (error) => {
        reject(error);
      });
    });
  }

  PopulateCategoryData(data: any) {
    data.forEach(element => {
      if (!this.originateCatData) {
        this.originateCatData.push({
          name: element.category,
          value: 1
        });
      } else {
        const findTower: any = this.originateCatData.findIndex(ele => ele.name === element.category);
        if (findTower === -1) {
          this.originateCatData.push({
            name: element.category,
            value: 1
          });
        } else {
          this.originateCatData.find(ele => ele.name === element.category).value++;
        }
      }
    });
  }

  PopulateCatStatusEstData(data: any) {
    // console.log(data);
    data.forEach(element => {
      if (!this.origCatStatEst) {
        this.origCatStatEst.push({
          name: element.status,
          value: 1
        });
      } else {
        const findStatus: any = this.origCatStatEst.findIndex(ele => ele.name === element.status);
        if (findStatus === -1) {
          this.origCatStatEst.push({
            name: element.status,
            value: 1
          });
        } else {
          this.origCatStatEst.find(ele => ele.name === element.status).value++;
        }
      }
    });
  }

  PopulateCatStatProbData(data: any) {
    data.forEach(element => {
      if (!this.origCatStatProb) {
        this.origCatStatProb.push({
          name: element.category,
          series: [{
            name: element.perprobability,
            value: 1
          }]
        });
      } else {
        const findorigincat: any = this.origCatStatProb.findIndex(ele => ele.name === element.category);
        if (findorigincat === -1) {
          this.origCatStatProb.push({
            name: element.category,
            series: [{
              name: element.perprobability,
              value: 1
            }]
          });
        } else {
          const findPerProb: any = (this.origCatStatProb[findorigincat].series === undefined) ? -1 :
           this.origCatStatProb[findorigincat].series.findIndex(ele => ele.name === element.perprobability);
          if (findPerProb === -1) {
            this.origCatStatProb[findorigincat].series.push({
              name: element.perprobability,
              value: 1
            });
          } else {
            this.origCatStatProb[findorigincat].series[findPerProb].value++;
          }
        }
      }
    });
  }

  PopulateorigCatStatReve(data: any) {
    // console.log(data);
    data.forEach(element => {
      if (!this.origCatStatReve) {
        this.origCatStatReve.push({
          name: element.category,
          series: [{
            name: element.status,
            value: element.revenue
          }]
        });
      } else {
        const findCat: any = this.origCatStatReve.findIndex(ele => ele.name === element.category);
        if (findCat === -1) {
          this.origCatStatReve.push({
            name: element.category,
            series: [{
              name: element.status,
              value: element.revenue
            }]
          });
        } else {
          const findStatus: any = this.origCatStatReve[findCat].series.findIndex(ele => ele.name === element.status);
          if (findStatus === -1) {
            this.origCatStatReve[findCat].series.push({
              name: element.status,
              value: element.revenue
            });
          } else {
            this.origCatStatReve[findCat].series[findStatus].value += element.revenue;
          }
        }
      }
    });
  }

    PopulateDashboard(data: any) {
    this.originateDetailsData = [];
    // console.log(data);
    data.forEach(element => {
      const percentprobability = element.OriginateProbability > 50 ? 'High' : 'Low';
      this.originateDetailsData.push({
        id: element.OriginateID,
        tower: element.OriginateTower,
        category: element.OriginateCategoryType,
        status: element.OriginateStatusType,
        title: element.OriginateTitle,
        description: element.OriginateDesc,
        stakeholder : element.OriginateStakeholder,
        probability : element.OriginateProbability,
        perprobability : percentprobability,
        estimate : element.OriginateEstimate,
        revenue: element.OriginateRevenue,
      });
    });
    this.PopulateCategoryData(this.originateDetailsData);
    this.PopulateCatStatusEstData(this.originateDetailsData);
    this.PopulateCatStatProbData(this.originateDetailsData);
    this.PopulateorigCatStatReve(this.originateDetailsData);
  }

  onSelectAll(event) {
    this.pieOriginCatData = [];
    this.originateCatData = [];
    this.origCatStatEstData = [];
    this.origCatStatEst = [];
    this.origCatStatProbData = [];
    this.origCatStatProb = [];
    this.origCatStatReveData = []
    this.origCatStatReve = [];
    this.PopulateDashboard(this.masterOriginateDetails);
    this.pieOriginCatData = this.originateCatData;
    this.origCatStatEstData = this.origCatStatEst;
    this.origCatStatProbData = this.origCatStatProb;
    this.origCatStatReveData = this.origCatStatReve;
    this.towerselected = '';
    this.categoryselected = '';
    this.searchOriginateForm.controls['originateTower'].setValue(-1);
    this.searchOriginateForm.controls['originateCategory'].setValue(-1);
  }

  onStatusSelect(event) {
    // console.log(event.value);
    // console.log(event.name);
    // console.log(this.originateDetailsData);
    const selectDialogData: any = [];
    this.originateDetailsData.forEach(element => {
      if (element.status === event.name) {
        selectDialogData.push(element);
      }
    });
    this.dialog.open(OriginatedashdialogComponent, {
      data: { detailsData: selectDialogData, type: 'Status' }, panelClass: 'show-idea'
    });
  }

  onBASelect(event) {
    const selectDialogData: any = [];
      this.originateDetailsData.forEach(element => {
        if (element.perprobability === event.name && element.category === event.series) {
          selectDialogData.push(element);
        }
      });
    this.dialog.open(OriginatedashdialogComponent, {
      data: { detailsData: selectDialogData, type: 'BenefitArea' }, panelClass: 'show-idea'
    });
  }

  onWTSelect(event) {
    const selectDialogData: any = [];
      this.originateDetailsData.forEach(element => {
        if (element.status === event.name && element.category === event.series) {
          selectDialogData.push(element);
        }
      });
    this.dialog.open(OriginatedashdialogComponent, {
      data: { detailsData: selectDialogData, type: 'WorkType' }, panelClass: 'show-idea'
    });
  }

  ClickPDF() {

    // All units are in the set measurement for the document
    // This can be changed to 'pt' (points), 'mm' (Default), 'cm', 'in'
    this.sample = new TS.Collections.List<any>(false);
    this.originateDetailsData.forEach(element => {
      this.sample.add(element);
    });
    this.pdf = new jsPDF('l', 'mm', 'a4');
    this.elCount = -1;
    this.pdf.setFontSize(22);
    this.pdf.setFont('times');
    this.pdf.setFontType('bolditalic');
    this.pdf.text('Automation Report', 150, 10, 'center');
    this.recursiveAddHtml();
  }

  recursiveAddHtml(): any {
    this.elCount++;
    if (this.elCount < this.elements.length) {
      this.pdf.setFontSize(16);
      this.pdf.setFont('helvetica');
      this.pdf.setFontType('italic');
      switch (this.elements[this.elCount]) {
        case 'CategoryArea':
              this.pdf.text('Category Wise View', 10 , 30);
              break;
        case 'CatStatProb':
              this.pdf.text('Category Status Wise View' , 10 , 30);
              break;
        case 'CatStatEst':
              this.pdf.text('Category Status Estimate View' , 10 , 30);
              break;
        case 'CatStatRev':
              this.pdf.text('Category Status Revenue View' , 10 , 30);
              break;
      }

      this.pdf.addHTML(document.getElementById(this.elements[this.elCount]), 80, 40 , () => {
        switch (this.elements[this.elCount]) {
          case 'CategoryArea':
                this.printCategoryData();
                break;
          case 'CatStatProb':
                this.printCatStatProbData();
                break;
          case 'CatStatEst':
                this.printCatStatEstData();
                break;
          case 'CatStatRev':
                this.printCatStatRevData();
                break;
        }
        this.pdf.setLineWidth(0.5);
        this.pdf.line(10, 150 , 150, 150);
        this.pdf.addPage();
        this.recursiveAddHtml();
      });
    } else {
      this.pdf.save('Automation_Dashboard.pdf');
    }
  }

  printCategoryData(): any {
    const rowsTower: any[]  = [];
    const rowsStatus: any[]  = [];
    let resultTower = new TS.Collections.List<any>(false);
    resultTower = this.sample.groupBy(ele =>  (ele.category)).toList();
    const columnsTower = [
      { title: 'ID', dataKey: 'id' },
      { title: 'Category', dataKey: 'tower' },
      { title: 'Count', dataKey: 'count' }];
      this.count = 0;
      resultTower.forEach(element => {
      rowsTower.push({
        id: ++this.count,
        tower: element.key,
        count: this.sample.count(ele => ele.category === element.key)
      });
    });
    this.pdf.autoTable(columnsTower, rowsTower, {
      margin: { vertical: 40 },
      styles: { overflow: 'linebreak', columnWidth: 'wrap' },
      columnStyles: { text: { columnWidth: 'auto' } }
    });
  }

  printCatStatProbData(): any {
    const rowsStatus: any[]  = [];
    let resultStatus = new TS.Collections.List<any>(false);
    resultStatus = this.sample.groupBy(ele =>  (ele.status)).toList();
    const columnsStatus = [
        { title: 'ID', dataKey: 'id' },
        { title: 'Status', dataKey: 'status' },
        { title: 'Count', dataKey: 'count' }];
    this.count = 0;
    resultStatus.forEach(element => {
      rowsStatus.push({
        id: ++this.count,
        status: element.key,
        count: this.sample.count(ele => ele.status === element.key)
      });
    });
    this.pdf.autoTable(columnsStatus, rowsStatus, {
      margin: { vertical : 40 },
      styles: { overflow: 'linebreak', columnWidth: 'wrap' },
      columnStyles: { text: { columnWidth: 'auto' } }
    });
  }

  OnTowerChange(event: any) {
    console.log(event);
    console.log(this.masterOriginateDetails);
    if (event !== -1) {
      let selectTowerWisedata: any;
      this.towerselected = event;
      if (this.categoryselected === '') {
        selectTowerWisedata = this.masterOriginateDetails.filter(element => element.OriginateTower === event);
      } else {
        selectTowerWisedata = this.masterOriginateDetails.filter(element => element.OriginateTower === event
          && element.OriginateCategoryType === this.categoryselected );
      }
      console.log(selectTowerWisedata);
      this.pieOriginCatData = [];
      this.origCatStatProbData = [];
      this.origCatStatEstData = [];
      this.origCatStatReveData = [];

      this.originateCatData = [];
      this.origCatStatEst = [];
      this.origCatStatProb = [];
      this.origCatStatReve = [];

      this.PopulateDashboard(selectTowerWisedata);

      this.pieOriginCatData = this.originateCatData;
      this.origCatStatEstData = this.origCatStatEst;
      this.origCatStatProbData = this.origCatStatProb;
      this.origCatStatReveData = this.origCatStatReve;
    }
  }

  OnCatChange(event: any) {
    console.log(event);
    console.log(this.masterOriginateDetails);
    if (event !== -1) {
      let selectTowerWisedata: any;
      this.categoryselected = event;
      if (this.towerselected === '') {
        selectTowerWisedata = this.masterOriginateDetails.filter(element => element.OriginateCategoryType === event);
      } else {
        selectTowerWisedata = this.masterOriginateDetails.filter(element => element.OriginateCategoryType === event
          && element.OriginateTower === this.towerselected );
      }
      console.log(selectTowerWisedata);
      this.pieOriginCatData = [];
      this.origCatStatProbData = [];
      this.origCatStatEstData = [];
      this.origCatStatReveData = [];

      this.originateCatData = [];
      this.origCatStatEst = [];
      this.origCatStatProb = [];
      this.origCatStatReve = [];

      this.PopulateDashboard(selectTowerWisedata);

      this.pieOriginCatData = this.originateCatData;
      this.origCatStatEstData = this.origCatStatEst;
      this.origCatStatProbData = this.origCatStatProb;
      this.origCatStatReveData = this.origCatStatReve;
    }
  }

  printCatStatEstData(): any {
    // const rowsBArea: any[]  = [];
    // let resultBArea = new TS.Collections.List<any>(false);
    // resultBArea = this.sample.groupBy(ele =>  (ele.BenefitArea)).toList();
    // // console.log(resultBArea);
    // const columnsBArea = [
    //     { title: 'ID', dataKey: 'id' },
    //     { title: 'Benefit Area', dataKey: 'benArea' },
    //     { title: 'Count', dataKey: 'count' }];
    // this.count = 0;
    // resultBArea.forEach(element => {
    //   rowsBArea.push({
    //     id: ++this.count,
    //     benArea: element.key,
    //     count: this.sample.count(ele => ele.BenefitArea === element.key)
    //   });

    // });
    // this.pdf.autoTable(columnsBArea, rowsBArea, {
    //   margin: { vertical : 40 },
    //   styles: { overflow: 'linebreak', columnWidth: 'wrap' },
    //   columnStyles: { text: { columnWidth: 'auto' } }
    // });
  }

  printCatStatRevData(): any {
    // const rowsBArea: any[]  = [];
    // let resultBArea = new TS.Collections.List<any>(false);
    // resultBArea = this.sample.groupBy(ele =>  (ele.Tool)).toList();
    // const columnsBArea = [
    //     { title: 'ID', dataKey: 'id' },
    //     { title: 'Tool', dataKey: 'tool' },
    //     { title: 'Count', dataKey: 'count' }];
    // this.count = 0;
    // resultBArea.forEach(element => {
    //   rowsBArea.push({
    //     id: ++this.count,
    //     tool: element.key,
    //     count: this.sample.count(ele => ele.Tool === element.key)
    //   });
    // });
    // this.pdf.autoTable(columnsBArea, rowsBArea, {
    //   margin: { vertical : 40 },
    //   styles: { overflow: 'linebreak', columnWidth: 'wrap' },
    //   columnStyles: { text: { columnWidth: 'auto' } }
    // });
  }

  ClickExcel() {
    this.selectDialogData = [];
    this.originateDetailsData.forEach(element => {
         this.selectDialogData.push(element);
    });
    this.excelService.exportAsExcelFile(this.selectDialogData, 'Originate_Dashboard');
  }
}
