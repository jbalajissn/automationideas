import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, HttpModule } from '@angular/http';
/*
Fetching Automation Details through Http Calls
*/
@Injectable()
export class OriginateService {
    constructor(private httpClient: HttpClient) {
    }

    // Fetch Automation Dashboard Details
    originateDetails(URL: string) {
        return this.httpClient.get<any>(URL, {});
    }

    searchoriginateDetails(URL: string , data: any) {
        return this.httpClient.post<any>(URL, data);
    }

    originateComments(URL: string , data: any){
        return this.httpClient.post<any>(URL, data);
    }
}
