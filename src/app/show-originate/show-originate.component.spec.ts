import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowOriginateComponent } from './show-originate.component';

describe('ShowOriginateComponent', () => {
  let component: ShowOriginateComponent;
  let fixture: ComponentFixture<ShowOriginateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowOriginateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowOriginateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
