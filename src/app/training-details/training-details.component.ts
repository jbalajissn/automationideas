import { Component, OnInit,Inject,ViewChild, OnChanges } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatTableDataSource, MatDrawerToggleResult } from '@angular/material';
import { FormControl, Validators, FormGroup, FormGroupDirective, FormBuilder,FormArray, FormControlName,AbstractControl } from '@angular/forms';
import {ShowPeopleComponent} from "../show-people/show-people.component";
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
import { DISABLED } from '@angular/forms/src/model';
import { CheckUserStatus } from './../utils/checkUserStatus';
import { HttpCOnfig } from '../utils/httpConfig';
import {TrainingDetailService} from './trainingDetailService';
import {DatePipe} from '@angular/common';


@Component({
  selector: 'app-training-details',
  templateUrl: './training-details.component.html',
  styleUrls: ['./training-details.component.css']
})
export class TrainingDetailsComponent implements OnInit {
  
  public trainingData:any;
  public isAdmin=false;
  public createUser=false;
  public cntrlenable=false;
  PeopleContributionForm: FormGroup;
  PeopledetailsForm:FormGroup; 
  public btnenable:boolean;
  public user:string;
  statval:string;
  dialogRef: any;
  peopleFormGroup1:FormGroup;
  peopleFormGroup2:FormGroup;
  public dat:any;

  constructor(private _formBuilder:FormBuilder,
    public userinfo:CheckUserStatus, public dialog: MatDialog ,private httpConfig: HttpCOnfig,public trainingDetailService:TrainingDetailService,  
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.getUserInfo();
      this.trainingData=data;
      console.log(data);
      
      this.checkuser();
      //this.statval=data.autoStatus;
      console.log("Fetchedval")
      console.log(data);


     }

  ngOnInit() {
    this.PeopleContributionForm=new FormGroup({
      formArray: this._formBuilder.array([
        this._formBuilder.group({
        TitleControl:new FormControl({value:this.trainingData.TrainingName,disabled:true}),
        Descriptioncntrl:new FormControl({value:this.trainingData.TrainingDescription}),
        Prerequisitecntrl:new FormControl(''),
        trngSkillType:new FormControl('', Validators.required),
        
        trngType:new FormControl('', Validators.required)
      
       }) ,     
       
       this._formBuilder.group({
         trngLocation:new FormControl('', Validators.required),
         trngMode:new FormControl('', Validators.required),
         TargetAudience:new FormControl(''),
         TrainingStartdate: new FormControl('', Validators.required),
         trainingEnddate  :new FormControl('',Validators.required),
         Autocomments:new FormControl({value:'',disabled:false},Validators.required)
       }) 
       ])
   
 });
 this.peopleFormGroup1=this._formBuilder.group({
  TitleControl:new FormControl({value:this.trainingData.TrainingName,disabled:true}),
  Descriptioncntrl:new FormControl({value:this.trainingData.TrainingDescription}),
  trngSkillType:new FormControl('',Validators.required),
  Prerequisitecntrl:new FormControl({value:'',disabled:false}),
  trngType:new FormControl({value:this.trainingData.TrainingMode,disabled:!this.cntrlenable},Validators.required),
 });
 this.peopleFormGroup2=this._formBuilder.group({
  trngLocation:new FormControl({value:'',disabled:false}, Validators.required),
  TargetAudience:new FormControl({value:'',disabled:false}),
  trngMode:new FormControl({value:this.trainingData.TrainingMode,disabled:!this.cntrlenable},Validators.required),
  TrainingStartdate: new FormControl({value:'',disabled:false}, Validators.required),
  trainingEnddate  :new FormControl({value:'',disabled:false},Validators.required),
  Autocomments:new FormControl({value:'',disabled:false},Validators.required)

 })
 
  }

  getUserInfo()
  {
    this.user=JSON.parse(this.userinfo.getUserDetails())[0].username;
  }
  get formArray(): AbstractControl | null { return this.PeopleContributionForm.get('formArray'); }

  checkuser(){
    this.isAdmin=this.userinfo.isUserAdmin();
    if(this.trainingData.Contributor==this.user)
    {
      this.createUser=true;
    }
    this.cntrlenable=this.isAdmin||this.createUser;
    //this.cntrlenable=true;
    console.log(this.isAdmin);
    console.log(this.createUser);
    console.log(this.user);
    console.log(this.cntrlenable);

  }

  
  onupdateTrainingDetails(){
    this.openDIalog(1);
    const frmArry: any = this.PeopleContributionForm.get('formArray') as FormArray;
    console.log(frmArry);
    const frmArryValues: any = frmArry.value as Array<any>;
    console.log(frmArryValues);
    
     this.dat=this.getDate(frmArryValues[1].TrainingStartdate)
     console.log(this.dat);
     //this. dat = new DatePipe(frmArryValues[1].TrainingStartdate);
   // =this. dat.transform(frmArryValues[1].TrainingStartdate, 'dd/MM/yyyy');
    const updatedTrainingData={
      TrainingDescription:frmArryValues[0].Descriptioncntrl,
      TrainingMode:frmArryValues[1].trngMode,
      TrainingType:frmArryValues[0].trngType,
      TrainingSkillType:frmArryValues[0].trngSkillType,
      TrainingStartDate:frmArryValues[1].TrainingStartdate,
      TrainingEndDate:frmArryValues[1].trainingEnddate,
      TargetAudience:frmArryValues[1].TargetAudience,
      TrainingLocation:frmArryValues[1].trngLocation,
      TrainingPrequisite:frmArryValues[0].Prerequisitecntrl,
      TrainingComments:frmArryValues[1].Autocomments,
      id:this.trainingData.TrainingId
     

    }
    console.log("Test6");
    console.log(updatedTrainingData);
    const trainingsubmitUrl=this.httpConfig.updateTrainingById();
    console.log(trainingsubmitUrl);
    this.trainingDetailService.updateTraining(trainingsubmitUrl,{data:updatedTrainingData}).subscribe((result)=>{
      if(result.status)
      {
        console.log(result.status);
        this.successGif();
      }else{
        console.log(result.message);
      }
    },error=>{
      console.log('error',error);
    })
  }
  openDIalog(code) {
    this.dialogRef = this.dialog.open(ProgressDialogComponent, {data: {code: code}});
  }
  successGif() {
    // first reset form data
    this.closeDialog();
    this.openDIalog(2);
    setTimeout(() => {
      this.closeDialog();
    }, 2000);
  }

  closeDialog() {
    this.dialogRef.close();
  }

  getDate(date):any{
    if (date) {
        let dateOld: Date = new Date(date);
        return dateOld.toLocaleDateString();
  }}
  
}
