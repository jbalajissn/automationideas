webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/add-automation/add-automation.component.css":
/***/ (function(module, exports) {

module.exports = ".formWrapper{\r\n    margin: 90px 0px 0px 0px;\r\n    font-size: 16px;\r\n}\r\n\r\n.example-full-width {\r\n    width: 100%;\r\n  }\r\n\r\n.date_wrap {\r\n    width: 50%;\r\n  }\r\n\r\n.submit{\r\n      margin-top: 10px;\r\n  }"

/***/ }),

/***/ "./src/app/add-automation/add-automation.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <div class=\"col-md-6 col-md-offset-3\">\r\n    <mat-card class=\"formWrapper mat-elevation-z1\">\r\n      <form [formGroup]=\"AutomationForm\" (ngSubmit)=\"onAddAutomationButton()\">\r\n        <mat-horizontal-stepper formArrayName=\"formArray\" linear>\r\n          <mat-step formGroupName=\"0\" [stepControl]=\"formArray.get([0])\">\r\n            <ng-template matStepLabel>Basic Info</ng-template>\r\n            <mat-form-field class=\"example-full-width\">\r\n              <mat-select placeholder=\"TOWER\" required (ngModelChange)=\"OnTowerChange($event)\" formControlName=\"clientTowerType\" >\r\n                <mat-option *ngFor=\"let clientTowerType of clientTowerTypes\" [value]=\"clientTowerType.value\">\r\n                  {{clientTowerType.value}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n            <mat-form-field class=\"example-full-width\">\r\n              <input type=\"text\" placeholder=\"Application\" required matInput [matAutocomplete]=\"auto\" formControlName=\"applicationName\">\r\n              <mat-autocomplete #auto=\"matAutocomplete\">\r\n                <mat-option *ngFor=\"let applicationName of filteredOptions | async\" [value]=\"applicationName.value\">\r\n                    {{ applicationName.value }}\r\n                </mat-option>\r\n              </mat-autocomplete>\r\n            </mat-form-field>\r\n            <!-- <mat-form-field class=\"example-full-width\">\r\n              <mat-select placeholder=\"Application\" required formControlName=\"applicationName\">\r\n                <mat-option *ngFor=\"let applicationName of applicationNames\" [value]=\"applicationName.value\">\r\n                  {{applicationName.value}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field> -->\r\n            <mat-form-field class=\"example-full-width\">\r\n              <input matInput autocomplete=\"off\" placeholder=\"Title\" required formControlName=\"title\">\r\n              <mat-error>\r\n                Title should be minimum of 10\r\n              </mat-error>\r\n            </mat-form-field>\r\n            <mat-form-field class=\"example-full-width\">\r\n              <textarea matInput autocomplete=\"off\" #message placeholder=\"Description\" maxlength=\"256\" required formControlName=\"description\" ></textarea>\r\n              <mat-hint align=\"end\">{{message.value.length}} / 256</mat-hint>\r\n              <mat-error *ngIf=\"AutomationForm.hasError('minlength',['description'])\">\r\n                <p>Description should be between 10 to 256 </p>\r\n              </mat-error>\r\n            </mat-form-field >            \r\n             <!--<mat-form-field class=\"example-full-width\">\r\n              <input matInput [matDatepickerFilter]=\"myFilter\" [matDatepicker]=\"picker\" placeholder=\"Choose a date\" required formControlName=\"date\">\r\n              <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\r\n              <mat-datepicker #picker></mat-datepicker>\r\n            </mat-form-field> -->\r\n             <mat-form-field class=\"date_wrap\">\r\n              <input matInput autocomplete=\"off\"  [matDatepickerFilter]=\"myFilter\" [matDatepicker]=\"pickerTo\" placeholder=\"StartDate\" required formControlName=\"date\">\r\n              <mat-datepicker-toggle matSuffix [for]=\"pickerTo\"></mat-datepicker-toggle>\r\n              <mat-datepicker #pickerTo></mat-datepicker>\r\n              </mat-form-field> \r\n              <mat-form-field >\r\n                <input matInput autocomplete=\"off\"  [matDatepickerFilter]=\"myFilter\" [matDatepicker]=\"pickerFrom\" placeholder=\"CompletionDate\" required formControlName=\"autoenddate\">\r\n              <mat-datepicker-toggle matSuffix [for]=\"pickerFrom\"></mat-datepicker-toggle>\r\n              <mat-datepicker   #pickerFrom></mat-datepicker>\r\n              </mat-form-field> \r\n                \r\n          \r\n            <mat-form-field class=\"example-full-width\">\r\n              <input matInput autocomplete=\"off\" placeholder=\"Lead\" required formControlName=\"anchor\">\r\n              <mat-error>\r\n                Lead is mandatory\r\n              </mat-error>\r\n            </mat-form-field>\r\n            <mat-form-field class=\"example-full-width\">\r\n              <input matInput autocomplete=\"off\" placeholder=\"POC\" required formControlName=\"assignedTo\">\r\n              <mat-error>\r\n                POC is mandatory\r\n              </mat-error>\r\n            </mat-form-field>\r\n          </mat-step>\r\n          <mat-step formGroupName=\"1\" [stepControl]=\"formArray.get([1])\">\r\n            <ng-template matStepLabel>Benefits Info</ng-template>\r\n            <mat-form-field style=\"width: 40%\">\r\n              <input matInput autocomplete=\"off\" type=\"number\" placeholder=\"Effort Reduction\" required formControlName=\"effort\">\r\n              <mat-error>\r\n                Effort Reduction is mandatory (in Hrs / Year)\r\n              </mat-error>\r\n            </mat-form-field>\r\n            <mat-form-field >\r\n              <input matInput autocomplete=\"off\" type=\"number\" placeholder=\"Implementation Effort\" required formControlName=\"implementation\">\r\n              <mat-error>\r\n                Implementation Effort is mandatory (in Hrs / Year)\r\n              </mat-error>\r\n            </mat-form-field>            \r\n            <mat-form-field style=\"width: 40%\">\r\n              <input matInput  autocomplete=\"off\" type=\"number\" placeholder=\"FTE Savings\" required formControlName=\"FTE\">\r\n              <mat-error>\r\n                FTE Savings\r\n              </mat-error>\r\n            </mat-form-field>\r\n            <mat-form-field >\r\n              <input matInput autocomplete=\"off\" type=\"number\" placeholder=\"Ticket Reduction / Year\" formControlName=\"ticket\">\r\n              <mat-error>\r\n                Ticket reductions / Year\r\n              </mat-error>\r\n            </mat-form-field>\r\n            <mat-form-field style=\"width: 40%\">\r\n              <input matInput autocomplete=\"off\" type=\"number\" placeholder=\"Cost Saving Per Day ($)\" required formControlName=\"costperday\">\r\n              <mat-error>\r\n                Cost Savings Per Day ( in $)\r\n              </mat-error>\r\n            </mat-form-field>\r\n            <mat-form-field >\r\n              <input matInput autocomplete=\"off\" type=\"number\" placeholder=\"Net Cost Saving\" required formControlName=\"netcostsaving\">\r\n              <mat-error>\r\n                Net Cost Savings Per Year ( in $)\r\n              </mat-error>\r\n            </mat-form-field>\r\n            \r\n            <mat-form-field class=\"example-full-width\">\r\n              <mat-select placeholder=\"Work Type\" required formControlName=\"workType\">\r\n                <mat-option *ngFor=\"let workType of workTypes\" [value]=\"workType.item\">\r\n                  {{workType.value}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n            <mat-form-field class=\"example-full-width\">\r\n              <mat-select placeholder=\"Benefit Area\" required formControlName=\"benefitArea\">\r\n                <mat-option *ngFor=\"let benefitArea of benefitAreas\" [value]=\"benefitArea.item\">\r\n                  {{benefitArea.value}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n            <mat-form-field class=\"example-full-width\">\r\n              <mat-select placeholder=\"Initiative Category\" required formControlName=\"initiative\">\r\n                <mat-option *ngFor=\"let initiative of initiativeCat\" [value]=\"initiative.item\">\r\n                  {{initiative.value}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n            <mat-form-field class=\"example-full-width\">\r\n              <mat-select placeholder=\"Tool Category\" required formControlName=\"methodology\" >\r\n                <mat-option *ngFor=\"let methodology of methodologies; let i = index\" [value]=\"methodology.item\">\r\n                  {{methodology.value}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </mat-step>\r\n        </mat-horizontal-stepper>\r\n        <button class=\"submit\" type=\"submit\" formcontrolName=\"submit\" [disabled]=\"!AutomationForm.valid\" mat-raised-button color=\"warn\">SUBMIT</button>\r\n      </form>\r\n    </mat-card>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/add-automation/add-automation.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddAutomationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils_checkUserStatus__ = __webpack_require__("./src/app/utils/checkUserStatus.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__addautomationservice__ = __webpack_require__("./src/app/add-automation/addautomationservice.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__progress_dialog_progress_dialog_component__ = __webpack_require__("./src/app/progress-dialog/progress-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_operators_startWith__ = __webpack_require__("./node_modules/rxjs/_esm5/operators/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_operators_map__ = __webpack_require__("./node_modules/rxjs/_esm5/operators/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_util__ = __webpack_require__("./node_modules/util/util.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_util___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_util__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};











var AddAutomationComponent = (function () {
    function AddAutomationComponent(_formBuilder, httpConfig, userinfo, addAutomationService, dialog) {
        this._formBuilder = _formBuilder;
        this.httpConfig = httpConfig;
        this.userinfo = userinfo;
        this.addAutomationService = addAutomationService;
        this.dialog = dialog;
        this.applicationName = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]();
        this.clientTowerTypes = [];
        this.workTypes = [];
        this.benefitAreas = [];
        this.initiativeCat = [];
        this.methodologies = [];
        this.automationAppData = [];
        this.applicationNames = [
            {
                item: 0,
                value: '-- Select Tower--'
            }
        ];
        // AutomationBasicForm
        this.myFilter = function (d) {
            var day = d.getDay();
            var year = d.getFullYear();
            var currentDate = new Date();
            var currentyear = currentDate.getFullYear();
            // Prevent Saturday and Sunday from being selected.
            return day !== 0 && day !== 6 && year > currentyear - 2 && year < currentyear + 2;
        };
        this.getUserInfo();
        console.log(this.user);
    }
    Object.defineProperty(AddAutomationComponent.prototype, "formArray", {
        get: function () { return this.AutomationForm.get('formArray'); },
        enumerable: true,
        configurable: true
    });
    AddAutomationComponent.prototype.getUserInfo = function () {
        this.user = JSON.parse(this.userinfo.getUserDetails())[0].username;
    };
    AddAutomationComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var automationEntityData, automationTowerData, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.AutomationForm = this._formBuilder.group({
                            formArray: this._formBuilder.array([
                                this._formBuilder.group({
                                    clientTowerType: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    applicationName: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    anchor: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    date: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    autoenddate: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    assignedTo: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    title: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].minLength(10)])),
                                    description: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].minLength(20)]))
                                }),
                                this._formBuilder.group({
                                    effort: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].minLength(2), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].maxLength(4),
                                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].min(10), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].max(9999)])),
                                    implementation: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].minLength(2), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].maxLength(4),
                                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].min(10), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].max(9999)])),
                                    ticket: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', null),
                                    FTE: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    costperday: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    netcostsaving: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    initiative: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    methodology: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    workType: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    benefitArea: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required)
                                }),
                            ])
                        });
                        this.automationBasicFormGroup = this._formBuilder.group({
                            clientTowerType: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            applicationName: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            anchor: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            date: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            autoenddate: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            assignedTo: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            title: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].minLength(10)])),
                            description: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].minLength(20)]))
                        });
                        this.automationDetailFormGroup = this._formBuilder.group({
                            effort: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].minLength(2), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].maxLength(4),
                                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].min(10), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].max(9999)])),
                            implementation: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].minLength(2), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].maxLength(4),
                                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].min(10), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].max(9999)])),
                            ticket: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', null),
                            FTE: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            costperday: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            netcostsaving: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            initiative: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            methodology: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            workType: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            benefitArea: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required)
                        });
                        automationEntityData = '';
                        return [4 /*yield*/, this.FetchAutomationEntity()];
                    case 1:
                        automationEntityData = _b.sent();
                        this.PopulateDropdowns(automationEntityData);
                        automationTowerData = '';
                        return [4 /*yield*/, this.FetchAutomationTowers()];
                    case 2:
                        automationTowerData = _b.sent();
                        this.PopulateTowers(automationTowerData);
                        _a = this;
                        return [4 /*yield*/, this.FetchAutomationApplicationName()];
                    case 3:
                        _a.automationAppData = _b.sent();
                        this.filteredOptions = this.applicationName.valueChanges
                            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators_startWith__["a" /* startWith */])(''), Object(__WEBPACK_IMPORTED_MODULE_8_rxjs_operators_map__["a" /* map */])(function (val) { return _this.filter(val); }));
                        return [2 /*return*/];
                }
            });
        });
    };
    AddAutomationComponent.prototype.filter = function (val) {
        return this.applicationNames.filter(function (option) {
            return option.value.toLowerCase().indexOf(val.toLowerCase()) === 0;
        });
    };
    AddAutomationComponent.prototype.FetchAutomationTowers = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var automationDetailsURL = _this.httpConfig.getAutomationTower();
            _this.addAutomationService.automationTower(automationDetailsURL).subscribe(function (result) {
                if (result.status) {
                    (result.status ? resolve(result.data) : reject(result));
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    AddAutomationComponent.prototype.FetchAutomationApplicationName = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var automationDetailsURL = _this.httpConfig.getAutomationAppNames();
            _this.addAutomationService.automationApplication(automationDetailsURL).subscribe(function (result) {
                if (result.status) {
                    (result.status ? resolve(result.data) : reject(result));
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    AddAutomationComponent.prototype.FetchAutomationEntity = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var automationDetailsURL = _this.httpConfig.getautomationEntity();
            _this.addAutomationService.automationEntity(automationDetailsURL).subscribe(function (result) {
                if (result.status) {
                    (result.status ? resolve(result.data) : reject(result));
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    AddAutomationComponent.prototype.PopulateTowers = function (dropdownResults) {
        console.log(dropdownResults);
        var serverData = [];
        dropdownResults.forEach(function (element) {
            serverData.push({
                item: element.ID,
                value: element.Value
            });
        });
        this.clientTowerTypes = serverData;
    };
    AddAutomationComponent.prototype.PopulateDropdowns = function (dropdownResults) {
        var _this = this;
        console.log(dropdownResults);
        var serverData = [];
        dropdownResults.forEach(function (element) {
            serverData.push({
                item: element.ID,
                value: element.Value,
                Type: element.Type
            });
        });
        serverData.forEach(function (element) {
            if (element.Type === 'automationWorkType') {
                _this.workTypes.push(element);
            }
            if (element.Type === 'automationInitiativeCat') {
                _this.initiativeCat.push(element);
            }
            if (element.Type === 'automationTool') {
                _this.methodologies.push(element);
            }
            if (element.Type === 'automationBenefitArea') {
                _this.benefitAreas.push(element);
            }
        });
    };
    /* Add Automation */
    AddAutomationComponent.prototype.onAddAutomationButton = function () {
        var _this = this;
        // login as admin if login type is admin or else as user
        this.openDIalog(1);
        var frmArry = this.AutomationForm.get('formArray');
        var frmArryValues = frmArry.value;
        var datePipe = new __WEBPACK_IMPORTED_MODULE_10__angular_common__["DatePipe"]('en-US');
        var automationData = {
            automationTower: frmArryValues[0].clientTowerType,
            automationApplication: frmArryValues[0].applicationName,
            automationTitle: frmArryValues[0].title,
            automationDescription: frmArryValues[0].description,
            automationDate: datePipe.transform(frmArryValues[0].date, 'dd/MM/yyyy'),
            automationActualCompletionDate: datePipe.transform(frmArryValues[0].autoenddate, 'dd/MM/yyyy'),
            automationLead: frmArryValues[0].anchor,
            automationResource: frmArryValues[0].assignedTo,
            automationEfforts: frmArryValues[1].effort,
            automationImplementation: frmArryValues[1].implementation,
            automationFTE: frmArryValues[1].FTE,
            automationcostperday: frmArryValues[1].costperday,
            automationnetcostsaving: frmArryValues[1].netcostsaving,
            automationticket: Object(__WEBPACK_IMPORTED_MODULE_9_util__["isNull"])(frmArryValues[1].ticket) ? 0 : frmArryValues[1].ticket,
            automationWorkType: frmArryValues[1].workType,
            automationBenefitArea: frmArryValues[1].benefitArea,
            automationInitiativeCat: frmArryValues[1].initiative,
            automationTool: frmArryValues[1].methodology,
            automationCreatedBy: this.user
        };
        console.log(automationData);
        var automationSaveURL = this.httpConfig.saveAutomationDetails();
        this.addAutomationService.saveAutomation(automationSaveURL, { data: automationData }).subscribe(function (result) {
            if (result.status) {
                // erase form data and show success gif
                console.log(result.status);
                _this.successGif();
            }
            else {
                console.log(result.message);
            }
        }, function (error) {
            console.log('error', error);
        });
    };
    AddAutomationComponent.prototype.arraytoString = function (toolvalues) {
        var strTool = '';
        toolvalues.forEach(function (element) {
            strTool += element;
        });
        return strTool;
    };
    AddAutomationComponent.prototype.openDIalog = function (code) {
        this.dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_6__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: code } });
    };
    AddAutomationComponent.prototype.closeDialog = function () {
        this.dialogRef.close();
    };
    AddAutomationComponent.prototype.successGif = function () {
        var _this = this;
        // first reset form data
        this.closeDialog();
        this.resetForm();
        this.openDIalog(2);
        setTimeout(function () {
            _this.closeDialog();
        }, 2000);
    };
    AddAutomationComponent.prototype.OnTowerChange = function (event) {
        var _this = this;
        this.applicationNames = [];
        this.applicationName.reset();
        var applicationData = [];
        this.automationAppData.forEach(function (element) {
            if (element.TowID === event) {
                applicationData.push({
                    item: element.ID,
                    value: element.Value
                });
            }
        });
        this.applicationNames = applicationData;
        this.filteredOptions = this.applicationName.valueChanges
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators_startWith__["a" /* startWith */])(''), Object(__WEBPACK_IMPORTED_MODULE_8_rxjs_operators_map__["a" /* map */])(function (val) { return _this.filter(val); }));
    };
    AddAutomationComponent.prototype.resetForm = function () {
        this.myform.resetForm();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* FormGroupDirective */]),
        __metadata("design:type", Object)
    ], AddAutomationComponent.prototype, "myform", void 0);
    AddAutomationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-add-automation',
            template: __webpack_require__("./src/app/add-automation/add-automation.component.html"),
            styles: [__webpack_require__("./src/app/add-automation/add-automation.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3__utils_httpConfig__["a" /* HttpCOnfig */],
            __WEBPACK_IMPORTED_MODULE_1__utils_checkUserStatus__["a" /* CheckUserStatus */],
            __WEBPACK_IMPORTED_MODULE_5__addautomationservice__["a" /* AddAutomationService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["f" /* MatDialog */]])
    ], AddAutomationComponent);
    return AddAutomationComponent;
}());



/***/ }),

/***/ "./src/app/add-automation/addautomationservice.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddAutomationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
Fetching Automation Details through Http Calls
*/
var AddAutomationService = (function () {
    function AddAutomationService(httpClient) {
        this.httpClient = httpClient;
    }
    // Fetch Automation Dashboard Details
    AddAutomationService.prototype.automationEntity = function (URL) {
        return this.httpClient.get(URL, {});
    };
    AddAutomationService.prototype.automationTower = function (URL) {
        return this.httpClient.get(URL, {});
    };
    AddAutomationService.prototype.automationApplication = function (URL) {
        return this.httpClient.get(URL, {});
    };
    AddAutomationService.prototype.saveAutomation = function (URL, data) {
        return this.httpClient.post(URL, data);
    };
    AddAutomationService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], AddAutomationService);
    return AddAutomationService;
}());



/***/ }),

/***/ "./src/app/add-ideas/add-ideas.component.css":
/***/ (function(module, exports) {

module.exports = ".wrapper{\r\n    padding: 0 !important;\r\n    height: 100vh;\r\n}\r\n\r\n.chart{\r\n    height: 200px;\r\n}"

/***/ }),

/***/ "./src/app/add-ideas/add-ideas.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-tab-group mat-stretch-tabs class=\"wrapper\">\r\n  <mat-tab label=\"Idea Logging\" layoutfill><app-idea-logging></app-idea-logging></mat-tab>\r\n  <mat-tab label=\"Idea of the month\"><app-idea-by-month></app-idea-by-month></mat-tab>\r\n  <mat-tab label=\"Trending\"><app-trendin></app-trendin></mat-tab>\r\n</mat-tab-group>"

/***/ }),

/***/ "./src/app/add-ideas/add-ideas.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddIdeasComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
var AddIdeasComponent = (function () {
    function AddIdeasComponent() {
        this.emailFormControl = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]('', [
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required,
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].pattern(EMAIL_REGEX)
        ]);
    }
    AddIdeasComponent.prototype.ngOnInit = function () {
    };
    AddIdeasComponent.prototype.onIdeaSubmit = function (form) {
        console.log(form.value);
    };
    AddIdeasComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-add-ideas',
            template: __webpack_require__("./src/app/add-ideas/add-ideas.component.html"),
            styles: [__webpack_require__("./src/app/add-ideas/add-ideas.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AddIdeasComponent);
    return AddIdeasComponent;
}());



/***/ }),

/***/ "./src/app/add-ideas/idea-by-month/idea-by-month.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/add-ideas/idea-by-month/idea-by-month.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\r\n  idea-by-month works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/add-ideas/idea-by-month/idea-by-month.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IdeaByMonthComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var IdeaByMonthComponent = (function () {
    function IdeaByMonthComponent() {
    }
    IdeaByMonthComponent.prototype.ngOnInit = function () {
    };
    IdeaByMonthComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-idea-by-month',
            template: __webpack_require__("./src/app/add-ideas/idea-by-month/idea-by-month.component.html"),
            styles: [__webpack_require__("./src/app/add-ideas/idea-by-month/idea-by-month.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], IdeaByMonthComponent);
    return IdeaByMonthComponent;
}());



/***/ }),

/***/ "./src/app/add-ideas/idea-logging/idea-logging-service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IdeasLogginService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// this is for making http calls 
var IdeasLogginService = (function () {
    function IdeasLogginService(httpClient, httpConfig) {
        this.httpClient = httpClient;
        this.httpConfig = httpConfig;
    }
    // sending idea details to server 
    IdeasLogginService.prototype.storeIdeas = function (data) {
        return this.httpClient.post(this.httpConfig.getIdeasInsertURL(), data);
    };
    IdeasLogginService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_0__utils_httpConfig__["a" /* HttpCOnfig */]])
    ], IdeasLogginService);
    return IdeasLogginService;
}());



/***/ }),

/***/ "./src/app/add-ideas/idea-logging/idea-logging.component.css":
/***/ (function(module, exports) {

module.exports = "\r\n\r\n.example-form {\r\n    min-width: 150px;\r\n    max-width: 500px;\r\n    width: 100%;\r\n  }\r\n  \r\n  .example-full-width {\r\n    width: 100%;\r\n  }\r\n  \r\n  .message{\r\n      color: white;\r\n      margin-bottom: 10px;\r\n      background:  darkcyan;\r\n  }\r\n  \r\n  .wrapper{\r\n    min-height: 100vh;\r\n  }\r\n  \r\n  .submit{\r\n    margin-top: 5px;\r\n  }\r\n  \r\n  .example-radio-group {\r\n  display: -webkit-inline-box;\r\n  display: -ms-inline-flexbox;\r\n  display: inline-flex;\r\n  -webkit-box-orient: horizontal;\r\n  -webkit-box-direction: normal;\r\n      -ms-flex-direction: row;\r\n          flex-direction: row;\r\n  width: 100%;\r\n}\r\n  \r\n  .example-radio-button {\r\n  margin: 5px;\r\n}\r\n  \r\n  .example-selected-value {\r\n  margin: 15px 0;\r\n}\r\n"

/***/ }),

/***/ "./src/app/add-ideas/idea-logging/idea-logging.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <mat-card class=\"s\"> -->\r\n<!-- this is for message -->\r\n<!-- <mat-card class=\"message\">\r\n    Add Ideas\r\n  </mat-card> -->\r\n\r\n<!-- this is for form -->\r\n<mat-card>\r\n  <mat-card class=\"message hidden\">\r\n    Add Ideas\r\n  </mat-card>\r\n  <form [formGroup]=\"ideaForm\" (ngSubmit)=\"onIdeaSubmit()\">\r\n    <mat-form-field class=\"example-full-width\">\r\n      <input matInput autocomplete=\"off\" placeholder=\"Email\" required formControlName=\"email\">\r\n      <mat-error>\r\n        Please enter a valid email address\r\n      </mat-error>\r\n    </mat-form-field>\r\n    <mat-form-field class=\"example-full-width\">\r\n      <input matInput autocomplete=\"off\" type=\"number\" placeholder=\"Employee ID\" required formControlName=\"employeeID\">\r\n      <mat-error *ngIf=\"ideaForm.hasError('maxlength',['employeeID'])\">\r\n        Employee Id cannot be more than 10\r\n      </mat-error>\r\n      <mat-error *ngIf=\"ideaForm.hasError('minlength',['employeeID'])\">\r\n        Employee Id cannot be less than 8\r\n      </mat-error>\r\n    </mat-form-field>\r\n    <mat-form-field class=\"example-full-width\">\r\n      <input matInput autocomplete=\"off\" placeholder=\"Title\" required formControlName=\"title\">\r\n      <mat-error *ngIf=\"ideaForm.hasError('minlength',['title'])\">\r\n        title should be minimum of 8\r\n      </mat-error>\r\n    </mat-form-field>\r\n    <mat-form-field class=\"example-full-width\">\r\n      <textarea matInput autocomplete=\"off\" #message maxlength=\"256\" required formControlName=\"description\" placeholder=\"Description\"></textarea>\r\n      <mat-hint align=\"end\">{{message.value.length}} / 256</mat-hint>\r\n      <mat-error *ngIf=\"ideaForm.hasError('minlength',['description'])\">\r\n        <p>Description should be between 10 to 256 </p>\r\n      </mat-error>\r\n    </mat-form-field>\r\n    <mat-form-field class=\"example-full-width\">\r\n      <mat-select placeholder=\"TOWER\" required formControlName=\"clientTowerType\">\r\n        <mat-option *ngFor=\"let clientTowerType of clientTowerTypes\" [value]=\"clientTowerType.value\">\r\n          {{clientTowerType.value}}\r\n        </mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n    <mat-form-field class=\"example-full-width\">\r\n      <mat-select placeholder=\"Technology(Java,SQL..)\" required formControlName=\"techs\">\r\n        <mat-option *ngFor=\"let tech of technology\" [value]=\"tech.value\">\r\n          {{tech.value}}\r\n        </mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n    <mat-radio-group class=\"example-radio-group\" required formControlName=\"ideaType\">\r\n      <mat-radio-button class=\"example-radio-button\" *ngFor=\"let typeOfIdea of typeOfIdeas\" [value]=\"typeOfIdea\">\r\n        {{typeOfIdea}}\r\n      </mat-radio-button>\r\n    </mat-radio-group>\r\n\r\n    <button class=\"submit\" type=\"submit\" [disabled]=\"!ideaForm.valid\" mat-raised-button color=\"warn\">Submit</button>\r\n  </form>\r\n</mat-card>\r\n\r\n<!-- </mat-card> -->"

/***/ }),

/***/ "./src/app/add-ideas/idea-logging/idea-logging.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IdeaLoggingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils_navServices__ = __webpack_require__("./src/app/utils/navServices.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__idea_logging_service__ = __webpack_require__("./src/app/add-ideas/idea-logging/idea-logging-service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__progress_dialog_progress_dialog_component__ = __webpack_require__("./src/app/progress-dialog/progress-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__add_automation_addautomationservice__ = __webpack_require__("./src/app/add-automation/addautomationservice.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








// const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
var EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@accenture.com/;
var IdeaLoggingComponent = (function () {
    function IdeaLoggingComponent(navService, ideadLogginService, dialog, addAutomationService, httpConfig) {
        this.navService = navService;
        this.ideadLogginService = ideadLogginService;
        this.dialog = dialog;
        this.addAutomationService = addAutomationService;
        this.httpConfig = httpConfig;
        this.typeOfIdeas = ['Innovate', 'Co-Innovate'];
        this.clientTowerTypes = [];
        this.technology = [];
        this.ideaForm = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["d" /* FormGroup */]({
            email: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].pattern(EMAIL_REGEX)])),
            employeeID: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].minLength(8),
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].maxLength(10)])),
            title: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].minLength(8)),
            techs: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].minLength(1)),
            clientTowerType: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
            ideaType: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
            description: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].maxLength(256), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].minLength(10)]))
        });
        this.navService.hideNavStatsu();
    }
    IdeaLoggingComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var automationTowerData, automationEntityData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        automationTowerData = '';
                        return [4 /*yield*/, this.FetchAutomationTowers()];
                    case 1:
                        automationTowerData = _a.sent();
                        this.PopulateTowers(automationTowerData);
                        automationEntityData = '';
                        return [4 /*yield*/, this.FetchAutomationEntity()];
                    case 2:
                        automationEntityData = _a.sent();
                        this.PopulateDropdowns(automationEntityData);
                        return [2 /*return*/];
                }
            });
        });
    };
    IdeaLoggingComponent.prototype.FetchAutomationTowers = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var automationDetailsURL = _this.httpConfig.getAutomationTower();
            _this.addAutomationService.automationTower(automationDetailsURL).subscribe(function (result) {
                if (result.status) {
                    (result.status ? resolve(result.data) : reject(result));
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    IdeaLoggingComponent.prototype.FetchAutomationEntity = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var automationDetailsURL = _this.httpConfig.getautomationEntity();
            _this.addAutomationService.automationEntity(automationDetailsURL).subscribe(function (result) {
                if (result.status) {
                    (result.status ? resolve(result.data) : reject(result));
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    IdeaLoggingComponent.prototype.PopulateDropdowns = function (dropdownResults) {
        var _this = this;
        console.log(dropdownResults);
        var serverData = [];
        dropdownResults.forEach(function (element) {
            serverData.push({
                item: element.ID,
                value: element.Value,
                Type: element.Type
            });
        });
        serverData.forEach(function (element) {
            if (element.Type === 'InnovtionTechnology') {
                _this.technology.push(element);
            }
        });
    };
    IdeaLoggingComponent.prototype.PopulateTowers = function (dropdownResults) {
        console.log(dropdownResults);
        var serverData = [];
        dropdownResults.forEach(function (element) {
            serverData.push({
                item: element.ID,
                value: element.Value
            });
        });
        this.clientTowerTypes = serverData;
    };
    IdeaLoggingComponent.prototype.onIdeaSubmit = function () {
        var _this = this;
        this.openDIalog(1);
        var data = {
            email: this.ideaForm.get('email').value,
            employeeID: this.ideaForm.get('employeeID').value,
            title: this.ideaForm.get('title').value,
            description: this.ideaForm.get('description').value,
            tower: this.ideaForm.get('clientTowerType').value,
            innovate_status: (this.ideaForm.get('ideaType').value === 'Innovate' ? 1 : 2),
            techs: this.ideaForm.get('techs').value
        };
        console.log(data);
        this.ideadLogginService.storeIdeas({ data: data }).subscribe(function (result) {
            if (result.status) {
                // erase form data and show success gif
                _this.successGif();
            }
            else {
                _this.closeDialog();
                console.log(result.message);
            }
        }, function (error) {
            _this.closeDialog();
            console.log("error", error);
        });
    };
    IdeaLoggingComponent.prototype.openDIalog = function (code) {
        this.dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_5__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: code } });
    };
    IdeaLoggingComponent.prototype.closeDialog = function () {
        this.dialogRef.close();
    };
    IdeaLoggingComponent.prototype.successGif = function () {
        var _this = this;
        // first reset form data
        this.closeDialog();
        this.resetForm();
        this.openDIalog(2);
        setTimeout(function () {
            _this.closeDialog();
        }, 2000);
    };
    // open animated gif 
    IdeaLoggingComponent.prototype.welcomeGif = function () {
        var _this = this;
        this.openDIalog(3);
        // close it after 2 seconds 
        setTimeout(function () { _this.closeDialog(); }, 4000);
    };
    // resetting form
    IdeaLoggingComponent.prototype.resetForm = function () {
        this.myform.resetForm();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* FormGroupDirective */]),
        __metadata("design:type", Object)
    ], IdeaLoggingComponent.prototype, "myform", void 0);
    IdeaLoggingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'app-idea-logging',
            template: __webpack_require__("./src/app/add-ideas/idea-logging/idea-logging.component.html"),
            styles: [__webpack_require__("./src/app/add-ideas/idea-logging/idea-logging.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__utils_navServices__["a" /* NavService */],
            __WEBPACK_IMPORTED_MODULE_4__idea_logging_service__["a" /* IdeasLogginService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_material__["f" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_7__add_automation_addautomationservice__["a" /* AddAutomationService */],
            __WEBPACK_IMPORTED_MODULE_6__utils_httpConfig__["a" /* HttpCOnfig */]])
    ], IdeaLoggingComponent);
    return IdeaLoggingComponent;
}());



/***/ }),

/***/ "./src/app/add-ideas/trendin/trendin.component.css":
/***/ (function(module, exports) {

module.exports = ".chatCard{\r\n    height:300px;\r\n\tdisplay:-webkit-box;\r\n\tdisplay:-ms-flexbox;\r\n\tdisplay:flex;\r\n\t-webkit-box-pack:center;\r\n\t    -ms-flex-pack:center;\r\n\t        justify-content:center;\r\n    -webkit-box-align:center;\r\n        -ms-flex-align:center;\r\n            align-items:center;\r\n    margin:  5px 5px 5px 0;\r\n}\r\n\r\n.wrapper{\r\n    min-height: 100vh;\r\n}\r\n\r\n.ngx-charts{\r\n    position: inherit;\r\n}\r\n\r\n"

/***/ }),

/***/ "./src/app/add-ideas/trendin/trendin.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"wrapper\">\r\n  <mat-card class=\"col-md-5 col-sm-5 chatCard\">      \r\n    <ngx-charts-advanced-pie-chart [scheme]=\"colorScheme\" [results]=\"single\" (select)=\"onSelect($event)\">\r\n    </ngx-charts-advanced-pie-chart>\r\n  </mat-card>\r\n  <mat-card class=\"col-md-5 col-sm-5 chatCard\">\r\n      <ngx-charts-tree-map  [scheme]=\"colorScheme\" [results]=\"single2\" (select)= \"onSelect($event)\">\r\n      </ngx-charts-tree-map>\r\n  </mat-card>\r\n</mat-card>\r\n"

/***/ }),

/***/ "./src/app/add-ideas/trendin/trendin.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrendinComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TrendinComponent = (function () {
    function TrendinComponent() {
        this.view = [200, 200];
        this.colorScheme = {
            domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
        };
    }
    TrendinComponent.prototype.ngOnInit = function () {
        this.single = [
            {
                "name": "Java",
                "value": 127
            },
            {
                "name": "JavaScript",
                "value": 3544
            },
            {
                "name": "SQL",
                "value": 654
            }
        ];
        this.single2 = [
            {
                "name": "SOS",
                "value": 322
            },
            {
                "name": "OPOM",
                "value": 165
            },
            {
                "name": "CMBS",
                "value": 345
            }
        ];
    };
    TrendinComponent.prototype.onSelect = function (event) {
        console.log(event);
    };
    TrendinComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-trendin',
            template: __webpack_require__("./src/app/add-ideas/trendin/trendin.component.html"),
            styles: [__webpack_require__("./src/app/add-ideas/trendin/trendin.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TrendinComponent);
    return TrendinComponent;
}());



/***/ }),

/***/ "./src/app/add-originate/add-originate.component.css":
/***/ (function(module, exports) {

module.exports = ".formWrapper{\r\n    margin: 90px 0px 0px 0px;\r\n    font-size: 16px;\r\n}\r\n\r\n.example-full-width {\r\n    width: 100%;\r\n  }\r\n\r\n.date_wrap {\r\n    width: 50%;\r\n  }\r\n\r\n.submit{\r\n      margin-top: 10px;\r\n  }"

/***/ }),

/***/ "./src/app/add-originate/add-originate.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <div class=\"col-md-6 col-md-offset-3\">\r\n    <mat-card class=\"formWrapper mat-elevation-z1\">\r\n      <form [formGroup]=\"OriginateForm\" (ngSubmit)=\"onAddOriginateButton()\">\r\n          <mat-horizontal-stepper formArrayName=\"formArray\" linear>\r\n              <mat-step formGroupName=\"0\" [stepControl]=\"formArray.get([0])\">\r\n                  <ng-template matStepLabel>Basic Info</ng-template>  \r\n          <mat-form-field class=\"example-full-width\">\r\n              <mat-select placeholder=\"TOWER\" required formControlName=\"originateTower\" >\r\n                <mat-option *ngFor=\"let originateTow of originateTowerType\" [value]=\"originateTow.value\">\r\n                  {{originateTow.value}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field>      \r\n            <mat-form-field class=\"example-full-width\">\r\n              <input matInput autocomplete=\"off\" placeholder=\"TITLE\" required formControlName=\"originateTitle\">\r\n              <mat-error>\r\n                Title should be minimum of 15\r\n              </mat-error>\r\n            </mat-form-field>\r\n            <mat-form-field class=\"example-full-width\">\r\n              <textarea matInput autocomplete=\"off\" #messagearea placeholder=\"DESCRIPTION\" maxlength=\"256\" required formControlName=\"originateDescription\" ></textarea>\r\n              <mat-hint align=\"end\">{{messagearea.value.length}} / 256</mat-hint>\r\n              <mat-error *ngIf=\"OriginateForm.hasError('minlength',['originateDescription'])\">\r\n                <p>Description should be between 20 to 256 </p>\r\n              </mat-error>\r\n            </mat-form-field >\r\n            <mat-form-field class=\"example-full-width\">\r\n              <mat-select placeholder=\"CATEGORY\" required formControlName=\"originateCategory\" >\r\n                <mat-option *ngFor=\"let originateCat of originateCategoryType\" [value]=\"originateCat.value\">\r\n                  {{originateCat.value}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n            <mat-form-field class=\"example-full-width\">\r\n                <input matInput autocomplete=\"off\" placeholder=\"Accenture POC \" required formControlName=\"originateACNStakeholder\">\r\n              </mat-form-field>\r\n              <mat-form-field class=\"example-full-width\">\r\n                  <input matInput autocomplete=\"off\" placeholder=\"Client POC \" required formControlName=\"originateCLNStakeholder\">\r\n                </mat-form-field>            \r\n            <mat-form-field class=\"example-full-width\">\r\n                <mat-select placeholder=\"Technology(Java,SQL..)\" required formControlName=\"originateTechnology\">\r\n                  <mat-option *ngFor=\"let tech of originateTechonologyType\" [value]=\"tech.value\">\r\n                    {{tech.value}}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>\r\n          </mat-step>\r\n          <mat-step formGroupName=\"1\" [stepControl]=\"formArray.get([1])\">\r\n            <ng-template matStepLabel>Benefits Info</ng-template>\r\n            <mat-form-field style=\"width: 45%\">\r\n              <mat-select placeholder=\"STATUS\" required  formControlName=\"originateStatus\" >\r\n                <mat-option *ngFor=\"let originateStat of originateStatusType\" [value]=\"originateStat.value\">\r\n                  {{originateStat.value}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field>           \r\n            <mat-form-field class=\"date_wrap\">\r\n                <input matInput autocomplete=\"off\"  [matDatepickerFilter]=\"myFilter\" [matDatepicker]=\"pickerTo\" placeholder=\"Target Date\" required formControlName=\"originateETCDate\">\r\n                <mat-datepicker-toggle matSuffix [for]=\"pickerTo\"></mat-datepicker-toggle>\r\n                <mat-datepicker #pickerTo></mat-datepicker>\r\n            </mat-form-field> \r\n            <mat-form-field style=\"width: 50%\">\r\n                <input matInput autocomplete=\"off\" type=\"number\" placeholder=\"Proposed CCI\" required formControlName=\"proposedCCI\">\r\n              </mat-form-field>\r\n              <mat-form-field >\r\n                  <input matInput autocomplete=\"off\" type=\"number\" placeholder=\"Approved CCI\" required formControlName=\"approvedCCI\">\r\n                </mat-form-field>\r\n            <mat-form-field style=\"width: 50%\">\r\n              <input matInput autocomplete=\"off\" type=\"number\" placeholder=\"Win % \" required formControlName=\"originateProbability\">\r\n              <mat-error>\r\n                Probability Should be < 100\r\n              </mat-error>\r\n            </mat-form-field>\r\n            <mat-form-field >\r\n              <input matInput autocomplete=\"off\" type=\"number\" placeholder=\"Estimate (MDs)\" required formControlName=\"originateROMEstimate\">\r\n            </mat-form-field>\r\n            <mat-form-field style=\"width: 50%\">\r\n              <input matInput autocomplete=\"off\" type=\"number\" placeholder=\"Revenue (AUD)\" required formControlName=\"originateRevenue\">\r\n            </mat-form-field>    \r\n            <mat-form-field >\r\n                <mat-select placeholder=\"Approver\"  formControlName=\"originateApprover\">\r\n                  <mat-option *ngFor=\"let approv of originateApprover\" [value]=\"approv.value\">\r\n                    {{approv.value}}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>\r\n              <mat-form-field class=\"example-full-width\">\r\n                  <textarea matInput autocomplete=\"off\" #message placeholder=\"COMMENTS\" maxlength=\"256\" required formControlName=\"originateComments\" ></textarea>\r\n                  <mat-hint align=\"end\">{{message.value.length}} / 256</mat-hint>\r\n                  <mat-error *ngIf=\"OriginateForm.hasError('minlength',['originateComments'])\">\r\n                    <p>Comments should be between 20 to 256 </p>\r\n                  </mat-error>\r\n                </mat-form-field >\r\n          </mat-step>\r\n        </mat-horizontal-stepper>      \r\n        <button class=\"submit\" type=\"submit\" formcontrolName=\"submit\" [disabled]=\"!OriginateForm.valid\" mat-raised-button color=\"warn\">SUBMIT</button>\r\n      </form>\r\n    </mat-card>\r\n  </div>\r\n</div>  "

/***/ }),

/***/ "./src/app/add-originate/add-originate.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddOriginateComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utils_checkUserStatus__ = __webpack_require__("./src/app/utils/checkUserStatus.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__progress_dialog_progress_dialog_component__ = __webpack_require__("./src/app/progress-dialog/progress-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__addoriginate__ = __webpack_require__("./src/app/add-originate/addoriginate.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_util__ = __webpack_require__("./node_modules/util/util.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_util___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_util__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};










var AddOriginateComponent = (function () {
    // OriginateForm
    function AddOriginateComponent(_formBuilder, httpConfig, userinfo, addOriginateService, router, dialog) {
        this._formBuilder = _formBuilder;
        this.httpConfig = httpConfig;
        this.userinfo = userinfo;
        this.addOriginateService = addOriginateService;
        this.router = router;
        this.dialog = dialog;
        this.originateTowerType = [];
        this.originateCategoryType = [];
        this.originateStatusType = [];
        this.originateTechonologyType = [];
        this.originateApprover = [];
        this.myFilter = function (d) {
            var day = d.getDay();
            var year = d.getFullYear();
            var currentDate = new Date();
            var currentyear = currentDate.getFullYear();
            // Prevent Saturday and Sunday from being selected.
            return day !== 0 && day !== 6 && year > currentyear - 2 && year < currentyear + 2;
        };
        this.getUserInfo();
    }
    Object.defineProperty(AddOriginateComponent.prototype, "formArray", {
        get: function () { return this.OriginateForm.get('formArray'); },
        enumerable: true,
        configurable: true
    });
    AddOriginateComponent.prototype.getUserInfo = function () {
        if (Object(__WEBPACK_IMPORTED_MODULE_8_util__["isNull"])(JSON.parse(this.userinfo.getUserDetails()))) {
            this.router.navigateByUrl('/login');
        }
        else {
            this.user = JSON.parse(this.userinfo.getUserDetails())[0].username;
        }
    };
    AddOriginateComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var originateEntityData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.OriginateForm = this._formBuilder.group({
                            formArray: this._formBuilder.array([
                                this._formBuilder.group({
                                    originateTitle: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].minLength(15)])),
                                    originateDescription: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].minLength(20)])),
                                    originateTower: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].required),
                                    originateCategory: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].required),
                                    originateACNStakeholder: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].required),
                                    originateCLNStakeholder: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].required),
                                    originateTechnology: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].minLength(1))
                                }),
                                this._formBuilder.group({
                                    originateStatus: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].required),
                                    originateETCDate: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].required),
                                    proposedCCI: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].required),
                                    approvedCCI: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].required),
                                    originateProbability: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].max(100)])),
                                    originateROMEstimate: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].required),
                                    originateRevenue: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].required),
                                    originateApprover: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */](),
                                    originateComments: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].minLength(20)]))
                                })
                            ])
                        });
                        originateEntityData = '';
                        return [4 /*yield*/, this.FetchOriginateEntity()];
                    case 1:
                        originateEntityData = _a.sent();
                        this.PopulateDropdowns(originateEntityData);
                        return [2 /*return*/];
                }
            });
        });
    };
    AddOriginateComponent.prototype.FetchOriginateEntity = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var originateDetailsURL = _this.httpConfig.getOriginateEntity();
            _this.addOriginateService.originateEntity(originateDetailsURL).subscribe(function (result) {
                if (result.status) {
                    (result.status ? resolve(result.data) : reject(result));
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    AddOriginateComponent.prototype.PopulateDropdowns = function (dropdownResults) {
        var _this = this;
        console.log(dropdownResults);
        var serverData = [];
        dropdownResults.forEach(function (element) {
            serverData.push({
                item: element.originateentityID,
                value: element.originateentityDesc,
                Type: element.originateentityTitle
            });
        });
        serverData.forEach(function (element) {
            if (element.Type === 'originateTower') {
                _this.originateTowerType.push(element);
            }
            if (element.Type === 'originateStatusType') {
                _this.originateStatusType.push(element);
            }
            if (element.Type === 'originateCategoryType') {
                _this.originateCategoryType.push(element);
            }
            if (element.Type === 'originateTechnology') {
                _this.originateTechonologyType.push(element);
            }
            if (element.Type === 'originateApprover') {
                _this.originateApprover.push(element);
            }
        });
    };
    AddOriginateComponent.prototype.openDIalog = function (code) {
        this.dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_6__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: code } });
    };
    AddOriginateComponent.prototype.closeDialog = function () {
        this.dialogRef.close();
    };
    AddOriginateComponent.prototype.successGif = function () {
        var _this = this;
        // first reset form data
        this.closeDialog();
        this.resetForm();
        this.openDIalog(2);
        setTimeout(function () {
            _this.closeDialog();
        }, 5000);
    };
    AddOriginateComponent.prototype.resetForm = function () {
        this.myform.resetForm();
    };
    AddOriginateComponent.prototype.onAddOriginateButton = function () {
        var _this = this;
        // login as admin if login type is admin or else as user
        this.openDIalog(1);
        var frmArry = this.OriginateForm.get('formArray');
        var frmArryValues = frmArry.value;
        var datePipe = new __WEBPACK_IMPORTED_MODULE_9__angular_common__["DatePipe"]('en-US');
        var originateData = {
            OriginateTower: frmArryValues[0].originateTower,
            OriginateCategoryType: frmArryValues[0].originateCategory,
            OriginateTitle: frmArryValues[0].originateTitle,
            OriginateDesc: frmArryValues[0].originateDescription,
            OriginateACNStakeholder: frmArryValues[0].originateACNStakeholder,
            OriginateCLNStakeholder: frmArryValues[0].originateCLNStakeholder,
            OriginateTechnology: frmArryValues[0].originateTechnology,
            OriginateETCDate: datePipe.transform(frmArryValues[0].originateETCDate, 'dd/MM/yyyy'),
            OriginateStatusType: frmArryValues[1].originateStatus,
            ProposedCCI: frmArryValues[1].proposedCCI,
            ApprovedCCI: frmArryValues[1].approvedCCI,
            OriginateProbability: frmArryValues[1].originateProbability,
            OriginateEstimate: frmArryValues[1].originateROMEstimate,
            OriginateRevenue: frmArryValues[1].originateRevenue,
            OriginateApprover: frmArryValues[1].originateApprover,
            OriginateComments: frmArryValues[1].originateComments,
            OriginateCreatedBy: this.user
        };
        console.log(originateData);
        var automationSaveURL = this.httpConfig.saveOriginateDetails();
        this.addOriginateService.saveoriginate(automationSaveURL, { data: originateData }).subscribe(function (result) {
            if (result.status) {
                // erase form data and show success gif
                console.log(result.status);
                _this.successGif();
            }
            else {
                console.log(result.message);
            }
        }, function (error) {
            console.log('error', error);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_3__angular_forms__["e" /* FormGroupDirective */]),
        __metadata("design:type", Object)
    ], AddOriginateComponent.prototype, "myform", void 0);
    AddOriginateComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-add-originate',
            template: __webpack_require__("./src/app/add-originate/add-originate.component.html"),
            styles: [__webpack_require__("./src/app/add-originate/add-originate.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_4__utils_httpConfig__["a" /* HttpCOnfig */],
            __WEBPACK_IMPORTED_MODULE_2__utils_checkUserStatus__["a" /* CheckUserStatus */],
            __WEBPACK_IMPORTED_MODULE_7__addoriginate__["a" /* AddOriginateService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */],
            __WEBPACK_IMPORTED_MODULE_5__angular_material__["f" /* MatDialog */]])
    ], AddOriginateComponent);
    return AddOriginateComponent;
}());



/***/ }),

/***/ "./src/app/add-originate/addoriginate.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddOriginateService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
Fetching Automation Details through Http Calls
*/
var AddOriginateService = (function () {
    function AddOriginateService(httpClient) {
        this.httpClient = httpClient;
    }
    // Fetch Automation Dashboard Details
    AddOriginateService.prototype.originateEntity = function (URL) {
        return this.httpClient.get(URL, {});
    };
    AddOriginateService.prototype.saveoriginate = function (URL, data) {
        return this.httpClient.post(URL, data);
    };
    AddOriginateService.prototype.updateoriginate = function (URL, data) {
        return this.httpClient.post(URL, data);
    };
    AddOriginateService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], AddOriginateService);
    return AddOriginateService;
}());



/***/ }),

/***/ "./src/app/add-people-contribution/add-people-contribution.component.css":
/***/ (function(module, exports) {

module.exports = ".formWrapper{\r\n    margin: 150px 0px 0px 0px;\r\n    font-size: 16px;\r\n}\r\n\r\n.example-full-width {\r\n    width: 100%;\r\n  }\r\n\r\n.date_wrap {\r\n    width: 50%;\r\n  }\r\n\r\n.submit{\r\n      margin-top: 10px;\r\n  }"

/***/ }),

/***/ "./src/app/add-people-contribution/add-people-contribution.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n    <div class=\"col-md-6 col-md-offset-3\">\r\n              \r\n      <mat-card class=\"formWrapper mat-elevation-z1\">\r\n       <form [formGroup]=\"PeopleContributionForm\" (ngSubmit)=\"onAddPeopleContributionButton()\">  \r\n                   <mat-horizontal-stepper formArrayName=\"formArray\" linear>\r\n              <mat-step formGroupName=\"0\" [stepControl]=\"formArray.get([0])\">\r\n                  <ng-template matStepLabel>Training Info</ng-template>\r\n                  <mat-form-field class=\"example-full-width\">\r\n                      <input matInput #title placeholder=\"Course Name\"  minLength=\"3\" required formControlName=\"TitleControl\">\r\n                      <mat-error *ngIf=\"PeopleContributionForm.hasError('minLength')\">\r\n                       <p>Title should be minimum of 10</p>\r\n                       </mat-error>\r\n                      </mat-form-field >\r\n                      <mat-form-field class=\"example-full-width\">\r\n                      <textarea matInput #message placeholder=\"Course Description\" maxlength=\"256\" required formControlName=\"Descriptioncntrl\"></textarea>\r\n                      <mat-hint align=\"end\">{{message.value.length}} / 256</mat-hint>\r\n                       </mat-form-field>\r\n                       <mat-form-field class=\"example-full-width\">\r\n                        <textarea matInput #prerequisite placeholder=\"Prerequisite\" maxlength=\"150\" required formControlName=\"Prerequisitecntrl\"></textarea>\r\n                        <mat-hint align=\"end\">{{message.value.length}} / 150</mat-hint>\r\n                         </mat-form-field>\r\n                      <mat-form-field class=\"example-full-width\">\r\n                        <mat-select placeholder=\"SkillType\" formControlName=\"trngSkillType\">\r\n                          <mat-option value=\"NewIT\">New IT</mat-option>\r\n                          <mat-option value=\"Domain\">Domain</mat-option>\r\n                          <mat-option value=\"Technical\">Technical</mat-option>\r\n                          <mat-option value=\"SoftSkill\">SoftSkill</mat-option>\r\n                        </mat-select>\r\n                      </mat-form-field>                    \r\n                              \r\n                              <mat-form-field class=\"example-full-width\" >\r\n                                <mat-select placeholder=\"TrainingType\" formControlName=\"trngType\" >\r\n                                  <mat-option value=\"LKM\">LKM</mat-option>\r\n                                  <mat-option value=\"Project\">Project</mat-option>\r\n                                  <mat-option value=\"External\">External</mat-option>          \r\n                                </mat-select>\r\n                                  </mat-form-field>\r\n                           \r\n              </mat-step> \r\n              <mat-step formGroupName=\"1\" [stepControl]=\"formArray.get([1])\"> \r\n                  <ng-template matStepLabel>Training Schedule</ng-template>\r\n              \r\n\r\n                  <mat-form-field class=\"example-full-width\">\r\n                    <textarea matInput #Audience placeholder=\"Target Audience\"  required formControlName=\"TargetAudience\"></textarea>\r\n                  </mat-form-field>\r\n                  \r\n        <mat-form-field class=\"example-full-width\">\r\n          <mat-select placeholder=\"Location\" formControlName=\"trngLocation\">\r\n            <mat-option value=\"CDCC2\">CDC2</mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n        <mat-form-field class=\"example-full-width\">\r\n          <mat-select placeholder=\"TrainingMode\" formControlName=\"trngMode\">\r\n            <mat-option value=\"Lync\">Lync</mat-option>\r\n            <mat-option value=\"ClassRoom\">ClassRoom</mat-option>\r\n            <mat-option value=\"External\">External</mat-option>          \r\n          </mat-select>\r\n        </mat-form-field>\r\n        <mat-form-field class=\"date_wrap\">\r\n          <input matInput  [matDatepickerFilter]=\"myFilter\" [matDatepicker]=\"pickerTo\" placeholder=\"StartDate\" required formControlName=\"TrainingStartdate\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"pickerTo\"></mat-datepicker-toggle>\r\n          <mat-datepicker #pickerTo></mat-datepicker>\r\n          </mat-form-field> \r\n          <mat-form-field >\r\n            <input matInput  [matDatepickerFilter]=\"myFilter\" [matDatepicker]=\"pickerFrom\" placeholder=\"EndDate\" required formControlName=\"trainingEnddate\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"pickerFrom\"></mat-datepicker-toggle>\r\n          <mat-datepicker   #pickerFrom></mat-datepicker>\r\n          </mat-form-field>\r\n                                  \r\n                  <mat-form-field class=\"example-full-width\">\r\n           \r\n                      <input type=\"textArea\" placeholder=\"Comments\" required matInput formControlName=\"Autocomments\">\r\n                      \r\n                    </mat-form-field>\r\n                                    \r\n                    \r\n                  </mat-step> \r\n                \r\n                </mat-horizontal-stepper>\r\n\r\n              <button class=\"submit\" type=\"submit\" formcontrolName=\"submit\" [disabled]=\"!PeopleContributionForm.valid\" mat-raised-button color=\"warn\">SUBMIT</button>\r\n      </form>\r\n        </mat-card>\r\n        </div>\r\n  </div>"

/***/ }),

/***/ "./src/app/add-people-contribution/add-people-contribution.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddPeopleContributionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__utils_checkUserStatus__ = __webpack_require__("./src/app/utils/checkUserStatus.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__progress_dialog_progress_dialog_component__ = __webpack_require__("./src/app/progress-dialog/progress-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__addPeopleContributionService__ = __webpack_require__("./src/app/add-people-contribution/addPeopleContributionService.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var AddPeopleContributionComponent = (function () {
    function AddPeopleContributionComponent(_formBuilder, userinfo, dialog, addPeopleContributionService, httpConfig) {
        this._formBuilder = _formBuilder;
        this.userinfo = userinfo;
        this.dialog = dialog;
        this.addPeopleContributionService = addPeopleContributionService;
        this.httpConfig = httpConfig;
        this.myFilter = function (d) {
            var day = d.getDay();
            var year = d.getFullYear();
            var currentDate = new Date();
            var currentyear = currentDate.getFullYear();
            // Prevent Saturday and Sunday from being selected.
            return day !== 0 && day !== 6 && year > currentyear - 2 && year < currentyear + 2;
        };
        this.getUserInfo();
    }
    AddPeopleContributionComponent.prototype.getUserInfo = function () {
        this.user = JSON.parse(this.userinfo.getUserDetails())[0].username;
    };
    Object.defineProperty(AddPeopleContributionComponent.prototype, "formArray", {
        get: function () { return this.PeopleContributionForm.get('formArray'); },
        enumerable: true,
        configurable: true
    });
    AddPeopleContributionComponent.prototype.onCloseCancel = function () {
    };
    AddPeopleContributionComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var automationStatus;
            return __generator(this, function (_a) {
                this.PeopleContributionForm = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["d" /* FormGroup */]({
                    formArray: this._formBuilder.array([
                        this._formBuilder.group({
                            TitleControl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            Prerequisitecntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            Descriptioncntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            trngSkillType: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            trngType: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                        }),
                        this._formBuilder.group({
                            //trainer:new FormControl('',Validators.required), 
                            trngLocation: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            trngMode: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            TargetAudience: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            TrainingStartdate: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            trainingEnddate: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            Autocomments: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: '', disabled: false }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required)
                        })
                    ])
                });
                this.P2FormGroup = this._formBuilder.group({
                    Autocomments: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: '' })
                });
                automationStatus = '';
                return [2 /*return*/];
            });
        });
    };
    AddPeopleContributionComponent.prototype.onAddPeopleContributionButton = function () {
        var _this = this;
        // login as admin if login type is admin or else as user
        this.openDIalog(1);
        var frmArry = this.PeopleContributionForm.get('formArray');
        var frmArryValues = frmArry.value;
        var peopleContributionData = {
            TrainingName: frmArryValues[0].TitleControl,
            TrainingDescription: frmArryValues[0].Descriptioncntrl,
            TrainingSkillType: frmArryValues[0].trngSkillType,
            TrainingMode: frmArryValues[1].trngMode,
            TrainingType: frmArryValues[0].trngType,
            Location: frmArryValues[1].trngLocation,
            TargetAudience: frmArryValues[1].TargetAudience,
            Prerequisite: frmArryValues[0].Prerequisitecntrl,
            // Contributor:frmArryValues[1].trainer,
            Contributor: this.user,
            TrainingComments: frmArryValues[1].Autocomments,
            TrainingStartdate: frmArryValues[1].TrainingStartdate,
            TrainingEnddate: frmArryValues[1].trainingEnddate,
        };
        console.log(peopleContributionData);
        var peopleContributionSaveURL = this.httpConfig.savePeopleContributionDetails();
        this.addPeopleContributionService.savePeopleContribution(peopleContributionSaveURL, { data: peopleContributionData }).subscribe(function (result) {
            if (result.status) {
                // erase form data and show success gif
                console.log(result.status);
                _this.successGif();
            }
            else {
                console.log(result.message);
            }
        }, function (error) {
            console.log('error', error);
        });
    };
    AddPeopleContributionComponent.prototype.openDIalog = function (code) {
        this.dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_5__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: code } });
    };
    AddPeopleContributionComponent.prototype.successGif = function () {
        var _this = this;
        // first reset form data
        this.closeDialog();
        this.resetForm();
        this.openDIalog(2);
        setTimeout(function () {
            _this.closeDialog();
        }, 2000);
    };
    AddPeopleContributionComponent.prototype.resetForm = function () {
        this.myform.resetForm();
    };
    AddPeopleContributionComponent.prototype.closeDialog = function () {
        this.dialogRef.close();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* FormGroupDirective */]),
        __metadata("design:type", Object)
    ], AddPeopleContributionComponent.prototype, "myform", void 0);
    AddPeopleContributionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-add-people-contribution',
            template: __webpack_require__("./src/app/add-people-contribution/add-people-contribution.component.html"),
            styles: [__webpack_require__("./src/app/add-people-contribution/add-people-contribution.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_4__utils_checkUserStatus__["a" /* CheckUserStatus */],
            __WEBPACK_IMPORTED_MODULE_1__angular_material__["f" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_6__addPeopleContributionService__["a" /* AddPeopleContributionService */],
            __WEBPACK_IMPORTED_MODULE_3__utils_httpConfig__["a" /* HttpCOnfig */]])
    ], AddPeopleContributionComponent);
    return AddPeopleContributionComponent;
}());



/***/ }),

/***/ "./src/app/add-people-contribution/addPeopleContributionService.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddPeopleContributionService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
Fetching Automation Details through Http Calls
*/
var AddPeopleContributionService = (function () {
    function AddPeopleContributionService(httpClient) {
        this.httpClient = httpClient;
    }
    AddPeopleContributionService.prototype.savePeopleContribution = function (URL, data) {
        return this.httpClient.post(URL, data);
    };
    AddPeopleContributionService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], AddPeopleContributionService);
    return AddPeopleContributionService;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-demo-idea-loggin></app-demo-idea-loggin>"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__show_idea_showIdeaService__ = __webpack_require__("./src/app/show-idea/showIdeaService.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils_navServices__ = __webpack_require__("./src/app/utils/navServices.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material_select__ = __webpack_require__("./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material_slider__ = __webpack_require__("./node_modules/@angular/material/esm5/slider.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__dashboard_dashboard_component__ = __webpack_require__("./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_routes__ = __webpack_require__("./src/app/app.routes.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_platform_browser_animations__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__toolbar_toolbar_component__ = __webpack_require__("./src/app/toolbar/toolbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__side_nav_side_nav_component__ = __webpack_require__("./src/app/side-nav/side-nav.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__not_found_not_found_component__ = __webpack_require__("./src/app/not-found/not-found.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__add_ideas_add_ideas_component__ = __webpack_require__("./src/app/add-ideas/add-ideas.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__swimlane_ngx_charts__ = __webpack_require__("./node_modules/@swimlane/ngx-charts/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__swimlane_ngx_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_17__swimlane_ngx_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__landing_page_landing_page_component__ = __webpack_require__("./src/app/landing-page/landing-page.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__add_ideas_idea_logging_idea_logging_component__ = __webpack_require__("./src/app/add-ideas/idea-logging/idea-logging.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__add_ideas_idea_by_month_idea_by_month_component__ = __webpack_require__("./src/app/add-ideas/idea-by-month/idea-by-month.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__add_ideas_trendin_trendin_component__ = __webpack_require__("./src/app/add-ideas/trendin/trendin.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__demo_idea_loggin_demo_idea_loggin_component__ = __webpack_require__("./src/app/demo-idea-loggin/demo-idea-loggin.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__add_ideas_idea_logging_idea_logging_service__ = __webpack_require__("./src/app/add-ideas/idea-logging/idea-logging-service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__progress_dialog_progress_dialog_component__ = __webpack_require__("./src/app/progress-dialog/progress-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__updated_tool_bar_updated_tool_bar_component__ = __webpack_require__("./src/app/updated-tool-bar/updated-tool-bar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__welcome_component_welcome_component_component__ = __webpack_require__("./src/app/welcome-component/welcome-component.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__ideas_ideas_component__ = __webpack_require__("./src/app/ideas/ideas.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ideas_ideas_services__ = __webpack_require__("./src/app/ideas/ideas-services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__image_slider_image_slider_component__ = __webpack_require__("./src/app/image-slider/image-slider.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33_ngx_carousel__ = __webpack_require__("./node_modules/ngx-carousel/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34_hammerjs__ = __webpack_require__("./node_modules/hammerjs/hammer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34_hammerjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_34_hammerjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__show_idea_show_idea_component__ = __webpack_require__("./src/app/show-idea/show-idea.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__utils_checkUserStatus__ = __webpack_require__("./src/app/utils/checkUserStatus.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__login_login_component__ = __webpack_require__("./src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__login_loginServices__ = __webpack_require__("./src/app/login/loginServices.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__show_idea_dataService__ = __webpack_require__("./src/app/show-idea/dataService.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__dashboard_dialog_dashboard_dialog_component__ = __webpack_require__("./src/app/dashboard-dialog/dashboard-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__show_idea_buildDataSource__ = __webpack_require__("./src/app/show-idea/buildDataSource.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__angular_material_autocomplete__ = __webpack_require__("./node_modules/@angular/material/esm5/autocomplete.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__show_idea_productionDataSource__ = __webpack_require__("./src/app/show-idea/productionDataSource.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__utils_excelService__ = __webpack_require__("./src/app/utils/excelService.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__add_automation_add_automation_component__ = __webpack_require__("./src/app/add-automation/add-automation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__automationdashboard_automationdashboard_component__ = __webpack_require__("./src/app/automationdashboard/automationdashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__automationdashboard_automationService__ = __webpack_require__("./src/app/automationdashboard/automationService.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__automationdashdialog_automationdashdialog_component__ = __webpack_require__("./src/app/automationdashdialog/automationdashdialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__add_automation_addautomationservice__ = __webpack_require__("./src/app/add-automation/addautomationservice.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__add_originate_addoriginate__ = __webpack_require__("./src/app/add-originate/addoriginate.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__show_originate_originateDetails__ = __webpack_require__("./src/app/show-originate/originateDetails.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__automation_details_automationdetailservice__ = __webpack_require__("./src/app/automation-details/automationdetailservice.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__show_automation_show_automation_component__ = __webpack_require__("./src/app/show-automation/show-automation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__show_automation_showAutomationService__ = __webpack_require__("./src/app/show-automation/showAutomationService.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__show_people_showPeopleService__ = __webpack_require__("./src/app/show-people/showPeopleService.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__dashboard_dialog_dashboardService__ = __webpack_require__("./src/app/dashboard-dialog/dashboardService.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__innovation_dashboard_innovationDashboardService__ = __webpack_require__("./src/app/innovation-dashboard/innovationDashboardService.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__add_people_contribution_add_people_contribution_component__ = __webpack_require__("./src/app/add-people-contribution/add-people-contribution.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__automation_details_automation_details_component__ = __webpack_require__("./src/app/automation-details/automation-details.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__add_people_contribution_addPeopleContributionService__ = __webpack_require__("./src/app/add-people-contribution/addPeopleContributionService.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__innovation_dashboard_innovation_dashboard_component__ = __webpack_require__("./src/app/innovation-dashboard/innovation-dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__show_people_show_people_component__ = __webpack_require__("./src/app/show-people/show-people.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__training_details_training_details_component__ = __webpack_require__("./src/app/training-details/training-details.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_64__training_details_trainingDetailService__ = __webpack_require__("./src/app/training-details/trainingDetailService.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_65__peopledashboard_peopledashboard_component__ = __webpack_require__("./src/app/peopledashboard/peopledashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_66__peopledashboard_peopleDashBoardService__ = __webpack_require__("./src/app/peopledashboard/peopleDashBoardService.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_67__add_originate_add_originate_component__ = __webpack_require__("./src/app/add-originate/add-originate.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_68__edit_originate_edit_originate_component__ = __webpack_require__("./src/app/edit-originate/edit-originate.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_69__show_originate_show_originate_component__ = __webpack_require__("./src/app/show-originate/show-originate.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_70__originatedashdialog_originatedashdialog_component__ = __webpack_require__("./src/app/originatedashdialog/originatedashdialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_71__originate_details_originate_details_component__ = __webpack_require__("./src/app/originate-details/originate-details.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_72__automationhistorydialog_automationhistorydialog_component__ = __webpack_require__("./src/app/automationhistorydialog/automationhistorydialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_73__originate_comments_view_originate_comments_view_component__ = __webpack_require__("./src/app/originate-comments-view/originate-comments-view.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













// material imports





























































var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_7__dashboard_dashboard_component__["a" /* DashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_10__toolbar_toolbar_component__["a" /* ToolbarComponent */],
                __WEBPACK_IMPORTED_MODULE_14__side_nav_side_nav_component__["a" /* SideNavComponent */],
                __WEBPACK_IMPORTED_MODULE_15__not_found_not_found_component__["a" /* NotFoundComponent */],
                __WEBPACK_IMPORTED_MODULE_16__add_ideas_add_ideas_component__["a" /* AddIdeasComponent */],
                __WEBPACK_IMPORTED_MODULE_21__add_ideas_trendin_trendin_component__["a" /* TrendinComponent */],
                __WEBPACK_IMPORTED_MODULE_18__landing_page_landing_page_component__["a" /* LandingPageComponent */],
                __WEBPACK_IMPORTED_MODULE_19__add_ideas_idea_logging_idea_logging_component__["a" /* IdeaLoggingComponent */],
                __WEBPACK_IMPORTED_MODULE_20__add_ideas_idea_by_month_idea_by_month_component__["a" /* IdeaByMonthComponent */],
                __WEBPACK_IMPORTED_MODULE_22__demo_idea_loggin_demo_idea_loggin_component__["a" /* DemoIdeaLogginComponent */],
                __WEBPACK_IMPORTED_MODULE_27__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */],
                __WEBPACK_IMPORTED_MODULE_28__updated_tool_bar_updated_tool_bar_component__["a" /* UpdatedToolBarComponent */],
                __WEBPACK_IMPORTED_MODULE_29__welcome_component_welcome_component_component__["a" /* WelcomeComponentComponent */],
                __WEBPACK_IMPORTED_MODULE_30__ideas_ideas_component__["a" /* IdeasComponent */],
                __WEBPACK_IMPORTED_MODULE_32__image_slider_image_slider_component__["a" /* ImageSliderComponent */],
                __WEBPACK_IMPORTED_MODULE_35__show_idea_show_idea_component__["a" /* ShowIdeaComponent */],
                __WEBPACK_IMPORTED_MODULE_37__login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_40__dashboard_dialog_dashboard_dialog_component__["a" /* DashboardDialogComponent */],
                __WEBPACK_IMPORTED_MODULE_45__add_automation_add_automation_component__["a" /* AddAutomationComponent */],
                __WEBPACK_IMPORTED_MODULE_46__automationdashboard_automationdashboard_component__["a" /* AutomationdashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_48__automationdashdialog_automationdashdialog_component__["a" /* AutomationdashdialogComponent */],
                __WEBPACK_IMPORTED_MODULE_53__show_automation_show_automation_component__["a" /* ShowAutomationComponent */],
                __WEBPACK_IMPORTED_MODULE_58__add_people_contribution_add_people_contribution_component__["a" /* AddPeopleContributionComponent */],
                __WEBPACK_IMPORTED_MODULE_59__automation_details_automation_details_component__["a" /* AutomationDetailsComponent */],
                __WEBPACK_IMPORTED_MODULE_61__innovation_dashboard_innovation_dashboard_component__["a" /* InnovationDashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_62__show_people_show_people_component__["a" /* ShowPeopleComponent */],
                __WEBPACK_IMPORTED_MODULE_63__training_details_training_details_component__["a" /* TrainingDetailsComponent */],
                __WEBPACK_IMPORTED_MODULE_65__peopledashboard_peopledashboard_component__["a" /* PeopledashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_67__add_originate_add_originate_component__["a" /* AddOriginateComponent */],
                __WEBPACK_IMPORTED_MODULE_68__edit_originate_edit_originate_component__["a" /* EditOriginateComponent */],
                __WEBPACK_IMPORTED_MODULE_69__show_originate_show_originate_component__["a" /* ShowOriginateComponent */],
                __WEBPACK_IMPORTED_MODULE_70__originatedashdialog_originatedashdialog_component__["a" /* OriginatedashdialogComponent */],
                __WEBPACK_IMPORTED_MODULE_71__originate_details_originate_details_component__["a" /* OriginateDetailsComponent */],
                __WEBPACK_IMPORTED_MODULE_72__automationhistorydialog_automationhistorydialog_component__["a" /* AutomationhistorydialogComponent */],
                __WEBPACK_IMPORTED_MODULE_73__originate_comments_view_originate_comments_view_component__["a" /* OriginateCommentsViewComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["BrowserModule"],
                __WEBPACK_IMPORTED_MODULE_8__app_routes__["a" /* appRouteModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_material__["b" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_material__["l" /* MatMenuModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_material__["r" /* MatTableModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_material__["t" /* MatToolbarModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_material__["i" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_material__["c" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_material__["p" /* MatSidenavModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_material__["k" /* MatListModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_material__["j" /* MatInputModule */],
                __WEBPACK_IMPORTED_MODULE_12__angular_forms__["f" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_11__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_12__angular_forms__["k" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_17__swimlane_ngx_charts__["NgxChartsModule"],
                __WEBPACK_IMPORTED_MODULE_42__angular_material_autocomplete__["a" /* MatAutocompleteModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_material__["d" /* MatCheckboxModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_material__["g" /* MatDialogModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_material__["s" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_material__["o" /* MatRadioModule */],
                __WEBPACK_IMPORTED_MODULE_26__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_material__["u" /* MatTooltipModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_material__["n" /* MatProgressSpinnerModule */],
                __WEBPACK_IMPORTED_MODULE_33_ngx_carousel__["a" /* NgxCarouselModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_material__["q" /* MatStepperModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_material__["e" /* MatDatepickerModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material_select__["a" /* MatSelectModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material_slider__["a" /* MatSliderModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_material__["m" /* MatNativeDateModule */]
            ],
            entryComponents: [__WEBPACK_IMPORTED_MODULE_27__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], __WEBPACK_IMPORTED_MODULE_35__show_idea_show_idea_component__["a" /* ShowIdeaComponent */],
                __WEBPACK_IMPORTED_MODULE_40__dashboard_dialog_dashboard_dialog_component__["a" /* DashboardDialogComponent */], __WEBPACK_IMPORTED_MODULE_48__automationdashdialog_automationdashdialog_component__["a" /* AutomationdashdialogComponent */], __WEBPACK_IMPORTED_MODULE_59__automation_details_automation_details_component__["a" /* AutomationDetailsComponent */], __WEBPACK_IMPORTED_MODULE_70__originatedashdialog_originatedashdialog_component__["a" /* OriginatedashdialogComponent */],
                __WEBPACK_IMPORTED_MODULE_63__training_details_training_details_component__["a" /* TrainingDetailsComponent */], __WEBPACK_IMPORTED_MODULE_71__originate_details_originate_details_component__["a" /* OriginateDetailsComponent */], __WEBPACK_IMPORTED_MODULE_72__automationhistorydialog_automationhistorydialog_component__["a" /* AutomationhistorydialogComponent */], __WEBPACK_IMPORTED_MODULE_73__originate_comments_view_originate_comments_view_component__["a" /* OriginateCommentsViewComponent */]],
            providers: [__WEBPACK_IMPORTED_MODULE_25__angular_common__["DatePipe"], __WEBPACK_IMPORTED_MODULE_1__utils_navServices__["a" /* NavService */], __WEBPACK_IMPORTED_MODULE_23__utils_httpConfig__["a" /* HttpCOnfig */], __WEBPACK_IMPORTED_MODULE_24__add_ideas_idea_logging_idea_logging_service__["a" /* IdeasLogginService */], __WEBPACK_IMPORTED_MODULE_26__angular_common_http__["b" /* HttpClientModule */], __WEBPACK_IMPORTED_MODULE_31__ideas_ideas_services__["a" /* IdeasServices */], __WEBPACK_IMPORTED_MODULE_47__automationdashboard_automationService__["a" /* AutomationService */],
                __WEBPACK_IMPORTED_MODULE_36__utils_checkUserStatus__["a" /* CheckUserStatus */], __WEBPACK_IMPORTED_MODULE_38__login_loginServices__["a" /* LoginService */], __WEBPACK_IMPORTED_MODULE_0__show_idea_showIdeaService__["a" /* ShowIdeaService */], __WEBPACK_IMPORTED_MODULE_39__show_idea_dataService__["a" /* MatTableDataService */], __WEBPACK_IMPORTED_MODULE_51__show_originate_originateDetails__["a" /* OriginateService */],
                __WEBPACK_IMPORTED_MODULE_41__show_idea_buildDataSource__["a" /* BuildDataSource */], __WEBPACK_IMPORTED_MODULE_43__show_idea_productionDataSource__["a" /* ProductionDataSource */], __WEBPACK_IMPORTED_MODULE_44__utils_excelService__["a" /* ExcelService */], __WEBPACK_IMPORTED_MODULE_49__add_automation_addautomationservice__["a" /* AddAutomationService */],
                __WEBPACK_IMPORTED_MODULE_54__show_automation_showAutomationService__["a" /* ShowAutomatonService */], __WEBPACK_IMPORTED_MODULE_52__automation_details_automationdetailservice__["a" /* AutomationDetailService */], __WEBPACK_IMPORTED_MODULE_64__training_details_trainingDetailService__["a" /* TrainingDetailService */], __WEBPACK_IMPORTED_MODULE_50__add_originate_addoriginate__["a" /* AddOriginateService */],
                __WEBPACK_IMPORTED_MODULE_60__add_people_contribution_addPeopleContributionService__["a" /* AddPeopleContributionService */], __WEBPACK_IMPORTED_MODULE_56__dashboard_dialog_dashboardService__["a" /* DashBoardService */], __WEBPACK_IMPORTED_MODULE_57__innovation_dashboard_innovationDashboardService__["a" /* innovationDashBoardService */],
                __WEBPACK_IMPORTED_MODULE_55__show_people_showPeopleService__["a" /* ShowPeopleService */], __WEBPACK_IMPORTED_MODULE_66__peopledashboard_peopleDashBoardService__["a" /* PeopleDashboardService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routes.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return appRouteModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__landing_page_landing_page_component__ = __webpack_require__("./src/app/landing-page/landing-page.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__login_login_component__ = __webpack_require__("./src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_ideas_idea_logging_idea_logging_component__ = __webpack_require__("./src/app/add-ideas/idea-logging/idea-logging.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__add_ideas_add_ideas_component__ = __webpack_require__("./src/app/add-ideas/add-ideas.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__add_ideas_trendin_trendin_component__ = __webpack_require__("./src/app/add-ideas/trendin/trendin.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__welcome_component_welcome_component_component__ = __webpack_require__("./src/app/welcome-component/welcome-component.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ideas_ideas_component__ = __webpack_require__("./src/app/ideas/ideas.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__add_automation_add_automation_component__ = __webpack_require__("./src/app/add-automation/add-automation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__automationdashboard_automationdashboard_component__ = __webpack_require__("./src/app/automationdashboard/automationdashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__show_automation_show_automation_component__ = __webpack_require__("./src/app/show-automation/show-automation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__add_people_contribution_add_people_contribution_component__ = __webpack_require__("./src/app/add-people-contribution/add-people-contribution.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__show_people_show_people_component__ = __webpack_require__("./src/app/show-people/show-people.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__peopledashboard_peopledashboard_component__ = __webpack_require__("./src/app/peopledashboard/peopledashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__innovation_dashboard_innovation_dashboard_component__ = __webpack_require__("./src/app/innovation-dashboard/innovation-dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__add_originate_add_originate_component__ = __webpack_require__("./src/app/add-originate/add-originate.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__edit_originate_edit_originate_component__ = __webpack_require__("./src/app/edit-originate/edit-originate.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__show_originate_show_originate_component__ = __webpack_require__("./src/app/show-originate/show-originate.component.ts");


















var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__landing_page_landing_page_component__["a" /* LandingPageComponent */]
    },
    {
        path: 'ideaLogging',
        component: __WEBPACK_IMPORTED_MODULE_6__welcome_component_welcome_component_component__["a" /* WelcomeComponentComponent */]
    },
    {
        path: 'login',
        component: __WEBPACK_IMPORTED_MODULE_1__login_login_component__["a" /* LoginComponent */]
    },
    {
        path: 'trending',
        component: __WEBPACK_IMPORTED_MODULE_5__add_ideas_trendin_trendin_component__["a" /* TrendinComponent */]
    },
    {
        path: 'ideaLogging',
        component: __WEBPACK_IMPORTED_MODULE_2__add_ideas_idea_logging_idea_logging_component__["a" /* IdeaLoggingComponent */]
    },
    {
        path: 'ideas',
        component: __WEBPACK_IMPORTED_MODULE_7__ideas_ideas_component__["a" /* IdeasComponent */]
    },
    {
        path: '404',
        component: __WEBPACK_IMPORTED_MODULE_6__welcome_component_welcome_component_component__["a" /* WelcomeComponentComponent */]
    },
    {
        path: 'addIdeas',
        component: __WEBPACK_IMPORTED_MODULE_3__add_ideas_add_ideas_component__["a" /* AddIdeasComponent */]
    },
    {
        path: 'addAutomation',
        component: __WEBPACK_IMPORTED_MODULE_8__add_automation_add_automation_component__["a" /* AddAutomationComponent */]
    },
    {
        path: 'automationDashboard',
        component: __WEBPACK_IMPORTED_MODULE_9__automationdashboard_automationdashboard_component__["a" /* AutomationdashboardComponent */]
    },
    {
        path: 'peopleDashboard',
        component: __WEBPACK_IMPORTED_MODULE_13__peopledashboard_peopledashboard_component__["a" /* PeopledashboardComponent */]
    },
    {
        path: 'innovationDashboard',
        component: __WEBPACK_IMPORTED_MODULE_14__innovation_dashboard_innovation_dashboard_component__["a" /* InnovationDashboardComponent */]
    },
    {
        path: 'showAutomation',
        component: __WEBPACK_IMPORTED_MODULE_10__show_automation_show_automation_component__["a" /* ShowAutomationComponent */]
    },
    {
        path: 'addPeopleContribution',
        component: __WEBPACK_IMPORTED_MODULE_11__add_people_contribution_add_people_contribution_component__["a" /* AddPeopleContributionComponent */]
    },
    {
        path: 'showPeopleContribution',
        component: __WEBPACK_IMPORTED_MODULE_12__show_people_show_people_component__["a" /* ShowPeopleComponent */]
    },
    {
        path: 'originateLogging',
        component: __WEBPACK_IMPORTED_MODULE_15__add_originate_add_originate_component__["a" /* AddOriginateComponent */]
    },
    {
        path: 'originate',
        component: __WEBPACK_IMPORTED_MODULE_16__edit_originate_edit_originate_component__["a" /* EditOriginateComponent */]
    },
    {
        path: 'originateDashboard',
        component: __WEBPACK_IMPORTED_MODULE_17__show_originate_show_originate_component__["a" /* ShowOriginateComponent */]
    },
    {
        path: '**',
        redirectTo: '/404'
    }
];
var appRouteModule = __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* RouterModule */].forRoot(routes);


/***/ }),

/***/ "./src/app/automation-details/automation-details.component.css":
/***/ (function(module, exports) {

module.exports = ".formWrapper{\r\n    margin: 90px 0px 0px 0px;\r\n    font-size: 16px;\r\n}\r\n\r\n.example-full-width {\r\n    width: 100%;\r\n  }\r\n\r\n.submit{\r\n      margin-top: 10px;\r\n  }\r\n\r\n.unedit\r\n  {\r\n      font-style:bold;\r\n      color: grey;\r\n      \r\n  }\r\n\r\n.date_wrap {\r\n    width: 50%;\r\n  }\r\n\r\nimg {\r\n    border: 1px solid #ddd;\r\n    border-radius: 4px;\r\n    padding: 5px;\r\n    width: 1000px;\r\n}"

/***/ }),

/***/ "./src/app/automation-details/automation-details.component.html":
/***/ (function(module, exports) {

module.exports = "    <form [formGroup]=\"AutomationForm\"  style=\"max-width: 40vw;\">  \r\n      <mat-horizontal-stepper formArrayName=\"formArray\" linear>\r\n        <mat-step formGroupName=\"0\" [stepControl]=\"formArray.get([0])\">\r\n        <ng-template matStepLabel>Basic Info</ng-template>\r\n        <mat-form-field class=\"example-full-width\">        \r\n          <input type=\"text\" autocomplete=\"off\" placeholder=\"Tower\" required matInput  placeholder=\"Tower\" type=\"text\" disabled=true [value]=\"automationdata.automationTower\" formControlName=\"TowerControl\">\r\n        </mat-form-field>\r\n        <mat-form-field class=\"example-full-width\">\r\n          <input type=\"text\" autocomplete=\"off\" placeholder=\"Application\" required matInput  type=\"text\" disabled=true [value]=\"automationdata.automationApplication\" formControlName=\"AutoAppcntrl\">\r\n        </mat-form-field>\r\n        <mat-form-field class=\"example-full-width\">\r\n          <input class=\"unedit\" autocomplete=\"off\" type=\"text\"  placeholder=\"Title\" required matInput  placeholder=\"Title\" type=\"text\" disabled=true [value]=\"automationdata.automationTitle\" formControlName=\"AutoTitlecntrl\">\r\n        </mat-form-field>\r\n        <mat-form-field class=\"example-full-width\">\r\n          <input type=\"text\" autocomplete=\"off\" placeholder=\"Description\" required matInput   type=\"text\" disabled=true [value]=\"automationdata.automationDescription\" formControlName=\"AutoDescriptioncntrl\">\r\n        </mat-form-field>\r\n        <mat-form-field class=\"example-full-width\">\r\n          <input type=\"text\" autocomplete=\"off\" placeholder=\"AutomationOwner\" required matInput   type=\"text\" disabled=true [value]=\"automationdata.automationCreatedBy\" formControlName=\"Autoownercntrl\">\r\n        </mat-form-field>\r\n        <mat-form-field class=\"example-full-width\">\r\n          <mat-select placeholder=\"Status\"  [disabled]=\"!cntrlenable\" [(ngModel)]=\"automationdata.automationStatus\"  required formControlName=\"automationStatus\">\r\n            <mat-option *ngFor=\"let status of automationStatus\" [value]=\"status.item\">\r\n            {{status.value}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n        <mat-form-field class=\"date_wrap\">\r\n          <input  matInput  [matDatepickerFilter]=\"myFilter\"  [matDatepicker]=\"pickerTo\" placeholder=\"AutomationStartDate\" disabled=true   [(ngModel)]=\"automationdata.automationDate\"  formControlName=\"Automationdatecntrl\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"pickerTo\" ></mat-datepicker-toggle>\r\n          <mat-datepicker #pickerTo></mat-datepicker>\r\n        </mat-form-field> \r\n        <mat-form-field >\r\n          <input matInput [matDatepickerFilter]=\"myFilter\"  [matDatepicker]=\"pickerFrom\" placeholder=\"CompletionDate\"  [(ngModel)]=\"automationdata.CompletionDate\"  required formControlName=\"Automationexpecteddatecntrl\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"pickerFrom\"></mat-datepicker-toggle>\r\n          <mat-datepicker   #pickerFrom></mat-datepicker>\r\n        </mat-form-field> \r\n        <mat-form-field class=\"example-full-width\">\r\n            <input autocomplete=\"off\" type=\"textArea\" placeholder=\"Automation Lead\" [disabled]=\"!cntrlenable\"   required matInput     [(ngModel)]=\"automationdata.automationLead\" formControlName=\"AutoLeadCCntrl\">                  \r\n          </mat-form-field>\r\n        <mat-form-field class=\"example-full-width\">\r\n          <input autocomplete=\"off\" type=\"textArea\" placeholder=\"Automation Resource\" [disabled]=\"!cntrlenable\"   required matInput     [(ngModel)]=\"automationdata.automationResource\" formControlName=\"AutoResourceCCntrl\">                  \r\n        </mat-form-field>\r\n      </mat-step>\r\n      <mat-step formGroupName=\"1\" [stepControl]=\"formArray.get([1])\"> \r\n      <ng-template matStepLabel>Benefits Info</ng-template>\r\n      <mat-form-field style=\"width: 40%\">\r\n        <input autocomplete=\"off\" matInput type=\"number\" placeholder=\"Effort Reduction\" [disabled]=\"!cntrlenable\" required [(ngModel)]=\"automationdata.automationEfforts\"   formControlName=\"effort\">\r\n        <mat-error>\r\n          Effort Reduction is mandatory (in Hrs / Year)\r\n        </mat-error>\r\n      </mat-form-field>\r\n      <mat-form-field >\r\n        <input autocomplete=\"off\" matInput type=\"number\" placeholder=\"Implementation Effort\" [disabled]=\"!cntrlenable\"  required [(ngModel)]=\"automationdata.automationImplementation\"  formControlName=\"implementation\">\r\n        <mat-error>\r\n          Implementation Effort is mandatory (in Hrs / Year)\r\n        </mat-error>\r\n      </mat-form-field>\r\n      <mat-form-field style=\"width: 40%\">\r\n        <input autocomplete=\"off\" matInput type=\"number\" placeholder=\"FTE Savings\" [disabled]=\"!cntrlenable\"  required [(ngModel)]=\"automationdata.automationFTE\"  formControlName=\"FTE\">\r\n        <mat-error>\r\n          FTE Savings\r\n        </mat-error>\r\n      </mat-form-field>\r\n      <mat-form-field >\r\n        <input autocomplete=\"off\" matInput type=\"number\" placeholder=\"Ticket Reduction / Year\" [disabled]=\"!cntrlenable\"  required [(ngModel)]=\"automationdata.automationticket\"  formControlName=\"ticket\">\r\n        <mat-error>\r\n          Ticket reductions / Year\r\n        </mat-error>\r\n      </mat-form-field>\r\n      <mat-form-field style=\"width: 40%\">\r\n        <input autocomplete=\"off\" matInput type=\"number\" placeholder=\"Cost Saving Per Day ($)\" [disabled]=\"!cntrlenable\"  required [(ngModel)]=\"automationdata.automationcostperday\"  formControlName=\"costperday\">\r\n        <mat-error>\r\n          Cost Savings Per Day ( in $)\r\n        </mat-error>\r\n      </mat-form-field>\r\n      <mat-form-field >\r\n        <input autocomplete=\"off\" matInput type=\"number\" placeholder=\"Net Cost Saving\" [disabled]=\"!cntrlenable\"  required [(ngModel)]=\"automationdata.automationnetcostsaving\"  formControlName=\"netcostsaving\">\r\n        <mat-error>\r\n          Net Cost Savings Per Year ( in $)\r\n        </mat-error>\r\n      </mat-form-field>\r\n      <mat-form-field class=\"example-full-width\">\r\n        <textarea matInput autocomplete=\"off\" #message maxlength=\"256\" required formControlName=\"Autocomments\"  [disabled]=\"!cntrlenable\" required [value]=\"automationdata.automationComments\" placeholder=\"Comments\"></textarea>\r\n        <mat-hint align=\"end\">{{message.value.length}} / 256</mat-hint>\r\n        <mat-error *ngIf=\"AutomationForm.hasError('minlength',['Autocomments'])\">\r\n          <p>Comments should be between 10 to 256 </p>\r\n        </mat-error>\r\n        <button mat-stroked-button color=\"primary\" (click)=\"OnShowHistory()\">Show History</button>\r\n      </mat-form-field>\r\n      <mat-form-field class=\"example-full-width\">\r\n        <mat-select placeholder=\"Work Type\" [disabled]=\"!cntrlenable\" [(ngModel)]=\"automationdata.automationWorkType\" required formControlName=\"workType\">\r\n          <mat-option *ngFor=\"let workType of workTypes\" [value]=\"workType.item\">\r\n            {{workType.value}}\r\n          </mat-option>\r\n        </mat-select>\r\n      </mat-form-field> \r\n      <mat-form-field class=\"example-full-width\">\r\n        <mat-select placeholder=\"Benefit Area\" [disabled]=\"!cntrlenable\" [(ngModel)]=\"automationdata.automationBenefitArea\" required formControlName=\"benefitArea\">\r\n          <mat-option *ngFor=\"let benefitArea of benefitAreas\" [value]=\"benefitArea.item\">\r\n            {{benefitArea.value}}\r\n          </mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n      <mat-form-field class=\"example-full-width\">\r\n        <mat-select placeholder=\"Initiative Category\" [disabled]=\"!cntrlenable\" [(ngModel)]=\"automationdata.automationInitiativeCat\" required formControlName=\"initiative\">\r\n          <mat-option *ngFor=\"let initiative of initiativeCat\" [value]=\"initiative.item\">\r\n            {{initiative.value}}\r\n          </mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n      <mat-form-field class=\"example-full-width\">\r\n        <mat-select placeholder=\"Tool Category\" [disabled]=\"!cntrlenable\" [(ngModel)]=\"automationdata.automationTool\" required formControlName=\"methodology\">\r\n          <mat-option *ngFor=\"let methodology of methodologies\" [value]=\"methodology.item\">\r\n            {{methodology.value}}\r\n          </mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n      <div style=\"margin-bottom: 1em\">\r\n        <button *ngIf=\"isAdmin||createUser\"  type=\"submit\" \r\n        [disabled]=\"!AutomationForm.valid\" class=\"btn btn-success\" (click)=\"onSaveAutomationButton()\">Save</button>\r\n      </div> \r\n      </mat-step>\r\n      </mat-horizontal-stepper>\r\n    </form>\r\n  \r\n"

/***/ }),

/***/ "./src/app/automation-details/automation-details.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutomationDetailsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__progress_dialog_progress_dialog_component__ = __webpack_require__("./src/app/progress-dialog/progress-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__automationhistorydialog_automationhistorydialog_component__ = __webpack_require__("./src/app/automationhistorydialog/automationhistorydialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__utils_checkUserStatus__ = __webpack_require__("./src/app/utils/checkUserStatus.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__automationdetailservice__ = __webpack_require__("./src/app/automation-details/automationdetailservice.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_util__ = __webpack_require__("./node_modules/util/util.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_util___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_util__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









var AutomationDetailsComponent = (function () {
    function AutomationDetailsComponent(_formBuilder, userinfo, dialog, httpConfig, automationdetailservice, data) {
        this._formBuilder = _formBuilder;
        this.userinfo = userinfo;
        this.dialog = dialog;
        this.httpConfig = httpConfig;
        this.automationdetailservice = automationdetailservice;
        this.data = data;
        this.isAdmin = false;
        this.createUser = false;
        this.cntrlenable = false;
        this.automationStatus = [];
        this.workTypes = [];
        this.benefitAreas = [];
        this.initiativeCat = [];
        this.methodologies = [];
        /* rebuildForm()
           {
             this.AutomationForm.controls.['Autocomments'].valueChanges.subscribe((value) => {
              console.log(value);
             });
            
           }
           */
        this.myFilter = function (d) {
            var day = d.getDay();
            var year = d.getFullYear();
            var currentDate = new Date();
            var currentyear = currentDate.getFullYear();
            // Prevent Saturday and Sunday from being selected.
            return day !== 0 && day !== 6 && year > currentyear - 2 && year < currentyear + 2;
        };
        this.getUserInfo();
        this.automationdata = data;
        this.checkuser();
        //this.statval=data.autoStatus;
        console.log(data);
    }
    Object.defineProperty(AutomationDetailsComponent.prototype, "formArray", {
        get: function () { return this.AutomationForm.get('formArray'); },
        enumerable: true,
        configurable: true
    });
    AutomationDetailsComponent.prototype.ngOnChanges = function () {
    };
    AutomationDetailsComponent.prototype.getUserInfo = function () {
        this.user = JSON.parse(this.userinfo.getUserDetails())[0].username;
    };
    AutomationDetailsComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var automationStatus, automationEntityData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.AutomationForm = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["d" /* FormGroup */]({
                            formArray: this._formBuilder.array([
                                this._formBuilder.group({
                                    TowerControl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.automationTower, disabled: true }),
                                    AutoAppcntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.automationApplication, disabled: true }),
                                    AutoTitlecntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.automationTitle, disabled: true }),
                                    AutoDescriptioncntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.automationDescription, disabled: true }),
                                    automationStatus: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.automationStatus }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    AutoLeadCCntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.automationLead, disabled: !this.cntrlenable }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    AutoResourceCCntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.automationResource, disabled: !this.cntrlenable }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    Automationdatecntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.automationDate, disabled: true }),
                                    Automationexpecteddatecntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.CompletionDate, disabled: !this.cntrlenable }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    // Autocreatedatecntrl:new FormControl({value:'',disabled:true}),
                                    Autoownercntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.automationCreatedBy, disabled: true })
                                }),
                                this._formBuilder.group({
                                    effort: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.automationEfforts, disabled: !this.cntrlenable }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].minLength(1), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].maxLength(4),
                                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].min(1), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].max(9999)])),
                                    implementation: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.automationImplementation, disabled: !this.cntrlenable }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].minLength(1), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].maxLength(4),
                                        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].min(1), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].max(9999)])),
                                    ticket: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.automationticket, disabled: !this.cntrlenable }, null),
                                    FTE: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.automationFTE, disabled: !this.cntrlenable }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    costperday: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.automationcostperday, disabled: !this.cntrlenable }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    netcostsaving: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.automationnetcostsaving, disabled: !this.cntrlenable }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    Autocomments: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.automationComments, disabled: !this.cntrlenable }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    // Automationdatecntrl:new FormControl((new Date()).toISOString()),
                                    methodology: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.automationTool, disabled: !this.cntrlenable }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    initiative: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.automationInitiativeCat, disabled: !this.cntrlenable }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    benefitArea: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.automationBenefitArea, disabled: !this.cntrlenable }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                    workType: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.automationdata.automationWorkType }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                                })
                            ])
                        });
                        this.automationBasicFormGroup = this._formBuilder.group({
                            TowerControl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: '', disabled: true }),
                            AutoAppcntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: '', disabled: true }),
                            AutoTitlecntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: '', disabled: true }),
                            AutoDescriptioncntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: '', disabled: true }),
                            Autocreatedatecntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: '', disabled: true }),
                            AutoLeadCCntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: '', disabled: !this.cntrlenable }),
                            AutoResourceCCntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: '', disabled: !this.cntrlenable }),
                            Automationdatecntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: '', disabled: !this.cntrlenable }),
                            Automationexpecteddatecntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: '', disabled: !this.cntrlenable }),
                            Autoownercntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: '', disabled: true })
                        });
                        this.automationDetailFormGroup = this._formBuilder.group({
                            //  effort: new FormControl('', Validators.compose([Validators.minLength(2), Validators.maxLength(4),
                            //  Validators.min(10), Validators.max(9999)])),
                            // implementation: new FormControl('', Validators.compose([Validators.minLength(2), Validators.maxLength(4),
                            // Validators.min(10), Validators.max(9999)])),
                            automationDetailFormGroup: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: '' }),
                            workType: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            methodology: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            initiative: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                            benefitArea: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required)
                        });
                        automationStatus = '';
                        return [4 /*yield*/, this.FetchAutomationStatus()];
                    case 1:
                        automationStatus = _a.sent();
                        this.PopulateStatus(automationStatus);
                        automationEntityData = '';
                        return [4 /*yield*/, this.FetchAutomationEntity()];
                    case 2:
                        automationEntityData = _a.sent();
                        console.log("Entity");
                        console.log(automationEntityData);
                        this.PopulateDropdowns(automationEntityData);
                        return [2 /*return*/];
                }
            });
        });
    };
    AutomationDetailsComponent.prototype.FetchAutomationEntity = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var automationDetailsURL = _this.httpConfig.getautomationEntity();
            console.log(automationDetailsURL);
            _this.automationdetailservice.automationEntity(automationDetailsURL).subscribe(function (result) {
                if (result.status) {
                    (result.status ? resolve(result.data) : reject(result));
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    AutomationDetailsComponent.prototype.PopulateDropdowns = function (dropdownResults) {
        var _this = this;
        console.log(dropdownResults);
        var serverData = [];
        dropdownResults.forEach(function (element) {
            serverData.push({
                item: element.ID,
                value: element.Value,
                Type: element.Type
            });
        });
        serverData.forEach(function (element) {
            if (element.Type === 'automationWorkType') {
                _this.workTypes.push(element);
            }
            if (element.Type === 'automationInitiativeCat') {
                _this.initiativeCat.push(element);
            }
            if (element.Type === 'automationTool') {
                _this.methodologies.push(element);
            }
            if (element.Type === 'automationBenefitArea') {
                _this.benefitAreas.push(element);
            }
        });
    };
    AutomationDetailsComponent.prototype.FetchAutomationStatus = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var automationDetailsURL = _this.httpConfig.getAutomationStatus();
            _this.automationdetailservice.automationStatus(automationDetailsURL).subscribe(function (result) {
                if (result.status) {
                    (result.status ? resolve(result.data) : reject(result));
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    AutomationDetailsComponent.prototype.PopulateStatus = function (dropdownResults) {
        console.log(dropdownResults);
        var serverData = [];
        dropdownResults.forEach(function (element) {
            serverData.push({
                item: element.ID,
                value: element.Value
            });
        });
        this.automationStatus = serverData;
    };
    AutomationDetailsComponent.prototype.onSaveAutomationButton = function () {
        var _this = this;
        this.openDIalog(1);
        var frmArry = this.AutomationForm.get('formArray');
        console.log(frmArry);
        var frmArryValues = frmArry.value;
        var automationupdatedData = {
            comment: frmArryValues[1].Autocomments,
            status: frmArryValues[0].automationStatus,
            lead: frmArryValues[0].AutoLeadCCntrl,
            resource: frmArryValues[0].AutoResourceCCntrl,
            creationdate: frmArryValues[0].Automationdatecntrl,
            completiondate: frmArryValues[0].Automationexpecteddatecntrl,
            automationWorkType: frmArryValues[1].workType,
            automationBenefitArea: frmArryValues[1].benefitArea,
            automationInitiativeCat: frmArryValues[1].initiative,
            automationTool: frmArryValues[1].methodology,
            automationEffort: frmArryValues[1].effort,
            automationImplementation: frmArryValues[1].implementation,
            automationFTE: frmArryValues[1].FTE,
            automationcostperday: frmArryValues[1].costperday,
            automationnetcostsaving: frmArryValues[1].netcostsaving,
            automationticket: Object(__WEBPACK_IMPORTED_MODULE_8_util__["isNull"])(frmArryValues[1].ticket) ? 0 : frmArryValues[1].ticket,
            automationModifiedUser: this.user,
            id: this.automationdata.automationID
        };
        console.log(automationupdatedData);
        var automationsubmitUrl = this.httpConfig.updateAutomation();
        console.log(automationsubmitUrl);
        this.automationdetailservice.updateAutomation(automationsubmitUrl, { data: automationupdatedData }).subscribe(function (result) {
            if (result.status) {
                console.log(result.status);
                _this.successGif();
            }
            else {
                console.log(result.message);
            }
        }, function (error) {
            console.log('error', error);
        });
    };
    AutomationDetailsComponent.prototype.OnShowHistory = function () {
        var _this = this;
        this.showDialog();
        var automationdetail = this.httpConfig.ViewAutomationCommentsHistor();
        this.automationdetailservice.getAutomationHistory(automationdetail, { id: this.automationdata.automationID }).subscribe(function (result) {
            _this.hideDialog();
            if (result.status) {
                console.log(result.data);
                if (result.data.length > 0) {
                    _this.dialogRef = _this.dialog.open(__WEBPACK_IMPORTED_MODULE_4__automationhistorydialog_automationhistorydialog_component__["a" /* AutomationhistorydialogComponent */], {
                        data: result.data, panelClass: 'show-automation'
                    });
                }
                else {
                    _this.dialogRef = _this.dialog.open(__WEBPACK_IMPORTED_MODULE_3__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: 7, message: 'No Comments History Available' } });
                }
            }
        }, function (error) {
            _this.hideDialog();
            console.log(error);
        });
    };
    AutomationDetailsComponent.prototype.showDialog = function () {
        this.dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_3__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: 1 } });
    };
    AutomationDetailsComponent.prototype.hideDialog = function () {
        this.dialogRef.close();
    };
    AutomationDetailsComponent.prototype.openDIalog = function (code) {
        this.dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_3__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: code } });
    };
    AutomationDetailsComponent.prototype.successGif = function () {
        var _this = this;
        // first reset form data
        this.closeDialog();
        this.openDIalog(2);
        setTimeout(function () {
            _this.closeDialog();
        }, 2000);
    };
    AutomationDetailsComponent.prototype.closeDialog = function () {
        this.dialogRef.close();
    };
    AutomationDetailsComponent.prototype.checkuser = function () {
        this.isAdmin = this.userinfo.isUserAdmin();
        if (this.automationdata.automationCreatedBy == this.user) {
            this.createUser = true;
        }
        this.cntrlenable = this.isAdmin || this.createUser;
        //this.cntrlenable=true;
        console.log(this.isAdmin);
        console.log(this.createUser);
        console.log(this.user);
        console.log(this.cntrlenable);
    };
    AutomationDetailsComponent.prototype.getDate = function (date) {
        if (date) {
            var dateOld = new Date(date);
            return dateOld;
        }
    };
    AutomationDetailsComponent.prototype.checkform = function () {
        if (!this.AutomationForm.dirty && !this.AutomationForm.valid) {
            this.btnenable = true;
        }
        else {
            this.btnenable = false;
        }
    };
    AutomationDetailsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-automation-details',
            template: __webpack_require__("./src/app/automation-details/automation-details.component.html"),
            styles: [__webpack_require__("./src/app/automation-details/automation-details.component.css")]
        }),
        __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_5__utils_checkUserStatus__["a" /* CheckUserStatus */],
            __WEBPACK_IMPORTED_MODULE_1__angular_material__["f" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_6__utils_httpConfig__["a" /* HttpCOnfig */],
            __WEBPACK_IMPORTED_MODULE_7__automationdetailservice__["a" /* AutomationDetailService */], Object])
    ], AutomationDetailsComponent);
    return AutomationDetailsComponent;
}());



/***/ }),

/***/ "./src/app/automation-details/automationdetailservice.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutomationDetailService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
Fetching Automation Details through Http Calls
*/
var AutomationDetailService = (function () {
    function AutomationDetailService(httpClient) {
        this.httpClient = httpClient;
    }
    AutomationDetailService.prototype.automationStatus = function (URL) {
        return this.httpClient.get(URL, {});
    };
    AutomationDetailService.prototype.automationEntity = function (URL) {
        return this.httpClient.get(URL, {});
    };
    AutomationDetailService.prototype.updateAutomation = function (URL, data) {
        console.log('Hello', URL);
        console.log('Hello', data);
        return this.httpClient.post(URL, data);
    };
    AutomationDetailService.prototype.getAutomationHistory = function (URL, id) {
        return this.httpClient.post(URL, id);
    };
    AutomationDetailService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], AutomationDetailService);
    return AutomationDetailService;
}());



/***/ }),

/***/ "./src/app/automationdashboard/automationService.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutomationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
Fetching Automation Details through Http Calls
*/
var AutomationService = (function () {
    function AutomationService(httpClient) {
        this.httpClient = httpClient;
    }
    // Fetch Automation Dashboard Details
    AutomationService.prototype.automationDashboard = function (URL) {
        return this.httpClient.get(URL, {});
    };
    AutomationService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], AutomationService);
    return AutomationService;
}());



/***/ }),

/***/ "./src/app/automationdashboard/automationdashboard.component.css":
/***/ (function(module, exports) {

module.exports = ".dashboardpanel\r\n{\r\n    margin:2px 2px;\r\n    padding:2px;\r\n    background: -webkit-gradient(linear, left top, left bottom, from(#e9e9ed), to(#eef0f5));\r\n    background: linear-gradient(#e9e9ed, #eef0f5);\r\n    border: 2px solid rgb(28, 50, 107);\r\n    border-radius: 5px;\r\n}\r\n\r\n.dashboardheader\r\n{\r\n    text-align: center;\r\n    font-size: 22px;\r\n    font-style: italic;\r\n}\r\n\r\n.filterCriteria\r\n{\r\n    border: black 2px solid;\r\n    background-color: aliceblue;\r\n    height: 60px;\r\n}\r\n\r\n.exportCriteria\r\n{\r\n    margin: 5px;    \r\n    height: 50px;    \r\n    padding: 5px;\r\n}\r\n\r\n.SearchAll\r\n{\r\n    float: right;\r\n    margin: 5px;\r\n    padding: 5px;\r\n}\r\n"

/***/ }),

/***/ "./src/app/automationdashboard/automationdashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" style=\"padding:20px\">\r\n  <div class=\"row\">\r\n    <form [formGroup]=\"searchAutomationForm\">\r\n      <div class=\"col-md-3 filterCriteria\">\r\n        <mat-form-field class=\"example-full-width\">\r\n          <mat-select placeholder=\"Initiative Category\" (ngModelChange)=\"OnInitiativeCatChange($event)\" formControlName=\"initiative\">\r\n            <mat-option *ngFor=\"let initiative of initiativeCat\" [value]=\"initiative.value\">\r\n              {{initiative.value}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-3 filterCriteria\">\r\n        <mat-form-field class=\"example-full-width\">\r\n          <mat-select placeholder=\"TOWER\" (ngModelChange)=\"OnTowerChange($event)\" formControlName=\"clientTowerType\">\r\n            <mat-option *ngFor=\"let clientTowerType of clientTowerTypes\" [value]=\"clientTowerType.value\">\r\n              {{clientTowerType.value}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n      <div class=\"col-md-4 filterCriteria\">\r\n        <mat-form-field class=\"example-full-width\">\r\n          <mat-select placeholder=\"Time Period\" (ngModelChange)=\"OnPeriodChange($event)\" formControlName=\"timePeriod\">\r\n            <mat-option *ngFor=\"let time of timeperiod\" [value]=\"time.value\">\r\n              {{time.value}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n        <button mat-button color=\"primary\" class=\"SearchAll\" (click)=\"onSelectAll($event)\">ALL</button>\r\n      </div>\r\n    </form>\r\n  </div>\r\n  <div class=\"row\" style=\"margin:2px\">\r\n    <div class=\"col-md-10 exportCriteria\" >\r\n      <button mat-raised-button color=\"warn\" (click)=\"ClickExcel()\">Export as Excel</button>\r\n      <button mat-raised-button color=\"warn\" (click)=\"ClickPDF()\">Export as PDF</button>\r\n    </div>    \r\n  </div>\r\n  <div  class=\"row\">\r\n    <div class=\"col-md-10 dashboardpanel\"  id=\"TowerArea\">\r\n      <ngx-charts-advanced-pie-chart *ngIf=\"pieChartdata && pieChartdata.length > 0\" [view]=\"view\" [scheme]=\"towerScheme\" [results]=\"pieChartdata\"\r\n        [gradient]=\"gradient\">\r\n      </ngx-charts-advanced-pie-chart>\r\n      <div class=\"dashboardheader\">\r\n        Tower\r\n      </div>\r\n    </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-md-10 dashboardpanel\" id=\"StatusArea\">\r\n        <ngx-charts-advanced-pie-chart *ngIf=\"pieChartStatusdata && pieChartStatusdata.length > 0\" [view]=\"view\" [scheme]=\"colorscheme\"\r\n          [results]=\"pieChartStatusdata\" [gradient]=\"gradient\" (select)=\"onStatusSelect($event)\">\r\n        </ngx-charts-advanced-pie-chart>\r\n        <div class=\"dashboardheader\">\r\n          Status\r\n        </div>\r\n      </div>\r\n    </div>  \r\n    <div class=\"row\">\r\n    <div class=\"col-md-5 dashboardpanel\" id=\"BenefitArea\" >\r\n      <ngx-charts-bar-vertical-stacked *ngIf=\"benefitAreaData && benefitAreaData.length > 0\" [view]=\"BAview\" [scheme]=\"verStacScheme\"\r\n        [results]=\"benefitAreaData\" [gradient]=\"true\" [xAxis]=\"true\" [yAxis]=\"true\" [legend]=\"true\" [legendTitle]=\"BAlegendTitle\"\r\n        (select)=\"onBASelect($event)\">\r\n      </ngx-charts-bar-vertical-stacked>\r\n      <div class=\"dashboardheader\">\r\n        BenefitArea\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-5 dashboardpanel\" id=\"WorkType\">\r\n      <ngx-charts-bar-vertical-2d *ngIf=\"horTypeToolData && horTypeToolData.length > 0\" [view]=\"VTTview\" [scheme]=\"WorkToolScheme\"\r\n        [results]=\"horTypeToolData\" [xAxis]=\"true\" [yAxis]=\"true\" [gradient]=\"false\" [legend]=\"true\" [legendTitle]=\"WTlegentTitle\"\r\n        (select)=\"onWTSelect($event)\">\r\n      </ngx-charts-bar-vertical-2d>\r\n      <div class=\"dashboardheader\">\r\n        WorkType/Tool\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/automationdashboard/automationdashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutomationdashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__automationService__ = __webpack_require__("./src/app/automationdashboard/automationService.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__automationdashdialog_automationdashdialog_component__ = __webpack_require__("./src/app/automationdashdialog/automationdashdialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__add_automation_addautomationservice__ = __webpack_require__("./src/app/add-automation/addautomationservice.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_typescript_linq__ = __webpack_require__("./node_modules/typescript-linq/TS.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_typescript_linq___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_typescript_linq__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__utils_excelService__ = __webpack_require__("./src/app/utils/excelService.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









var AutomationdashboardComponent = (function () {
    function AutomationdashboardComponent(httpConfig, automationService, addAutomationService, _formBuilder, excelService, dialog) {
        this.httpConfig = httpConfig;
        this.automationService = automationService;
        this.addAutomationService = addAutomationService;
        this._formBuilder = _formBuilder;
        this.excelService = excelService;
        this.dialog = dialog;
        this.BAview = [480, 350];
        this.VTTview = [480, 350];
        this.view = [880, 350];
        this.towerWiseServerdata = [];
        this.selectDialogData = [];
        this.clientTowerTypes = [];
        this.initiativeCat = [];
        this.timeperiod = [];
        this.elements = ['TowerArea', 'StatusArea', 'BenefitArea', 'WorkType'];
        this.elCount = -1;
        this.pdf = new jsPDF('l', 'mm', 'a4');
        this.towerName = '';
        this.initiativeCatName = '';
        this.createdDate = '';
        this.sample = new __WEBPACK_IMPORTED_MODULE_7_typescript_linq__["TS"].Collections.List(false);
        this.pieChartdata = [];
        this.pieChartStatusdata = [];
        this.horTypeToolData = [];
        this.benefitAreaData = [];
        this.delayedPieChart = [];
        this.towerWisedata = [];
        this.statusWisedata = [];
        this.benefitWisedata = [];
        this.typeToolData = [];
        this.delayedData = [];
        this.gradient = false;
        this.BAlegendTitle = 'Initiative';
        this.WTlegentTitle = 'Tool';
        this.count = 0;
        this.verStacScheme = {
            domain: ['#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886']
        };
        this.colorscheme = {
            domain: ['#647c8a', '#3f51b5', '#2196f3', '#00b862', '#afdf0a', '#a7b61a', '#f3e562', '#ff9800', '#ff5722', '#ff4514']
        };
        this.towerScheme = {
            domain: ['#A10A28', '#D3342D', '#EF6D49', '#FAAD67', '#FDDE90', '#DBED91', '#A9D770', '#6CBA67', '#2C9653', '#146738']
        };
        this.WorkToolScheme = {
            domain: ['#55C22D', '#C1F33D', '#3CC099', '#AFFFFF', '#8CFC9D', '#76CFFA', '#BA60FB', '#EE6490', '#C42A1C', '#FC9F32']
        };
    }
    AutomationdashboardComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var serverResult, automationEntityData, automationTowerData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        serverResult = '';
                        this.searchAutomationForm = this._formBuilder.group({
                            initiative: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */](),
                            clientTowerType: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */](),
                            timePeriod: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]()
                        });
                        automationEntityData = '';
                        return [4 /*yield*/, this.FetchAutomationEntity()];
                    case 1:
                        automationEntityData = _a.sent();
                        this.PopulateDropdowns(automationEntityData);
                        automationTowerData = '';
                        return [4 /*yield*/, this.FetchAutomationTowers()];
                    case 2:
                        automationTowerData = _a.sent();
                        this.PopulateTowers(automationTowerData);
                        return [4 /*yield*/, this.FetchDetailsServer()];
                    case 3:
                        serverResult = _a.sent();
                        this.PopulateDashboard(serverResult);
                        this.pieChartdata = this.towerWisedata;
                        this.pieChartStatusdata = this.statusWisedata;
                        this.benefitAreaData = this.benefitWisedata;
                        this.horTypeToolData = this.typeToolData;
                        this.delayedPieChart = this.delayedData;
                        return [2 /*return*/];
                }
            });
        });
    };
    AutomationdashboardComponent.prototype.FetchAutomationTowers = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var automationDetailsURL = _this.httpConfig.getAutomationTower();
            _this.addAutomationService.automationTower(automationDetailsURL).subscribe(function (result) {
                if (result.status) {
                    (result.status ? resolve(result.data) : reject(result));
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    AutomationdashboardComponent.prototype.FetchAutomationEntity = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var automationDetailsURL = _this.httpConfig.getautomationEntity();
            _this.addAutomationService.automationEntity(automationDetailsURL).subscribe(function (result) {
                if (result.status) {
                    (result.status ? resolve(result.data) : reject(result));
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    AutomationdashboardComponent.prototype.PopulateTowers = function (dropdownResults) {
        var serverData = [];
        dropdownResults.forEach(function (element) {
            serverData.push({
                item: element.ID,
                value: element.Value
            });
        });
        this.clientTowerTypes = serverData;
    };
    AutomationdashboardComponent.prototype.PopulateDropdowns = function (dropdownResults) {
        var _this = this;
        var serverData = [];
        dropdownResults.forEach(function (element) {
            serverData.push({
                item: element.ID,
                value: element.Value,
                Type: element.Type
            });
        });
        serverData.forEach(function (element) {
            if (element.Type === 'automationInitiativeCat') {
                _this.initiativeCat.push(element);
            }
            if (element.Type === 'TimePeriod') {
                _this.timeperiod.push(element);
            }
        });
    };
    AutomationdashboardComponent.prototype.FetchDetailsServer = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var automationDetailsURL = _this.httpConfig.getAutomationDetails();
            _this.automationService.automationDashboard(automationDetailsURL).subscribe(function (result) {
                if (result.status) {
                    (result.status ? resolve(result.data) : reject(result));
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    AutomationdashboardComponent.prototype.PopulateTowerData = function (data) {
        var _this = this;
        data.forEach(function (element) {
            if (!_this.towerWisedata) {
                _this.towerWisedata.push({
                    name: element.tower,
                    value: 1
                });
            }
            else {
                var findTower = _this.towerWisedata.findIndex(function (ele) { return ele.name === element.tower; });
                if (findTower === -1) {
                    _this.towerWisedata.push({
                        name: element.tower,
                        value: 1
                    });
                }
                else {
                    _this.towerWisedata.find(function (ele) { return ele.name === element.tower; }).value++;
                }
            }
        });
    };
    AutomationdashboardComponent.prototype.PopulateStatusData = function (data) {
        var _this = this;
        // console.log(data);
        data.forEach(function (element) {
            if (!_this.statusWisedata) {
                _this.statusWisedata.push({
                    name: element.status,
                    value: 1
                });
            }
            else {
                var findStatus = _this.statusWisedata.findIndex(function (ele) { return ele.name === element.status; });
                if (findStatus === -1) {
                    _this.statusWisedata.push({
                        name: element.status,
                        value: 1
                    });
                }
                else {
                    _this.statusWisedata.find(function (ele) { return ele.name === element.status; }).value++;
                }
            }
        });
    };
    AutomationdashboardComponent.prototype.PopulateDelayedData = function (element) {
        // console.log(data);
        var status = '';
        var dtDate = new Date();
        var dtCurrentDate = new Date(dtDate).valueOf();
        if ((element.STATUS === 'New' || element.STATUS === 'In Progress') && element.automationDate != null) {
            var dtETCDate = new Date(element.automationDate).valueOf();
            //console.log(dtCurrentDate, " / ", dtETCDate, " / ", element.status);
            if (dtCurrentDate > dtETCDate) {
                status = 'Delayed';
            }
            else {
                status = element.STATUS;
            }
        }
        else {
            status = element.STATUS;
        }
        // console.log(dtCurrentDate, " / ", dtETCDate, " / ", element.status , " / " , status);
        return status;
    };
    AutomationdashboardComponent.prototype.PopulateBenefitData = function (data) {
        var _this = this;
        data.forEach(function (element) {
            if (!_this.benefitWisedata) {
                _this.benefitWisedata.push({
                    name: element.BenefitArea,
                    series: [{
                            name: element.Initiative,
                            value: 1
                        }]
                });
            }
            else {
                var findBenefitArea = _this.benefitWisedata.findIndex(function (ele) { return ele.name === element.BenefitArea; });
                if (findBenefitArea === -1) {
                    _this.benefitWisedata.push({
                        name: element.BenefitArea,
                        series: [{
                                name: element.Initiative,
                                value: 1
                            }]
                    });
                }
                else {
                    var findIncentive = _this.benefitWisedata[findBenefitArea].series.findIndex(function (ele) { return ele.name === element.Initiative; });
                    if (findIncentive === -1) {
                        _this.benefitWisedata[findBenefitArea].series.push({
                            name: element.Initiative,
                            value: 1
                        });
                    }
                    else {
                        _this.benefitWisedata[findBenefitArea].series[findIncentive].value++;
                    }
                }
            }
        });
    };
    AutomationdashboardComponent.prototype.PopulateTypeToolData = function (data) {
        var _this = this;
        // console.log(data);
        data.forEach(function (element) {
            if (!_this.typeToolData) {
                _this.typeToolData.push({
                    name: element.WorkType,
                    series: [{
                            name: element.Tool,
                            value: 1
                        }]
                });
            }
            else {
                var findTypeArea = _this.typeToolData.findIndex(function (ele) { return ele.name === element.WorkType; });
                if (findTypeArea === -1) {
                    _this.typeToolData.push({
                        name: element.WorkType,
                        series: [{
                                name: element.Tool,
                                value: 1
                            }]
                    });
                }
                else {
                    var findTool = _this.typeToolData[findTypeArea].series.findIndex(function (ele) { return ele.name === element.Tool; });
                    if (findTool === -1) {
                        _this.typeToolData[findTypeArea].series.push({
                            name: element.Tool,
                            value: 1
                        });
                    }
                    else {
                        _this.typeToolData[findTypeArea].series[findTool].value++;
                    }
                }
            }
        });
    };
    AutomationdashboardComponent.prototype.PopulateDashboard = function (data) {
        var _this = this;
        this.towerWiseServerdata = [];
        // console.log(data);
        data.forEach(function (element) {
            var statusData = _this.PopulateDelayedData(element);
            _this.towerWiseServerdata.push({
                id: element.automationID,
                tower: element.automationTower,
                application: element.automationApplication,
                title: element.automationTitle,
                description: element.automationDescription,
                efforts: element.automationEfforts,
                implementation: element.automationImplementation,
                FTE: element.automationFTE,
                costperday: element.automationcostperday,
                costsaving: element.automationnetcostsaving,
                ticket: element.automationticket,
                date: element.automationDate,
                status: statusData,
                WorkType: element.WorkType,
                BenefitArea: element.BenefitArea,
                Tool: element.Tool,
                Initiative: element.Initiative,
                AutomationCreationDate: element.automationCreationDate
            });
        });
        this.PopulateTowerData(this.towerWiseServerdata);
        this.PopulateStatusData(this.towerWiseServerdata);
        this.PopulateBenefitData(this.towerWiseServerdata);
        this.PopulateTypeToolData(this.towerWiseServerdata);
        this.PopulateDelayedData(this.towerWiseServerdata);
    };
    AutomationdashboardComponent.prototype.OnTowerChange = function (event) {
        var _this = this;
        if (event !== -1) {
            var selectTowerWisedata_1 = [];
            if (this.initiativeCatName === '' && this.createdDate === '') {
                this.towerWiseServerdata.forEach(function (element) {
                    if (element.tower === event) {
                        selectTowerWisedata_1.push(element);
                    }
                });
            }
            else {
                if (this.createdDate === '') {
                    this.towerWiseServerdata.forEach(function (element) {
                        if (element.tower === event && element.Initiative === _this.initiativeCatName) {
                            selectTowerWisedata_1.push(element);
                        }
                    });
                }
                else if (this.initiativeCatName === '') {
                    this.towerWiseServerdata.forEach(function (element) {
                        var eventEndTime = new Date(element.AutomationCreationDate);
                        var eventStartTime = new Date(_this.createdDate);
                        if (element.tower === event && eventEndTime.valueOf() > eventStartTime.valueOf()) {
                            selectTowerWisedata_1.push(element);
                        }
                    });
                }
                else {
                    this.towerWiseServerdata.forEach(function (element) {
                        var eventEndTime = new Date(element.AutomationCreationDate);
                        var eventStartTime = new Date(_this.createdDate);
                        if (element.tower === event && element.Initiative === _this.initiativeCatName
                            && eventEndTime.valueOf() > eventStartTime.valueOf()) {
                            selectTowerWisedata_1.push(element);
                        }
                    });
                }
            }
            this.pieChartStatusdata = [];
            this.statusWisedata = [];
            this.benefitAreaData = [];
            this.benefitWisedata = [];
            this.typeToolData = [];
            this.towerWisedata = [];
            this.towerName = event;
            this.PopulateTowerData(selectTowerWisedata_1);
            this.PopulateStatusData(selectTowerWisedata_1);
            this.PopulateBenefitData(selectTowerWisedata_1);
            this.PopulateTypeToolData(selectTowerWisedata_1);
            this.pieChartdata = this.towerWisedata;
            this.pieChartStatusdata = this.statusWisedata;
            this.benefitAreaData = this.benefitWisedata;
            this.horTypeToolData = this.typeToolData;
        }
    };
    AutomationdashboardComponent.prototype.OnInitiativeCatChange = function (event) {
        var _this = this;
        if (event !== -1) {
            var selectTowerWisedata_2 = [];
            if (this.towerName === '' && this.createdDate === '') {
                this.towerWiseServerdata.forEach(function (element) {
                    if (element.Initiative === event) {
                        selectTowerWisedata_2.push(element);
                    }
                });
            }
            else {
                if (this.createdDate === '') {
                    this.towerWiseServerdata.forEach(function (element) {
                        if (element.Initiative === event && element.tower === _this.towerName) {
                            selectTowerWisedata_2.push(element);
                        }
                    });
                }
                else {
                    if (this.towerName === '') {
                        this.towerWiseServerdata.forEach(function (element) {
                            var eventEndTime = new Date(element.AutomationCreationDate);
                            var eventStartTime = new Date(_this.createdDate);
                            if (eventEndTime.valueOf() > eventStartTime.valueOf()) {
                                selectTowerWisedata_2.push(element);
                            }
                        });
                    }
                    else {
                        this.towerWiseServerdata.forEach(function (element) {
                            var eventEndTime = new Date(element.AutomationCreationDate);
                            var eventStartTime = new Date(_this.createdDate);
                            if (element.Initiative === event && element.tower === _this.towerName
                                && eventEndTime.valueOf() > eventStartTime.valueOf()) {
                                selectTowerWisedata_2.push(element);
                            }
                        });
                    }
                }
            }
            this.pieChartStatusdata = [];
            this.statusWisedata = [];
            this.benefitAreaData = [];
            this.benefitWisedata = [];
            this.typeToolData = [];
            this.towerWisedata = [];
            this.initiativeCatName = event;
            this.PopulateTowerData(selectTowerWisedata_2);
            this.PopulateStatusData(selectTowerWisedata_2);
            this.PopulateBenefitData(selectTowerWisedata_2);
            this.PopulateTypeToolData(selectTowerWisedata_2);
            this.pieChartdata = this.towerWisedata;
            this.pieChartStatusdata = this.statusWisedata;
            this.benefitAreaData = this.benefitWisedata;
            this.horTypeToolData = this.typeToolData;
        }
    };
    AutomationdashboardComponent.prototype.onSelectAll = function (event) {
        this.pieChartdata = [];
        this.towerWisedata = [];
        this.pieChartStatusdata = [];
        this.statusWisedata = [];
        this.benefitAreaData = [];
        this.benefitWisedata = [];
        this.typeToolData = [];
        this.towerName = '';
        this.initiativeCatName = '';
        this.createdDate = '';
        this.PopulateTowerData(this.towerWiseServerdata);
        this.PopulateStatusData(this.towerWiseServerdata);
        this.PopulateBenefitData(this.towerWiseServerdata);
        this.PopulateTypeToolData(this.towerWiseServerdata);
        this.pieChartdata = this.towerWisedata;
        this.pieChartStatusdata = this.statusWisedata;
        this.benefitAreaData = this.benefitWisedata;
        this.horTypeToolData = this.typeToolData;
        this.searchAutomationForm.controls['initiative'].setValue(-1);
        this.searchAutomationForm.controls['clientTowerType'].setValue(-1);
        this.searchAutomationForm.controls['timePeriod'].setValue(-1);
    };
    AutomationdashboardComponent.prototype.OnPeriodChange = function (event) {
        var _this = this;
        if (event !== -1) {
            var selectTowerWisedata_3 = [];
            var dtDate = new Date();
            if (event === 'Last 3 months') {
                dtDate.setMonth(-3);
            }
            if (event === 'Last 6 months') {
                dtDate.setMonth(-6);
            }
            if (event === 'Last 9 months') {
                dtDate.setMonth(-9);
            }
            if (event === 'Last 12 months') {
                dtDate.setMonth(-12);
            }
            this.createdDate = dtDate.valueOf();
            var eventStartTime_1 = new Date(this.createdDate);
            if (this.towerName === '' && this.initiativeCatName === '') {
                this.towerWiseServerdata.forEach(function (element) {
                    var eventEndTime = new Date(element.AutomationCreationDate);
                    if (eventEndTime.valueOf() > eventStartTime_1.valueOf()) {
                        selectTowerWisedata_3.push(element);
                    }
                });
            }
            else {
                if (this.initiativeCatName === '') {
                    this.towerWiseServerdata.forEach(function (element) {
                        var eventEndTime = new Date(element.AutomationCreationDate);
                        if (eventEndTime.valueOf() > eventStartTime_1.valueOf() && element.tower === _this.towerName) {
                            selectTowerWisedata_3.push(element);
                        }
                    });
                }
                else {
                    if (this.towerName === '') {
                        this.towerWiseServerdata.forEach(function (element) {
                            var eventEndTime = new Date(element.AutomationCreationDate);
                            if (eventEndTime.valueOf() > eventStartTime_1.valueOf() && element.Initiative === _this.initiativeCatName) {
                                selectTowerWisedata_3.push(element);
                            }
                        });
                    }
                    else {
                        this.towerWiseServerdata.forEach(function (element) {
                            var eventEndTime = new Date(element.AutomationCreationDate);
                            //console.log('Sample');
                            //console.log(element.tower, ' And ', this.towerName);
                            //console.log(element.Initiative, ' And ', this.initiativeCatName);
                            //console.log(eventEndTime.valueOf(), ' And ', eventStartTime.valueOf());
                            if (element.tower === _this.towerName && element.Initiative === _this.initiativeCatName
                                && eventEndTime.valueOf() > eventStartTime_1.valueOf()) {
                                selectTowerWisedata_3.push(element);
                            }
                        });
                    }
                }
            }
            this.pieChartStatusdata = [];
            this.statusWisedata = [];
            this.benefitAreaData = [];
            this.benefitWisedata = [];
            this.typeToolData = [];
            this.towerWisedata = [];
            this.PopulateTowerData(selectTowerWisedata_3);
            this.PopulateStatusData(selectTowerWisedata_3);
            this.PopulateBenefitData(selectTowerWisedata_3);
            this.PopulateTypeToolData(selectTowerWisedata_3);
            this.pieChartdata = this.towerWisedata;
            this.pieChartStatusdata = this.statusWisedata;
            this.benefitAreaData = this.benefitWisedata;
            this.horTypeToolData = this.typeToolData;
        }
    };
    AutomationdashboardComponent.prototype.onStatusSelect = function (event) {
        var _this = this;
        // console.log(event.value);
        // console.log(event.name);
        // console.log(this.towerWiseServerdata);
        var selectDialogData = [];
        if (this.towerName === '' && this.initiativeCatName === '') {
            this.towerWiseServerdata.forEach(function (element) {
                if (element.status === event.name) {
                    selectDialogData.push(element);
                }
            });
        }
        else {
            if (this.towerName !== '' && this.initiativeCatName === '') {
                this.towerWiseServerdata.forEach(function (element) {
                    if (element.status === event.name && element.tower === _this.towerName) {
                        selectDialogData.push(element);
                    }
                });
            }
            else {
                this.towerWiseServerdata.forEach(function (element) {
                    if (element.status === event.name && element.tower === _this.towerName &&
                        element.Initiative === _this.initiativeCatName) {
                        selectDialogData.push(element);
                    }
                });
            }
        }
        this.dialog.open(__WEBPACK_IMPORTED_MODULE_5__automationdashdialog_automationdashdialog_component__["a" /* AutomationdashdialogComponent */], {
            data: { detailsData: selectDialogData, type: 'Status' }, panelClass: 'show-idea'
        });
    };
    AutomationdashboardComponent.prototype.onBASelect = function (event) {
        var _this = this;
        var selectDialogData = [];
        console.log(event);
        if (this.towerName === '' && this.initiativeCatName === '') {
            this.towerWiseServerdata.forEach(function (element) {
                if (element.Initiative === event.name && element.BenefitArea === event.series) {
                    selectDialogData.push(element);
                }
            });
        }
        else {
            if (this.towerName !== '' && this.initiativeCatName === '') {
                this.towerWiseServerdata.forEach(function (element) {
                    if (element.tower === _this.towerName && element.Initiative === event.name
                        && element.BenefitArea === event.series) {
                        selectDialogData.push(element);
                    }
                });
            }
            else {
                this.towerWiseServerdata.forEach(function (element) {
                    if (element.tower === _this.towerName && element.Initiative === event.name
                        && element.BenefitArea === event.series && element.Initiative === _this.initiativeCatName) {
                        selectDialogData.push(element);
                    }
                });
            }
        }
        this.dialog.open(__WEBPACK_IMPORTED_MODULE_5__automationdashdialog_automationdashdialog_component__["a" /* AutomationdashdialogComponent */], {
            data: { detailsData: selectDialogData, type: 'BenefitArea' }, panelClass: 'show-idea'
        });
    };
    AutomationdashboardComponent.prototype.onWTSelect = function (event) {
        var _this = this;
        var selectDialogData = [];
        if (this.towerName === '') {
            this.towerWiseServerdata.forEach(function (element) {
                if (element.Tool === event.name && element.WorkType === event.series) {
                    selectDialogData.push(element);
                }
            });
        }
        else {
            this.towerWiseServerdata.forEach(function (element) {
                if (element.Tool === event.name && element.WorkType === event.series &&
                    element.tower === _this.towerName) {
                    selectDialogData.push(element);
                }
            });
        }
        this.dialog.open(__WEBPACK_IMPORTED_MODULE_5__automationdashdialog_automationdashdialog_component__["a" /* AutomationdashdialogComponent */], {
            data: { detailsData: selectDialogData, type: 'WorkType' }, panelClass: 'show-idea'
        });
    };
    AutomationdashboardComponent.prototype.ClickPDF = function () {
        var _this = this;
        // All units are in the set measurement for the document
        // This can be changed to 'pt' (points), 'mm' (Default), 'cm', 'in'
        this.sample = new __WEBPACK_IMPORTED_MODULE_7_typescript_linq__["TS"].Collections.List(false);
        this.towerWiseServerdata.forEach(function (element) {
            _this.sample.add(element);
        });
        this.pdf = new jsPDF('l', 'mm', 'a4');
        this.elCount = -1;
        this.pdf.setFontSize(22);
        this.pdf.setFont('times');
        this.pdf.setFontType('bolditalic');
        this.pdf.text('Automation Report', 150, 10, 'center');
        this.recursiveAddHtml();
    };
    AutomationdashboardComponent.prototype.recursiveAddHtml = function () {
        var _this = this;
        this.elCount++;
        if (this.elCount < this.elements.length) {
            this.pdf.setFontSize(16);
            this.pdf.setFont('helvetica');
            this.pdf.setFontType('italic');
            switch (this.elements[this.elCount]) {
                case 'TowerArea':
                    this.pdf.text("Tower Wise View", 10, 30);
                    break;
                case 'StatusArea':
                    this.pdf.text("Status Wise View", 10, 30);
                    break;
                case 'BenefitArea':
                    this.pdf.text("Benefit Vs Initiative Type", 10, 30);
                    break;
                case 'WorkType':
                    this.pdf.text("Work Type Vs Tool Used", 10, 30);
                    break;
            }
            //this.pdf.text(this.elements[this.elCount] , 10 , 30);
            this.pdf.addHTML(document.getElementById(this.elements[this.elCount]), 80, 40, function () {
                switch (_this.elements[_this.elCount]) {
                    case 'TowerArea':
                        _this.printTowerData();
                        break;
                    case 'StatusArea':
                        _this.printStatusData();
                        break;
                    case 'BenefitArea':
                        _this.printBenefitData();
                        break;
                    case 'WorkType':
                        _this.printToolData();
                        break;
                }
                _this.pdf.setLineWidth(0.5);
                _this.pdf.line(10, 150, 150, 150);
                _this.pdf.addPage();
                _this.recursiveAddHtml();
            });
        }
        else {
            this.printDelayedData();
            this.pdf.save('Automation_Dashboard_' + new Date().getTime() + '.pdf');
        }
    };
    AutomationdashboardComponent.prototype.printTowerData = function () {
        var _this = this;
        var rowsTower = [];
        var rowsStatus = [];
        var resultTower = new __WEBPACK_IMPORTED_MODULE_7_typescript_linq__["TS"].Collections.List(false);
        resultTower = this.sample.groupBy(function (ele) { return (ele.tower); }).toList();
        var columnsTower = [
            { title: 'ID', dataKey: 'id' },
            { title: 'Tower', dataKey: 'tower' },
            { title: 'Count', dataKey: 'count' }
        ];
        this.count = 0;
        resultTower.forEach(function (element) {
            rowsTower.push({
                id: ++_this.count,
                tower: element.key,
                count: _this.sample.count(function (ele) { return ele.tower === element.key; })
            });
        });
        this.pdf.autoTable(columnsTower, rowsTower, {
            margin: { vertical: 40 },
            styles: { overflow: 'linebreak', columnWidth: 'wrap' },
            columnStyles: { text: { columnWidth: 'auto' } }
        });
    };
    AutomationdashboardComponent.prototype.printStatusData = function () {
        var _this = this;
        var rowsStatus = [];
        var resultStatus = new __WEBPACK_IMPORTED_MODULE_7_typescript_linq__["TS"].Collections.List(false);
        resultStatus = this.sample.groupBy(function (ele) { return (ele.status); }).toList();
        var columnsStatus = [
            { title: 'ID', dataKey: 'id' },
            { title: 'Status', dataKey: 'status' },
            { title: 'Count', dataKey: 'count' }
        ];
        this.count = 0;
        resultStatus.forEach(function (element) {
            rowsStatus.push({
                id: ++_this.count,
                status: element.key,
                count: _this.sample.count(function (ele) { return ele.status === element.key; })
            });
        });
        this.pdf.autoTable(columnsStatus, rowsStatus, {
            margin: { vertical: 40 },
            styles: { overflow: 'linebreak', columnWidth: 'wrap' },
            columnStyles: { text: { columnWidth: 'auto' } }
        });
    };
    AutomationdashboardComponent.prototype.printBenefitData = function () {
        var _this = this;
        var rowsBArea = [];
        var resultBArea = new __WEBPACK_IMPORTED_MODULE_7_typescript_linq__["TS"].Collections.List(false);
        resultBArea = this.sample.groupBy(function (ele) { return (ele.BenefitArea); }).toList();
        // console.log(resultBArea);
        var columnsBArea = [
            { title: 'ID', dataKey: 'id' },
            { title: 'Benefit Area', dataKey: 'benArea' },
            { title: 'Count', dataKey: 'count' }
        ];
        this.count = 0;
        resultBArea.forEach(function (element) {
            rowsBArea.push({
                id: ++_this.count,
                benArea: element.key,
                count: _this.sample.count(function (ele) { return ele.BenefitArea === element.key; })
            });
        });
        this.pdf.autoTable(columnsBArea, rowsBArea, {
            margin: { vertical: 40 },
            styles: { overflow: 'linebreak', columnWidth: 'wrap' },
            columnStyles: { text: { columnWidth: 'auto' } }
        });
    };
    AutomationdashboardComponent.prototype.printToolData = function () {
        var _this = this;
        var rowsBArea = [];
        var resultBArea = new __WEBPACK_IMPORTED_MODULE_7_typescript_linq__["TS"].Collections.List(false);
        resultBArea = this.sample.groupBy(function (ele) { return (ele.Tool); }).toList();
        var columnsBArea = [
            { title: 'ID', dataKey: 'id' },
            { title: 'Tool', dataKey: 'tool' },
            { title: 'Count', dataKey: 'count' }
        ];
        this.count = 0;
        resultBArea.forEach(function (element) {
            rowsBArea.push({
                id: ++_this.count,
                tool: element.key,
                count: _this.sample.count(function (ele) { return ele.Tool === element.key; })
            });
        });
        this.pdf.autoTable(columnsBArea, rowsBArea, {
            margin: { vertical: 40 },
            styles: { overflow: 'linebreak', columnWidth: 'wrap' },
            columnStyles: { text: { columnWidth: 'auto' } }
        });
    };
    AutomationdashboardComponent.prototype.printDelayedData = function () {
        var _this = this;
        this.pdf.setFontSize(18);
        this.pdf.setFont('times');
        this.pdf.setFontType('bold');
        this.pdf.text('Delayed Automations', 40, 10);
        this.pdf.setFontSize(16);
        this.pdf.setFont('helvetica');
        var rowsDelayed = [];
        var resultDelayed = new __WEBPACK_IMPORTED_MODULE_7_typescript_linq__["TS"].Collections.List(false);
        resultDelayed = this.sample.where(function (ele) { return (ele.status === 'Delayed'); }).toList();
        var columnsDelayed = [
            { title: 'ID', dataKey: 'id' },
            { title: 'Tower', dataKey: 'tower' },
            { title: 'Application', dataKey: 'application' },
            { title: 'Title', dataKey: 'title' },
            { title: 'ETC', dataKey: 'date' },
            { title: 'BenefitArea', dataKey: 'BenefitArea' }
        ];
        this.count = 0;
        resultDelayed.forEach(function (element) {
            rowsDelayed.push({
                id: ++_this.count,
                tower: element.tower,
                application: element.application,
                title: element.title,
                date: element.date,
                BenefitArea: element.BenefitArea
            });
        });
        this.pdf.autoTable(columnsDelayed, rowsDelayed, {
            margin: { vertical: 40 },
            styles: { overflow: 'linebreak', columnWidth: 'wrap' },
            columnStyles: { text: { columnWidth: 'auto' } }
        });
    };
    AutomationdashboardComponent.prototype.ClickExcel = function () {
        var _this = this;
        this.selectDialogData = [];
        console.log(this.towerName);
        console.log(this.initiativeCatName);
        if (this.towerName === '' && this.initiativeCatName === '') {
            this.towerWiseServerdata.forEach(function (element) {
                _this.selectDialogData.push(element);
            });
        }
        else {
            if (this.towerName !== '' && this.initiativeCatName === '') {
                this.towerWiseServerdata.forEach(function (element) {
                    if (element.tower === _this.towerName) {
                        _this.selectDialogData.push(element);
                    }
                });
            }
            else if (this.towerName === '' && this.initiativeCatName !== '') {
                this.towerWiseServerdata.forEach(function (element) {
                    if (element.Initiative === _this.initiativeCatName) {
                        _this.selectDialogData.push(element);
                    }
                });
            }
            else {
                this.towerWiseServerdata.forEach(function (element) {
                    if (element.tower === _this.towerName && element.Initiative === _this.initiativeCatName) {
                        _this.selectDialogData.push(element);
                    }
                });
            }
        }
        this.excelService.exportAsExcelFile(this.selectDialogData, 'Automation_Dashboard');
    };
    AutomationdashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-automationdashboard',
            template: __webpack_require__("./src/app/automationdashboard/automationdashboard.component.html"),
            styles: [__webpack_require__("./src/app/automationdashboard/automationdashboard.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__utils_httpConfig__["a" /* HttpCOnfig */],
            __WEBPACK_IMPORTED_MODULE_3__automationService__["a" /* AutomationService */],
            __WEBPACK_IMPORTED_MODULE_6__add_automation_addautomationservice__["a" /* AddAutomationService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_8__utils_excelService__["a" /* ExcelService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["f" /* MatDialog */]])
    ], AutomationdashboardComponent);
    return AutomationdashboardComponent;
}());



/***/ }),

/***/ "./src/app/automationdashdialog/automationdashdialog.component.css":
/***/ (function(module, exports) {

module.exports = "example-container {\r\n  display: -webkit-box;\r\n  display: -ms-flexbox;\r\n  display: flex;\r\n  -webkit-box-orient: vertical;\r\n  -webkit-box-direction: normal;\r\n      -ms-flex-direction: column;\r\n          flex-direction: column;\r\n  max-height: 500px;\r\n  min-width: 300px;\r\n}\r\n\r\n.mat-table {\r\n  overflow: auto;\r\n  max-height: 500px;\r\n}\r\n\r\n.mat-header-cell {\r\n    font-size: 16px;    \r\n    font-style: italic;    \r\n    font-weight: 700;\r\n}\r\n\r\n"

/***/ }),

/***/ "./src/app/automationdashdialog/automationdashdialog.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container mat-elevation-z8\">\r\n  <mat-table #table [dataSource]=\"automationDatasource\">\r\n\r\n    <!--- Note that these columns can be defined in any order.\r\n          The actual rendered columns are set as a property on the row definition\" -->\r\n\r\n    <!-- TOWER Column -->\r\n    <ng-container matColumnDef=\"{{displayedColumns[0]}}\">\r\n      <mat-header-cell *matHeaderCellDef> {{displayedColumns[0]}} </mat-header-cell>\r\n      <mat-cell *matCellDef=\"let element\"> {{element.TOWER}} </mat-cell>\r\n    </ng-container>\r\n\r\n    <!-- APPLICATION Column -->\r\n    <ng-container matColumnDef=\"{{displayedColumns[1]}}\">\r\n      <mat-header-cell *matHeaderCellDef> {{displayedColumns[1]}} </mat-header-cell>\r\n      <mat-cell *matCellDef=\"let element\"> {{element.APPLICATION}} </mat-cell>\r\n    </ng-container>\r\n\r\n    <!-- TITLE Column -->\r\n    <ng-container matColumnDef=\"{{displayedColumns[2]}}\">\r\n      <mat-header-cell *matHeaderCellDef> {{displayedColumns[2]}} </mat-header-cell>\r\n      <mat-cell *matCellDef=\"let element\"> {{element.TITLE}} </mat-cell>\r\n    </ng-container>\r\n\r\n    <!-- INITIATIVE Column -->\r\n    <ng-container matColumnDef=\"{{displayedColumns[3]}}\">\r\n      <mat-header-cell *matHeaderCellDef> {{displayedColumns[3]}} </mat-header-cell>\r\n      <mat-cell *matCellDef=\"let element\"> {{element.INITIATIVE}} </mat-cell>\r\n    </ng-container>\r\n\r\n    <!-- TOOL Column -->\r\n    <ng-container matColumnDef=\"{{displayedColumns[4]}}\">\r\n      <mat-header-cell *matHeaderCellDef> {{displayedColumns[4]}} </mat-header-cell>\r\n      <mat-cell *matCellDef=\"let element\"> {{element.TOOL}} </mat-cell>\r\n    </ng-container>\r\n\r\n    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n    <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n  </mat-table>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/automationdashdialog/automationdashdialog.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutomationdashdialogComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material_table__ = __webpack_require__("./node_modules/@angular/material/esm5/table.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var AutomationdashdialogComponent = (function () {
    function AutomationdashdialogComponent(dialogRef, loginDialog, data) {
        this.dialogRef = dialogRef;
        this.loginDialog = loginDialog;
        this.data = data;
        this.automationdata = [];
        this.selectTowerWisedata = data.detailsData;
        this.selectedType = data.type;
    }
    AutomationdashdialogComponent.prototype.ngOnInit = function () {
        this.BuildDialog(this.selectTowerWisedata);
    };
    AutomationdashdialogComponent.prototype.BuildDialog = function (data) {
        var _this = this;
        data.forEach(function (element) {
            _this.automationdata.push({
                TOWER: element.tower,
                APPLICATION: element.application,
                TITLE: element.title,
                STATUS: element.status,
                WORKTYPE: element.WorkType,
                BENEFITAREA: element.BenefitArea,
                TOOL: element.Tool,
                INITIATIVE: element.Initiative
            });
        });
        console.log(this.automationdata);
        this.displayedColumns = ['TOWER', 'APPLICATION', 'TITLE', 'INITIATIVE', 'TOOL'];
        this.automationDatasource = new __WEBPACK_IMPORTED_MODULE_2__angular_material_table__["a" /* MatTableDataSource */](this.automationdata);
    };
    AutomationdashdialogComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-automationdashdialog',
            template: __webpack_require__("./src/app/automationdashdialog/automationdashdialog.component.html"),
            styles: [__webpack_require__("./src/app/automationdashdialog/automationdashdialog.component.css")]
        }),
        __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["h" /* MatDialogRef */],
            __WEBPACK_IMPORTED_MODULE_1__angular_material__["f" /* MatDialog */], Object])
    ], AutomationdashdialogComponent);
    return AutomationdashdialogComponent;
}());



/***/ }),

/***/ "./src/app/automationhistorydialog/automationhistorydialog.component.css":
/***/ (function(module, exports) {

module.exports = ".example-container {\r\n  display: -webkit-box;\r\n  display: -ms-flexbox;\r\n  display: flex;\r\n  -webkit-box-orient: vertical;\r\n  -webkit-box-direction: normal;\r\n      -ms-flex-direction: column;\r\n          flex-direction: column;\r\n  max-height: 500px;\r\n  min-width: 300px;\r\n}\r\n\r\n.mat-table {\r\n  overflow: auto;\r\n  max-height: 500px;\r\n}\r\n\r\n.mat-header-cell {\r\n    font-size: 16px;    \r\n    font-style: italic;    \r\n    font-weight: 700;\r\n}\r\n\r\n.cdk-column-No {\r\n  max-width: 30px;\r\n}\r\n\r\n.cdk-column-COMMENT {\r\n  min-width: 170px;\r\n  margin: 0 2px;\r\n}\r\n\r\n.cdk-column-USER {\r\n  min-width: 50px;\r\n  margin: 0 2px;\r\n}\r\n\r\n.cdk-column-DATE {\r\n  min-width: 50px;\r\n  margin: 0 2px;\r\n}"

/***/ }),

/***/ "./src/app/automationhistorydialog/automationhistorydialog.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container mat-elevation-z8\">\r\n  <mat-table #table [dataSource]=\"automationDatasource\">\r\n\r\n    <!--- Note that these columns can be defined in any order.\r\n          The actual rendered columns are set as a property on the row definition\" -->\r\n    <!-- SL Column -->\r\n    <ng-container matColumnDef=\"{{displayedColumns[0]}}\">\r\n      <mat-header-cell *matHeaderCellDef> {{displayedColumns[0]}} </mat-header-cell>\r\n      <mat-cell *matCellDef=\"let element\"> {{element.No}} </mat-cell>\r\n    </ng-container>\r\n\r\n    <!-- COMMENT Column -->\r\n    <ng-container matColumnDef=\"{{displayedColumns[1]}}\">\r\n      <mat-header-cell *matHeaderCellDef> {{displayedColumns[1]}} </mat-header-cell>\r\n      <mat-cell *matCellDef=\"let element\"> {{element.COMMENT}} </mat-cell>\r\n    </ng-container>\r\n\r\n    <!-- USER Column -->\r\n    <ng-container matColumnDef=\"{{displayedColumns[2]}}\">\r\n      <mat-header-cell *matHeaderCellDef> {{displayedColumns[2]}} </mat-header-cell>\r\n      <mat-cell *matCellDef=\"let element\"> {{element.USER}} </mat-cell>\r\n    </ng-container>\r\n\r\n    <!-- DATE Column -->\r\n    <ng-container matColumnDef=\"{{displayedColumns[3]}}\">\r\n      <mat-header-cell *matHeaderCellDef> {{displayedColumns[3]}} </mat-header-cell>\r\n      <mat-cell *matCellDef=\"let element\"> {{element.DATE}} </mat-cell>\r\n    </ng-container>\r\n\r\n    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n    <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n  </mat-table>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/automationhistorydialog/automationhistorydialog.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutomationhistorydialogComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material_table__ = __webpack_require__("./node_modules/@angular/material/esm5/table.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var AutomationhistorydialogComponent = (function () {
    function AutomationhistorydialogComponent(dialogRef, loginDialog, data) {
        this.dialogRef = dialogRef;
        this.loginDialog = loginDialog;
        this.data = data;
        this.automationdata = [];
        this.selectTowerWisedata = data;
    }
    AutomationhistorydialogComponent.prototype.ngOnInit = function () {
        this.BuildDialog(this.selectTowerWisedata);
    };
    AutomationhistorydialogComponent.prototype.BuildDialog = function (data) {
        var _this = this;
        var i = 0;
        data.forEach(function (element) {
            i = i + 1;
            _this.automationdata.push({
                No: i,
                COMMENT: element.autodetailsComments,
                USER: element.autodetailsUser,
                DATE: element.autodetailsCreatedDate
            });
        });
        console.log(this.automationdata);
        this.displayedColumns = ['No', 'COMMENT', 'USER', 'DATE'];
        this.automationDatasource = new __WEBPACK_IMPORTED_MODULE_2__angular_material_table__["a" /* MatTableDataSource */](this.automationdata);
    };
    AutomationhistorydialogComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-automationhistorydialog',
            template: __webpack_require__("./src/app/automationhistorydialog/automationhistorydialog.component.html"),
            styles: [__webpack_require__("./src/app/automationhistorydialog/automationhistorydialog.component.css")]
        }),
        __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["h" /* MatDialogRef */],
            __WEBPACK_IMPORTED_MODULE_1__angular_material__["f" /* MatDialog */], Object])
    ], AutomationhistorydialogComponent);
    return AutomationhistorydialogComponent;
}());



/***/ }),

/***/ "./src/app/dashboard-dialog/dashboard-dialog.component.css":
/***/ (function(module, exports) {

module.exports = ".chatCard{\r\n    height:300px;\r\n\tdisplay:-webkit-box;\r\n\tdisplay:-ms-flexbox;\r\n\tdisplay:flex;\r\n\t-webkit-box-pack:center;\r\n\t    -ms-flex-pack:center;\r\n\t        justify-content:center;\r\n    -webkit-box-align:center;\r\n        -ms-flex-align:center;\r\n            align-items:center;\r\n    margin:  5px 5px 5px 0;\r\n}\r\n\r\n.ngx-charts{\r\n    position: inherit;\r\n}\r\n\r\n.graphWrapper{\r\n    max-height: 300px;\r\n}\r\n\r\n.excelbutton{\r\n    margin: 10px;\r\n}\r\n"

/***/ }),

/***/ "./src/app/dashboard-dialog/dashboard-dialog.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"graphWrapper\">\r\n    <div class=\"col-md-5 col-sm-5 chatCard\">      \r\n      <ngx-charts-advanced-pie-chart *ngIf=\"single && single.length > 0\" [scheme]=\"colorScheme\" [results]=\"single\" [gradient]=\"gradient\">\r\n      </ngx-charts-advanced-pie-chart>\r\n    </div>\r\n    <div class=\"col-md-5 col-sm-5 chatCard\">      \r\n        <ngx-charts-advanced-pie-chart  *ngIf=\"multi && multi.length > 0\" [scheme]=\"statColorcheme\" [results]=\"multi\" [gradient]=\"gradient\">\r\n        </ngx-charts-advanced-pie-chart>\r\n    </div>\r\n     <!-- <div class=\"col-md-5 col-sm-5 chatCard\">      \r\n        <ngx-charts-bar-vertical-2d\r\n        [scheme]=\"colorScheme\"\r\n        [results]=\"multi\"\r\n        [gradient]=\"gradient\"\r\n        [xAxis]=\"showXAxis\"\r\n        [yAxis]=\"showYAxis\"\r\n        [legend]=\"showLegend\"\r\n        [showXAxisLabel]=\"showXAxisLabel\"\r\n        [showYAxisLabel]=\"showYAxisLabel\"\r\n        [xAxisLabel]=\"xAxisLabel\"\r\n        [yAxisLabel]=\"yAxisLabel\"\r\n        (select)=\"onSelect($event)\">\r\n      </ngx-charts-bar-vertical-2d>\r\n    </div>\r\n    <div class=\"col-md-5 col-sm-5 chatCard\">      \r\n        <ngx-charts-area-chart\r\n          [scheme]=\"colorScheme\"\r\n          [results]=\"multi\"\r\n          [gradient]=\"gradient\"\r\n          [xAxis]=\"showXAxis\"\r\n          [yAxis]=\"showYAxis\"\r\n          [legend]=\"showLegend\"\r\n          [showXAxisLabel]=\"showXAxisLabel\"\r\n          [showYAxisLabel]=\"showYAxisLabel\"\r\n          [xAxisLabel]=\"xAxisLabel\"\r\n          [yAxisLabel]=\"yAxisLabel\"\r\n          [autoScale]=\"autoScale\"\r\n          (select)=\"onSelect($event)\">\r\n    </ngx-charts-area-chart>\r\n    </div> -->\r\n    <button class=\"excelbutton\"  (click)=\"exportDataAsExcel()\" mat-raised-button color=\"warn\">Export As Excel</button>\r\n</div>\r\n  "

/***/ }),

/***/ "./src/app/dashboard-dialog/dashboard-dialog.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardDialogComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils_excelService__ = __webpack_require__("./src/app/utils/excelService.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dashboardService__ = __webpack_require__("./src/app/dashboard-dialog/dashboardService.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




//import {single,multi} from './data';

var DashboardDialogComponent = (function () {
    function DashboardDialogComponent(dialogRef, excelService, httpConfig, dashboardService, data) {
        this.dialogRef = dialogRef;
        this.excelService = excelService;
        this.httpConfig = httpConfig;
        this.dashboardService = dashboardService;
        this.data = data;
        this.view = [700, 400];
        this.colorScheme = {
            // domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
            domain: ['#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886']
        };
        this.statColorcheme = {
            // domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
            domain: ['#A10A28', '#D3342D', '#EF6D49', '#FAAD67', '#FDDE90', '#DBED91', '#A9D770', '#6CBA67', '#2C9653', '#146738']
        };
        // options
        this.showXAxis = true;
        this.showYAxis = true;
        this.gradient = false;
        this.showLegend = true;
        this.showXAxisLabel = true;
        this.xAxisLabel = 'Ideas';
        this.showYAxisLabel = true;
        this.yAxisLabel = 'Status';
        // line, area
        this.autoScale = true;
        this.getTowerCount();
        this.getStatusCount();
        //  console.log(this.getStatusCount());
    }
    DashboardDialogComponent.prototype.ngOnInit = function () {
    };
    DashboardDialogComponent.prototype.getTowerCount = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var innovationDashBoardDetailsURL = _this.httpConfig.getIdeasCountByTower();
            _this.dashboardService.innovationDashboard(innovationDashBoardDetailsURL).subscribe(function (result) {
                if (result.status) {
                    //(result.status ? resolve(result.data) : reject(result));
                    if (result.data.length > 0) {
                        _this.single = [];
                        _this.single = result.data;
                        console.log(_this.single);
                    }
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    DashboardDialogComponent.prototype.getStatusCount = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var innovationDashBoardDetailsURL = _this.httpConfig.getIdeasCountByStatus();
            _this.dashboardService.innovationDashboard(innovationDashBoardDetailsURL).subscribe(function (result) {
                if (result.status) {
                    //(result.status ? resolve(result.data) : reject(result));
                    if (result.data.length > 0) {
                        _this.multi = [];
                        _this.multi = result.data;
                        console.log(_this.multi);
                    }
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    DashboardDialogComponent.prototype.onSelect = function (event) {
        console.log(event);
    };
    // exporting data as excel 
    DashboardDialogComponent.prototype.exportDataAsExcel = function () {
        this.excelService.exportAsExcelFile(this.single, 'tempFile');
    };
    DashboardDialogComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'app-dashboard-dialog',
            template: __webpack_require__("./src/app/dashboard-dialog/dashboard-dialog.component.html"),
            styles: [__webpack_require__("./src/app/dashboard-dialog/dashboard-dialog.component.css")]
        }),
        __param(4, Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_2__angular_material__["a" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_material__["h" /* MatDialogRef */],
            __WEBPACK_IMPORTED_MODULE_0__utils_excelService__["a" /* ExcelService */], __WEBPACK_IMPORTED_MODULE_3__utils_httpConfig__["a" /* HttpCOnfig */], __WEBPACK_IMPORTED_MODULE_4__dashboardService__["a" /* DashBoardService */], Object])
    ], DashboardDialogComponent);
    return DashboardDialogComponent;
}());



/***/ }),

/***/ "./src/app/dashboard-dialog/dashboardService.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashBoardService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
Fetching Automation Details through Http Calls
*/
var DashBoardService = (function () {
    function DashBoardService(httpClient) {
        this.httpClient = httpClient;
    }
    // Fetch Automation Dashboard Details
    DashBoardService.prototype.innovationDashboard = function (URL) {
        return this.httpClient.get(URL, {});
    };
    DashBoardService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], DashBoardService);
    return DashBoardService;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.component.css":
/***/ (function(module, exports) {

module.exports = "\r\n.sideNav{\r\n    min-height: 100vh;\r\n    padding: 6px;\r\n    background: currentColor;\r\n    margin: 0 !important;\r\n}\r\n\r\n.mainCard{\r\n    min-height: 100vh;\r\n    background: beige;\r\n    padding: 0 !important;\r\n}"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<app-toolbar></app-toolbar>\r\n<div>\r\n  <mat-card class=\"sideNav col-md-2\">\r\n    <app-side-nav></app-side-nav>\r\n  </mat-card>\r\n</div>\r\n<mat-card class=\"mainCard col-md-10\">\r\n  <router-outlet></router-outlet>\r\n</mat-card>\r\n"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils_navServices__ = __webpack_require__("./src/app/utils/navServices.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DashboardComponent = (function () {
    function DashboardComponent(navService) {
        this.navService = navService;
        this.navStatus = true;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this.subScribeToNavService();
        this.navService.showNavStatus();
    };
    DashboardComponent.prototype.subScribeToNavService = function () {
        var _this = this;
        this.navService.getNavStatus().subscribe(function (status) {
            _this.navStatus = status;
        });
    };
    DashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__("./src/app/dashboard/dashboard.component.html"),
            styles: [__webpack_require__("./src/app/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__utils_navServices__["a" /* NavService */]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/demo-idea-loggin/demo-idea-loggin.component.css":
/***/ (function(module, exports) {

module.exports = "\r\n.toolBar {\r\n  background: darkslategrey;\r\n  top: 0;\r\n  position: fixed;\r\n  z-index: 999;\r\n}\r\n\r\nmat-toolbar-row {\r\n  -webkit-box-pack: justify;\r\n      -ms-flex-pack: justify;\r\n          justify-content: space-between;\r\n}\r\n\r\n.example-spacer {\r\n  -webkit-box-flex: 1;\r\n      -ms-flex: 1 1 auto;\r\n          flex: 1 1 auto;\r\n}\r\n\r\n.toolbar-menu {\r\n  color: black;\r\n}\r\n\r\n.logo {\r\n  height: 26px;\r\n  margin: 0 4px 5px 15px;\r\n  vertical-align: middle;\r\n}\r\n\r\n.title {\r\n  vertical-align: middle;\r\n  margin: 0 4px 3px 10px;\r\n  color: white;\r\n}\r\n\r\n.mainContent {\r\n  margin-top: 64px;\r\n}\r\n\r\n.mainText {\r\n  color: white;\r\n  position: relative;\r\n  font-weight: bold;\r\n  top: 40%;\r\n}\r\n\r\n.formCard {\r\n  top: 20%;\r\n}\r\n\r\n.titleText {\r\n  color: aqua;\r\n  font-size: x-large;\r\n  padding: 11px;\r\n  border: 1px solid;\r\n}\r\n\r\n.description {\r\n  margin-top: 30px;\r\n  font-style: italic;\r\n}\r\n"

/***/ }),

/***/ "./src/app/demo-idea-loggin/demo-idea-loggin.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\r\n  <app-updated-tool-bar></app-updated-tool-bar>\r\n  <div class=\"mainContent\">\r\n    <router-outlet></router-outlet>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/demo-idea-loggin/demo-idea-loggin.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DemoIdeaLogginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DemoIdeaLogginComponent = (function () {
    function DemoIdeaLogginComponent() {
    }
    DemoIdeaLogginComponent.prototype.ngOnInit = function () {
    };
    DemoIdeaLogginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-demo-idea-loggin',
            template: __webpack_require__("./src/app/demo-idea-loggin/demo-idea-loggin.component.html"),
            styles: [__webpack_require__("./src/app/demo-idea-loggin/demo-idea-loggin.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], DemoIdeaLogginComponent);
    return DemoIdeaLogginComponent;
}());



/***/ }),

/***/ "./src/app/edit-originate/edit-originate.component.css":
/***/ (function(module, exports) {

module.exports = ".topBuffer{\r\n    margin: 10px 0px 10px 10px;\r\n    background: cadetblue;\r\n}\r\n\r\n.example-header-image {\r\n    background-image: url('https://www.accenture.com/us-en/_acnmedia/Accenture/next-gen-4/oracle-technology-vision-2017/img/Accenture-Oracle-Tech-Vision-2017-Lockup.png');\r\n    background-size: cover;\r\n  }\r\n\r\n.mat-card-title{\r\n    color: white;\r\n    font-weight: bolder;\r\n    margin: 10px;\r\n    width: 100%\r\n  }\r\n\r\n.mat-card-title{\r\n    color: #0ed6ec;\r\n    font-weight: bolder;\r\n    margin: 10px;\r\n    width: 100%\r\n  }\r\n\r\n.mat-card-desc{\r\n    color: #86e400;\r\n    font-weight: normal;\r\n    margin: 10px;\r\n    width: 100%\r\n  }\r\n\r\n.Completed{\r\n     color:green;\r\n     font-weight: bold\r\n }\r\n\r\n.Cancelled{\r\n     color:red;\r\n     font-weight: bold\r\n }\r\n\r\n/*.New\r\n {\r\n     background-image: url(\"C:\\Users\\a.a.uthayakumar\\Innovation\\Resource\\new.png\");\r\n     \r\n }*/\r\n\r\n.Pending Approval\r\n {\r\n     color:orange;\r\n     font-weight: bold\r\n }\r\n\r\n.description{\r\n      margin-top: 20px;\r\n      color: cornsilk;\r\n      height: 2em; \r\n      font-weight: bold;\r\n      line-height: 1em; \r\n      overflow: hidden;\r\n  }\r\n\r\n.mat-button{\r\n      color: white;\r\n      padding: 0px;\r\n      border: #0F0D0D 2px;\r\n  }\r\n\r\n.mat-dialog-container{\r\n      padding: 2px;\r\n  }\r\n\r\n.searchDiv {\r\n    max-width: 300px;\r\n    display: block;    \r\n    -webkit-box-sizing: border-box;    \r\n            box-sizing: border-box;\r\n    border: 2px solid cadetblue;    \r\n    border-radius: 4px;\r\n    font-size: 16px;\r\n    margin : 10px 2px 10px 30px;\r\n    background-color: cadetblue;    \r\n    padding: 5px;\r\n    -webkit-transition: width 0.4s ease-in-out;\r\n    transition: width 0.4s ease-in-out;\r\n}\r\n\r\n.searchbox_1{\r\n    background-color: #609ea0;\r\n    padding:10px;\r\n    width:320px;\r\n    margin: 10px 30px;\r\n    -webkit-box-sizing:border-box;\r\n    box-sizing:border-box;\r\n    border-radius:6px;\r\n    }\r\n\r\n.search_1{\r\n    width:250px;\r\n    height:35px;\r\n    padding:5px;\r\n    border-radius:6px;\r\n    border:none;\r\n    color:#0F0D0D;;\r\n    font-weight:bold;\r\n    background-color:#E2EFF7;\r\n    -webkit-box-shadow:\r\n0 -2px 2px 0 rgba(199, 199, 199, 0.55),\r\n0 1px 1px 0 #fff,\r\n0 2px 2px 1px #fafafa,\r\n0 2px 4px 0 #b2b2b2 inset,\r\n0 -1px 1px 0 #f2f2f2 inset,\r\n0 15px 15px 0 rgba(41, 41, 41, 0.09) inset;\r\nbox-shadow:\r\n0 -2px 2px 0 rgba(199, 199, 199, 0.55),\r\n0 1px 1px 0 #fff,\r\n0 2px 2px 1px #fafafa,\r\n0 2px 4px 0 #b2b2b2 inset,\r\n0 -1px 1px 0 #f2f2f2 inset,\r\n0 15px 15px 0 rgba(41, 41, 41, 0.09) inset;\r\n    }\r\n\r\n.submit_1{\r\n    width:35px;\r\n    height:35px;\r\n    min-width: 15px;\r\n    border:none;\r\n    cursor:pointer;\r\n    background-color: #609ea0\r\n    }\r\n\r\n.search_1:focus{\r\n    outline:0;\r\n    }\r\n"

/***/ }),

/***/ "./src/app/edit-originate/edit-originate.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper\" style=\"padding-top:4px\">\r\n  <form [formGroup]=\"searchOriginateForm\" (ngSubmit)=\"onSearchSubmit()\">\r\n  <div class='searchbox_1'>\r\n    <div>\r\n        <input type=\"search\" class=\"search_1\" placeholder=\"Search\" formControlName=\"searchField\"/>   \r\n        <button class=\"glyphicon glyphicon-search submit_1\" mat-button></button> \r\n    </div>\r\n  </div>\r\n  </form>\r\n  <div *ngFor=\"let originate of originateList; let i = index\" class=\"col-md-4\">\r\n    <mat-card class=\"topBuffer\">\r\n      <mat-card-header>\r\n          <img style=\"position:absolute;top:2%;left:91%;right:0;bottom:100;width:8%;\" \r\n          [src]=\"getimgsrc(originate.OriginateStatusType)\">\r\n        <div mat-card-avatar class=\"example-header-image\"></div>  \r\n        <mat-card-title>\r\n            {{originate.OriginateTitle | slice:0:50}} \r\n        </mat-card-title> \r\n        <mat-card-title class=\"mat-card-desc\">\r\n          Category   : {{originate.OriginateCategoryType}} \r\n        </mat-card-title> \r\n        <mat-card-title class=\"mat-card-desc\">\r\n          Tower       : {{originate.OriginateTower}}\r\n        </mat-card-title>\r\n        <mat-card-title class=\"mat-card-desc\">\r\n          Accenture POC : {{originate.OriginateACNStakeholder}}\r\n        </mat-card-title>\r\n      </mat-card-header> \r\n      <mat-card-actions>\r\n        <button  (click)=\"onOriginateClick(i)\" mat-button>Show details</button>\r\n        <button  (click)=\"onOriginatComments(i)\" mat-button>Show Comments</button>\r\n      </mat-card-actions>\r\n    </mat-card>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/edit-originate/edit-originate.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditOriginateComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__show_originate_originateDetails__ = __webpack_require__("./src/app/show-originate/originateDetails.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__progress_dialog_progress_dialog_component__ = __webpack_require__("./src/app/progress-dialog/progress-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__utils_checkUserStatus__ = __webpack_require__("./src/app/utils/checkUserStatus.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__originate_details_originate_details_component__ = __webpack_require__("./src/app/originate-details/originate-details.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__originate_comments_view_originate_comments_view_component__ = __webpack_require__("./src/app/originate-comments-view/originate-comments-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_util__ = __webpack_require__("./node_modules/util/util.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_util___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_util__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var EditOriginateComponent = (function () {
    function EditOriginateComponent(userinfo, showOriginateService, matDialog, router, httpConfig) {
        this.userinfo = userinfo;
        this.showOriginateService = showOriginateService;
        this.matDialog = matDialog;
        this.router = router;
        this.httpConfig = httpConfig;
        this.originateTowerType = [];
        this.originateCategoryType = [];
        this.originateList = [];
        this.isAdmin = false;
        this.searchOriginateForm = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["d" /* FormGroup */]({
            searchField: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].required)
        });
        this.getOriginateList();
        this.getUserInfo();
    }
    EditOriginateComponent.prototype.ngOnInit = function () {
    };
    // Check user admin or not
    EditOriginateComponent.prototype.getUserInfo = function () {
        if (Object(__WEBPACK_IMPORTED_MODULE_9_util__["isNull"])(JSON.parse(this.userinfo.getUserDetails()))) {
            this.router.navigateByUrl('/login');
        }
        else {
            this.user = JSON.parse(this.userinfo.getUserDetails())[0].username;
        }
    };
    EditOriginateComponent.prototype.getOriginateList = function () {
        var _this = this;
        this.showDialog();
        var originateGetAll = this.httpConfig.getOriginateDetails();
        this.showOriginateService.originateDetails(originateGetAll).subscribe(function (result) {
            _this.hideDialog();
            console.log(result);
            if (result.status) {
                console.log(result);
                _this.originateList = [];
                _this.originateList = result.data;
            }
        }, function (error) {
            console.log(error);
            _this.hideDialog();
        });
    };
    // showing the dialog
    EditOriginateComponent.prototype.showDialog = function () {
        this.dialogRef = this.matDialog.open(__WEBPACK_IMPORTED_MODULE_4__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: 1 } });
    };
    // hiding the dialog
    EditOriginateComponent.prototype.hideDialog = function () {
        this.dialogRef.close();
    };
    EditOriginateComponent.prototype.getimgsrc = function (status) {
        if (status === 'Completed') {
            return ('../../assets/completed.png');
        }
        else if (status === 'New') {
            return ('../../assets/new.png');
        }
        else if (status === 'Deferred') {
            return ('../../assets/imageedit_13_8227496638.png');
        }
        else if (status === 'Cancelled') {
            return ('../../assets/cancel.png');
        }
        else if (status === 'On going') {
            return ('../../assets/inprogress.png');
        }
    };
    EditOriginateComponent.prototype.onSearchSubmit = function () {
        var _this = this;
        this.showDialog();
        var strStringFetch = this.searchOriginateForm.get('searchField').value;
        strStringFetch = '%' + strStringFetch + '%';
        console.log(strStringFetch);
        var originateSearch = this.httpConfig.getSearchOriginate();
        this.showOriginateService.searchoriginateDetails(originateSearch, { strSearch: strStringFetch }).subscribe(function (result) {
            _this.hideDialog();
            if (result.status) {
                console.log(result);
                if (result.data.length > 0) {
                    _this.originateList = [];
                    _this.originateList = result.data;
                }
                else {
                    _this.dialogRef = _this.matDialog.open(__WEBPACK_IMPORTED_MODULE_4__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: 6, message: 'Your Search did not return any match' } });
                }
            }
        }, function (error) {
            _this.hideDialog();
            console.log(error);
        });
    };
    EditOriginateComponent.prototype.onOriginateClick = function (i) {
        // showing idea in dialog
        console.log(this.originateList[i]);
        this.showOriginateDialogRef = this.matDialog.open(__WEBPACK_IMPORTED_MODULE_7__originate_details_originate_details_component__["a" /* OriginateDetailsComponent */], {
            data: this.originateList[i]
        });
    };
    EditOriginateComponent.prototype.onOriginatComments = function (i) {
        var _this = this;
        console.log(this.originateList[i].OriginateID);
        var originateComments = this.httpConfig.commentsOriginate();
        this.showOriginateService.searchoriginateDetails(originateComments, { strSearch: this.originateList[i].OriginateID })
            .subscribe(function (result) {
            if (result.status) {
                console.log(result);
                if (result.data.length > 0) {
                    _this.dialogRef = _this.matDialog.open(__WEBPACK_IMPORTED_MODULE_8__originate_comments_view_originate_comments_view_component__["a" /* OriginateCommentsViewComponent */], { data: result.data });
                }
            }
        }, function (error) {
            _this.hideDialog();
            console.log(error);
        });
    };
    EditOriginateComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-edit-originate',
            template: __webpack_require__("./src/app/edit-originate/edit-originate.component.html"),
            styles: [__webpack_require__("./src/app/edit-originate/edit-originate.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__utils_checkUserStatus__["a" /* CheckUserStatus */],
            __WEBPACK_IMPORTED_MODULE_1__show_originate_originateDetails__["a" /* OriginateService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_material__["f" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_10__angular_router__["a" /* Router */],
            __WEBPACK_IMPORTED_MODULE_5__utils_httpConfig__["a" /* HttpCOnfig */]])
    ], EditOriginateComponent);
    return EditOriginateComponent;
}());



/***/ }),

/***/ "./src/app/ideas/ideas-services.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IdeasServices; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// this class helps us in making ideas http calls
var IdeasServices = (function () {
    function IdeasServices(httpClinet, httpConfig) {
        this.httpClinet = httpClinet;
        this.httpConfig = httpConfig;
    }
    // getting all the ideas from server 
    IdeasServices.prototype.getAllIdeas = function (data) {
        return this.httpClinet.post(this.httpConfig.getIdeas(), data);
    };
    // get ideas count 
    IdeasServices.prototype.getIdeasCount = function (data) {
        return this.httpClinet.post(this.httpConfig.getLikesCount(), data);
    };
    // getSearchIdea
    IdeasServices.prototype.getSearchIdea = function (data) {
        return this.httpClinet.post(this.httpConfig.searchIdeaDetails(), data);
    };
    IdeasServices = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_0__utils_httpConfig__["a" /* HttpCOnfig */]])
    ], IdeasServices);
    return IdeasServices;
}());



/***/ }),

/***/ "./src/app/ideas/ideas.component.css":
/***/ (function(module, exports) {

module.exports = ".topBuffer{\r\n    margin: 10px 0px 10px 10px;\r\n    background: cadetblue;\r\n}\r\n\r\n.example-header-image {\r\n    background-image: url('https://www.accenture.com/us-en/_acnmedia/Accenture/next-gen-4/oracle-technology-vision-2017/img/Accenture-Oracle-Tech-Vision-2017-Lockup.png');\r\n    background-size: cover;\r\n  }\r\n\r\n.mat-card-title{\r\n    color: white;\r\n    font-weight: bold;\r\n    margin: 10px;\r\n  }\r\n\r\n.description{\r\n      margin-top: 20px;\r\n      color: white;\r\n      height: 2em; \r\n      line-height: 1em; \r\n      overflow: hidden;\r\n  }\r\n\r\n.mat-button{\r\n      color: white;\r\n      padding: 0px;\r\n  }\r\n\r\n.mat-dialog-container{\r\n      padding: 2px;\r\n  }\r\n\r\n.searchDiv {\r\n    max-width: 300px;\r\n    display: block;    \r\n    -webkit-box-sizing: border-box;    \r\n            box-sizing: border-box;\r\n    border: 2px solid cadetblue;    \r\n    border-radius: 4px;\r\n    font-size: 16px;\r\n    margin : 10px 2px 10px 30px;\r\n    background-color: cadetblue;    \r\n    padding: 5px;\r\n    -webkit-transition: width 0.4s ease-in-out;\r\n    transition: width 0.4s ease-in-out;\r\n}\r\n\r\n.searchbox_1{\r\n    background-color: #609ea0;\r\n    padding:10px;\r\n    width:320px;\r\n    margin: 10px 30px;\r\n    -webkit-box-sizing:border-box;\r\n    box-sizing:border-box;\r\n    border-radius:6px;\r\n    }\r\n\r\n.search_1{\r\n    width:250px;\r\n    height:35px;\r\n    padding:5px;\r\n    border-radius:6px;\r\n    border:none;\r\n    color:#0F0D0D;;\r\n    font-weight:bold;\r\n    background-color:#E2EFF7;\r\n    -webkit-box-shadow:\r\n0 -2px 2px 0 rgba(199, 199, 199, 0.55),\r\n0 1px 1px 0 #fff,\r\n0 2px 2px 1px #fafafa,\r\n0 2px 4px 0 #b2b2b2 inset,\r\n0 -1px 1px 0 #f2f2f2 inset,\r\n0 15px 15px 0 rgba(41, 41, 41, 0.09) inset;\r\nbox-shadow:\r\n0 -2px 2px 0 rgba(199, 199, 199, 0.55),\r\n0 1px 1px 0 #fff,\r\n0 2px 2px 1px #fafafa,\r\n0 2px 4px 0 #b2b2b2 inset,\r\n0 -1px 1px 0 #f2f2f2 inset,\r\n0 15px 15px 0 rgba(41, 41, 41, 0.09) inset;\r\n    }\r\n\r\n.submit_1{\r\n    width:35px;\r\n    height:35px;\r\n    min-width: 15px;\r\n    border:none;\r\n    cursor:pointer;\r\n    background-color: #609ea0\r\n    }\r\n\r\n.search_1:focus{\r\n    outline:0;\r\n    }\r\n"

/***/ }),

/***/ "./src/app/ideas/ideas.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper\" style=\"padding-top: 4px\">\r\n  <form [formGroup]=\"searchIdeaForm\" (ngSubmit)=\"onSearchSubmit()\">\r\n  <div class='searchbox_1'>\r\n    <div>\r\n        <input type=\"search\" class=\"search_1\" placeholder=\"Search\" formControlName=\"searchField\"/>   \r\n        <button class=\"glyphicon glyphicon-search submit_1\" mat-button></button> \r\n    </div>\r\n  </div>\r\n  </form>\r\n  <div *ngFor=\"let idea of ideasList; let i = index\" class=\"col-md-4\">\r\n    <mat-card class=\"topBuffer\">\r\n      <mat-card-header>\r\n        <div mat-card-avatar class=\"example-header-image\"></div>\r\n        <mat-card-title>{{idea.title}}</mat-card-title>\r\n      </mat-card-header>\r\n      <mat-card-content style=\"margin-top: 30px\">\r\n        <p class=\"description\">{{idea.description}}</p>\r\n      </mat-card-content>\r\n      <mat-card-actions>\r\n        <button (click)=\"onIdeaClick(i)\" mat-button>Show More</button>\r\n      </mat-card-actions>\r\n    </mat-card>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/ideas/ideas.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IdeasComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__show_idea_show_idea_component__ = __webpack_require__("./src/app/show-idea/show-idea.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ideas_services__ = __webpack_require__("./src/app/ideas/ideas-services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__progress_dialog_progress_dialog_component__ = __webpack_require__("./src/app/progress-dialog/progress-dialog.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var IdeasComponent = (function () {
    function IdeasComponent(ideaServices, dialog) {
        this.ideaServices = ideaServices;
        this.dialog = dialog;
        this.ideasList = [];
        this.searchIdeaForm = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["d" /* FormGroup */]({
            searchField: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].required)
        });
        this.getIdeas();
    }
    IdeasComponent.prototype.ngOnInit = function () {
    };
    // getting ideas from server 
    IdeasComponent.prototype.getIdeas = function () {
        var _this = this;
        this.showDialog();
        this.ideaServices.getAllIdeas({}).subscribe(function (result) {
            _this.hideDialog();
            if (result.status) {
                _this.ideasList = result.data;
            }
        }, function (error) {
            console.log(error);
            _this.hideDialog();
        });
    };
    // when user clicks onidea 
    IdeasComponent.prototype.onIdeaClick = function (i) {
        var _this = this;
        // showing idea in dialog 
        // get id of selected idea
        // getting likes count 
        this.showDialog();
        this.ideaServices.getIdeasCount({ id: this.ideasList[i].id }).subscribe(function (result) {
            _this.hideDialog();
            if (result.status) {
                _this.showIdeaDialogRef = _this.dialog.open(__WEBPACK_IMPORTED_MODULE_0__show_idea_show_idea_component__["a" /* ShowIdeaComponent */], { data: result.data[0], panelClass: 'show-idea' });
            }
        }, function (error) {
            _this.hideDialog();
            console.log(error);
        });
    };
    // showing the dialog
    IdeasComponent.prototype.showDialog = function () {
        this.dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_5__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: 1 } });
    };
    // hiding the dialog
    IdeasComponent.prototype.hideDialog = function () {
        this.dialogRef.close();
    };
    IdeasComponent.prototype.onSearchSubmit = function () {
        var _this = this;
        this.showDialog();
        var strStringFetch = this.searchIdeaForm.get('searchField').value;
        strStringFetch = '%' + strStringFetch + '%';
        this.ideaServices.getSearchIdea({ strSearch: strStringFetch }).subscribe(function (result) {
            _this.hideDialog();
            if (result.status) {
                console.log(result);
                if (result.data.length > 0) {
                    _this.ideasList = [];
                    _this.ideasList = result.data;
                }
                else {
                    _this.dialogRef = _this.dialog.open(__WEBPACK_IMPORTED_MODULE_5__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: 5, message: 'Your Search did not return any match' } });
                }
            }
        }, function (error) {
            _this.hideDialog();
            console.log(error);
        });
    };
    IdeasComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'app-ideas',
            template: __webpack_require__("./src/app/ideas/ideas.component.html"),
            styles: [__webpack_require__("./src/app/ideas/ideas.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ideas_services__["a" /* IdeasServices */],
            __WEBPACK_IMPORTED_MODULE_3__angular_material__["f" /* MatDialog */]])
    ], IdeasComponent);
    return IdeasComponent;
}());



/***/ }),

/***/ "./src/app/image-slider/image-slider.component.css":
/***/ (function(module, exports) {

module.exports = ".bannerStyle img {\r\n        background-color: rgb(207, 23, 23);\r\n        min-height: 100px;\r\n        text-align: center;\r\n        line-height: 100px;\r\n    }\r\n    .leftRs {\r\n        position: absolute;\r\n        margin: auto;\r\n        top: 0;\r\n        bottom: 0;\r\n        width: 50px;\r\n        height: 50px;\r\n        -webkit-box-shadow: 1px 2px 10px -1px rgba(0, 0, 0, .3);\r\n                box-shadow: 1px 2px 10px -1px rgba(0, 0, 0, .3);\r\n        border-radius: 999px;\r\n        left: 0;\r\n    }\r\n    .rightRs {\r\n        position: absolute;\r\n        margin: auto;\r\n        top: 0;\r\n        bottom: 0;\r\n        width: 50px;\r\n        height: 50px;\r\n        -webkit-box-shadow: 1px 2px 10px -1px rgba(0, 0, 0, .3);\r\n                box-shadow: 1px 2px 10px -1px rgba(0, 0, 0, .3);\r\n        border-radius: 999px;\r\n        right: 0;\r\n    }\r\n    img {\r\n        height: 306px;\r\n        vertical-align: middle;\r\n    }\r\n    .slider{\r\n        margin-top: 70px;\r\n        position: relative;\r\n    }\r\n\r\n"

/***/ }),

/***/ "./src/app/image-slider/image-slider.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-12 slider\">\r\n    <ngx-carousel [inputs]=\"carouselBanner\" [moveToSlide]=\"1\" (onMove)=\"onmoveFn($event)\">\r\n        <ngx-item NgxCarouselItem class=\"bannerStyle\">\r\n          <img src=\"https://www.accenture.com//www.accenture.com/t20171117T101233Z__w__/in-en/_acnmedia/Accenture/Conversion-Assets/DotCom/Images/About-Accenture/Global/23/Accenture-About-New-Marquee-2.png\" alt=\"\">\r\n        </ngx-item>\r\n        <ngx-item NgxCarouselItem class=\"bannerStyle\">\r\n            <img  src=\"https://www.accenture.com//www.accenture.com/t20171117T101233Z__w__/in-en/_acnmedia/Accenture/Conversion-Assets/DotCom/Images/About-Accenture/Global/23/Accenture-About-New-Marquee-2.png\" alt=\"\">\r\n        </ngx-item>\r\n        <ngx-item NgxCarouselItem class=\"bannerStyle\">\r\n            <img  src=\"https://www.accenture.com//www.accenture.com/t20171117T101233Z__w__/in-en/_acnmedia/Accenture/Conversion-Assets/DotCom/Images/About-Accenture/Global/23/Accenture-About-New-Marquee-2.png\" alt=\"\">\r\n        </ngx-item>\r\n        <button NgxCarouselPrev class='leftRs'>&lt;</button>\r\n        <button NgxCarouselNext class='rightRs'>&gt;</button>\r\n      </ngx-carousel>\r\n      \r\n\r\n</div>"

/***/ }),

/***/ "./src/app/image-slider/image-slider.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageSliderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ImageSliderComponent = (function () {
    function ImageSliderComponent() {
    }
    ImageSliderComponent.prototype.ngOnInit = function () {
        this.carouselBanner = {
            grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
            slide: 1,
            speed: 400,
            interval: 4000,
            point: {
                visible: true,
                pointStyles: "\n          .ngxcarouselPoint {\n            list-style-type: none;\n            text-align: center;\n            padding: 12px;\n            margin: 0;\n            white-space: nowrap;\n            overflow: auto;\n            position: absolute;\n            width: 100%;\n            bottom: 20px;\n            left: 0;\n            box-sizing: border-box;\n          }\n          .ngxcarouselPoint li {\n            display: inline-block;\n            border-radius: 999px;\n            background: rgba(255, 255, 255, 0.55);\n            padding: 5px;\n            margin: 0 3px;\n            transition: .4s ease all;\n          }\n          .ngxcarouselPoint li.active {\n              background: white;\n              width: 10px;\n          }\n        "
            },
            load: 2,
            loop: true,
            touch: true
        };
    };
    /* It will be triggered on every slide*/
    ImageSliderComponent.prototype.onmoveFn = function (data) {
    };
    ImageSliderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-image-slider',
            template: __webpack_require__("./src/app/image-slider/image-slider.component.html"),
            styles: [__webpack_require__("./src/app/image-slider/image-slider.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ImageSliderComponent);
    return ImageSliderComponent;
}());



/***/ }),

/***/ "./src/app/innovation-dashboard/innovation-dashboard.component.css":
/***/ (function(module, exports) {

module.exports = ".filterCriteria\r\n{\r\n    border: black 2px solid;\r\n    background-color: aliceblue;\r\n    height: 60px;\r\n}\r\n.formWrapper{\r\n    margin: 200px 200px 2000px 200px;\r\n    font-size: 16px;\r\n}\r\n.dashboardpanel\r\n{\r\n    margin:2px 2px;\r\n    padding:2px;\r\n    background: -webkit-gradient(linear, left top, left bottom, from(#e9e9ed), to(#eef0f5));\r\n    background: linear-gradient(#e9e9ed, #eef0f5);\r\n    border: 2px solid rgb(28, 50, 107);\r\n    border-radius: 5px;\r\n    min-height: 300px;\r\n}\r\n.graphWrapper{\r\n    max-height: 350px;\r\n}\r\n.dashboardheader\r\n{\r\n    text-align: center;\r\n    font-size: 22px;\r\n    font-style: italic;\r\n}"

/***/ }),

/***/ "./src/app/innovation-dashboard/innovation-dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" style=\"padding:20px\">\r\n    <!-- <mat-card class=\"formWrapper\"> -->\r\n  <div class=\"row graphWrapper\">\r\n    <div class=\"col-md-5 dashboardpanel\" id=\"Towerdata\">      \r\n      <ngx-charts-advanced-pie-chart *ngIf=\"single && single.length > 0\" [scheme]=\"colorScheme\" [results]=\"single\" [gradient]=\"gradient\">\r\n      </ngx-charts-advanced-pie-chart>\r\n      <div class=\"dashboardheader\">\r\n          Tower Wise View\r\n        </div>\r\n    </div>\r\n    <div class=\"col-md-5 dashboardpanel\" id=\"StatusData\">      \r\n      <ngx-charts-advanced-pie-chart  *ngIf=\"multi && multi.length > 0\" [scheme]=\"statColorcheme\" [results]=\"multi\" [gradient]=\"gradient\">\r\n      </ngx-charts-advanced-pie-chart>\r\n      <div class=\"dashboardheader\">\r\n          Innovation Status\r\n        </div>\r\n    </div>\r\n  </div>  \r\n  <div class=\"row graphWrapper\">\r\n    <div class=\"col-md-5 dashboardpanel\" id=\"TechData\">      \r\n      <ngx-charts-advanced-pie-chart  *ngIf=\"tech && tech.length > 0\" [scheme]=\"TechColorcheme\" [results]=\"tech\" [gradient]=\"gradient\">\r\n      </ngx-charts-advanced-pie-chart>\r\n      <div class=\"dashboardheader\">\r\n          Technology\r\n       </div>\r\n    </div>\r\n    <div class=\"col-md-5 dashboardpanel\" id=\"BenefitArea\" >\r\n        <ngx-charts-advanced-pie-chart  *ngIf=\"finance && finance.length > 0\" [scheme]=\"TechColorcheme\" [results]=\"finance\" [gradient]=\"gradient\">\r\n          </ngx-charts-advanced-pie-chart>\r\n        <div class=\"dashboardheader\">\r\n          Quarter Wise Hours\r\n        </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/innovation-dashboard/innovation-dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InnovationDashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils_excelService__ = __webpack_require__("./src/app/utils/excelService.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__innovationDashboardService__ = __webpack_require__("./src/app/innovation-dashboard/innovationDashboardService.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



//import {single,multi} from './data';

var InnovationDashboardComponent = (function () {
    function InnovationDashboardComponent(
        //public dialogRef: MatDialogRef<DashboardDialogComponent>,
        excelService, httpConfig, dashboardService) {
        this.excelService = excelService;
        this.httpConfig = httpConfig;
        this.dashboardService = dashboardService;
        this.pdf = new jsPDF('l', 'mm', 'a4');
        this.BAview = [480, 350];
        this.view = [450, 350];
        this.elements = ['Towerdata', 'StatusData', 'TechData'];
        this.elCount = -1;
        this.colorScheme = {
            // domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
            domain: ['#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886']
        };
        this.statColorcheme = {
            // domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
            domain: ['#A10A28', '#D3342D', '#EF6D49', '#FAAD67', '#FDDE90', '#DBED91', '#A9D770', '#6CBA67', '#2C9653', '#146738']
        };
        this.TechColorcheme = {
            // domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
            domain: ['#042e40', '#075170', '#53868b', '#b5d5e2', '#008b8b', '#bdb76b', '#3a90b3', '#6cacc6', '#9dc7d9', '#b5d5e2']
        };
        this.verStacScheme = {
            domain: ['#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886']
        };
        this.BAlegendTitle = 'Efforts';
        // options
        this.showXAxis = true;
        this.showYAxis = true;
        this.gradient = false;
        this.showLegend = true;
        this.showXAxisLabel = true;
        this.xAxisLabel = 'Ideas';
        this.showYAxisLabel = true;
        this.yAxisLabel = 'Status';
        // line, area
        this.autoScale = true;
    }
    InnovationDashboardComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.getTowerCount();
                this.getStatusCount();
                this.getTechCount();
                this.getFinanceHourCount();
                return [2 /*return*/];
            });
        });
    };
    InnovationDashboardComponent.prototype.getTowerCount = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var innovationDashBoardDetailsURL = _this.httpConfig.getIdeasCountByTower();
            _this.dashboardService.innovationDashboard(innovationDashBoardDetailsURL).subscribe(function (result) {
                if (result.status) {
                    //(result.status ? resolve(result.data) : reject(result));
                    if (result.data.length > 0) {
                        // this.single = [];
                        _this.single = result.data;
                    }
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    InnovationDashboardComponent.prototype.getTechCount = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var innovationDashBoardDetailsURL = _this.httpConfig.getIdeasCountByTech();
            _this.dashboardService.innovationDashboard(innovationDashBoardDetailsURL).subscribe(function (result) {
                if (result.status) {
                    if (result.data.length > 0) {
                        _this.tech = [];
                        _this.tech = result.data;
                    }
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    InnovationDashboardComponent.prototype.getStatusCount = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var innovationDashBoardDetailsURL = _this.httpConfig.getIdeasCountByStatus();
            _this.dashboardService.innovationDashboard(innovationDashBoardDetailsURL).subscribe(function (result) {
                if (result.status) {
                    //(result.status ? resolve(result.data) : reject(result));
                    if (result.data.length > 0) {
                        _this.multi = [];
                        _this.multi = result.data;
                        console.log(_this.multi);
                    }
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    InnovationDashboardComponent.prototype.getFinanceHourCount = function () {
        var _this = this;
        console.log("Hello");
        return new Promise(function (resolve, reject) {
            var innovationDashBoardDetailsURL = _this.httpConfig.getIdeasCountByQuarter();
            console.log(innovationDashBoardDetailsURL);
            _this.dashboardService.innovationDashboard(innovationDashBoardDetailsURL).subscribe(function (result) {
                if (result.status) {
                    //(result.status ? resolve(result.data) : reject(result));
                    if (result.data.length > 0) {
                        _this.finance = [];
                        _this.finance = result.data;
                        console.log(_this.finance);
                    }
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    InnovationDashboardComponent.prototype.exportDataAspdf = function () {
        this.pdf.setFontSize(22);
        this.pdf.setFont('times');
        this.pdf.setFontType('bolditalic');
        this.pdf.text('Automation Report', 150, 10, 'center');
    };
    InnovationDashboardComponent.prototype.recursiveAddHtml = function () {
        var _this = this;
        this.elCount++;
        if (this.elCount < this.elements.length) {
            this.pdf.setFontSize(16);
            this.pdf.setFont('helvetica');
            this.pdf.setFontType('italic');
            this.pdf.text(this.elements[this.elCount], 10, 30);
            this.pdf.addHTML(document.getElementById(this.elements[this.elCount]), 80, 40, function () {
                switch (_this.elements[_this.elCount]) {
                    case 'Towerdata':
                        _this.printTowerData(_this.single);
                        break;
                    case 'StatusData':
                        //  this.printStatusData();
                        break;
                    case 'TechData':
                        //this.printBenefitData();
                        break;
                }
                _this.pdf.setLineWidth(0.5);
                _this.pdf.line(10, 150, 150, 150);
                _this.pdf.addPage();
                _this.recursiveAddHtml();
            });
        }
        else {
            // this.printDelayedData();
            //this.pdf.save('Automation_Dashboard.pdf');
        }
    };
    InnovationDashboardComponent.prototype.printTowerData = function (data) {
        var _this = this;
        var rowsTower = [];
        var rowsStatus = [];
        var columnsTower = [
            { title: 'ID', dataKey: 'id' },
            { title: 'Tower', dataKey: 'tower' },
            { title: 'Count', dataKey: 'count' }
        ];
        this.count = 0;
        console.log("testb");
        console.log(data);
        data.forEach(function (element) {
            rowsTower.push({
                id: ++_this.count,
                tower: element.name,
                count: element.value
            });
        });
        console.log("Hi");
        console.log(rowsTower);
    };
    // exporting data as excel 
    InnovationDashboardComponent.prototype.exportDataAsExcel = function () {
        this.excelService.exportAsExcelFile(this.single, 'tempFile');
    };
    InnovationDashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'app-innovation-dashboard',
            template: __webpack_require__("./src/app/innovation-dashboard/innovation-dashboard.component.html"),
            styles: [__webpack_require__("./src/app/innovation-dashboard/innovation-dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__utils_excelService__["a" /* ExcelService */], __WEBPACK_IMPORTED_MODULE_2__utils_httpConfig__["a" /* HttpCOnfig */], __WEBPACK_IMPORTED_MODULE_3__innovationDashboardService__["a" /* innovationDashBoardService */]])
    ], InnovationDashboardComponent);
    return InnovationDashboardComponent;
}());



/***/ }),

/***/ "./src/app/innovation-dashboard/innovationDashboardService.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return innovationDashBoardService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
Fetching Automation Details through Http Calls
*/
var innovationDashBoardService = (function () {
    function innovationDashBoardService(httpClient) {
        this.httpClient = httpClient;
    }
    // Fetch Automation Dashboard Details
    innovationDashBoardService.prototype.innovationDashboard = function (URL) {
        return this.httpClient.get(URL, {});
    };
    innovationDashBoardService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], innovationDashBoardService);
    return innovationDashBoardService;
}());



/***/ }),

/***/ "./src/app/landing-page/landing-page.component.css":
/***/ (function(module, exports) {

module.exports = ".bannerStyle img {\r\n    background-color: rgb(207, 23, 23);\r\n    min-height: 100px;\r\n    text-align: center;\r\n    line-height: 100px;\r\n}\r\n.leftRs {\r\n    position: absolute;\r\n    margin: auto;\r\n    top: 0;\r\n    bottom: 0;\r\n    width: 50px;\r\n    height: 50px;\r\n    -webkit-box-shadow: 1px 2px 10px -1px rgba(0, 0, 0, .3);\r\n            box-shadow: 1px 2px 10px -1px rgba(0, 0, 0, .3);\r\n    border-radius: 999px;\r\n    left: 0;\r\n}\r\n.rightRs {\r\n    position: absolute;\r\n    margin: auto;\r\n    top: 0;\r\n    bottom: 0;\r\n    width: 50px;\r\n    height: 50px;\r\n    -webkit-box-shadow: 1px 2px 10px -1px rgba(0, 0, 0, .3);\r\n            box-shadow: 1px 2px 10px -1px rgba(0, 0, 0, .3);\r\n    border-radius: 999px;\r\n    right: 0;\r\n}\r\nimg {\r\n    height: 306px;\r\n    vertical-align: middle;\r\n}\r\n.slider{\r\n    margin-top: 70px;\r\n    position: relative;\r\n}\r\n\r\n"

/***/ }),

/***/ "./src/app/landing-page/landing-page.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-12 col-sm-12 col-xs-12  text-center\">\r\n  <div class=\"col-md-12 slider\">\r\n    <ngx-carousel [inputs]=\"carouselBanner\" [moveToSlide]=\"1\" (onMove)=\"onmoveFn($event)\">\r\n      <ngx-item NgxCarouselItem class=\"bannerStyle\">\r\n        <img src=\"https://i.imgur.com/NtitLfJ.png\"\r\n          alt=\"\">\r\n      </ngx-item>\r\n      <!-- automate -->\r\n      <ngx-item NgxCarouselItem class=\"bannerStyle\">\r\n        <img src=\"https://i.imgur.com/yxdlpaK.png\"\r\n          alt=\"\">\r\n      </ngx-item>\r\n      <!-- innovate -->\r\n      <ngx-item NgxCarouselItem class=\"bannerStyle\">\r\n        <img src=\"https://i.imgur.com/oz8JQji.png\"\r\n          alt=\"\">\r\n      </ngx-item>\r\n      <!-- originate -->\r\n      <ngx-item NgxCarouselItem class=\"bannerStyle\">\r\n        <img src=\"https://i.imgur.com/dpX7Ji8.png\"\r\n          alt=\"\">\r\n      </ngx-item>\r\n      <button NgxCarouselPrev class='leftRs'>&lt;</button>\r\n      <button NgxCarouselNext class='rightRs'>&gt;</button>\r\n    </ngx-carousel>\r\n\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/landing-page/landing-page.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LandingPageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LandingPageComponent = (function () {
    function LandingPageComponent() {
    }
    LandingPageComponent.prototype.ngOnInit = function () {
        this.carouselBanner = {
            grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
            slide: 1,
            speed: 400,
            interval: 2000,
            point: {
                visible: true,
                pointStyles: "\n          .ngxcarouselPoint {\n            list-style-type: none;\n            text-align: center;\n            padding: 12px;\n            margin: 0;\n            white-space: nowrap;\n            overflow: auto;\n            position: absolute;\n            width: 100%;\n            bottom: 20px;\n            left: 0;\n            box-sizing: border-box;\n          }\n          .ngxcarouselPoint li {\n            display: inline-block;\n            border-radius: 999px;\n            background: rgba(255, 255, 255, 0.55);\n            padding: 5px;\n            margin: 0 3px;\n            transition: .4s ease all;\n          }\n          .ngxcarouselPoint li.active {\n              background: white;\n              width: 10px;\n          }\n        "
            },
            load: 0,
            loop: true,
            touch: true
        };
    };
    /* It will be triggered on every slide*/
    LandingPageComponent.prototype.onmoveFn = function (data) {
    };
    LandingPageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-landing-page',
            template: __webpack_require__("./src/app/landing-page/landing-page.component.html"),
            styles: [__webpack_require__("./src/app/landing-page/landing-page.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], LandingPageComponent);
    return LandingPageComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/***/ (function(module, exports) {

module.exports = ".formWrapper{\r\n    margin: 90px 0px 0px 0px;\r\n}\r\n\r\n.example-full-width {\r\n    width: 100%;\r\n  }\r\n\r\n.example-radio-group {\r\n    display: -webkit-inline-box;\r\n    display: -ms-inline-flexbox;\r\n    display: inline-flex;\r\n    -webkit-box-orient: horizontal;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: row;\r\n            flex-direction: row;\r\n    width: 100%;\r\n  }\r\n\r\n.example-radio-button {\r\n    margin: 5px;\r\n  }\r\n\r\n.submit{\r\n      margin-top: 10px;\r\n  }"

/***/ }),

/***/ "./src/app/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <div class=\"col-md-6 col-md-offset-3\">\r\n    <mat-card class=\"formWrapper mat-elevation-z1\">\r\n      <!-- form starts here -->\r\n      <form [formGroup]=\"loginForm\" (ngSubmit)=\"onLoginButton()\">\r\n        <mat-form-field class=\"example-full-width\">\r\n          <input matInput placeholder=\"Email\" required formControlName=\"email\">\r\n          <mat-error>\r\n            Please enter a valid email address\r\n          </mat-error>\r\n        </mat-form-field>\r\n        <mat-form-field class=\"example-full-width\">\r\n          <input type=\"password\" matInput placeholder=\"Password\" required formControlName=\"password\">\r\n          <mat-error *ngIf=\"loginForm.hasError('minlength',['password'])\">\r\n            Password length should be min of 5\r\n          </mat-error>\r\n          <mat-error *ngIf=\"loginForm.hasError('maxlength',['password'])\">\r\n            Password length should not be more than 20\r\n          </mat-error>\r\n        </mat-form-field>\r\n        <mat-radio-group class=\"example-radio-group\" required formControlName=\"loginType\">\r\n          <mat-radio-button class=\"example-radio-button\" *ngFor=\"let loginType of loginTypes\" [value]=\"loginType\">\r\n            {{loginType}}\r\n          </mat-radio-button>\r\n        </mat-radio-group>\r\n        <button class=\"submit\" type=\"submit\" [disabled]=\"!loginForm.valid\" mat-raised-button color=\"warn\">Login</button>\r\n      </form>\r\n    </mat-card>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils_checkUserStatus__ = __webpack_require__("./src/app/utils/checkUserStatus.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__progress_dialog_progress_dialog_component__ = __webpack_require__("./src/app/progress-dialog/progress-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__loginServices__ = __webpack_require__("./src/app/login/loginServices.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








// email pattern 
var EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
var LoginComponent = (function () {
    function LoginComponent(httpConfig, dialog, loginService, checkUserStatus, router) {
        this.httpConfig = httpConfig;
        this.dialog = dialog;
        this.loginService = loginService;
        this.checkUserStatus = checkUserStatus;
        this.router = router;
        // type of loggin admin / normal user 
        this.loginTypes = ["Admin", "Non-Admin"];
        // login form 
        this.loginForm = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["d" /* FormGroup */]({
            email: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('@accenture.com', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].pattern(EMAIL_REGEX)])),
            password: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].minLength(5), __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].maxLength(20)])),
            loginType: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].required)
        });
        this.checkUserLoggedInStatus();
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    // when user clicks on login button 
    LoginComponent.prototype.onLoginButton = function () {
        // login as admin if login type is admin or else as user 
        var loginType = this.loginForm.get('loginType').value;
        var loginURL = '';
        var data = {
            email: this.loginForm.get('email').value,
            password: this.loginForm.get('password').value
        };
        if (loginType === "Admin") {
            loginURL = this.httpConfig.getAdminURL();
        }
        else {
            loginURL = this.httpConfig.getUserLoginURL();
        }
        this.loginUserFromServer(loginURL, data);
    };
    LoginComponent.prototype.loginUserFromServer = function (URL, data) {
        var _this = this;
        this.showDIalog(1);
        this.loginService.loginUser(URL, data).subscribe(function (result) {
            _this.hideDialog();
            if (result.status) {
                // store user inforamtion 
                _this.checkUserStatus.saveLoginInformation(JSON.stringify(result.data));
                _this.checkUserStatus.setUserLoggedStatus(true);
                // now goto home page and hide login button 
                _this.redirectOnSuccess();
            }
            else {
                _this.showErrorMessage({ code: 4, message: result.message });
            }
        }, function (error) {
            _this.hideDialog();
            console.log(error);
        });
    };
    // showing dialog 
    LoginComponent.prototype.showDIalog = function (code) {
        this.dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_6__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: 1 } });
    };
    // show error message
    LoginComponent.prototype.showErrorMessage = function (data) {
        this.dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_6__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: data });
    };
    // hiding dialog 
    LoginComponent.prototype.hideDialog = function () {
        this.dialogRef.close();
    };
    /* check whether user logged in or not
    if logged in re direct to /ideas */
    LoginComponent.prototype.checkUserLoggedInStatus = function () {
        if (this.checkUserStatus.isUserLoggedIn()) {
            this.redirectOnSuccess();
        }
    };
    LoginComponent.prototype.redirectOnSuccess = function () {
        this.router.navigateByUrl('/ideas');
    };
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'app-login',
            template: __webpack_require__("./src/app/login/login.component.html"),
            styles: [__webpack_require__("./src/app/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__utils_httpConfig__["a" /* HttpCOnfig */],
            __WEBPACK_IMPORTED_MODULE_5__angular_material__["f" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_7__loginServices__["a" /* LoginService */],
            __WEBPACK_IMPORTED_MODULE_1__utils_checkUserStatus__["a" /* CheckUserStatus */],
            __WEBPACK_IMPORTED_MODULE_0__angular_router__["a" /* Router */]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/login/loginServices.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
this class helps us in making all the HTTP calls for login components
*/
var LoginService = (function () {
    function LoginService(httpClient) {
        this.httpClient = httpClient;
    }
    // make login request to server 
    LoginService.prototype.loginUser = function (URL, data) {
        return this.httpClient.post(URL, { email: data.email, password: data.password });
    };
    LoginService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], LoginService);
    return LoginService;
}());



/***/ }),

/***/ "./src/app/not-found/not-found.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/not-found/not-found.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\r\n  not-found works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/not-found/not-found.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotFoundComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NotFoundComponent = (function () {
    function NotFoundComponent() {
    }
    NotFoundComponent.prototype.ngOnInit = function () {
    };
    NotFoundComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-not-found',
            template: __webpack_require__("./src/app/not-found/not-found.component.html"),
            styles: [__webpack_require__("./src/app/not-found/not-found.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], NotFoundComponent);
    return NotFoundComponent;
}());



/***/ }),

/***/ "./src/app/originate-comments-view/originate-comments-view.component.css":
/***/ (function(module, exports) {

module.exports = ".example-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n    max-height: 500px;\r\n    min-width: 300px;\r\n  }\r\n  \r\n  .mat-table {\r\n    overflow: auto;\r\n    max-height: 500px;\r\n  }\r\n  \r\n  .mat-header-cell {\r\n      font-size: 16px;    \r\n      font-style: italic;    \r\n      font-weight: 700;\r\n  }\r\n  \r\n  .cdk-column-No {\r\n    min-width: 50px;\r\n    margin: 0 2px;\r\n  }\r\n  \r\n  .cdk-column-COMMENT {\r\n    min-width: 200px;\r\n    margin: 0 2px;\r\n  }\r\n  \r\n  .cdk-column-USER {\r\n    min-width: 100px;\r\n    margin: 0 2px;\r\n  }\r\n  \r\n  .cdk-column-DATE {\r\n    min-width: 100px;\r\n    margin: 0 2px;\r\n  }\r\n  \r\n  .mat-dialog-container {\r\n    background: #585b72;\r\n  }"

/***/ }),

/***/ "./src/app/originate-comments-view/originate-comments-view.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container mat-elevation-z8\">\n  <mat-table #table [dataSource]=\"originateCommentsDataSource\">\n\n    <!--- Note that these columns can be defined in any order.\n          The actual rendered columns are set as a property on the row definition\" -->\n    <!-- SL Column -->\n    <ng-container matColumnDef=\"{{displayedColumns[0]}}\">\n      <mat-header-cell *matHeaderCellDef> {{displayedColumns[0]}} </mat-header-cell>\n      <mat-cell *matCellDef=\"let element\"> {{element.No}} </mat-cell>\n    </ng-container>\n\n    <!-- COMMENT Column -->\n    <ng-container matColumnDef=\"{{displayedColumns[1]}}\">\n      <mat-header-cell *matHeaderCellDef> {{displayedColumns[1]}} </mat-header-cell>\n      <mat-cell *matCellDef=\"let element\"> {{element.COMMENT}} </mat-cell>\n    </ng-container>\n\n    <!-- USER Column -->\n    <ng-container matColumnDef=\"{{displayedColumns[2]}}\">\n      <mat-header-cell *matHeaderCellDef> {{displayedColumns[2]}} </mat-header-cell>\n      <mat-cell *matCellDef=\"let element\"> {{element.USER}} </mat-cell>\n    </ng-container>\n\n    <!-- DATE Column -->\n    <ng-container matColumnDef=\"{{displayedColumns[3]}}\">\n      <mat-header-cell *matHeaderCellDef> {{displayedColumns[3]}} </mat-header-cell>\n      <mat-cell *matCellDef=\"let element\"> {{element.DATE | date: 'dd/MM/yyyy' }} </mat-cell>\n    </ng-container>\n\n    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n    <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\n  </mat-table>\n</div>\n"

/***/ }),

/***/ "./src/app/originate-comments-view/originate-comments-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OriginateCommentsViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material_table__ = __webpack_require__("./node_modules/@angular/material/esm5/table.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var OriginateCommentsViewComponent = (function () {
    function OriginateCommentsViewComponent(dialogRef, loginDialog, data) {
        this.dialogRef = dialogRef;
        this.loginDialog = loginDialog;
        this.data = data;
        this.originateComments = [];
        console.log(data);
        this.selectecOriginateDate = data;
    }
    OriginateCommentsViewComponent.prototype.ngOnInit = function () {
        this.BuildDialog(this.selectecOriginateDate);
    };
    OriginateCommentsViewComponent.prototype.BuildDialog = function (data) {
        var _this = this;
        var i = 0;
        data.forEach(function (element) {
            i = i + 1;
            _this.originateComments.push({
                No: i,
                COMMENT: element.Originatecomments,
                USER: element.OriginateCommentsBy,
                DATE: element.OriginateCommentsDate
            });
        });
        console.log(this.originateComments);
        this.displayedColumns = ['No', 'COMMENT', 'USER', 'DATE'];
        this.originateCommentsDataSource = new __WEBPACK_IMPORTED_MODULE_2__angular_material_table__["a" /* MatTableDataSource */](this.originateComments);
    };
    OriginateCommentsViewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-originate-comments-view',
            template: __webpack_require__("./src/app/originate-comments-view/originate-comments-view.component.html"),
            styles: [__webpack_require__("./src/app/originate-comments-view/originate-comments-view.component.css")]
        }),
        __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["h" /* MatDialogRef */],
            __WEBPACK_IMPORTED_MODULE_1__angular_material__["f" /* MatDialog */], Object])
    ], OriginateCommentsViewComponent);
    return OriginateCommentsViewComponent;
}());



/***/ }),

/***/ "./src/app/originate-details/originate-details.component.css":
/***/ (function(module, exports) {

module.exports = ".formWrapper{\r\n    margin: 90px 0px 0px 0px;\r\n    font-size: 16px;\r\n}\r\n\r\n.example-full-width {\r\n    width: 100%;\r\n  }\r\n\r\n.submit{\r\n      margin-top: 10px;\r\n  }\r\n\r\n.unedit\r\n  {\r\n      font-style:bold;\r\n      color: grey;\r\n      \r\n  }\r\n\r\n.date_wrap {\r\n    width: 50%;\r\n  }\r\n\r\nimg {\r\n    border: 1px solid #ddd;\r\n    border-radius: 4px;\r\n    padding: 5px;\r\n    width: 1000px;\r\n}"

/***/ }),

/***/ "./src/app/originate-details/originate-details.component.html":
/***/ (function(module, exports) {

module.exports = "<form [formGroup]=\"OriginateForm\" (ngSubmit)=\"onAddOriginateButton()\">  \r\n  <mat-horizontal-stepper formArrayName=\"formArray\" linear>\r\n    <mat-step formGroupName=\"0\" [stepControl]=\"formArray.get([0])\">\r\n        <ng-template matStepLabel>Basic Info</ng-template>  \r\n<mat-form-field class=\"example-full-width\">\r\n    <mat-select placeholder=\"TOWER\" required formControlName=\"originateTower\" >\r\n      <mat-option *ngFor=\"let originateTow of originateTowerType\" [value]=\"originateTow.value\">\r\n        {{originateTow.value}}\r\n      </mat-option>\r\n    </mat-select>\r\n  </mat-form-field>      \r\n  <mat-form-field class=\"example-full-width\">\r\n    <input matInput autocomplete=\"off\" placeholder=\"TITLE\" required formControlName=\"originateTitle\" [value]=\"originateData.OriginateTitle\" >\r\n    <mat-error>\r\n      Title should be minimum of 15\r\n    </mat-error>\r\n  </mat-form-field>\r\n  <mat-form-field class=\"example-full-width\">\r\n    <textarea matInput autocomplete=\"off\" #messagearea placeholder=\"DESCRIPTION\" maxlength=\"256\" required [value]=\"originateData.OriginateDesc\"  formControlName=\"originateDescription\" ></textarea>\r\n    <mat-hint align=\"end\">{{messagearea.value.length}} / 256</mat-hint>\r\n    <mat-error *ngIf=\"OriginateForm.hasError('minlength',['originateDescription'])\">\r\n      <p>Description should be between 20 to 256 </p>\r\n    </mat-error>\r\n  </mat-form-field >\r\n  <mat-form-field class=\"example-full-width\">\r\n    <mat-select placeholder=\"CATEGORY\" required formControlName=\"originateCategory\" >\r\n      <mat-option *ngFor=\"let originateCat of originateCategoryType\" [value]=\"originateCat.value\">\r\n        {{originateCat.value}}\r\n      </mat-option>\r\n    </mat-select>\r\n  </mat-form-field>\r\n  <mat-form-field class=\"example-full-width\">\r\n      <input matInput autocomplete=\"off\" placeholder=\"Accenture POC \" required formControlName=\"originateACNStakeholder\">\r\n    </mat-form-field>\r\n    <mat-form-field class=\"example-full-width\">\r\n        <input matInput autocomplete=\"off\" placeholder=\"Client POC \" required formControlName=\"originateCLNStakeholder\">\r\n      </mat-form-field>            \r\n  <mat-form-field class=\"example-full-width\">\r\n      <mat-select placeholder=\"Technology(Java,SQL..)\" required formControlName=\"originateTechnology\">\r\n        <mat-option *ngFor=\"let tech of originateTechonologyType\" [value]=\"tech.value\">\r\n          {{tech.value}}\r\n        </mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n</mat-step>\r\n<mat-step formGroupName=\"1\" [stepControl]=\"formArray.get([1])\">\r\n  <ng-template matStepLabel>Benefits Info</ng-template>\r\n  <mat-form-field style=\"width: 45%\">\r\n    <mat-select placeholder=\"STATUS\" required  formControlName=\"originateStatus\" >\r\n      <mat-option *ngFor=\"let originateStat of originateStatusType\" [value]=\"originateStat.value\">\r\n        {{originateStat.value}}\r\n      </mat-option>\r\n    </mat-select>\r\n  </mat-form-field>           \r\n  <mat-form-field class=\"date_wrap\">\r\n      <input matInput autocomplete=\"off\"  [matDatepickerFilter]=\"myFilter\" [matDatepicker]=\"pickerTo\" placeholder=\"Target Date\" required formControlName=\"originateETCDate\">\r\n      <mat-datepicker-toggle matSuffix [for]=\"pickerTo\"></mat-datepicker-toggle>\r\n      <mat-datepicker #pickerTo></mat-datepicker>\r\n  </mat-form-field> \r\n  <mat-form-field style=\"width: 50%\">\r\n      <input matInput autocomplete=\"off\" type=\"number\" placeholder=\"Proposed CCI\" required formControlName=\"proposedCCI\">\r\n    </mat-form-field>\r\n    <mat-form-field >\r\n        <input matInput autocomplete=\"off\" type=\"number\" placeholder=\"Approved CCI\" required formControlName=\"approvedCCI\">\r\n      </mat-form-field>\r\n  <mat-form-field style=\"width: 50%\">\r\n    <input matInput autocomplete=\"off\" type=\"number\" placeholder=\"Win % \" required formControlName=\"originateProbability\">\r\n    <mat-error>\r\n      Probability Should be < 100\r\n    </mat-error>\r\n  </mat-form-field>\r\n  <mat-form-field >\r\n    <input matInput autocomplete=\"off\" type=\"number\" placeholder=\"Estimate (MDs)\" required formControlName=\"originateROMEstimate\">\r\n  </mat-form-field>\r\n  <mat-form-field style=\"width: 50%\">\r\n    <input matInput autocomplete=\"off\" type=\"number\" placeholder=\"Revenue (AUD)\" required formControlName=\"originateRevenue\">\r\n  </mat-form-field>    \r\n  <mat-form-field >\r\n      <mat-select placeholder=\"Approver\"  formControlName=\"originateApprover\">\r\n        <mat-option *ngFor=\"let approv of originateApprover\" [value]=\"approv.value\">\r\n          {{approv.value}}\r\n        </mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n    <mat-form-field class=\"example-full-width\">\r\n        <textarea matInput autocomplete=\"off\" #message placeholder=\"COMMENTS\" maxlength=\"256\" required formControlName=\"originateComments\" ></textarea>\r\n        <mat-hint align=\"end\">{{message.value.length}} / 256</mat-hint>\r\n        <mat-error *ngIf=\"OriginateForm.hasError('minlength',['originateComments'])\">\r\n          <p>Comments should be between 20 to 256 </p>\r\n        </mat-error>\r\n      </mat-form-field >\r\n</mat-step>\r\n</mat-horizontal-stepper>             \r\n  <button class=\"submit\" type=\"submit\" formcontrolName=\"submit\" [disabled]=\"!OriginateForm.valid\" mat-raised-button color=\"warn\">SUBMIT</button>\r\n</form>"

/***/ }),

/***/ "./src/app/originate-details/originate-details.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OriginateDetailsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utils_checkUserStatus__ = __webpack_require__("./src/app/utils/checkUserStatus.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__progress_dialog_progress_dialog_component__ = __webpack_require__("./src/app/progress-dialog/progress-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__add_originate_addoriginate__ = __webpack_require__("./src/app/add-originate/addoriginate.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_util__ = __webpack_require__("./node_modules/util/util.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_util___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_util__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









var OriginateDetailsComponent = (function () {
    // OriginateForm
    function OriginateDetailsComponent(_formBuilder, httpConfig, dialog, userinfo, addOriginateService, router, data) {
        this._formBuilder = _formBuilder;
        this.httpConfig = httpConfig;
        this.dialog = dialog;
        this.userinfo = userinfo;
        this.addOriginateService = addOriginateService;
        this.router = router;
        this.data = data;
        this.originateTowerType = [];
        this.originateTechonologyType = [];
        this.originateApprover = [];
        this.originateCategoryType = [];
        this.originateStatusType = [];
        this.myFilter = function (d) {
            var day = d.getDay();
            var year = d.getFullYear();
            var currentDate = new Date();
            var currentyear = currentDate.getFullYear();
            // Prevent Saturday and Sunday from being selected.
            return day !== 0 && day !== 6 && year > currentyear - 2 && year < currentyear + 2;
        };
        this.getUserInfo();
        console.log(data);
        this.originateData = data;
    }
    Object.defineProperty(OriginateDetailsComponent.prototype, "formArray", {
        get: function () { return this.OriginateForm.get('formArray'); },
        enumerable: true,
        configurable: true
    });
    OriginateDetailsComponent.prototype.getUserInfo = function () {
        if (Object(__WEBPACK_IMPORTED_MODULE_8_util__["isNull"])(JSON.parse(this.userinfo.getUserDetails()))) {
            this.router.navigateByUrl('/login');
        }
        else {
            this.user = JSON.parse(this.userinfo.getUserDetails())[0].username;
        }
    };
    OriginateDetailsComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var originateEntityData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.OriginateForm = this._formBuilder.group({
                            formArray: this._formBuilder.array([
                                this._formBuilder.group({
                                    originateTitle: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* FormControl */]({ value: this.originateData.OriginateTitle, disabled: true }, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].minLength(15)])),
                                    originateDescription: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* FormControl */]({ value: this.originateData.OriginateDesc, disabled: false }, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].minLength(20)])),
                                    originateTower: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* FormControl */]({ value: this.originateData.OriginateTower, disabled: true }, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].required),
                                    originateCategory: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* FormControl */]({ value: this.originateData.OriginateCategoryType, disabled: false }, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].required),
                                    originateACNStakeholder: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* FormControl */]({ value: this.originateData.OriginateACNStakeholder, disabled: false }, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].required),
                                    originateCLNStakeholder: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* FormControl */]({ value: this.originateData.OriginateCLNStakeholder, disabled: false }, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].required),
                                    originateTechnology: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* FormControl */]({ value: this.originateData.OriginateTechnology, disabled: false }, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].minLength(1))
                                }),
                                this._formBuilder.group({
                                    originateStatus: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* FormControl */]({ value: this.originateData.OriginateStatusType, disabled: false }, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].required),
                                    originateETCDate: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* FormControl */]({ value: this.originateData.OriginateETCDate, disabled: false }, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].required),
                                    proposedCCI: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* FormControl */]({ value: this.originateData.ProposedCCI, disabled: false }, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].required),
                                    approvedCCI: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* FormControl */]({ value: this.originateData.ApprovedCCI, disabled: false }, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].required),
                                    originateProbability: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* FormControl */]({ value: this.originateData.OriginateProbability, disabled: false }, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].max(100)])),
                                    originateROMEstimate: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* FormControl */]({ value: this.originateData.OriginateEstimate, disabled: false }, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].required),
                                    originateRevenue: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* FormControl */]({ value: this.originateData.OriginateRevenue, disabled: false }, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].required),
                                    originateApprover: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* FormControl */](),
                                    originateComments: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* FormControl */]({ value: this.originateData.OriginateComments, disabled: false }, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["l" /* Validators */].minLength(20)]))
                                })
                            ])
                        });
                        originateEntityData = '';
                        return [4 /*yield*/, this.FetchOriginateEntity()];
                    case 1:
                        originateEntityData = _a.sent();
                        this.PopulateDropdowns(originateEntityData);
                        return [2 /*return*/];
                }
            });
        });
    };
    OriginateDetailsComponent.prototype.FetchOriginateEntity = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var originateDetailsURL = _this.httpConfig.getOriginateEntity();
            _this.addOriginateService.originateEntity(originateDetailsURL).subscribe(function (result) {
                if (result.status) {
                    (result.status ? resolve(result.data) : reject(result));
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    OriginateDetailsComponent.prototype.PopulateDropdowns = function (dropdownResults) {
        var _this = this;
        console.log(dropdownResults);
        var serverData = [];
        dropdownResults.forEach(function (element) {
            serverData.push({
                item: element.originateentityID,
                value: element.originateentityDesc,
                Type: element.originateentityTitle
            });
        });
        serverData.forEach(function (element) {
            if (element.Type === 'originateTower') {
                _this.originateTowerType.push(element);
            }
            if (element.Type === 'originateStatusType') {
                _this.originateStatusType.push(element);
            }
            if (element.Type === 'originateCategoryType') {
                _this.originateCategoryType.push(element);
            }
            if (element.Type === 'originateTechnology') {
                _this.originateTechonologyType.push(element);
            }
            if (element.Type === 'originateApprover') {
                _this.originateApprover.push(element);
            }
        });
    };
    OriginateDetailsComponent.prototype.openDIalog = function (code) {
        this.dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_6__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: code } });
    };
    OriginateDetailsComponent.prototype.closeDialog = function () {
        this.dialogRef.close();
    };
    OriginateDetailsComponent.prototype.successGif = function () {
        var _this = this;
        // first reset form data
        this.closeDialog();
        this.openDIalog(2);
        setTimeout(function () { _this.closeDialog(); }, 2000);
    };
    OriginateDetailsComponent.prototype.resetForm = function () {
        this.myform.resetForm();
    };
    OriginateDetailsComponent.prototype.onAddOriginateButton = function () {
        var _this = this;
        // login as admin if login type is admin or else as user
        this.openDIalog(1);
        var frmArry = this.OriginateForm.get('formArray');
        var frmArryValues = frmArry.value;
        var originateDataUpdate = {
            OriginateID: this.originateData.OriginateID,
            OriginateCategoryType: frmArryValues[0].originateCategory,
            OriginateDesc: frmArryValues[0].originateDescription,
            OriginateACNStakeholder: frmArryValues[0].originateACNStakeholder,
            OriginateCLNStakeholder: frmArryValues[0].originateCLNStakeholder,
            OriginateTechnology: frmArryValues[0].originateTechnology,
            OriginateETCDate: frmArryValues[1].originateETCDate,
            OriginateStatusType: frmArryValues[1].originateStatus,
            ProposedCCI: frmArryValues[1].proposedCCI,
            ApprovedCCI: frmArryValues[1].approvedCCI,
            OriginateProbability: frmArryValues[1].originateProbability,
            OriginateEstimate: frmArryValues[1].originateROMEstimate,
            OriginateRevenue: frmArryValues[1].originateRevenue,
            OriginateApprover: frmArryValues[1].originateApprover,
            OriginateComments: frmArryValues[1].originateComments,
            OriginateCreatedBy: this.user
        };
        var originateUpdateURL = this.httpConfig.updateoriginate();
        console.log(originateDataUpdate);
        this.addOriginateService.updateoriginate(originateUpdateURL, { data: originateDataUpdate }).subscribe(function (result) {
            if (result.status) {
                console.log(result.status);
                _this.successGif();
                location.reload();
            }
            else {
                console.log(result.message);
            }
        }, function (error) {
            console.log('error', error);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_4__angular_forms__["e" /* FormGroupDirective */]),
        __metadata("design:type", Object)
    ], OriginateDetailsComponent.prototype, "myform", void 0);
    OriginateDetailsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-originate-details',
            template: __webpack_require__("./src/app/originate-details/originate-details.component.html"),
            styles: [__webpack_require__("./src/app/originate-details/originate-details.component.css")]
        }),
        __param(6, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_3__angular_material__["a" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["b" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_5__utils_httpConfig__["a" /* HttpCOnfig */],
            __WEBPACK_IMPORTED_MODULE_3__angular_material__["f" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_2__utils_checkUserStatus__["a" /* CheckUserStatus */],
            __WEBPACK_IMPORTED_MODULE_7__add_originate_addoriginate__["a" /* AddOriginateService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */], Object])
    ], OriginateDetailsComponent);
    return OriginateDetailsComponent;
}());



/***/ }),

/***/ "./src/app/originatedashdialog/originatedashdialog.component.css":
/***/ (function(module, exports) {

module.exports = "example-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n    max-height: 500px;\r\n    min-width: 300px;\r\n  }\r\n  \r\n  .mat-table {\r\n    overflow: auto;\r\n    max-height: 500px;\r\n  }\r\n  \r\n  .mat-header-cell {\r\n      font-size: 16px;    \r\n      font-style: italic;    \r\n      font-weight: 700;\r\n  }"

/***/ }),

/***/ "./src/app/originatedashdialog/originatedashdialog.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container mat-elevation-z8\">\r\n  <mat-table #table [dataSource]=\"originateDatasource\">\r\n    <!-- TOWER Column -->\r\n    <ng-container matColumnDef=\"{{displayedColumns[0]}}\">\r\n        <mat-header-cell *matHeaderCellDef> {{displayedColumns[0]}} </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.tower}} </mat-cell>\r\n      </ng-container>\r\n\r\n    <!-- CATEGORY Column -->\r\n    <ng-container matColumnDef=\"{{displayedColumns[1]}}\">\r\n      <mat-header-cell *matHeaderCellDef> {{displayedColumns[1]}} </mat-header-cell>\r\n      <mat-cell *matCellDef=\"let element\"> {{element.category}} </mat-cell>\r\n    </ng-container>\r\n\r\n    <!-- STATUS Column -->\r\n    <ng-container matColumnDef=\"{{displayedColumns[2]}}\">\r\n      <mat-header-cell *matHeaderCellDef> {{displayedColumns[2]}} </mat-header-cell>\r\n      <mat-cell *matCellDef=\"let element\"> {{element.status}} </mat-cell>\r\n    </ng-container>\r\n\r\n    <!-- TITLE Column -->\r\n    <ng-container matColumnDef=\"{{displayedColumns[3]}}\">\r\n      <mat-header-cell *matHeaderCellDef> {{displayedColumns[3]}} </mat-header-cell>\r\n      <mat-cell *matCellDef=\"let element\"> {{element.title}} </mat-cell>\r\n    </ng-container>\r\n\r\n    <!-- DESCRIPTION Column -->\r\n    <ng-container matColumnDef=\"{{displayedColumns[4]}}\">\r\n      <mat-header-cell *matHeaderCellDef> {{displayedColumns[4]}} </mat-header-cell>\r\n      <mat-cell *matCellDef=\"let element\"> {{element.description}} </mat-cell>\r\n    </ng-container>\r\n\r\n    <!-- STAKEHOLDER Column -->\r\n    <ng-container matColumnDef=\"{{displayedColumns[5]}}\">\r\n      <mat-header-cell *matHeaderCellDef> {{displayedColumns[5]}} </mat-header-cell>\r\n      <mat-cell *matCellDef=\"let element\"> {{element.stakeholder}} </mat-cell>\r\n    </ng-container>\r\n\r\n    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n    <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n  </mat-table>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/originatedashdialog/originatedashdialog.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OriginatedashdialogComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material_table__ = __webpack_require__("./node_modules/@angular/material/esm5/table.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var OriginatedashdialogComponent = (function () {
    function OriginatedashdialogComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.originatedata = [];
        this.selectOriginatedata = data.detailsData;
        this.selectedType = data.type;
    }
    OriginatedashdialogComponent.prototype.ngOnInit = function () {
        this.BuildDialog(this.selectOriginatedata);
    };
    OriginatedashdialogComponent.prototype.BuildDialog = function (data) {
        var _this = this;
        data.forEach(function (element) {
            _this.originatedata.push({
                tower: element.tower,
                category: element.category,
                status: element.status,
                title: element.title,
                description: element.description,
                stakeholder: element.stakeholder,
                probability: element.probability,
                estimate: element.estimate,
                revenue: element.revenue
            });
        });
        this.displayedColumns = ['TOWER', 'CATEGORY', 'STATUS', 'TITLE', 'DESCRIPTION', 'STAKEHOLDER'];
        this.originateDatasource = new __WEBPACK_IMPORTED_MODULE_2__angular_material_table__["a" /* MatTableDataSource */](this.originatedata);
    };
    OriginatedashdialogComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-originatedashdialog',
            template: __webpack_require__("./src/app/originatedashdialog/originatedashdialog.component.html"),
            styles: [__webpack_require__("./src/app/originatedashdialog/originatedashdialog.component.css")]
        }),
        __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["h" /* MatDialogRef */], Object])
    ], OriginatedashdialogComponent);
    return OriginatedashdialogComponent;
}());



/***/ }),

/***/ "./src/app/peopledashboard/peopleDashBoardService.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PeopleDashboardService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PeopleDashboardService = (function () {
    function PeopleDashboardService(httpClient) {
        this.httpClient = httpClient;
    }
    //Fetch all Training
    PeopleDashboardService.prototype.getAlltrainings = function (URL) {
        return this.httpClient.get(URL, {});
    };
    PeopleDashboardService.prototype.getEnrolledUserCount = function (URL) {
        return this.httpClient.get(URL, {});
    };
    PeopleDashboardService.prototype.getEnrolledUserCountbyMonth = function (URL) {
        return this.httpClient.get(URL, {});
    };
    PeopleDashboardService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], PeopleDashboardService);
    return PeopleDashboardService;
}());



/***/ }),

/***/ "./src/app/peopledashboard/peopledashboard.component.css":
/***/ (function(module, exports) {

module.exports = ".filterCriteria\r\n{\r\n    border: black 2px solid;\r\n    background-color: aliceblue;\r\n    height: 60px;\r\n}\r\n.formWrapper{\r\n    margin: 200px 200px 2000px 200px;\r\n    font-size: 16px;\r\n}\r\n.filterCriteria1\r\n{\r\n    height:39px;\r\n    background-color: aliceblue;\r\n    \r\n}\r\n.btn{\r\n    margin-top: 100;\r\n    height:46px;\r\n}\r\n.dashboardpanel\r\n{\r\n    margin:2px 2px;\r\n    padding:2px;\r\n    background: -webkit-gradient(linear, left top, left bottom, from(#e9e9ed), to(#eef0f5));\r\n    background: linear-gradient(#e9e9ed, #eef0f5);\r\n    border: 2px solid rgb(28, 50, 107);\r\n    border-radius: 5px;\r\n    height: 400px;\r\n}\r\n.example-full-width {\r\n    width: 15%;\r\n  }\r\n.dashboard\r\n{\r\n    margin:2px 2px;\r\n    padding:2px;\r\n    background: -webkit-gradient(linear, left top, left bottom, from(#e9e9ed), to(#eef0f5));\r\n    background: linear-gradient(#e9e9ed, #eef0f5);\r\n    border: 2px solid rgb(28, 50, 107);\r\n    border-radius: 5px;\r\n    height: 800px;\r\n    width: 800px;\r\n}\r\n.graphWrapper{\r\n    max-height: 150px;\r\n}\r\n.dashboardheader\r\n{\r\n    text-align: center;\r\n    font-size: 22px;\r\n    font-style: italic;\r\n}"

/***/ }),

/***/ "./src/app/peopledashboard/peopledashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" style=\"padding:20px\">\r\n    <form [formGroup]=\"PeopleDashboardForm\">\r\n  <div class=\"col-md-12  \">\r\n      <mat-form-field class=\"example-full-width wrapper filterCriteria1\">\r\n          <mat-select placeholder=\"Timeperiod\" (ngModelChange)=\"OnPeriodChange($event)\" formControlName=\"timePeriod\">\r\n            <mat-option value=\"6\">Last 6 months</mat-option>\r\n            <mat-option value=\"3\">Last 3 months</mat-option>\r\n            <mat-option value=\"-3\">Next 3 months</mat-option>\r\n            \r\n          </mat-select>\r\n        </mat-form-field>     \r\n    \r\n      <button mat-raised-button class=\"btn\" color=\"warn\" (click)=\"ClickExcel()\">Export as Excel</button>\r\n      <button mat-raised-button class=\"btn\" color=\"warn\" (click)=\"ClickPDF()\">Export as PDF</button>\r\n    \r\n  </div>\r\n  </form>\r\n</div>\r\n<div class=\"container\" style=\"padding:20px\">\r\n<div  class=\"row\">\r\n  <div class=\"col-md-5 dashboardpanel\"  id=\"CompleteTraining\">\r\n    <ngx-charts-advanced-pie-chart *ngIf=\"PiechartTrainingType && PiechartTrainingType.length > 0\" [view]=\"view\" [scheme]=\"SkillTypeScheme\" [results]=\"PiechartTrainingType\"\r\n      [gradient]=\"gradient\">\r\n    </ngx-charts-advanced-pie-chart>\r\n    <div class=\"dashboardheader\">\r\n      Trainings\r\n    </div>\r\n </div>\r\n  <div class=\"col-md-5 dashboardpanel\"  id=\"SkilledEmpCount\">\r\n    <ngx-charts-advanced-pie-chart *ngIf=\"PieChartUserCount && PieChartUserCount.length > 0\" [view]=\"view\" [scheme]=\"UserCountScheme\" [results]=\"PieChartUserCount\"\r\n      [gradient]=\"gradient\">\r\n    </ngx-charts-advanced-pie-chart>\r\n    <div class=\"dashboardheader\">\r\n       Enrolled UserCount\r\n    </div>\r\n </div>\r\n\r\n \r\n</div>\r\n<div  class=\"row\">\r\n  <div class=\"col-md-5 dashboardpanel\" id=\"TrainingHistory\" >\r\n    <ngx-charts-bar-vertical-stacked *ngIf=\"stackChartdata && stackChartdata.length > 0\" [view]=\"BAview\" [scheme]=\"verStacScheme\"\r\n      [results]=\"stackChartdata\" [gradient]=\"true\" [xAxis]=\"true\" [yAxis]=\"true\" [legend]=\"true\" [legendTitle]=\"BAlegendTitle\"\r\n      (select)=\"onBASelect($event)\">\r\n    </ngx-charts-bar-vertical-stacked>\r\n    <div class=\"dashboardheader\">\r\n        Training History\r\n    </div>\r\n    </div>\r\n    <div class=\"col-md-5 dashboardpanel\" id=\"EnrolledUserCount\" >\r\n      <ngx-charts-bar-vertical-stacked *ngIf=\"barChartTrained && barChartTrained.length > 0\" [view]=\"BAview\" [scheme]=\"verStacScheme\"\r\n        [results]=\"barChartTrained\" [gradient]=\"true\" [xAxis]=\"true\" [yAxis]=\"true\" [legend]=\"true\" [legendTitle]=\"BAlegendTitle\">\r\n             \r\n      </ngx-charts-bar-vertical-stacked>\r\n      <div class=\"dashboardheader\">\r\n        Enrolled UserCount\r\n      </div>\r\n</div>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/peopledashboard/peopledashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PeopledashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__peopleDashBoardService__ = __webpack_require__("./src/app/peopledashboard/peopleDashBoardService.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_typescript_linq__ = __webpack_require__("./node_modules/typescript-linq/TS.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_typescript_linq___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_typescript_linq__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





//import { element } from 'protractor';
var PeopledashboardComponent = (function () {
    function PeopledashboardComponent(peopleDashboardservice, _formBuilder, httpConfig) {
        this.peopleDashboardservice = peopleDashboardservice;
        this._formBuilder = _formBuilder;
        this.httpConfig = httpConfig;
        this.elements = ['CompleteTraining', 'SkilledEmpCount', 'TrainingHistory', 'EnrolledUserCount'];
        this.pdf = new jsPDF('l', 'mm', 'a4');
        this.elCount = -1;
        this.count = 0;
        this.sample = new __WEBPACK_IMPORTED_MODULE_4_typescript_linq__["TS"].Collections.List(false);
        this.view = [480, 350];
        this.BAview = [480, 350];
        this.BAlegendTitle = 'Initiative';
        this.SkillTypeScheme = {
            domain: ['#647c8a', '#3f51b5', '#2196f3', '#00b862', '#afdf0a', '#a7b61a', '#f3e562', '#ff9800', '#ff5722', '#ff4514']
        };
        this.UserCountScheme = {
            domain: ['#A10A28', '#D3342D', '#EF6D49', '#FAAD67', '#FDDE90', '#DBED91', '#A9D770', '#6CBA67', '#2C9653', '#146738']
        };
        this.verStacScheme = {
            domain: ['#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886']
        };
        this.gradient = false;
        this.trainingTech = ["NewIT", "Domain", "Technical", "SoftSkill"];
        this.trainingTypeData = [];
        this.FutureTrainingTypeData = [];
        this.trainingUserCount = [];
        this.trainingUserCount1 = [];
        this.PiechartTrainingType = [];
        this.PieChartUserCount = [];
        this.PieChartFutureTraining = [];
        this.barChartTraining = [];
        this.futTrainingDate = ["name", "series"];
        this.futTrainingDate2 = [];
        this.futTrainingChartData = [];
        this.stackChartdata = [];
        this.usrCountByMonth = [];
        this.barChartTrained = [];
        //public mon:Array<any>=[];
        this.mon = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    }
    PeopledashboardComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, UsertrainingCount, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        this.PeopleDashboardForm = this._formBuilder.group({
                            timePeriod: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]()
                        });
                        _a = this;
                        return [4 /*yield*/, this.getAllTrainings()];
                    case 1:
                        _a.trainingList = _c.sent();
                        this.TrainingcountbyType(this.trainingList);
                        this.PiechartTrainingType = this.trainingTypeData;
                        console.log(this.PiechartTrainingType);
                        return [4 /*yield*/, this.getUserCount()];
                    case 2:
                        UsertrainingCount = _c.sent();
                        console.log("Users");
                        console.log(UsertrainingCount);
                        this.TrainingUserCount(UsertrainingCount);
                        this.PieChartUserCount = this.trainingUserCount1;
                        this.futureTrainingList(this.trainingList);
                        this.PieChartFutureTraining = this.FutureTrainingTypeData;
                        this.pastTrainingList();
                        this.stackChartdata = this.futTrainingDate2;
                        console.log("Im here");
                        console.log(this.stackChartdata);
                        //let UsertrainingCountbyMonth:any;
                        _b = this;
                        return [4 /*yield*/, this.getEnrolledUserCountbyMonth()];
                    case 3:
                        //let UsertrainingCountbyMonth:any;
                        _b.UsertrainingCountbyMonth = _c.sent();
                        console.log(this.UsertrainingCountbyMonth);
                        this.UserCountByMonth(this.UsertrainingCountbyMonth);
                        this.barChartTrained = this.usrCountByMonth;
                        return [2 /*return*/];
                }
            });
        });
    };
    PeopledashboardComponent.prototype.getAllTrainings = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var getalltrainingList = _this.httpConfig.getAllTrainings();
            console.log(getalltrainingList);
            _this.peopleDashboardservice.getAlltrainings(getalltrainingList).subscribe(function (result) {
                if (result.status) {
                    (result.status ? resolve(result.data) : reject(result));
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    PeopledashboardComponent.prototype.TrainingcountbyType = function (data) {
        var _this = this;
        if (data.length > 0) {
            this.trainingTech.forEach(function (element) {
                var TypeCount = data.filter(function (train) { return train.TrainingSkillType === element; });
                if (TypeCount.length > 0) {
                    _this.trainingTypeData.push({
                        name: element,
                        value: TypeCount.length
                    });
                }
                else {
                    _this.trainingTypeData.push({
                        name: element,
                        value: TypeCount.length
                    });
                }
            });
        }
        else {
            console.log("Training List is empty");
        }
    };
    PeopledashboardComponent.prototype.getUserCount = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var getUserCountTrainingType = _this.httpConfig.getTrainingsUserCount();
            console.log(getUserCountTrainingType);
            _this.peopleDashboardservice.getEnrolledUserCount(getUserCountTrainingType).subscribe(function (result) {
                if (result.status) {
                    (result.status ? resolve(result.data) : reject(result));
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    PeopledashboardComponent.prototype.TrainingUserCount = function (data) {
        var _this = this;
        if (data.length > 0) {
            data.forEach(function (element) { return _this.trainingUserCount1.push({
                name: element["TrainingSkillType"],
                value: element["EmployeeCount"]
            }); });
        }
        else {
            console.log("Enrolled userlist is empty");
        }
    };
    PeopledashboardComponent.prototype.futureTrainingList = function (data) {
        var _this = this;
        if (data.length > 0) {
            var datepipe = void 0;
            var date = new Date();
            var mon = date.getMonth();
            console.log(date);
            var filtertrainingList_1;
            filtertrainingList_1 = data.filter(function (item) {
                var itemTime = new Date(item.TrainingStartDate);
                return itemTime > date;
            });
            console.log(filtertrainingList_1);
            this.trainingTech.forEach(function (element) {
                console.log(element);
                var pastSkillTypeCount = filtertrainingList_1.filter(function (train) { return train.TrainingSkillType == element; });
                console.log("HI");
                console.log(pastSkillTypeCount);
                if (pastSkillTypeCount.length > 0) {
                    console.log("BeforePushingData");
                    var result = void 0;
                    console.log(_this.FutureTrainingTypeData);
                    _this.FutureTrainingTypeData.push({
                        name: element,
                        value: pastSkillTypeCount.length
                    });
                }
                else {
                    _this.FutureTrainingTypeData.push({
                        name: element,
                        value: pastSkillTypeCount.length
                    });
                }
                console.log("AfterPushingData1");
                console.log(_this.FutureTrainingTypeData);
            });
        }
        else {
            console.log("Training List is empty");
        }
    };
    PeopledashboardComponent.prototype.pastTrainingList = function () {
        var data;
        data = this.trainingList;
        console.log("I m in fn");
        var dt = new Date();
        var mn, i;
        i = 3;
        if (data.length > 0) {
            var _loop_1 = function () {
                console.log(i);
                console.log("in while loop");
                var mnth;
                mnth = this_1.monthAdd(dt, -i);
                var filterbyMonth = void 0, pastSkillTypeCount1 = void 0;
                var trngcount = [];
                filterbyMonth = data.filter(function (item) {
                    var itemTime = new Date(item.TrainingStartDate).getMonth();
                    var itemYr = new Date(item.TrainingStartDate).getFullYear();
                    return (itemTime === (mnth.getMonth() + 1) && (itemYr === mnth.getFullYear()));
                });
                console.log(filterbyMonth);
                var _loop_2 = function (tech) {
                    console.log("For Loop");
                    console.log(tech);
                    pastSkillTypeCount1 = filterbyMonth.filter(function (train) { return train.TrainingSkillType === tech; });
                    console.log(pastSkillTypeCount1.length);
                    trngcount.push({
                        name: tech,
                        value: pastSkillTypeCount1.length
                    });
                    console.log(trngcount);
                };
                for (var _i = 0, _a = this_1.trainingTech; _i < _a.length; _i++) {
                    var tech = _a[_i];
                    _loop_2(tech);
                }
                this_1.futTrainingDate2.push({
                    name: this_1.mon[mnth.getMonth() + 1],
                    series: trngcount
                });
                console.log(this_1.futTrainingDate2);
                i--;
            };
            var this_1 = this;
            //  mn=this.monthAdd(dt,-6);
            while (i > 0) {
                _loop_1();
            }
        }
        else {
            console.log("Training List is empty");
        }
    };
    PeopledashboardComponent.prototype.addToMonth = function (date, months) {
        var d = new Date(date || new Date());
        d.setMonth(d.getMonth() + (months || 0), d.getDate());
        return d;
    };
    PeopledashboardComponent.prototype.monthAdd = function (date, month) {
        var temp = date;
        console.log("month1");
        console.log(month);
        temp = new Date(date.getFullYear(), date.getMonth(), 1);
        var test = temp.getMonth() + (month + 1);
        console.log("test1");
        console.log(test);
        temp.setMonth(temp.getMonth() + (month + 1));
        console.log("month");
        console.log(temp);
        temp.setDate(temp.getDate() - 1);
        console.log("date");
        console.log(temp);
        console.log("compare");
        console.log(date.getDate());
        console.log(temp.getDate());
        if (date.getDate() < temp.getDate()) {
            temp.setDate(date.getDate());
        }
        console.log("final");
        console.log(temp);
        return temp;
    };
    PeopledashboardComponent.prototype.getEnrolledUserCountbyMonth = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var getUserCountByMonth = _this.httpConfig.getEnrolledUserCountByMonth();
            console.log(getUserCountByMonth);
            _this.peopleDashboardservice.getEnrolledUserCountbyMonth(getUserCountByMonth).subscribe(function (result) {
                if (result.status) {
                    (result.status ? resolve(result.data) : reject(result));
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    PeopledashboardComponent.prototype.UserCountByMonth = function (data) {
        var dt = new Date();
        var mn, i;
        i = 3;
        if (data.length > 0) {
            var _loop_3 = function () {
                var mnth;
                mnth = this_2.monthAdd(dt, -i);
                var cntbyMonth = void 0, skillcnt = void 0;
                var trngcount = [];
                cntbyMonth = data.filter(function (item) {
                    var itemTime = new Date(item.trngStrtDate).getMonth();
                    var itemYr = new Date(item.trngStrtDate).getFullYear();
                    return (itemTime === (mnth.getMonth() + 1) && (itemYr === mnth.getFullYear()));
                });
                console.log(cntbyMonth);
                var _loop_4 = function (tech) {
                    console.log("For Loop");
                    console.log(tech);
                    skillcnt = cntbyMonth.filter(function (train) { return train.TrainingSkillType === tech; });
                    console.log(skillcnt);
                    var cnt = void 0;
                    if (skillcnt.length > 0) {
                        cnt = skillcnt[0].empcount;
                    }
                    else {
                        cnt = 0;
                    }
                    trngcount.push({
                        name: tech,
                        value: cnt
                    });
                };
                for (var _i = 0, _a = this_2.trainingTech; _i < _a.length; _i++) {
                    var tech = _a[_i];
                    _loop_4(tech);
                }
                this_2.usrCountByMonth.push({
                    name: this_2.mon[mnth.getMonth() + 1],
                    series: trngcount
                });
                console.log(this_2.usrCountByMonth);
                i--;
            };
            var this_2 = this;
            while (i > 0) {
                _loop_3();
            }
        }
        else {
            console.log("Enrolled User count is empty");
        }
    };
    PeopledashboardComponent.prototype.ClickPDF = function () {
        this.sample = new __WEBPACK_IMPORTED_MODULE_4_typescript_linq__["TS"].Collections.List(false);
        this.pdf = new jsPDF('l', 'mm', 'a4');
        this.elCount = -1;
        this.pdf.setFontSize(22);
        this.pdf.setFont('times');
        this.pdf.setFontType('bolditalic');
        this.pdf.text('People Training Report', 150, 10, 'center');
        this.recursiveAddHtml();
    };
    PeopledashboardComponent.prototype.recursiveAddHtml = function () {
        var _this = this;
        this.elCount++;
        if (this.elCount < this.elements.length) {
            this.pdf.setFontSize(16);
            this.pdf.setFont('helvetica');
            this.pdf.setFontType('italic');
            switch (this.elements[this.elCount]) {
                case 'CompleteTraining':
                    this.pdf.text("Complete Training List by Skill Type", 10, 30);
                    break;
                case 'SkilledEmpCount':
                    this.pdf.text(" Resources Training Enrollment Count", 10, 30);
                    break;
                case 'TrainingHistory':
                    this.pdf.text("Training History", 10, 30);
                    break;
                case 'EnrolledUserCount':
                    this.pdf.text("Employees trained count in previous months", 10, 30);
                    break;
            }
            //this.pdf.text(this.elements[this.elCount] , 10 , 30);
            this.pdf.addHTML(document.getElementById(this.elements[this.elCount]), 80, 40, function () {
                switch (_this.elements[_this.elCount]) {
                    case 'CompleteTraining':
                        _this.printoverallTraining();
                        break;
                    case 'SkilledEmpCount':
                        _this.printOverallTrainedEmployeeCount();
                        break;
                    case 'TrainingHistory':
                        _this.printTrainingHistory();
                        break;
                    case 'EnrolledUserCount':
                        _this.printEnrolledUserCount();
                        break;
                }
                _this.pdf.setLineWidth(0.5);
                _this.pdf.line(10, 150, 150, 150);
                _this.pdf.addPage();
                _this.recursiveAddHtml();
            });
        }
        else {
            this.pdf.save('Automation_Dashboard.pdf');
        }
    };
    PeopledashboardComponent.prototype.printoverallTraining = function () {
        var _this = this;
        var rowsArea = [];
        var columnsArea = [
            { title: 'ID', dataKey: 'id' },
            { title: 'TrainingType', dataKey: 'TrainingType' },
            { title: 'Count', dataKey: 'count' }
        ];
        this.count = 0;
        this.PiechartTrainingType.forEach(function (element) {
            rowsArea.push({
                id: ++_this.count,
                TrainingType: element.name,
                count: element.value
            });
        });
        this.pdf.autoTable(columnsArea, rowsArea, {
            margin: { vertical: 40 },
            styles: { overflow: 'linebreak', columnWidth: 'wrap' },
            columnStyles: { text: { columnWidth: 'auto' } }
        });
    };
    PeopledashboardComponent.prototype.printOverallTrainedEmployeeCount = function () {
        var _this = this;
        var rowsArea = [];
        var columnsArea = [
            { title: 'ID', dataKey: 'id' },
            { title: 'TrainingType', dataKey: 'TrainingType' },
            { title: 'Count', dataKey: 'count' }
        ];
        this.count = 0;
        this.PieChartUserCount.forEach(function (element) {
            rowsArea.push({
                id: ++_this.count,
                TrainingType: element.name,
                count: element.value
            });
        });
        this.pdf.autoTable(columnsArea, rowsArea, {
            margin: { vertical: 40 },
            styles: { overflow: 'linebreak', columnWidth: 'wrap' },
            columnStyles: { text: { columnWidth: 'auto' } }
        });
    };
    PeopledashboardComponent.prototype.printTrainingHistory = function () {
        var _this = this;
        var test1, i, j;
        j = 40;
        if (this.futTrainingChartData.length > 0) {
            var rowsArea_1 = [];
            var maincolumnsArea = [
                // { title: 'ID', dataKey: 'id' },
                { title: 'TrainingType', dataKey: 'TrainingType' },
                { title: 'Count', dataKey: 'count' }
            ];
            var lcount = 0;
            this.futTrainingChartData.forEach(function (element) {
                j = j + 50;
                if (element.series.length > 0) {
                    rowsArea_1.push({
                        TrainingType: element.name,
                    });
                    element.series.forEach(function (ele) {
                        rowsArea_1.push({
                            // id:++lcount,
                            TrainingType: ele.name,
                            count: ele.value
                        });
                    });
                    var k = 30;
                    var l = 40;
                    console.log("Col Value");
                    console.log(rowsArea_1);
                    _this.pdf.text(k, l, "");
                    k = k + 10;
                    l = l + 10;
                }
            });
            this.pdf.autoTable(maincolumnsArea, rowsArea_1, {
                margin: { vertical: 40 },
                styles: { overflow: 'linebreak', columnWidth: 'wrap' },
                columnStyles: { text: { columnWidth: 'auto' } }
            });
        }
    };
    PeopledashboardComponent.prototype.printEnrolledUserCount = function () {
        var _this = this;
        var test1, i, j;
        j = 40;
        if (this.barChartTrained.length > 0) {
            var rowsArea_2 = [];
            var maincolumnsArea = [
                { title: 'TrainingType', dataKey: 'TrainingType' },
                { title: 'Count', dataKey: 'count' }
            ];
            var lcount = 0;
            this.barChartTrained.forEach(function (element) {
                j = j + 50;
                if (element.series.length > 0) {
                    rowsArea_2.push({
                        TrainingType: element.name,
                    });
                    element.series.forEach(function (ele) {
                        rowsArea_2.push({
                            // id:++lcount,
                            TrainingType: ele.name,
                            count: ele.value
                        });
                    });
                    var k = 30;
                    var l = 40;
                    console.log("Col Value");
                    console.log(rowsArea_2);
                    _this.pdf.text(k, l, "");
                    k = k + 10;
                    l = l + 10;
                }
            });
            this.pdf.autoTable(maincolumnsArea, rowsArea_2, {
                margin: { vertical: 40 },
                styles: { overflow: 'linebreak', columnWidth: 'wrap' },
                columnStyles: { text: { columnWidth: 'auto' } }
            });
        }
    };
    PeopledashboardComponent.prototype.OnPeriodChange = function (event) {
        var mn;
        var data, data1, cntbyMonth, skillcnt;
        var futTrainingDate3 = [];
        data = this.trainingList;
        data1 = this.UsertrainingCountbyMonth;
        console.log("inside evwent");
        console.log(event);
        if (event !== -1) {
            if (event == 6) {
                this.evntchange = 6;
            }
            if (event == 3) {
                this.evntchange = 3;
            }
            if (event == -3) {
                this.evntchange = -3;
            }
        }
        var dt = new Date();
        /*training History for future date */
        if (this.evntchange > 0) {
            if (data.length > 0) {
                var cnt = void 0;
                cnt = this.evntchange;
                var _loop_5 = function () {
                    var cntbyMonth_1 = void 0, skillcnt_1;
                    var usrtrngcount = [];
                    console.log(this_3.evntchange);
                    console.log("in while loop");
                    var mnth;
                    mnth = this_3.monthAdd(dt, -cnt);
                    var filterbyMonth = void 0, pastSkillTypeCount1 = void 0;
                    var trngcount = [];
                    filterbyMonth = data.filter(function (item) {
                        var itemTime = new Date(item.TrainingStartDate).getMonth();
                        var itemYr = new Date(item.TrainingStartDate).getFullYear();
                        return (itemTime === (mnth.getMonth() + 1) && (itemYr === mnth.getFullYear()));
                    });
                    console.log(filterbyMonth);
                    /*for user count */
                    cntbyMonth_1 = data.filter(function (item) {
                        var itemTime = new Date(item.trngStrtDate).getMonth();
                        var itemYr = new Date(item.trngStrtDate).getFullYear();
                        return (itemTime === (mnth.getMonth() + 1) && (itemYr === mnth.getFullYear()));
                    });
                    var _loop_6 = function (tech) {
                        console.log("For Loop");
                        console.log(tech);
                        pastSkillTypeCount1 = filterbyMonth.filter(function (train) { return train.TrainingSkillType === tech; });
                        console.log(pastSkillTypeCount1.length);
                        trngcount.push({
                            name: tech,
                            value: pastSkillTypeCount1.length
                        });
                        console.log(trngcount);
                    };
                    for (var _i = 0, _a = this_3.trainingTech; _i < _a.length; _i++) {
                        var tech = _a[_i];
                        _loop_6(tech);
                    }
                    console.log("brfore push");
                    console.log(trngcount);
                    console.log(this_3.mon[mnth.getMonth() + 1]);
                    futTrainingDate3.push({
                        name: this_3.mon[mnth.getMonth() + 1],
                        series: trngcount
                    });
                    console.log(futTrainingDate3);
                    /*for user training count*/
                    cnt--;
                };
                var this_3 = this;
                while (cnt > 0) {
                    _loop_5();
                }
                console.log(futTrainingDate3);
                this.futTrainingChartData = futTrainingDate3;
                this.stackChartdata = futTrainingDate3;
                //  this.barChartTrained=futuserTrainingDate3;
            }
            if (data1.length > 0) {
                var usrcnt1 = void 0;
                var futuserTrainingDate3 = [];
                usrcnt1 = this.evntchange;
                console.log(data1.length);
                console.log(this.evntchange);
                //  mn=this.monthAdd(dt,-6);
                if (usrcnt1 > 0) {
                    var _loop_7 = function () {
                        var mnth;
                        mnth = this_4.monthAdd(dt, -usrcnt1);
                        var cntbyMonth_2 = void 0, skillcnt_2 = void 0;
                        var trngcount = [];
                        cntbyMonth_2 = data1.filter(function (item) {
                            var itemTime = new Date(item.trngStrtDate).getMonth();
                            var itemYr = new Date(item.trngStrtDate).getFullYear();
                            return (itemTime === (mnth.getMonth() + 1) && (itemYr === mnth.getFullYear()));
                        });
                        console.log(cntbyMonth_2);
                        var _loop_8 = function (tech) {
                            console.log("For Loop");
                            console.log(tech);
                            skillcnt_2 = cntbyMonth_2.filter(function (train) { return train.TrainingSkillType === tech; });
                            console.log(skillcnt_2);
                            var cnt = void 0;
                            if (skillcnt_2.length > 0) {
                                cnt = skillcnt_2[0].empcount;
                            }
                            else {
                                cnt = 0;
                            }
                            trngcount.push({
                                name: tech,
                                value: cnt
                            });
                        };
                        for (var _i = 0, _a = this_4.trainingTech; _i < _a.length; _i++) {
                            var tech = _a[_i];
                            _loop_8(tech);
                        }
                        futuserTrainingDate3.push({
                            name: this_4.mon[mnth.getMonth() + 1],
                            series: trngcount
                        });
                        console.log(futuserTrainingDate3);
                        usrcnt1--;
                    };
                    var this_4 = this;
                    while (usrcnt1 > 0) {
                        _loop_7();
                    }
                    this.futTrainingChartData =
                        this.barChartTrained = futuserTrainingDate3;
                }
            }
            else {
                console.log("Training is empty");
            }
        }
        /*training History for past  date */
        if (this.evntchange < 0) {
            if (data.length > 0) {
                var dummyval = 0;
                this.evntchange = this.evntchange + 1;
                var _loop_9 = function () {
                    console.log(dummyval);
                    console.log("in while loop");
                    var mnth;
                    mnth = this_5.monthAdd(dt, -dummyval);
                    console.log("add");
                    console.log(mnth);
                    var filterbyMonth = void 0, pastSkillTypeCount1 = void 0;
                    var trngcount = [];
                    filterbyMonth = data.filter(function (item) {
                        var itemTime = new Date(item.TrainingStartDate).getMonth();
                        var itemYr = new Date(item.TrainingStartDate).getFullYear();
                        return (itemTime === (mnth.getMonth()) && (itemYr === mnth.getFullYear()));
                    });
                    console.log(filterbyMonth);
                    var _loop_10 = function (tech) {
                        console.log("For Loop");
                        console.log(tech);
                        pastSkillTypeCount1 = filterbyMonth.filter(function (train) { return train.TrainingSkillType === tech; });
                        console.log(pastSkillTypeCount1.length);
                        trngcount.push({
                            name: tech,
                            value: pastSkillTypeCount1.length
                        });
                        console.log(trngcount);
                    };
                    for (var _i = 0, _a = this_5.trainingTech; _i < _a.length; _i++) {
                        var tech = _a[_i];
                        _loop_10(tech);
                    }
                    futTrainingDate3.push({
                        name: this_5.mon[mnth.getMonth()],
                        series: trngcount
                    });
                    console.log(futTrainingDate3);
                    dummyval--;
                };
                var this_5 = this;
                while (dummyval >= this.evntchange) {
                    _loop_9();
                }
                console.log(futTrainingDate3);
                this.futTrainingChartData = futTrainingDate3;
                this.stackChartdata = futTrainingDate3;
            }
            if (data1.length > 0) {
                var usrcnt1 = void 0;
                var futuserTrainingDate3 = [];
                var dummyval = 0;
                usrcnt1 = this.evntchange;
                console.log(data1.length);
                console.log(this.evntchange);
                var _loop_11 = function () {
                    var mnth;
                    mnth = this_6.monthAdd(dt, -dummyval);
                    var cntbyMonth_3 = void 0, skillcnt_3 = void 0;
                    var trngcount = [];
                    cntbyMonth_3 = data1.filter(function (item) {
                        var itemTime = new Date(item.trngStrtDate).getMonth();
                        var itemYr = new Date(item.trngStrtDate).getFullYear();
                        return (itemTime === (mnth.getMonth()) && (itemYr === mnth.getFullYear()));
                    });
                    var _loop_12 = function (tech) {
                        skillcnt_3 = cntbyMonth_3.filter(function (train) { return train.TrainingSkillType === tech; });
                        var cnt = void 0;
                        if (skillcnt_3.length > 0) {
                            cnt = skillcnt_3[0].empcount;
                        }
                        else {
                            cnt = 0;
                        }
                        trngcount.push({
                            name: tech,
                            value: cnt
                        });
                    };
                    for (var _i = 0, _a = this_6.trainingTech; _i < _a.length; _i++) {
                        var tech = _a[_i];
                        _loop_12(tech);
                    }
                    futuserTrainingDate3.push({
                        name: this_6.mon[mnth.getMonth()],
                        series: trngcount
                    });
                    dummyval--;
                };
                var this_6 = this;
                //  mn=this.monthAdd(dt,-6);
                while (dummyval >= usrcnt1) {
                    _loop_11();
                }
                this.barChartTrained = futuserTrainingDate3;
            }
        }
    };
    PeopledashboardComponent.prototype.ClickExcel = function () {
    };
    PeopledashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-peopledashboard',
            template: __webpack_require__("./src/app/peopledashboard/peopledashboard.component.html"),
            styles: [__webpack_require__("./src/app/peopledashboard/peopledashboard.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__peopleDashBoardService__["a" /* PeopleDashboardService */], __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_2__utils_httpConfig__["a" /* HttpCOnfig */]])
    ], PeopledashboardComponent);
    return PeopledashboardComponent;
}());



/***/ }),

/***/ "./src/app/progress-dialog/progress-dialog.component.css":
/***/ (function(module, exports) {

module.exports = ".check_mark {\r\n    width: 80px;\r\n    height: 130px;\r\n    margin: 0 auto;\r\n  }\r\n  \r\n  button {\r\n    cursor: pointer;\r\n    margin-left: 15px;\r\n  }\r\n  \r\n  .hide{\r\n    display:none;\r\n  }\r\n  \r\n  .sa-icon {\r\n    width: 80px;\r\n    height: 80px;\r\n    border: 4px solid gray;\r\n    border-radius: 40px;\r\n    border-radius: 50%;\r\n    margin: 20px auto;\r\n    padding: 0;\r\n    position: relative;\r\n    -webkit-box-sizing: content-box;\r\n            box-sizing: content-box;\r\n  }\r\n  \r\n  .sa-icon.sa-success {\r\n    border-color: #4CAF50;\r\n  }\r\n  \r\n  .sa-icon.sa-success::before, .sa-icon.sa-success::after {\r\n    content: '';\r\n    border-radius: 40px;\r\n    border-radius: 50%;\r\n    position: absolute;\r\n    width: 60px;\r\n    height: 120px;\r\n    background: white;\r\n    -webkit-transform: rotate(45deg);\r\n    transform: rotate(45deg);\r\n  }\r\n  \r\n  .sa-icon.sa-success::before {\r\n    border-radius: 120px 0 0 120px;\r\n    top: -7px;\r\n    left: -33px;\r\n    -webkit-transform: rotate(-45deg);\r\n    transform: rotate(-45deg);\r\n    -webkit-transform-origin: 60px 60px;\r\n    transform-origin: 60px 60px;\r\n  }\r\n  \r\n  .sa-icon.sa-success::after {\r\n    border-radius: 0 120px 120px 0;\r\n    top: -11px;\r\n    left: 30px;\r\n    -webkit-transform: rotate(-45deg);\r\n    transform: rotate(-45deg);\r\n    -webkit-transform-origin: 0px 60px;\r\n    transform-origin: 0px 60px;\r\n  }\r\n  \r\n  .sa-icon.sa-success .sa-placeholder {\r\n    width: 80px;\r\n    height: 80px;\r\n    border: 4px solid rgba(76, 175, 80, .5);\r\n    border-radius: 40px;\r\n    border-radius: 50%;\r\n    -webkit-box-sizing: content-box;\r\n            box-sizing: content-box;\r\n    position: absolute;\r\n    left: -4px;\r\n    top: -4px;\r\n    z-index: 2;\r\n  }\r\n  \r\n  .sa-icon.sa-success .sa-fix {\r\n    width: 5px;\r\n    height: 90px;\r\n    background-color: white;\r\n    position: absolute;\r\n    left: 28px;\r\n    top: 8px;\r\n    z-index: 1;\r\n    -webkit-transform: rotate(-45deg);\r\n    transform: rotate(-45deg);\r\n  }\r\n  \r\n  .sa-icon.sa-success.animate::after {\r\n    -webkit-animation: rotatePlaceholder 4.25s ease-in;\r\n    animation: rotatePlaceholder 4.25s ease-in;\r\n  }\r\n  \r\n  .sa-icon.sa-success {\r\n    border-color: transparent\\9;\r\n  }\r\n  \r\n  .sa-icon.sa-success .sa-line.sa-tip {\r\n    -ms-transform: rotate(45deg) \\9;\r\n  }\r\n  \r\n  .sa-icon.sa-success .sa-line.sa-long {\r\n    -ms-transform: rotate(-45deg) \\9;\r\n  }\r\n  \r\n  .animateSuccessTip {\r\n    -webkit-animation: animateSuccessTip 0.75s;\r\n    animation: animateSuccessTip 0.75s;\r\n  }\r\n  \r\n  .animateSuccessLong {\r\n    -webkit-animation: animateSuccessLong 0.75s;\r\n    animation: animateSuccessLong 0.75s;\r\n  }\r\n  \r\n  @-webkit-keyframes animateSuccessLong {\r\n    0% {\r\n      width: 0;\r\n      right: 46px;\r\n      top: 54px;\r\n    }\r\n    65% {\r\n      width: 0;\r\n      right: 46px;\r\n      top: 54px;\r\n    }\r\n    84% {\r\n      width: 55px;\r\n      right: 0px;\r\n      top: 35px;\r\n    }\r\n    100% {\r\n      width: 47px;\r\n      right: 8px;\r\n      top: 38px;\r\n    }\r\n  }\r\n  \r\n  @-webkit-keyframes animateSuccessTip {\r\n    0% {\r\n      width: 0;\r\n      left: 1px;\r\n      top: 19px;\r\n    }\r\n    54% {\r\n      width: 0;\r\n      left: 1px;\r\n      top: 19px;\r\n    }\r\n    70% {\r\n      width: 50px;\r\n      left: -8px;\r\n      top: 37px;\r\n    }\r\n    84% {\r\n      width: 17px;\r\n      left: 21px;\r\n      top: 48px;\r\n    }\r\n    100% {\r\n      width: 25px;\r\n      left: 14px;\r\n      top: 45px;\r\n    }\r\n  }\r\n  \r\n  @keyframes animateSuccessTip {\r\n    0% {\r\n      width: 0;\r\n      left: 1px;\r\n      top: 19px;\r\n    }\r\n    54% {\r\n      width: 0;\r\n      left: 1px;\r\n      top: 19px;\r\n    }\r\n    70% {\r\n      width: 50px;\r\n      left: -8px;\r\n      top: 37px;\r\n    }\r\n    84% {\r\n      width: 17px;\r\n      left: 21px;\r\n      top: 48px;\r\n    }\r\n    100% {\r\n      width: 25px;\r\n      left: 14px;\r\n      top: 45px;\r\n    }\r\n  }\r\n  \r\n  @keyframes animateSuccessLong {\r\n    0% {\r\n      width: 0;\r\n      right: 46px;\r\n      top: 54px;\r\n    }\r\n    65% {\r\n      width: 0;\r\n      right: 46px;\r\n      top: 54px;\r\n    }\r\n    84% {\r\n      width: 55px;\r\n      right: 0px;\r\n      top: 35px;\r\n    }\r\n    100% {\r\n      width: 47px;\r\n      right: 8px;\r\n      top: 38px;\r\n    }\r\n  }\r\n  \r\n  .sa-icon.sa-success .sa-line {\r\n    height: 5px;\r\n    background-color: #4CAF50;\r\n    display: block;\r\n    border-radius: 2px;\r\n    position: absolute;\r\n    z-index: 2;\r\n  }\r\n  \r\n  .sa-icon.sa-success .sa-line.sa-tip {\r\n    width: 25px;\r\n    left: 14px;\r\n    top: 46px;\r\n    -webkit-transform: rotate(45deg);\r\n    transform: rotate(45deg);\r\n  }\r\n  \r\n  .sa-icon.sa-success .sa-line.sa-long {\r\n    width: 47px;\r\n    right: 8px;\r\n    top: 38px;\r\n    -webkit-transform: rotate(-45deg);\r\n    transform: rotate(-45deg);\r\n  }\r\n  \r\n  @-webkit-keyframes rotatePlaceholder {\r\n    0% {\r\n      transform: rotate(-45deg);\r\n      -webkit-transform: rotate(-45deg);\r\n    }\r\n    5% {\r\n      transform: rotate(-45deg);\r\n      -webkit-transform: rotate(-45deg);\r\n    }\r\n    12% {\r\n      transform: rotate(-405deg);\r\n      -webkit-transform: rotate(-405deg);\r\n    }\r\n    100% {\r\n      transform: rotate(-405deg);\r\n      -webkit-transform: rotate(-405deg);\r\n    }\r\n  }\r\n  \r\n  @keyframes rotatePlaceholder {\r\n    0% {\r\n      transform: rotate(-45deg);\r\n      -webkit-transform: rotate(-45deg);\r\n    }\r\n    5% {\r\n      transform: rotate(-45deg);\r\n      -webkit-transform: rotate(-45deg);\r\n    }\r\n    12% {\r\n      transform: rotate(-405deg);\r\n      -webkit-transform: rotate(-405deg);\r\n    }\r\n    100% {\r\n      transform: rotate(-405deg);\r\n      -webkit-transform: rotate(-405deg);\r\n    }\r\n  }\r\n  \r\n  .welcomeGif{\r\n    height: 250px;\r\n    width: 250px;\r\n  }\r\n  \r\n  .topBuffer{\r\n    margin: 5px;\r\n    min-width: 400px;\r\n    border-radius: 5px;\r\n    padding: 5px;\r\n    color: red;\r\n}\r\n  \r\n  .example-header-image {\r\n    background-image: url('https://www.accenture.com/us-en/_acnmedia/Accenture/next-gen-4/oracle-technology-vision-2017/img/Accenture-Oracle-Tech-Vision-2017-Lockup.png');\r\n    background-size: cover;\r\n  }\r\n  \r\n  .mat-card-title{\r\n    color: white;\r\n    font-weight: bold;\r\n    margin: 10px;\r\n  }\r\n  \r\n  .description{\r\n      margin-top: 20px;\r\n      color: white;\r\n      height: 2em; \r\n      line-height: 1em; \r\n      overflow: hidden;\r\n  }\r\n  \r\n  .mat-dialog-container{\r\n    padding: 5px;\r\n}"

/***/ }),

/***/ "./src/app/progress-dialog/progress-dialog.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-card *ngIf=\"code < 5\">\r\n  <div *ngIf=\"code == 1\" class=\"marker\">\r\n    <mat-spinner color=\"warn\" [strokeWidth]=\"1\"></mat-spinner>\r\n  </div>\r\n  <!-- this is for success mark -->\r\n  <div *ngIf=\"code == 2\" class=\"check_mark\">\r\n    <div class=\"sa-icon sa-success animate\">\r\n      <span class=\"sa-line sa-tip animateSuccessTip\"></span>\r\n      <span class=\"sa-line sa-long animateSuccessLong\"></span>\r\n      <div class=\"sa-placeholder\"></div>\r\n      <div class=\"sa-fix\"></div>\r\n    </div>\r\n  </div>\r\n  <span *ngIf=\"code == 2\">Thank you for your submission</span>\r\n  <div *ngIf=\"code == 7\" class=\"check_mark\">\r\n      <div class=\"sa-icon sa-success animate\">\r\n        <span class=\"sa-line sa-tip animateSuccessTip\"></span>\r\n        <span class=\"sa-line sa-long animateSuccessLong\"></span>\r\n        <div class=\"sa-placeholder\"></div>\r\n        <div class=\"sa-fix\"></div>\r\n      </div>\r\n    </div>\r\n  <span *ngIf=\"code == 7\">Registered Successfully</span>\r\n  <div *ngIf=\"code == 3\">\r\n    <img class=\"welcomeGif\" src=\"https://brandspace.accenture.com/Pages/downloads/artworks/images/animated_signature/Acc_Animated_Signature_Technology.gif\">\r\n  </div>\r\n  <!-- thi is for like login status -->\r\n  <div *ngIf=\"code == 4\">\r\n    <p>{{message}}</p>\r\n  </div>  \r\n</mat-card>\r\n<div *ngIf=\"code == 5\" style=\"border: 2px;border-color: red;border-style: solid;border-radius: 5px\">\r\n    <p class=\"topBuffer\">\r\n      <span style=\"color:red;font-size: medium;font-weight: bold\"> ! Innovation Search</span>\r\n      <br/>\r\n      <br/>\r\n      <span>{{message}}</span>\r\n    </p>     \r\n</div>\r\n<div *ngIf=\"code == 6\" style=\"border: 2px;border-color: red;border-style: solid;border-radius: 5px\">\r\n    <p class=\"topBuffer\">\r\n      <span style=\"color:red;font-size: medium;font-weight: bold\"> ! Automation Search</span>\r\n      <br/>\r\n      <br/>\r\n      <span>{{message}}</span>\r\n    </p>     \r\n</div>\r\n<div *ngIf=\"code == 7\" style=\"border: 2px;border-color: red;border-style: solid;border-radius: 5px\">\r\n  <p class=\"topBuffer\">\r\n    <span style=\"color:red;font-size: medium;font-weight: bold\"> ! Automation History</span>\r\n    <br/>\r\n    <br/>\r\n    <span>{{message}}</span>\r\n  </p>     \r\n</div>"

/***/ }),

/***/ "./src/app/progress-dialog/progress-dialog.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressDialogComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var ProgressDialogComponent = (function () {
    function ProgressDialogComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.message = '';
        if (this.data.code === 4 || this.data.code === 5 || this.data.code === 6 || this.data.code === 7) {
            this.dialogRef.disableClose = false;
            this.message = this.data.message;
        }
        else {
            this.dialogRef.disableClose = true;
        }
        this.code = data.code;
    }
    // when we close dialog
    ProgressDialogComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    ProgressDialogComponent.prototype.ngOnInit = function () {
    };
    ProgressDialogComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-progress-dialog',
            template: __webpack_require__("./src/app/progress-dialog/progress-dialog.component.html"),
            styles: [__webpack_require__("./src/app/progress-dialog/progress-dialog.component.css"), __webpack_require__("./src/app/progress-dialog/progress-dialog.scss")]
        }),
        __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["h" /* MatDialogRef */], Object])
    ], ProgressDialogComponent);
    return ProgressDialogComponent;
}());



/***/ }),

/***/ "./src/app/progress-dialog/progress-dialog.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/show-automation/show-automation.component.css":
/***/ (function(module, exports) {

module.exports = ".topBuffer{\r\n    margin: 10px 0px 10px 10px;\r\n    background: cadetblue;\r\n}\r\n\r\n.example-header-image {\r\n    background-image: url('https://www.accenture.com/us-en/_acnmedia/Accenture/next-gen-4/oracle-technology-vision-2017/img/Accenture-Oracle-Tech-Vision-2017-Lockup.png');\r\n    background-size: cover;\r\n  }\r\n\r\n.mat-card-title{\r\n    color: white;\r\n    font-weight: bolder;\r\n    margin: 10px;\r\n    width: 100%\r\n  }\r\n\r\n.Completed{\r\n     color:green;\r\n     font-weight: bold\r\n }\r\n\r\n.Cancelled{\r\n     color:red;\r\n     font-weight: bold\r\n }\r\n\r\n/*.New\r\n {\r\n     background-image: url(\"C:\\Users\\a.a.uthayakumar\\Innovation\\Resource\\new.png\");\r\n     \r\n }*/\r\n\r\n.Pending Approval\r\n {\r\n     color:orange;\r\n     font-weight: bold\r\n }\r\n\r\n.description{\r\n      margin-top: 20px;\r\n      color: cornsilk;\r\n      height: 2em; \r\n      font-weight: bold;\r\n      line-height: 1em; \r\n      overflow: hidden;\r\n  }\r\n\r\n.mat-button{\r\n      color: white;\r\n      padding: 0px;\r\n      border: #0F0D0D 2px;\r\n  }\r\n\r\n.mat-dialog-container{\r\n      padding: 2px;\r\n  }\r\n\r\n.searchDiv {\r\n    max-width: 300px;\r\n    display: block;    \r\n    -webkit-box-sizing: border-box;    \r\n            box-sizing: border-box;\r\n    border: 2px solid cadetblue;    \r\n    border-radius: 4px;\r\n    font-size: 16px;\r\n    margin : 10px 2px 10px 30px;\r\n    background-color: cadetblue;    \r\n    padding: 5px;\r\n    -webkit-transition: width 0.4s ease-in-out;\r\n    transition: width 0.4s ease-in-out;\r\n}\r\n\r\n.searchbox_1{\r\n    background-color: #609ea0;\r\n    padding:10px;\r\n    width:auto;\r\n    margin: 10px 30px;\r\n    -webkit-box-sizing:border-box;\r\n    box-sizing:border-box;\r\n    border-radius:6px;\r\n    }\r\n\r\n.search_1{\r\n    width:250px;\r\n    height:35px;\r\n    margin-left: 5px;\r\n    padding:5px;\r\n    border-radius:6px;\r\n    border:none;\r\n    color:#0F0D0D;;\r\n    font-weight:bold;\r\n    background-color:#E2EFF7;\r\n    -webkit-box-shadow:\r\n0 -2px 2px 0 rgba(199, 199, 199, 0.55),\r\n0 1px 1px 0 #fff,\r\n0 2px 2px 1px #fafafa,\r\n0 2px 4px 0 #b2b2b2 inset,\r\n0 -1px 1px 0 #f2f2f2 inset,\r\n0 15px 15px 0 rgba(41, 41, 41, 0.09) inset;\r\nbox-shadow:\r\n0 -2px 2px 0 rgba(199, 199, 199, 0.55),\r\n0 1px 1px 0 #fff,\r\n0 2px 2px 1px #fafafa,\r\n0 2px 4px 0 #b2b2b2 inset,\r\n0 -1px 1px 0 #f2f2f2 inset,\r\n0 15px 15px 0 rgba(41, 41, 41, 0.09) inset;\r\n    }\r\n\r\n.submit_1{\r\n    width:35px;\r\n    height:35px;\r\n    min-width: 15px;\r\n    border:none;\r\n    cursor:pointer;\r\n    background-color: #609ea0\r\n    }\r\n\r\n.search_1:focus{\r\n    outline:0;\r\n    }\r\n\r\n.searchCompletion {\r\n        font-size: medium;\r\n        font-style: oblique;\r\n        font-weight: bold;\r\n    }\r\n"

/***/ }),

/***/ "./src/app/show-automation/show-automation.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper\" style=\"padding-top:4px\">\r\n  <form [formGroup]=\"searchAutomationForm\" (ngSubmit)=\"onSearchSubmit()\">\r\n  <div class='searchbox_1'>\r\n    <div >\r\n        <mat-form-field class=\"example-full-width\">\r\n        <mat-select placeholder=\"Search By\" (ngModelChange)=\"OnSearchByChange($event)\" formControlName=\"searchby\">\r\n            <mat-option *ngFor=\"let searchparam of searchparameters\" [value]=\"searchparam.name\">\r\n              {{searchparam.value}}\r\n            </mat-option>\r\n        </mat-select>\r\n        </mat-form-field>\r\n        <input type=\"search\" class=\"search_1\" placeholder=\"Search\" formControlName=\"searchField\"/>   \r\n        <button class=\"glyphicon glyphicon-search submit_1\" mat-button></button> \r\n        <span class=\"searchCompletion\">Completion ETC : </span><mat-slider class=\"example-margin\" (change)=\"OnTimeByChange($event)\" thumbLabel [max]=\"6\"\r\n        [min]=\"0\"   [step]=\"1\"></mat-slider> (months)\r\n        <span class=\"searchCompletion\">Completed Automation Last: </span><mat-slider class=\"example-margin\" (change)=\"OnTimeByCompletedChange($event)\" thumbLabel [max]=\"6\"\r\n        [min]=\"0\"   [step]=\"1\"></mat-slider> (months)\r\n    </div>\r\n  </div>\r\n  </form>\r\n  <div *ngFor=\"let automation of automationlist; let i = index\" class=\"col-md-4\">\r\n    <mat-card class=\"topBuffer\">\r\n      <mat-card-header>\r\n          <!-- <div style=\"position:absolute;top:0;left:100;right:0;bottom:0;\" [ngClass]=\"{'Completed' : automation.STATUS==='Completed','Pending Approval' : automation.STATUS==='Pending Approval', 'Cancelled' : automation.STATUS==='Cancelled','New' : automation.STATUS==='New'}\">{{automation.STATUS}} </div> -->\r\n          <!-- <img style=\"position:absolute;top:0;left:100;right:0;bottom:0;width:12%;\" [src]=\"{{'../../assets/new.png':automation.STATUS==='New','../../assets/Pending-Approval1.png':automation.STATUS==='Pending Approval','../../assets/completed.png':automation.STATUS==='Completed'}}\"> -->\r\n            <img style=\"position:absolute;top:2%;left:91%;right:0;bottom:100;width:8%;\" [src]=\"getimgsrc(automation.STATUS)\">\r\n        <div mat-card-avatar class=\"example-header-image\"></div>  \r\n        <mat-card-title>\r\n            {{automation.automationTitle | slice:0:50}} \r\n        </mat-card-title> \r\n        <mat-card-title>\r\n           Application: {{automation.automationApplication}} \r\n        </mat-card-title> \r\n        <mat-card-title>\r\n          Tower    : {{automation.automationTower}}\r\n        </mat-card-title>   \r\n      </mat-card-header> \r\n      <mat-card-actions>\r\n        <button  (click)=\"onAutomationClick(i)\" mat-button>show details</button>\r\n        <!-- <button *ngIf=\"isAdmin\" (click)=\"onAutomationClick(i)\" mat-button>Edit</button> -->\r\n      </mat-card-actions>\r\n    </mat-card>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/show-automation/show-automation.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShowAutomationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__showAutomationService__ = __webpack_require__("./src/app/show-automation/showAutomationService.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__progress_dialog_progress_dialog_component__ = __webpack_require__("./src/app/progress-dialog/progress-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__utils_checkUserStatus__ = __webpack_require__("./src/app/utils/checkUserStatus.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__automation_details_automation_details_component__ = __webpack_require__("./src/app/automation-details/automation-details.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ShowAutomationComponent = (function () {
    function ShowAutomationComponent(userStatus, showAutomationService, matDialog, httpConfig) {
        this.userStatus = userStatus;
        this.showAutomationService = showAutomationService;
        this.matDialog = matDialog;
        this.httpConfig = httpConfig;
        this.searchSelection = '';
        this.searchparameters = [{ name: 'automationTower', value: 'Tower' },
            { name: 'automationApplication', value: 'Application' },
            { name: 'automationTitle', value: 'Title' },
            { name: 'STATUS', value: 'Status' }];
        this.automationlist = [];
        this.automationlistInit = [];
        this.isAdmin = false;
        this.searchAutomationForm = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["d" /* FormGroup */]({
            searchby: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].required),
            searchField: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["l" /* Validators */].required)
        });
        this.checkUserAdminOrNot();
        this.getAutomationList();
    }
    ShowAutomationComponent.prototype.ngOnInit = function () {
    };
    // check user admin or not 
    ShowAutomationComponent.prototype.checkUserAdminOrNot = function () {
        this.isAdmin = this.userStatus.isUserAdmin();
    };
    ShowAutomationComponent.prototype.getAutomationList = function () {
        var _this = this;
        this.showDialog();
        var automationGetALL = this.httpConfig.getAllAutomationDetails();
        this.showAutomationService.getAllAutomation(automationGetALL).subscribe(function (result) {
            _this.hideDialog();
            console.log(result);
            if (result.status) {
                console.log(result);
                _this.automationlist = [];
                _this.automationlist = result.data;
                _this.automationlistInit = [];
                _this.automationlistInit = _this.automationlist;
            }
        }, function (error) {
            console.log(error);
            _this.hideDialog();
        });
    };
    // showing the dialog
    ShowAutomationComponent.prototype.showDialog = function () {
        this.dialogRef = this.matDialog.open(__WEBPACK_IMPORTED_MODULE_4__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: 1 } });
    };
    // hiding the dialog
    ShowAutomationComponent.prototype.hideDialog = function () {
        this.dialogRef.close();
    };
    ShowAutomationComponent.prototype.getimgsrc = function (status) {
        if (status == "Completed") {
            return ('../../assets/completed.png');
        }
        if (status == "New")
            return ('../../assets/new.png');
        if (status == "Pending Approval")
            return ('../../assets/imageedit_13_8227496638.png');
        if (status == "Cancelled")
            return ('../../assets/cancel.png');
        if (status == "In Progress")
            return ('../../assets/inprogress.png');
    };
    ShowAutomationComponent.prototype.OnSearchByChange = function (event) {
        console.log(event);
        if (event !== -1) {
            this.searchSelection = event;
            console.log(this.searchSelection);
        }
        else {
            this.searchSelection = 'automationTitle';
        }
    };
    ShowAutomationComponent.prototype.OnTimeByChange = function (event) {
        var automationlistTime = [];
        if (event.value > 0) {
            var additiondays = 30;
            additiondays = additiondays * event.value;
            var dtDate_1 = new Date();
            var dtTd = new Date();
            var eventmonthTime_1 = new Date(dtTd.setDate(dtDate_1.getDate() + additiondays));
            this.automationlistInit.forEach(function (element) {
                var eventEndTime = new Date(element.automationActualCompletionDate);
                if (eventEndTime > dtDate_1 && eventEndTime < eventmonthTime_1) {
                    automationlistTime.push(element);
                }
            });
        }
        else {
            automationlistTime = this.automationlistInit;
        }
        if (automationlistTime.length > 0) {
            this.automationlist = [];
            this.automationlist = automationlistTime;
        }
        else {
            this.dialogRef = this.matDialog.open(__WEBPACK_IMPORTED_MODULE_4__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: 6, message: 'Your Search did not return any match' } });
        }
    };
    ShowAutomationComponent.prototype.OnTimeByCompletedChange = function (event) {
        var automationlistTime = [];
        if (event.value > 0) {
            var additiondays = -30;
            additiondays = additiondays * event.value;
            var dtDate_2 = new Date();
            var dtTd = new Date();
            var eventmonthTime_2 = new Date(dtTd.setDate(dtDate_2.getDate() + additiondays));
            this.automationlistInit.forEach(function (element) {
                var eventEndTime = new Date(element.automationActualCompletionDate);
                if (eventEndTime < dtDate_2 && eventEndTime > eventmonthTime_2) {
                    automationlistTime.push(element);
                }
            });
        }
        else {
            automationlistTime = this.automationlistInit;
        }
        if (automationlistTime.length > 0) {
            this.automationlist = [];
            this.automationlist = automationlistTime;
        }
        else {
            this.dialogRef = this.matDialog.open(__WEBPACK_IMPORTED_MODULE_4__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: 6, message: 'Your Search did not return any match' } });
        }
    };
    ShowAutomationComponent.prototype.onSearchSubmit = function () {
        var _this = this;
        this.showDialog();
        var strStringFetch = this.searchAutomationForm.get('searchField').value;
        strStringFetch = '%' + strStringFetch + '%';
        console.log(strStringFetch);
        var automationGetALL = this.httpConfig.getSearchAutomation();
        this.showAutomationService.getSearchAutomation(automationGetALL, { strSearch: strStringFetch,
            strfield: this.searchSelection }).subscribe(function (result) {
            _this.hideDialog();
            if (result.status) {
                console.log(result);
                if (result.data.length > 0) {
                    _this.automationlist = [];
                    _this.automationlist = result.data;
                    _this.automationlistInit = [];
                    _this.automationlistInit = _this.automationlist;
                }
                else {
                    _this.dialogRef = _this.matDialog.open(__WEBPACK_IMPORTED_MODULE_4__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: 6, message: 'Your Search did not return any match' } });
                }
            }
        }, function (error) {
            _this.hideDialog();
            console.log(error);
        });
    };
    ShowAutomationComponent.prototype.onAutomationClick = function (i) {
        var _this = this;
        // showing idea in dialog
        this.showDialog();
        var automationdetail = this.httpConfig.getAutomationDetail();
        console.log(this.automationlist[i].automationID);
        this.showAutomationService.getAutomationDetail(automationdetail, { id: this.automationlist[i].automationID }).subscribe(function (result) {
            _this.hideDialog();
            if (result.status) {
                console.log(result.data[0]);
                _this.showAutomationDialogRef = _this.matDialog.open(__WEBPACK_IMPORTED_MODULE_7__automation_details_automation_details_component__["a" /* AutomationDetailsComponent */], {
                    data: result.data[0]
                });
            }
        }, function (error) {
            _this.hideDialog();
            console.log(error);
        });
        // get id of selected idea
        // getting likes count
        console.log(i);
    };
    ShowAutomationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-show-automation',
            template: __webpack_require__("./src/app/show-automation/show-automation.component.html"),
            styles: [__webpack_require__("./src/app/show-automation/show-automation.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__utils_checkUserStatus__["a" /* CheckUserStatus */],
            __WEBPACK_IMPORTED_MODULE_1__showAutomationService__["a" /* ShowAutomatonService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_material__["f" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_5__utils_httpConfig__["a" /* HttpCOnfig */]])
    ], ShowAutomationComponent);
    return ShowAutomationComponent;
}());



/***/ }),

/***/ "./src/app/show-automation/showAutomationService.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShowAutomatonService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
Fetching Automation Details through Http Calls
*/
var ShowAutomatonService = (function () {
    function ShowAutomatonService(httpClient) {
        this.httpClient = httpClient;
    }
    // Fetch Automation Dashboard Details
    ShowAutomatonService.prototype.getAllAutomation = function (URL) {
        return this.httpClient.get(URL, {});
    };
    ShowAutomatonService.prototype.getSearchAutomation = function (URL, data) {
        return this.httpClient.post(URL, data);
    };
    ShowAutomatonService.prototype.getAutomationDetail = function (URL, data) {
        console.log(data);
        return this.httpClient.post(URL, data);
    };
    ShowAutomatonService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], ShowAutomatonService);
    return ShowAutomatonService;
}());



/***/ }),

/***/ "./src/app/show-idea/buildDataSource.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BuildDataSource; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BuildDataSource = (function () {
    function BuildDataSource() {
        this.datService$ = new __WEBPACK_IMPORTED_MODULE_1_rxjs__["BehaviorSubject"]([]);
        this.data = [];
    }
    Object.defineProperty(BuildDataSource.prototype, "data$", {
        // getting the data
        get: function () {
            return this.datService$.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    // add team 
    BuildDataSource.prototype.addTeamData = function (data) {
        this.data = this.data.concat(data);
        this.datService$.next(this.data);
    };
    // getting team data as Array 
    BuildDataSource.prototype.getTeamDataAsArray = function () {
        return this.data;
    };
    // get team data 
    BuildDataSource.prototype.getTeamData = function () {
        return this.datService$;
    };
    // reset the data 
    BuildDataSource.prototype.resetData = function () {
        this.data = [];
        this.datService$.next(this.data);
    };
    BuildDataSource = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], BuildDataSource);
    return BuildDataSource;
}());



/***/ }),

/***/ "./src/app/show-idea/dataService.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MatTableDataService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("./node_modules/rxjs/_esm5/BehaviorSubject.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MatTableDataService = (function () {
    function MatTableDataService() {
        this.datService$ = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["a" /* BehaviorSubject */]([]);
        this.data = [];
    }
    Object.defineProperty(MatTableDataService.prototype, "data$", {
        // getting the data
        get: function () {
            return this.datService$.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    // add team 
    MatTableDataService.prototype.addTeamData = function (data) {
        this.data = this.data.concat(data);
        this.datService$.next(this.data);
    };
    // delete specific index data 
    MatTableDataService.prototype.removeUSerFromTeam = function (index) {
        this.data.splice(index, 1);
        this.datService$.next(this.data);
    };
    // getting team data as Array 
    MatTableDataService.prototype.getTeamDataAsArray = function () {
        return this.data;
    };
    // get team data 
    MatTableDataService.prototype.getTeamData = function () {
        return this.datService$;
    };
    // reset the data 
    MatTableDataService.prototype.resetData = function () {
        this.data = [];
        this.datService$.next(this.data);
    };
    MatTableDataService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], MatTableDataService);
    return MatTableDataService;
}());



/***/ }),

/***/ "./src/app/show-idea/productionDataSource.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductionDataSource; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProductionDataSource = (function () {
    function ProductionDataSource() {
        this.datService$ = new __WEBPACK_IMPORTED_MODULE_1_rxjs__["BehaviorSubject"]([]);
        this.data = [];
    }
    Object.defineProperty(ProductionDataSource.prototype, "data$", {
        // getting the data
        get: function () {
            return this.datService$.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    // add team 
    ProductionDataSource.prototype.addTeamData = function (data) {
        this.data = this.data.concat(data);
        this.datService$.next(this.data);
    };
    // getting team data as Array 
    ProductionDataSource.prototype.getTeamDataAsArray = function () {
        return this.data;
    };
    // get team data 
    ProductionDataSource.prototype.getTeamData = function () {
        return this.datService$;
    };
    // reset the data 
    ProductionDataSource.prototype.resetData = function () {
        this.data = [];
        this.datService$.next(this.data);
    };
    ProductionDataSource = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], ProductionDataSource);
    return ProductionDataSource;
}());



/***/ }),

/***/ "./src/app/show-idea/show-idea.component.css":
/***/ (function(module, exports) {

module.exports = "p {\r\n  word-wrap: break-word;\r\n}\r\n\r\n.title {\r\n  font-size: 20px;\r\n}\r\n\r\n.choices {\r\n  display: -webkit-box;\r\n  display: -ms-flexbox;\r\n  display: flex;\r\n}\r\n\r\n.likeButton {\r\n  color: blue;\r\n}\r\n\r\n.progress {\r\n  height: 100%;\r\n  margin: 0px 0px 0px 10px;\r\n}\r\n\r\n.progressDetails {\r\n  margin: 10px 0px 0px 0px;\r\n  max-height: 400px;\r\n}\r\n\r\n.stepper {\r\n  background: #1d86a4;\r\n}\r\n\r\n.labelText {\r\n  color: white;\r\n}\r\n\r\n.actionButtons {\r\n  margin: 10px 0px 0px 0px;\r\n  display: -webkit-box;\r\n  display: -ms-flexbox;\r\n  display: flex;\r\n  -webkit-box-orient: horizontal;\r\n  -webkit-box-direction: reverse;\r\n      -ms-flex-direction: row-reverse;\r\n          flex-direction: row-reverse;\r\n}\r\n\r\n.approvalInfo {\r\n  display: -webkit-box;\r\n  display: -ms-flexbox;\r\n  display: flex;\r\n  -webkit-box-orient: vertical;\r\n  -webkit-box-direction: normal;\r\n      -ms-flex-direction: column;\r\n          flex-direction: column;\r\n}\r\n\r\n.approvalHistory {\r\n  margin: 0px 0px 10px 0px;\r\n}\r\n\r\n.approvalLabel p {\r\n  margin: 0 !important;\r\n  /* padding: 0; */\r\n  color: red;\r\n}\r\n\r\n.addTeamStyle {\r\n  display: -webkit-box;\r\n  display: -ms-flexbox;\r\n  display: flex;\r\n}\r\n\r\n.emailField {\r\n  width: 90%;\r\n}\r\n\r\n.AddTeamButton {\r\n  width: 10%;\r\n}\r\n\r\n.emailFIeldValue {\r\n  width: 100%;\r\n}\r\n\r\n.example-container {\r\n  margin-top: 10px;\r\n  display: -webkit-box;\r\n  display: -ms-flexbox;\r\n  display: flex;\r\n  -webkit-box-orient: vertical;\r\n  -webkit-box-direction: normal;\r\n      -ms-flex-direction: column;\r\n          flex-direction: column;\r\n  max-height: 500px;\r\n  min-width: 300px;\r\n}\r\n\r\n.mat-table {\r\n  overflow: auto;\r\n  max-height: 500px;\r\n}\r\n\r\n.deleteIcon {\r\n  cursor: pointer;\r\n}\r\n\r\n.datesLayout {\r\n  display: -webkit-box;\r\n  display: -ms-flexbox;\r\n  display: flex;\r\n  -webkit-box-pack: justify;\r\n      -ms-flex-pack: justify;\r\n          justify-content: space-between;\r\n  -webkit-box-align: center;\r\n      -ms-flex-align: center;\r\n          align-items: center;\r\n}\r\n\r\n.buildInput {\r\n  display: -webkit-box;\r\n  display: -ms-flexbox;\r\n  display: flex;\r\n  -webkit-box-align: center;\r\n      -ms-flex-align: center;\r\n          align-items: center;\r\n  -webkit-box-pack: justify;\r\n      -ms-flex-pack: justify;\r\n          justify-content: space-between;\r\n}\r\n\r\n.example-form {\r\n  -webkit-box-flex: 1;\r\n      -ms-flex-positive: 1;\r\n          flex-grow: 1;\r\n}\r\n\r\n.buildInputField {\r\n  width: 100%;\r\n}\r\n\r\n.buildSubmit{\r\n  margin-top: 50px;\r\n}\r\n\r\n.businessSubmit{\r\n  margin-top: 50px;\r\n  margin-right: 100px;\r\n  float: right;\r\n}"

/***/ }),

/***/ "./src/app/show-idea/show-idea.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-md-6\">\r\n    <strong>Title: </strong>{{ideaData.title}}\r\n  </div>\r\n  <div class=\"col-md-4\">\r\n    <strong>Status: </strong>{{ideaStatus}}\r\n  </div>  \r\n</div>  \r\n<div class=\"row\">&nbsp;</div>    \r\n<p> <strong>Description: </strong>{{ideaData.description}}</p>\r\n<p> <strong>Technologies: </strong>{{ideaData.techs}}</p>\r\n<p> <strong>Email: </strong>{{ideaData.email}}</p>\r\n<p> <strong>Employee ID: </strong>{{ideaData.employeeID}}</p>\r\n<p> <strong>Date: </strong>{{ideaData.created_at | date: 'dd MMMM yyyy'}}</p>\r\n<div class=\"choices\">\r\n  <button class=\"likeButton\" [disabled]=\"isLikeButtonBusy\" mat-raised-button (click)=\"onLike()\">{{likeButtonString}}</button>\r\n  <button *ngIf=\"isAdmin\" class=\"progress\" mat-raised-button color=\"warn\" (click)=\"onProgress()\">{{progressText}}</button>\r\n</div>\r\n\r\n<!-- this is for progress -->\r\n<div *ngIf=\"isProgressVisible\" class=\"progressDetails\">\r\n  <mat-horizontal-stepper class=\"stepper\">\r\n    <mat-step>\r\n      <form [formGroup]=\"approValForm\">\r\n        <ng-template matStepLabel>\r\n          <div class=\"approvalLabel text-center\">\r\n            <span matTooltip=\"Approval Status!\">\r\n              <p [ngStyle]=\"getApprovalStyle()\">\r\n                <strong>Approval</strong>\r\n              </p>\r\n            </span>\r\n          </div>\r\n        </ng-template>\r\n        <mat-card class=\"approvalInfo\">\r\n          <strong *ngIf=\"isApproved\">Idea is approved</strong>\r\n          <strong *ngIf=\"!isApproved\">Idea is not pproved</strong>\r\n          <br>\r\n          <!-- this is for approval history -->\r\n          <div *ngIf=\"isApprovalHistory\" class=\"approvalHistory\">\r\n            <p *ngIf=\"approvedHistoryData.length == 0\">No History found</p>\r\n            <ol>\r\n              <li *ngFor=\"let data of approvedHistoryData\">\r\n                <div>\r\n                    Status was <b> {{data.status}} </b> by <b><i><u> {{data.username}} </u></i></b> on {{data.created_at | date : 'dd'}}/{{data.created_at | date:'MM'}}/{{data.created_at | date: 'yyyy'}} with comments : <i><u> {{data.comments}} </u></i>\r\n                </div>\r\n              </li>\r\n            </ol>\r\n          </div>\r\n          <mat-form-field class=\"example-full-width\" >\r\n            <input matInput  placeholder=\"Comments\" required autocomplete=\"off\" formControlName=\"comments\">\r\n            <mat-error>\r\n              Comment must be minimum 10 char\r\n            </mat-error>\r\n          </mat-form-field>\r\n          <div class=\"actionButtons\">\r\n            <button *ngIf=\"!isApproved\" [disabled]=\"!approValForm.valid || isApprovalButtonBusy\" (click)=\"onApprove(1)\" mat-raised-button\r\n              color=\"primary\">Approve</button>\r\n            <button *ngIf=\"isApproved\" [disabled]=\"!approValForm.valid || isApprovalButtonBusy\" mat-raised-button color=\"warn\" (click)=\"onApprove(0)\">Disapprove</button>\r\n            <button mat-button *ngIf=\"!isApprovalHistory\" (click)=\"showApprovedHistory()\">Show History</button>\r\n            <button mat-button *ngIf=\"isApprovalHistory\" (click)=\"showApprovedHistory()\">Hide History</button>\r\n          </div>\r\n        </mat-card>\r\n      </form>\r\n    </mat-step>\r\n\r\n\r\n    <!-- this is for feasibility -->\r\n    <mat-step >\r\n      <form [formGroup]=\"feasibilityForm\">\r\n        <ng-template matStepLabel>\r\n          <div class=\"approvalLabel text-center\">\r\n            <span matTooltip=\"Feasibility Status!\">\r\n              <p [ngStyle]=\"getFeasibilityStyle()\">\r\n                <strong>Feasibility</strong>\r\n              </p>\r\n            </span>\r\n          </div>\r\n        </ng-template>\r\n        <mat-card class=\"approvalInfo\">\r\n          <strong>{{isFeasibile ? 'This Idea is feasible' : 'Feasiblity to be Checked'}}</strong>\r\n          <br>\r\n          <div *ngIf=\"feasHistoryButtonStatus\">\r\n            <p *ngIf=\"!isFeasData\">No History found</p>\r\n            <ol>\r\n              <li *ngFor=\"let data of feasData\">\r\n                status is {{data.status}} with comments {{data.comments}}\r\n              </li>\r\n            </ol>\r\n          </div>\r\n\r\n           <!-- this is for selecting quarter Year -->\r\n           <div class=\" row\">\r\n              <mat-form-field class=\"example-full-width col-md-5\">\r\n                  <mat-select placeholder=\"Quarter\"  required formControlName=\"quartsValue\">\r\n                    <mat-option *ngFor=\"let quart of quartsDropdown\" [value]=\"quart.item\">\r\n                      {{quart.value}}\r\n                    </mat-option>\r\n                  </mat-select>\r\n                </mat-form-field>\r\n             <mat-form-field class=\"example-full-width col-md-4\">\r\n            <input matInput placeholder=\"Proposed Man Hours\" required autocomplete=\"off\" formControlName=\"quarthours\">\r\n            <mat-error>\r\n              Enter hours required to build \r\n              </mat-error>\r\n            </mat-form-field>\r\n          </div>  \r\n          <mat-form-field class=\"example-full-width\">\r\n            <input matInput placeholder=\"Comments\" required autocomplete=\"off\" formControlName=\"comments\" >\r\n            <mat-error>\r\n              Comment must be min 10 char\r\n            </mat-error>\r\n          </mat-form-field>\r\n          <div class=\"actionButtons\">\r\n            <button *ngIf=\"!isFeasibile\" [disabled]=\"!feasibilityForm.valid || isApprovalButtonBusy\" (click)=\"onFeasible(1)\" mat-raised-button\r\n              color=\"primary\">Make Feasible</button>\r\n            <button *ngIf=\"isFeasibile\" [disabled]=\"!feasibilityForm.valid || isApprovalButtonBusy\" mat-raised-button color=\"warn\" (click)=\"onFeasible(0)\">Disapprove</button>\r\n            <button mat-button (click)=\"showFeassibilityHistory()\">{{feasHistoryButtonStatus ? 'Hide History' : 'Show History'}}</button>\r\n          </div>\r\n        </mat-card>\r\n      </form>\r\n    </mat-step>\r\n\r\n\r\n    <!-- this is for team phase -->\r\n    <mat-step >\r\n      <form [formGroup]=\"teamForm\">\r\n        <ng-template matStepLabel>\r\n          <div class=\"approvalLabel text-center\">\r\n            <span matTooltip=\"Team Status!\">\r\n              <p (click)=\"getTeamData()\" [ngStyle]=\"getTeamStyle()\">\r\n                <strong>Team</strong>\r\n              </p>\r\n            </span>\r\n          </div>\r\n        </ng-template>\r\n        <!-- team details -->\r\n        <mat-card>\r\n          <div>\r\n            <!-- <p *ngIf=\"!isTeamAvailable\">\r\n              <strong>No Team Details Found</strong>\r\n            </p> -->\r\n          </div>\r\n          <!-- adding team members -->\r\n          <div class=\"addTeamStyle\">\r\n            <div class=\"emailField\">\r\n              <mat-form-field class=\"emailFIeldValue\">\r\n                <input matInput placeholder=\"Add Team superated by comma(,)\" autocomplete=\"off\" required formControlName=\"team\">\r\n                <mat-error>\r\n                  Enter atleast 5 char\r\n                </mat-error>\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"AddTeamButton\">\r\n              <button mat-raised-button (click)=\"AddTeam()\" [disabled]=\"!teamForm.valid\" color=\"warn\">Add</button>\r\n            </div>\r\n          </div>\r\n          <!-- table data -->\r\n          <div class=\"example-container mat-elevation-z1\">\r\n            <mat-table #table [dataSource]=\"demoDataSource\">\r\n              <!-- Position Column\r\n              <ng-container matColumnDef=\"position\">\r\n                <mat-header-cell *matHeaderCellDef> No. </mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\"> {{element.position}} </mat-cell>\r\n              </ng-container> -->\r\n\r\n              <!-- Name Column -->\r\n              <ng-container matColumnDef=\"name\">\r\n                <mat-header-cell *matHeaderCellDef> Name || Email </mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\"> {{element.name}} </mat-cell>\r\n              </ng-container>\r\n\r\n              <!-- Action Column -->\r\n              <ng-container matColumnDef=\"action\">\r\n                <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element; let i = index\">\r\n                  <mat-icon class=\"deleteIcon\" (click)=\"deleteUser(i)\">delete</mat-icon>\r\n                </mat-cell>\r\n              </ng-container>\r\n\r\n              <!-- headers -->\r\n              <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n              <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n\r\n            </mat-table>\r\n          </div>\r\n\r\n        </mat-card>\r\n      </form>\r\n    </mat-step>\r\n\r\n\r\n    <!-- this is for build phases -->\r\n    <mat-step>\r\n      <ng-template matStepLabel>\r\n        <div class=\"approvalLabel text-center\">\r\n          <span matTooltip=\"Build Status!\">\r\n            <p (click)=\"getBuildData()\" [ngStyle]=\"getBuildStyle()\">\r\n              <strong>Build</strong>\r\n            </p>\r\n          </span>\r\n        </div>\r\n      </ng-template>\r\n      <!-- date values starts here -->\r\n      <mat-card>\r\n        <div class=\"buildInput\">\r\n          <form class=\"example-form\" [formGroup]=\"buildForm\" (ngSubmit)=\"buildFormSubmit(buildForm)\">\r\n            <div class=\"datesLayout\">\r\n              <mat-form-field class=\"example-full-width\">\r\n                <input required formControlName=\"builStartDate\" matInput [min]=\"minDate\" [matDatepicker]=\"startPicker\" placeholder=\"Choose a start date\">\r\n                <mat-error *ngIf=\"buildForm.hasError('required','builStartDate')\">\r\n                  Select start date\r\n                </mat-error>\r\n                <mat-error>\r\n                  Check dates start date should be before end date\r\n                </mat-error>\r\n                <mat-datepicker-toggle matSuffix [for]=\"startPicker\"></mat-datepicker-toggle>\r\n                <mat-datepicker #startPicker></mat-datepicker>\r\n              </mat-form-field>\r\n              <mat-form-field class=\"example-full-width\">\r\n                <input required formControlName=\"builEndDate\" matInput [min]=\"minDate\" [matDatepicker]=\"endPicker\" placeholder=\"Choose a end date\">\r\n                <mat-error *ngIf=\"buildForm.hasError('required','builEndDate')\">\r\n                  Select end date\r\n                </mat-error>\r\n                <mat-error>\r\n                  Check dates start date should be before end date\r\n                </mat-error>\r\n                <mat-datepicker-toggle matSuffix [for]=\"endPicker\"></mat-datepicker-toggle>\r\n                <mat-datepicker #endPicker></mat-datepicker>\r\n              </mat-form-field>\r\n            </div>\r\n            <mat-form-field class=\"buildInputField\">\r\n              <input matInput placeholder=\"Build phase title\" required autocomplete=\"off\" formControlName=\"buildTitle\">\r\n              <mat-error *ngIf=\"buildForm.hasError('minlength','buildTitle')\">\r\n                Title should be atleast 6 char\r\n              </mat-error>\r\n            </mat-form-field>\r\n            <p *ngIf=\"dateError\">{{dateErrorMessage}}</p>\r\n            <button class=\"buildSubmit\" [disabled]=\"!buildForm.valid\" class=\"text-center\" mat-raised-button color=\"warn\">Submit</button>\r\n          </form>\r\n        </div>\r\n      </mat-card>\r\n      <!-- build phase data in table format -->\r\n      <!-- table data -->\r\n      <div class=\"example-container mat-elevation-z1\">\r\n        <mat-table #table [dataSource]=\"buildDataSourceTable\">\r\n\r\n          <!-- Name Column -->\r\n          <ng-container matColumnDef=\"title\">\r\n            <mat-header-cell *matHeaderCellDef> Title </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let teamData\"> {{teamData.title}} </mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"date\">\r\n            <mat-header-cell *matHeaderCellDef> Date </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let teamData\"> {{teamData.created_at | date: 'dd MMMM yyyy'}} </mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"start date\">\r\n            <mat-header-cell *matHeaderCellDef> Start date </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let teamData\"> {{teamData.start_date | date: 'dd MMMM yyyy'}} </mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"end date\">\r\n            <mat-header-cell *matHeaderCellDef> End date </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let teamData\"> {{teamData.end_date | date: 'dd MMMM yyyy' }} </mat-cell>\r\n          </ng-container>\r\n\r\n          <!-- Action Column -->\r\n          <ng-container matColumnDef=\"action\">\r\n            <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let teamData; let i = index\">\r\n              <mat-icon class=\"deleteIcon\" (click)=\"deleteUser(i)\">delete</mat-icon>\r\n            </mat-cell>\r\n          </ng-container>\r\n\r\n          <!-- headers -->\r\n          <mat-header-row *matHeaderRowDef=\"teamDisplayColumns\"></mat-header-row>\r\n          <mat-row *matRowDef=\"let row; columns: teamDisplayColumns;\"></mat-row>\r\n        </mat-table>\r\n      </div>\r\n    </mat-step>\r\n\r\n    <mat-step>\r\n      <ng-template matStepLabel>\r\n        <div class=\"approvalLabel text-center\">\r\n          <span matTooltip=\"Production Status\">\r\n            <p (click)=\"getProductionData()\" [ngStyle]=\"getProductionStyle()\">\r\n              <strong>Production</strong>\r\n            </p>\r\n          </span>\r\n        </div>\r\n      </ng-template>\r\n      <!-- date values starts here -->\r\n      <mat-card>\r\n        <div class=\"buildInput\">\r\n          <form class=\"example-form\" [formGroup]=\"productionForm\" (ngSubmit)=\"productionFormSubmit(productionForm)\">\r\n            <div class=\"datesLayout\">\r\n              <mat-form-field class=\"example-full-width\">\r\n                <input required formControlName=\"builStartDate\" matInput [min]=\"minDate\" [matDatepicker]=\"startProdPicker\" placeholder=\"Choose a start date\">\r\n                <mat-error *ngIf=\"productionForm.hasError('required','builStartDate')\">\r\n                  Select start date\r\n                </mat-error>\r\n                <mat-error>\r\n                  Check dates start date should be before end date\r\n                </mat-error>\r\n                <mat-datepicker-toggle matSuffix [for]=\"startProdPicker\"></mat-datepicker-toggle>\r\n                <mat-datepicker #startProdPicker></mat-datepicker>\r\n              </mat-form-field>\r\n              <mat-form-field class=\"example-full-width\">\r\n                <input required formControlName=\"builEndDate\" matInput [min]=\"minDate\" [matDatepicker]=\"endProdPicker\" placeholder=\"Choose a end date\">\r\n                <mat-error *ngIf=\"productionForm.hasError('required','builEndDate')\">\r\n                  Select end date\r\n                </mat-error>\r\n                <mat-error>\r\n                  Check dates start date should be before end date\r\n                </mat-error>\r\n                <mat-datepicker-toggle matSuffix [for]=\"endProdPicker\"></mat-datepicker-toggle>\r\n                <mat-datepicker #endProdPicker></mat-datepicker>\r\n              </mat-form-field>\r\n            </div>\r\n            <mat-form-field class=\"buildInputField\">\r\n              <input matInput placeholder=\"Production phase title\" required autocomplete=\"off\" formControlName=\"buildTitle\">\r\n              <mat-error *ngIf=\"productionForm.hasError('minlength','buildTitle')\">\r\n                Title should be atleast 6 char\r\n              </mat-error>\r\n            </mat-form-field>\r\n            <p *ngIf=\"dateError\">{{dateErrorMessage}}</p>\r\n            <button class=\"buildSubmit\" [disabled]=\"!productionForm.valid\" class=\"text-center\" mat-raised-button color=\"warn\">Submit</button>\r\n          </form>\r\n        </div>\r\n      </mat-card>\r\n      <!-- build phase data in table format -->\r\n      <!-- table data -->\r\n      <div class=\"example-container mat-elevation-z1\">\r\n        <mat-table #table [dataSource]=\"productionDataSourceTable\">\r\n\r\n          <!-- Name Column -->\r\n          <ng-container matColumnDef=\"title\">\r\n            <mat-header-cell *matHeaderCellDef> Title </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let teamData\"> {{teamData.title}} </mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"date\">\r\n            <mat-header-cell *matHeaderCellDef> Date </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let teamData\"> {{teamData.created_at | date: 'dd MMMM yyyy'}} </mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"start date\">\r\n            <mat-header-cell *matHeaderCellDef> Start date </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let teamData\"> {{teamData.start_date | date: 'dd MMMM yyyy'}} </mat-cell>\r\n          </ng-container>\r\n\r\n          <ng-container matColumnDef=\"end date\">\r\n            <mat-header-cell *matHeaderCellDef> End date </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let teamData\"> {{teamData.end_date | date: 'dd MMMM yyyy' }} </mat-cell>\r\n          </ng-container>\r\n\r\n          <!-- Action Column -->\r\n          <ng-container matColumnDef=\"action\">\r\n            <mat-header-cell *matHeaderCellDef> Action </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let teamData; let i = index\">\r\n              <mat-icon class=\"deleteIcon\" (click)=\"deleteUser(i)\">delete</mat-icon>\r\n            </mat-cell>\r\n          </ng-container>\r\n\r\n          <!-- headers -->\r\n          <mat-header-row *matHeaderRowDef=\"productionDisplayColumns\"></mat-header-row>\r\n          <mat-row *matRowDef=\"let row; columns: productionDisplayColumns;\"></mat-row>\r\n        </mat-table>\r\n      </div>\r\n    </mat-step>\r\n    <mat-step>\r\n      <ng-template matStepLabel>\r\n        <div class=\"approvalLabel text-center\">\r\n          <span matTooltip=\"Business Benefits!\">\r\n            <p (click)=\"getBusinessBenefitsData() \" [ngStyle]=\"getBenefitsStyle()\">\r\n              <strong>Business Benefits</strong>\r\n            </p>\r\n          </span>\r\n        </div>\r\n      </ng-template>\r\n      <mat-card>\r\n        <div>\r\n          <form class=\"example-form\" [formGroup]=\"businessBenefitsform\" (ngSubmit)=\"benefitsFormSubmit(businessBenefitsform)\">\r\n            <div class=\"row\">\r\n              <mat-form-field class=\"example-full-width col-md-8\">\r\n                  <textarea matInput #message maxlength=\"256\" required formControlName=\"businessBenefits\" placeholder=\"Business Benefits\"></textarea>\r\n                  <mat-hint align=\"end\">{{message.value.length}} / 256</mat-hint>\r\n                  <mat-error *ngIf=\"buildForm.hasError('minlength','businessBenefits')\">\r\n                      Provide descriptive business benefits minimum 15 characters\r\n                  </mat-error>               \r\n              </mat-form-field>\r\n              </div>  \r\n              <div class=\"row\">\r\n              <mat-form-field class=\"example-full-width col-md-8\">\r\n                  <textarea matInput #appreciation maxlength=\"256\" required formControlName=\"businessAppreciation\" placeholder=\"Business Appreciation\"></textarea>\r\n                  <mat-hint align=\"end\">{{appreciation.value.length}} / 256</mat-hint> \r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"row\">\r\n              <br/>\r\n            </div>\r\n            <div class=\"row\">\r\n              <mat-form-field class=\"example-full-width col-md-5\">\r\n                <mat-select placeholder=\"Benefits Classification\" required formControlName=\"businessbenefitsClassification\">\r\n                  <mat-option *ngFor=\"let benefit of benefits\" [value]=\"benefit.value\">\r\n                    {{benefit.value}}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>\r\n              <mat-form-field class=\"example-full-width col-md-5\">\r\n                <input matInput placeholder=\"Business Hours\" formControlName=\"benefitsHours\">\r\n              </mat-form-field>\r\n              </div>  \r\n              <div class=\"row\">\r\n              <button  [disabled]=\"!businessBenefitsform.valid\" class=\" businessSubmit text-center\" mat-raised-button color=\"warn\">Submit</button>\r\n            </div>\r\n          </form>\r\n        </div>\r\n      </mat-card>\r\n    </mat-step>\r\n  </mat-horizontal-stepper>\r\n</div>"

/***/ }),

/***/ "./src/app/show-idea/show-idea.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShowIdeaComponent; });
/* unused harmony export DemoDataSource */
/* unused harmony export BuildDataSourceTable */
/* unused harmony export ProductionDataSourceTable */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__dataService__ = __webpack_require__("./src/app/show-idea/dataService.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__showIdeaService__ = __webpack_require__("./src/app/show-idea/showIdeaService.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__progress_dialog_progress_dialog_component__ = __webpack_require__("./src/app/progress-dialog/progress-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__utils_checkUserStatus__ = __webpack_require__("./src/app/utils/checkUserStatus.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_cdk_collections__ = __webpack_require__("./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__buildDataSource__ = __webpack_require__("./src/app/show-idea/buildDataSource.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__productionDataSource__ = __webpack_require__("./src/app/show-idea/productionDataSource.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__add_automation_addautomationservice__ = __webpack_require__("./src/app/add-automation/addautomationservice.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};












var ShowIdeaComponent = (function () {
    function ShowIdeaComponent(userStatus, showIdeaService, dialogRef, formBuilder, matDataService, buildDataSource, productionDataSource, addAutomationService, loginDialog, httpConfig, data) {
        this.userStatus = userStatus;
        this.showIdeaService = showIdeaService;
        this.dialogRef = dialogRef;
        this.formBuilder = formBuilder;
        this.matDataService = matDataService;
        this.buildDataSource = buildDataSource;
        this.productionDataSource = productionDataSource;
        this.addAutomationService = addAutomationService;
        this.loginDialog = loginDialog;
        this.httpConfig = httpConfig;
        this.data = data;
        // this is like button text
        this.likeButtonString = 'Like';
        this.likeButtonStatus = false;
        this.isAdmin = false;
        this.isLikeButtonBusy = false;
        this.isProgressVisible = false;
        this.progressText = 'Show Progress';
        this.benefits = [];
        this.isApproved = false;
        this.isApprovalHistory = false;
        this.isApprovalButtonBusy = false;
        this.feasHistoryButtonText = "show history";
        // phases history details
        this.approvedHistoryData = [];
        this.phasesData = [];
        //Quarter dropdown
        this.quartsDropdown = [];
        // views for phases
        // form group for approval phase
        this.approValForm = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormGroup */]({
            comments: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].minLength(10)]))
        });
        /* everything about feasibility */
        this.isFeasibile = false;
        this.isFeasHistory = false;
        this.isFeasData = false;
        this.feasData = [];
        this.feasHistoryButtonStatus = false;
        this.feasibilityForm = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormGroup */]({
            quartsValue: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required])),
            comments: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].minLength(10)])),
            quarthours: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].minLength(2), , __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].maxLength(3)])),
        });
        /* everything about team status */
        this.isTeamAvailable = false;
        this.teamForm = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormGroup */]({
            team: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].minLength(5)]))
        });
        this.displayedColumns = ["name", "action"];
        /* everything about build status */
        this.minDate = new Date();
        this.isBuild = false;
        this.teamDisplayColumns = ["title", "date", "start date", "end date"];
        this.buildForm = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormGroup */]({
            buildTitle: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].minLength(6)])),
            builStartDate: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required])),
            builEndDate: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required]))
        });
        /* everyhting about production status */
        this.productionDisplayColumns = ["title", "date", "start date", "end date"];
        this.isProduction = false;
        this.productionForm = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormGroup */]({
            buildTitle: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].minLength(6)])),
            builStartDate: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required])),
            builEndDate: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required]))
        });
        /* everything about production Benefits */
        this.busBenefits = [];
        this.isBenefits = false;
        this.isEffortReduction = false;
        this.businessBenefits = '';
        this.businessbenefitsClassification = '';
        this.businessAppreciation = '';
        this.benefitsHours = '';
        this.businessBenefitsform = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormGroup */]({
            businessBenefits: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].minLength(10)])),
            businessbenefitsClassification: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required])),
            businessAppreciation: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */](),
            benefitsHours: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]()
        });
        this.ideaData = data;
        console.log(data);
        this.ideaStatus = this.ideaStatusValue(data);
        this.likeCount = this.ideaData.likes;
        this.likeButtonString = "Like\t" + this.likeCount;
        // check user admin or not
        this.checkUserAdminOrNot();
    }
    ShowIdeaComponent.prototype.ideaStatusValue = function (data) {
        if (data.approval_status == 0) {
            return "Approval Pending";
        }
        else if (data.feasibility_status == 0) {
            return "Feasibility Pending";
        }
        else if (data.team_status == 0) {
            return "Team Creation Phase";
        }
        else if (data.build_status == 0) {
            return "Build Phase";
        }
        else if (data.production_status == 0) {
            return "Production Phase";
        }
        else if (data.business_impact_status == 0) {
            return "Deployed & Business Impact Analysis";
        }
        else {
            return "Idea Implemented";
        }
    };
    ShowIdeaComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var automationEntityData, quarts;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // intializing all the form by step
                        this.demoDataSource = new DemoDataSource(this.matDataService);
                        this.buildDataSourceTable = new BuildDataSourceTable(this.buildDataSource);
                        this.productionDataSourceTable = new ProductionDataSourceTable(this.productionDataSource);
                        automationEntityData = '';
                        return [4 /*yield*/, this.FetchAutomationEntity()];
                    case 1:
                        automationEntityData = _a.sent();
                        this.PopulateDropdowns(automationEntityData);
                        quarts = '';
                        return [4 /*yield*/, this.FetchQuarterEntity()];
                    case 2:
                        quarts = _a.sent();
                        this.PopulateQuarts(quarts);
                        return [2 /*return*/];
                }
            });
        });
    };
    ShowIdeaComponent.prototype.FetchAutomationEntity = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var automationDetailsURL = _this.httpConfig.getautomationEntity();
            _this.addAutomationService.automationEntity(automationDetailsURL).subscribe(function (result) {
                if (result.status) {
                    (result.status ? resolve(result.data) : reject(result));
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    ShowIdeaComponent.prototype.PopulateDropdowns = function (dropdownResults) {
        var _this = this;
        console.log(dropdownResults);
        var serverData = [];
        dropdownResults.forEach(function (element) {
            serverData.push({
                item: element.ID,
                value: element.Value,
                Type: element.Type
            });
        });
        serverData.forEach(function (element) {
            if (element.Type === 'BusinessBenefits') {
                _this.benefits.push(element);
            }
        });
    };
    // for quarter dropdown
    ShowIdeaComponent.prototype.FetchQuarterEntity = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var automationDetailsURL = _this.httpConfig.getQuarterData();
            _this.showIdeaService.quarts(automationDetailsURL).subscribe(function (result) {
                if (result.status) {
                    (result.status ? resolve(result.data) : reject(result));
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    ShowIdeaComponent.prototype.PopulateQuarts = function (dropdownResults) {
        console.log(dropdownResults);
        var serverData = [];
        dropdownResults.forEach(function (element) {
            serverData.push({
                item: element.automationEntityID,
                value: element.automationEntityValue
            });
        });
        this.quartsDropdown = serverData;
    };
    // when user clicks on like button 
    ShowIdeaComponent.prototype.onLike = function () {
        var _this = this;
        if (this.checkUserLoggedInOrNot()) {
            // first disable the button before making server call 
            this.isLikeButtonBusy = true;
            var data = { count: this.getLikeCount(), id: this.ideaData.id };
            // now update like button count on server 
            this.showIdeaService.updateLikeCount(data).subscribe(function (result) {
                _this.isLikeButtonBusy = false;
                if (result.status) {
                    // update the button status 
                    _this.changeLikeStatus();
                }
            }, function (error) {
                _this.isLikeButtonBusy = false;
            });
        }
        else {
            // open please login dialog 
            this.loginDialogRef = this.loginDialog.open(__WEBPACK_IMPORTED_MODULE_3__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: 4, message: 'Please Login to Like!' } });
        }
    };
    // check user logged in or not 
    ShowIdeaComponent.prototype.checkUserLoggedInOrNot = function () {
        return this.userStatus.isUserLoggedIn();
    };
    // changing like buttin status and text 
    ShowIdeaComponent.prototype.changeLikeStatus = function () {
        if (this.likeButtonStatus) {
            this.likeButtonStatus = false;
            if (this.likeCount >= 0)
                this.likeCount = this.likeCount - 1;
            this.likeButtonString = "Like\t" + this.likeCount;
        }
        else {
            this.likeButtonStatus = true;
            this.likeCount = this.likeCount + 1;
            this.likeButtonString = "Liked\t" + this.likeCount;
        }
    };
    // get like button count based on that liked or un liked 
    ShowIdeaComponent.prototype.getLikeCount = function () {
        if (!this.likeButtonStatus)
            return this.likeCount + 1;
        else
            return this.likeCount - 1;
    };
    // check user admin or not 
    ShowIdeaComponent.prototype.checkUserAdminOrNot = function () {
        this.isAdmin = this.userStatus.isUserAdmin();
    };
    // when user cliks on showProgress button 
    ShowIdeaComponent.prototype.onProgress = function () {
        this.getApprovalHistory();
    };
    // get phases data
    ShowIdeaComponent.prototype.getApprovalHistory = function () {
        var _this = this;
        if (!this.isProgressVisible) {
            this.progressText = "Getting data..";
            var data = { id: this.ideaData.id };
            this.showIdeaService.getPhaseData(data).subscribe(function (result) {
                if (result.status) {
                    _this.isProgressVisible = !_this.isProgressVisible;
                    _this.phasesData = result.data;
                    _this.showProgressDetails();
                }
                else {
                    console.log(result.message);
                }
            });
        }
        else {
            this.isProgressVisible = !this.isProgressVisible;
            this.showProgressDetails();
        }
    };
    // showing progress detais; 
    ShowIdeaComponent.prototype.showProgressDetails = function () {
        this.progressText = (this.isProgressVisible ? "Hide Progress" : "Show Progress");
        if (this.isProgressVisible)
            this.getProgressDetails();
    };
    // getting progress details 
    ShowIdeaComponent.prototype.getProgressDetails = function () {
        this.approvalText = (this.ideaData.approval_status == 0 ? "Idea has not been Approved" : "Idea has been approved");
        this.ideaData.approval_status == 0 ? this.isApproved == false : this.isApproved = true;
        this.ideaData.feasibility_status == 0 ? this.isFeasibile == false : this.isFeasibile = true;
        /* chage the color of phases text based on overdue data */
        this.phasesStatus();
    };
    // checking phases status and date 
    ShowIdeaComponent.prototype.phasesStatus = function () {
        // first lets change approval text color 
    };
    // when user cliks on approve button for approval phase 
    ShowIdeaComponent.prototype.onApprove = function (status) {
        return __awaiter(this, void 0, void 0, function () {
            var data, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isApprovalButtonBusy = true;
                        data = {
                            id: this.ideaData.id,
                            comments: this.approValForm.get('comments').value,
                            status: status,
                            created_by: JSON.parse(this.userStatus.getUserDetails())[0].id
                        };
                        return [4 /*yield*/, this.onApprovalUpdate(data).catch(function (error) {
                                console.log(error);
                            })
                            // on successfull updation of approval 
                        ];
                    case 1:
                        result = _a.sent();
                        // on successfull updation of approval 
                        if (result) {
                            if (status == 1) {
                                this.ideaData.approval_status = 1;
                                this.isApproved = true;
                            }
                            else {
                                this.ideaData.approval_status = 0;
                                this.isApproved = false;
                            }
                            this.approvedHistoryData.push(data);
                            this.approValForm.reset();
                            this.getApprovalStyle();
                            this.isApprovalButtonBusy = false;
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    // get user details from local storage 
    ShowIdeaComponent.prototype.getUserDetails = function () {
        return this.userStatus.getUserDetails();
    };
    // when user clicks on get history for approved details 
    ShowIdeaComponent.prototype.showApprovedHistory = function () {
        var _this = this;
        this.isApprovalHistory = !this.isApprovalHistory;
        if (this.isApprovalHistory && this.approvedHistoryData.length == 0) {
            // get approved details history from server for selected idea id
            this.showIdeaService.getApprovalHistory({ id: this.ideaData.id }).subscribe(function (result) {
                if (result.status) {
                    if (result.data.count > 0) {
                        result.data.forEach(function (element) {
                        });
                    }
                    _this.approvedHistoryData = result.data;
                }
            }, function (error) {
                console.log(error);
            });
        }
    };
    // updating approval status on server 
    ShowIdeaComponent.prototype.onApprovalUpdate = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.showIdeaService.updateApproveStatus(data).subscribe(function (result) {
                (result.status ? resolve(result.data) : reject(result));
            }, function (error) {
                reject(error);
            });
        });
    };
    //getting quarterhours in feasibility form
    ShowIdeaComponent.prototype.getQuarterHours = function () {
        var _this = this;
        console.log(this.ideaData.id);
        this.showIdeaService.getQuarterHours({ id: this.ideaData.id }).subscribe(function (result) {
            if (result.status) {
                console.log(result);
                // this.businessBenefits = result.data[0].businessBenefits;
                _this.feasibilityForm.controls['quarthours'].setValue(result.data[0].quarthours);
            }
            else {
                console.log(result.message);
            }
        }, function (error) {
        });
    };
    // showing feasibility history 
    ShowIdeaComponent.prototype.showFeassibilityHistory = function () {
        var _this = this;
        console.log("hello");
        this.feasHistoryButtonStatus = !this.feasHistoryButtonStatus;
        // get the feasibility history from server
        this.feasData = [];
        this.showIdeaService.getFeasibilityHistpry({ id: this.ideaData.id }).subscribe(function (result) {
            if (result.status) {
                _this.feasData = result.data;
                console.log(_this.feasData);
                console.log(_this.isFeasData);
                _this.feasData.length > 0 ? _this.isFeasData = true : _this.isFeasData = false;
                console.log(_this.isFeasData);
            }
            else {
                console.log(result.message);
            }
        }, function (error) {
        });
    };
    // when user clicks on approve/disapprove button 
    ShowIdeaComponent.prototype.onFeasible = function (status) {
        var _this = this;
        this.isApprovalButtonBusy = true;
        var data = {
            id: this.ideaData.id,
            comments: this.feasibilityForm.get('comments').value,
            quarter: this.feasibilityForm.get('quartsValue').value,
            quarterhours: this.feasibilityForm.get('quarthours').value,
            status: status,
            created_by: JSON.parse(this.userStatus.getUserDetails())[0].id
        };
        this.showIdeaService.updateFeasData(data).subscribe(function (result) {
            if (result.status) {
                if (status == 1) {
                    _this.ideaData.feasibility_status = 1;
                    _this.isFeasibile = true;
                }
                else {
                    _this.ideaData.feasibility_status = 0;
                    _this.isFeasibile = false;
                }
                _this.isApprovalButtonBusy = false;
                _this.feasibilityForm.reset();
                _this.getFeasibilityStyle();
                _this.showFeassibilityHistory();
            }
            else {
                _this.isApprovalButtonBusy = false;
            }
        });
    };
    /*  everything about teams*/
    // delete user from team 
    ShowIdeaComponent.prototype.deleteUser = function (index) {
        var _this = this;
        var updateData = this.matDataService.getTeamDataAsArray();
        updateData = updateData.filter(function (items) { return items.name != updateData[index].name; });
        var teamString = "";
        updateData.map(function (item) {
            (teamString.length == 0 ? (teamString = item.name.trim()) : teamString = teamString + "," + item.name.trim());
        });
        // update team data on server
        if (teamString.trim().length > 0) {
            var postData = {
                id: this.ideaData.id,
                team_memebers: teamString,
                created_by: JSON.parse(this.userStatus.getUserDetails())[0].id
            };
            this.showIdeaService.addTeamDataOnServer(postData).subscribe(function (result) {
                if (result.status) {
                    _this.matDataService.removeUSerFromTeam(index);
                }
            });
        }
    };
    // for adding new team members 
    ShowIdeaComponent.prototype.AddTeam = function () {
        var _this = this;
        var teamString = "";
        var tempTeamData = this.teamForm.get('team').value.trim().split(",").filter(function (item) { return item.trim().length > 0; });
        var data = tempTeamData.map(function (item) {
            (teamString.length == 0 ? (teamString = item.trim()) : teamString = teamString + "," + item.trim());
            return {
                name: item,
                action: ''
            };
        });
        // update team data on server
        var postData = {
            id: this.ideaData.id,
            team_memebers: teamString,
            created_by: JSON.parse(this.userStatus.getUserDetails())[0].id
        };
        this.showIdeaService.addTeamDataOnServer(postData).subscribe(function (result) {
            if (result.status) {
                _this.matDataService.addTeamData(data);
                _this.teamForm.reset();
                _this.isTeamAvailable = true;
                _this.getTeamStyle();
            }
            else {
                console.log(result.message);
            }
        }, function (error) {
            console.log(error);
        });
    };
    // getting the team data from server for given id 
    ShowIdeaComponent.prototype.getTeamData = function () {
        var _this = this;
        if (this.matDataService.getTeamDataAsArray().length == 0) {
            this.showIdeaService.getTeamData({ id: this.ideaData.id }).subscribe(function (result) {
                if (result.status) {
                    // now add data to table service with desired format 
                    if (result.data.length > 0) {
                        var teamData_1 = [];
                        result.data[0].team_memebers.split(",").map(function (item) {
                            teamData_1.push({ name: item, action: '' });
                        });
                        _this.matDataService.addTeamData(teamData_1);
                    }
                }
                else {
                    console.log(result.message);
                }
            }, function (error) {
                console.log(error);
            });
        }
    };
    /* everything about build status */
    // getting the build data 
    ShowIdeaComponent.prototype.getBuildData = function () {
        var _this = this;
        if (this.buildDataSource.getTeamDataAsArray().length == 0) {
            this.showIdeaService.getBuildData({ id: this.ideaData.id }).subscribe(function (result) {
                console.log("hey!", result);
                if (result.status) {
                    _this.buildDataSource.addTeamData(result.data);
                }
                else {
                    console.log(result.message);
                }
            }, function (error) {
            });
        }
    };
    /* everything about production */
    ShowIdeaComponent.prototype.getProductionData = function () {
        var _this = this;
        if (this.productionDataSource.getTeamDataAsArray().length == 0) {
            this.showIdeaService.getProductionData({ id: this.ideaData.id }).subscribe(function (result) {
                console.log("hey!!", result);
                if (result.status) {
                    _this.productionDataSource.addTeamData(result.data);
                }
                else {
                    console.log(result.message);
                }
            }, function (error) {
            });
        }
    };
    ShowIdeaComponent.prototype.getBusinessBenefitsData = function () {
        var _this = this;
        console.log(this.ideaData.id);
        this.showIdeaService.getBusinessBenefitsData({ id: this.ideaData.id }).subscribe(function (result) {
            if (result.status) {
                if (result.data.length > 0) {
                    console.log(result);
                    // this.businessBenefits = result.data[0].businessBenefits;
                    _this.businessBenefitsform.controls['businessBenefits'].setValue(result.data[0].businessBenefits);
                    _this.businessBenefitsform.controls['businessAppreciation'].setValue(result.data[0].businessAppreciation);
                    _this.businessBenefitsform.controls['businessbenefitsClassification'].setValue(result.data[0].businessbenefitsClassification);
                    _this.businessBenefitsform.controls['benefitsHours'].setValue(result.data[0].benefitsHours);
                }
            }
            else {
                console.log(result.message);
            }
        }, function (error) {
        });
    };
    // insert production data 
    ShowIdeaComponent.prototype.productionFormSubmit = function (form) {
        var _this = this;
        // check start date is before end data 
        if (!this.compareTwoDays(form.get('builEndDate').value, form.get('builStartDate').value)) {
            this.productionForm.controls['builEndDate'].setErrors({});
            return true;
        }
        var data = {
            id: this.ideaData.id,
            title: form.get('buildTitle').value,
            start_date: new Date(form.get('builStartDate').value),
            end_date: new Date(form.get('builEndDate').value),
            created_by: JSON.parse(this.userStatus.getUserDetails())[0].id,
            created_at: new Date(),
            status: 1
        };
        console.log("data ", data);
        this.showIdeaService.insertProductionData(data).subscribe(function (result) {
            if (result.status) {
                _this.productionDataSource.addTeamData([data]);
                _this.productionForm.reset();
                _this.isProduction = true;
                _this.getProductionStyle();
                _this.myform.reset();
            }
            else {
                console.log(result.message);
            }
        }, function (error) {
        });
    };
    // on build data submit 
    ShowIdeaComponent.prototype.buildFormSubmit = function (form) {
        var _this = this;
        // check start date is before end data 
        if (!this.compareTwoDays(form.get('builEndDate').value, form.get('builStartDate').value)) {
            this.buildForm.controls['builEndDate'].setErrors({});
            return true;
        }
        var data = {
            id: this.ideaData.id,
            title: form.get('buildTitle').value,
            start_date: new Date(form.get('builStartDate').value),
            end_date: new Date(form.get('builEndDate').value),
            created_by: JSON.parse(this.userStatus.getUserDetails())[0].id,
            created_at: new Date(),
            status: 1
        };
        console.log("data ", data);
        this.showIdeaService.insertBuildData(data).subscribe(function (result) {
            if (result.status) {
                _this.buildDataSource.addTeamData([data]);
                _this.buildForm.reset();
                _this.myform.reset();
                _this.isBuild = true;
                _this.getBuildStyle();
            }
            else {
                console.log(result.message);
            }
        }, function (error) {
        });
    };
    // Save Business Benefits Data
    ShowIdeaComponent.prototype.benefitsFormSubmit = function (form) {
        var _this = this;
        // check start date is before end data 
        var data = {
            id: this.ideaData.id,
            businessBenefits: form.get('businessBenefits').value,
            businessbenefitsClassification: form.get('businessbenefitsClassification').value,
            benefitsHours: form.get('benefitsHours').value,
            businessAppreciation: form.get('businessAppreciation').value,
            created_by: JSON.parse(this.userStatus.getUserDetails())[0].id,
            created_dt: new Date(),
            status: 1
        };
        console.log('data : ', data);
        this.showIdeaService.saveBusinessBenefitsData(data).subscribe(function (result) {
            if (result.status) {
                //this.businessBenefitsform.reset();
                _this.isBenefits = true;
                _this.getBenefitsStyle();
            }
            else {
                console.log(result.message);
            }
        }, function (error) {
        });
    };
    /* everything about changing phases text color */
    ShowIdeaComponent.prototype.getApprovalStyle = function () {
        var style = { 'color': 'white' };
        this.ideaData.approval_status == 1 ? style.color = 'green' : style.color = 'red';
        if (this.ideaData.approvalData) {
            if (this.getDayDifference(this.ideaData.approvalData.created_at) >= 10) {
                style.color = 'orange';
            }
        }
        return style;
    };
    ShowIdeaComponent.prototype.getFeasibilityStyle = function () {
        var style = { 'color': 'white' };
        this.ideaData.feasibility_status == 1 ? style.color = 'green' : style.color = 'red';
        if (this.ideaData.feasibilityData) {
            if (this.getDayDifference(this.ideaData.feasibilityData.created_at) >= 10) {
                style.color = 'orange';
            }
        }
        return style;
    };
    ShowIdeaComponent.prototype.getTeamStyle = function () {
        var style = { 'color': 'white' };
        this.ideaData.team_status == 1 ? style.color = 'green' : ((this.isTeamAvailable == true) ? style.color = 'green' : style.color = 'red');
        //this.isTeamAvailable == true ? style.color = 'green' : style.color = 'red';
        return style;
    };
    // updating build status color 
    ShowIdeaComponent.prototype.getBuildStyle = function () {
        var style = { 'color': 'white' };
        this.ideaData.build_status == 1 ? style.color = 'green' : ((this.isBuild == true) ? style.color = 'green' : style.color = 'red');
        return style;
    };
    // updating production status color 
    ShowIdeaComponent.prototype.getProductionStyle = function () {
        var style = { 'color': 'white' };
        this.ideaData.production_status == 1 ? style.color = 'green' : ((this.isProduction == true) ? style.color = 'green' : style.color = 'red');
        return style;
    };
    // updating production status color 
    ShowIdeaComponent.prototype.getBenefitsStyle = function () {
        var style = { 'color': 'white' };
        this.ideaData.business_impact_status == 1 ? style.color = 'green' : ((this.isBenefits == true) ? style.color = 'green' : style.color = 'red');
        return style;
    };
    // get days diff 
    ShowIdeaComponent.prototype.getDayDifference = function (status_date) {
        return ((new Date().valueOf() - new Date(status_date).valueOf()) / (1000 * 60 * 60 * 24));
    };
    // check start date before or not 
    ShowIdeaComponent.prototype.compareTwoDays = function (end_date, start_date) {
        var difference = Math.round(new Date(end_date).getTime() - new Date(start_date).getTime()) / ((1000 * 60 * 60 * 24));
        return difference >= 0;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* FormGroupDirective */]),
        __metadata("design:type", Object)
    ], ShowIdeaComponent.prototype, "myform", void 0);
    ShowIdeaComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__angular_core__["Component"])({
            selector: 'app-show-idea',
            template: __webpack_require__("./src/app/show-idea/show-idea.component.html"),
            styles: [__webpack_require__("./src/app/show-idea/show-idea.component.css")]
        }),
        __param(10, Object(__WEBPACK_IMPORTED_MODULE_5__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__angular_material__["a" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__utils_checkUserStatus__["a" /* CheckUserStatus */],
            __WEBPACK_IMPORTED_MODULE_2__showIdeaService__["a" /* ShowIdeaService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["h" /* MatDialogRef */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_0__dataService__["a" /* MatTableDataService */],
            __WEBPACK_IMPORTED_MODULE_8__buildDataSource__["a" /* BuildDataSource */],
            __WEBPACK_IMPORTED_MODULE_9__productionDataSource__["a" /* ProductionDataSource */],
            __WEBPACK_IMPORTED_MODULE_10__add_automation_addautomationservice__["a" /* AddAutomationService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["f" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_11__utils_httpConfig__["a" /* HttpCOnfig */], Object])
    ], ShowIdeaComponent);
    return ShowIdeaComponent;
}());

// export interface TeamData {
//   name : string,
//   action : string;
//   date : string;
// }
// demo class for getting data stream 
var DemoDataSource = (function (_super) {
    __extends(DemoDataSource, _super);
    function DemoDataSource(matTableDataSource) {
        var _this = _super.call(this) || this;
        _this.matTableDataSource = matTableDataSource;
        return _this;
    }
    DemoDataSource.prototype.connect = function () {
        return this.matTableDataSource.data$;
    };
    DemoDataSource.prototype.disconnect = function () {
        this.matTableDataSource.resetData();
    };
    return DemoDataSource;
}(__WEBPACK_IMPORTED_MODULE_7__angular_cdk_collections__["a" /* DataSource */]));

/* build data source class */
var BuildDataSourceTable = (function (_super) {
    __extends(BuildDataSourceTable, _super);
    function BuildDataSourceTable(teamDataSource) {
        var _this = _super.call(this) || this;
        _this.teamDataSource = teamDataSource;
        return _this;
    }
    BuildDataSourceTable.prototype.connect = function () {
        return this.teamDataSource.data$;
    };
    BuildDataSourceTable.prototype.disconnect = function () {
        this.teamDataSource.resetData();
    };
    return BuildDataSourceTable;
}(__WEBPACK_IMPORTED_MODULE_7__angular_cdk_collections__["a" /* DataSource */]));

/* production data source class */
var ProductionDataSourceTable = (function (_super) {
    __extends(ProductionDataSourceTable, _super);
    function ProductionDataSourceTable(producionDatSource) {
        var _this = _super.call(this) || this;
        _this.producionDatSource = producionDatSource;
        return _this;
    }
    ProductionDataSourceTable.prototype.connect = function () {
        return this.producionDatSource.data$;
    };
    ProductionDataSourceTable.prototype.disconnect = function () {
        this.producionDatSource.resetData();
    };
    return ProductionDataSourceTable;
}(__WEBPACK_IMPORTED_MODULE_7__angular_cdk_collections__["a" /* DataSource */]));



/***/ }),

/***/ "./src/app/show-idea/showIdeaService.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShowIdeaService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
this is for making all the idea related http calls
*/
var ShowIdeaService = (function () {
    function ShowIdeaService(httpClient, httpConfig) {
        this.httpClient = httpClient;
        this.httpConfig = httpConfig;
    }
    ShowIdeaService.prototype.quarts = function (URL) {
        return this.httpClient.post(URL, {});
    };
    // updating an idea like count 
    ShowIdeaService.prototype.updateLikeCount = function (data) {
        return this.httpClient.post(this.httpConfig.getUpdateLikeURL(), data);
    };
    // getting approval history 
    ShowIdeaService.prototype.getApprovalHistory = function (data) {
        return this.httpClient.post(this.httpConfig.getApprovalHistotyURL(), data);
    };
    // update approval data 
    ShowIdeaService.prototype.updateApproveStatus = function (data) {
        return this.httpClient.post(this.httpConfig.updateApproveStatus(), data);
    };
    // get feasibility history 
    ShowIdeaService.prototype.getFeasibilityHistpry = function (data) {
        return this.httpClient.post(this.httpConfig.getFeasiBulityHiURL(), data);
    };
    // get quarterHours
    ShowIdeaService.prototype.getQuarterHours = function (data) {
        return this.httpClient.post(this.httpConfig.getquarthours(), data);
    };
    // updateFesibilityData 
    ShowIdeaService.prototype.updateFeasData = function (data) {
        return this.httpClient.post(this.httpConfig.getFeasDataURL(), data);
    };
    // getting phases data 
    ShowIdeaService.prototype.getPhaseData = function (data) {
        return this.httpClient.post(this.httpConfig.getPhasesDataURL(), data);
    };
    // getting team data 
    ShowIdeaService.prototype.getTeamData = function (data) {
        return this.httpClient.post(this.httpConfig.getTeamDataURL(), data);
    };
    // adding team data on server 
    ShowIdeaService.prototype.addTeamDataOnServer = function (data) {
        return this.httpClient.post(this.httpConfig.addTeamDataURL(), data);
    };
    // getting build data 
    ShowIdeaService.prototype.getBuildData = function (data) {
        return this.httpClient.post(this.httpConfig.getBuilDataURL(), data);
    };
    // insert  build data 
    ShowIdeaService.prototype.insertBuildData = function (data) {
        return this.httpClient.post(this.httpConfig.isnertBuildDataURL(), data);
    };
    // get production Data 
    ShowIdeaService.prototype.getProductionData = function (data) {
        return this.httpClient.post(this.httpConfig.getProductionDataURL(), data);
    };
    // insert production data 
    ShowIdeaService.prototype.insertProductionData = function (data) {
        return this.httpClient.post(this.httpConfig.insertProductionData(), data);
    };
    // getBusinessBenefitsData
    ShowIdeaService.prototype.getBusinessBenefitsData = function (data) {
        return this.httpClient.post(this.httpConfig.getBusinessBenefitData(), data);
    };
    // saveBusinessBenefisData
    ShowIdeaService.prototype.saveBusinessBenefitsData = function (data) {
        return this.httpClient.post(this.httpConfig.saveBusinessBenefitsData(), data);
    };
    ShowIdeaService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__utils_httpConfig__["a" /* HttpCOnfig */]])
    ], ShowIdeaService);
    return ShowIdeaService;
}());



/***/ }),

/***/ "./src/app/show-originate/originateDetails.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OriginateService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
Fetching Automation Details through Http Calls
*/
var OriginateService = (function () {
    function OriginateService(httpClient) {
        this.httpClient = httpClient;
    }
    // Fetch Automation Dashboard Details
    OriginateService.prototype.originateDetails = function (URL) {
        return this.httpClient.get(URL, {});
    };
    OriginateService.prototype.searchoriginateDetails = function (URL, data) {
        return this.httpClient.post(URL, data);
    };
    OriginateService.prototype.originateComments = function (URL, data) {
        return this.httpClient.post(URL, data);
    };
    OriginateService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], OriginateService);
    return OriginateService;
}());



/***/ }),

/***/ "./src/app/show-originate/show-originate.component.css":
/***/ (function(module, exports) {

module.exports = ".dashboardpanel\r\n{\r\n    margin:2px 2px;\r\n    padding:2px;\r\n    background: -webkit-gradient(linear, left top, left bottom, from(#e9e9ed), to(#eef0f5));\r\n    background: linear-gradient(#e9e9ed, #eef0f5);\r\n    border: 2px solid rgb(28, 50, 107);\r\n    border-radius: 5px;\r\n}\r\n\r\n.dashboardheader\r\n{\r\n    text-align: center;\r\n    font-size: 22px;\r\n    font-style: italic;\r\n}\r\n\r\n.filterCriteria\r\n{\r\n    border: black 2px solid;\r\n    background-color: aliceblue;\r\n    height: 60px;\r\n}\r\n\r\n.exportCriteria\r\n{\r\n    margin: 5px;    \r\n    height: 50px;    \r\n    padding: 5px;\r\n}\r\n\r\n.SearchAll\r\n{\r\n    float: right;\r\n    margin: 5px;\r\n    padding: 5px;\r\n}\r\n"

/***/ }),

/***/ "./src/app/show-originate/show-originate.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" style=\"padding:20px\">\r\n  <div class=\"row\">\r\n      <form [formGroup]=\"searchOriginateForm\">\r\n          <div class=\"col-md-3 filterCriteria\">\r\n            <mat-form-field class=\"example-full-width\">\r\n              <mat-select placeholder=\"Opportunity Tower\" (ngModelChange)=\"OnTowerChange($event)\" formControlName=\"originateTower\">\r\n                <mat-option *ngFor=\"let tower of originateTower\" [value]=\"tower.value\">\r\n                  {{tower.value}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-3 filterCriteria\">\r\n            <mat-form-field class=\"example-full-width\">\r\n              <mat-select placeholder=\"Opportunity Category\" (ngModelChange)=\"OnCatChange($event)\" formControlName=\"originateCategory\">\r\n                <mat-option *ngFor=\"let category of originateCategory\" [value]=\"category.value\">\r\n                  {{category.value}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"col-md-4 filterCriteria\">\r\n            <button mat-button color=\"primary\" class=\"SearchAll\" (click)=\"onSelectAll($event)\">ALL</button>\r\n          </div>\r\n        </form>\r\n  </div>\r\n  <div class=\"row\" style=\"margin:2px\">\r\n    <div class=\"col-md-10 exportCriteria\" >\r\n      <button mat-raised-button color=\"warn\" (click)=\"ClickExcel()\">Export as Excel</button>\r\n      <button mat-raised-button color=\"warn\" disabled=\"disabled\" (click)=\"ClickPDF()\">Export as PDF</button>\r\n    </div>    \r\n  </div>\r\n  <div  class=\"row\">\r\n    <div class=\"col-md-5 dashboardpanel\"  id=\"OriginateCategoryArea\">\r\n       <!-- <fusioncharts *ngIf=\"pieOriginCatData != null\"  width=\"476\" height=\"350\" type=\"pie2d\"  dataFormat=\"json\"\r\n        [dataSource]=\"pieOriginCatData\">\r\n      </fusioncharts>  -->\r\n      <ngx-charts-tree-map *ngIf=\"pieOriginCatData && pieOriginCatData.length > 0\" \r\n        [view]=\"view\" [scheme]=\"originatecategoryscheme\" [results]=\"pieOriginCatData\"\r\n        [gradient]=\"true\"\r\n       >\r\n      </ngx-charts-tree-map>\r\n      <div class=\"dashboardheader\">\r\n        Originate Category\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-5 dashboardpanel\" id=\"OrigCatStatusProbability\" >\r\n      <ngx-charts-bar-vertical-stacked *ngIf=\"origCatStatProbData && origCatStatProbData.length > 0\" [view]=\"BAview\" [scheme]=\"verStacScheme\"\r\n        [results]=\"origCatStatProbData\" [gradient]=\"true\" [xAxis]=\"true\" [yAxis]=\"true\" [legend]=\"true\" [legendTitle]=\"BAlegendTitle\"\r\n        (select)=\"onBASelect($event)\">\r\n      </ngx-charts-bar-vertical-stacked>\r\n      <div class=\"dashboardheader\">\r\n        Originate Category / Status - Probability\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"row\">\r\n    <div class=\"col-md-5 dashboardpanel\" id=\"OrigCatStatusEstimate\">\r\n      <ngx-charts-advanced-pie-chart *ngIf=\"origCatStatEstData && origCatStatEstData.length > 0\" [view]=\"view\" [scheme]=\"colorscheme\"\r\n        [results]=\"origCatStatEstData\" [gradient]=\"gradient\" (select)=\"onStatusSelect($event)\">\r\n      </ngx-charts-advanced-pie-chart>\r\n      <div class=\"dashboardheader\">\r\n        Originate Status\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-5 dashboardpanel\" id=\"OrigCatStatusRevenue\">\r\n      <ngx-charts-bar-vertical-2d *ngIf=\"origCatStatReveData && origCatStatReveData.length > 0\" [view]=\"VTTview\" [scheme]=\"WorkToolScheme\"\r\n        [results]=\"origCatStatReveData\" [xAxis]=\"true\" [yAxis]=\"true\" [gradient]=\"false\" [legend]=\"true\" [legendTitle]=\"WTlegentTitle\"\r\n        (select)=\"onWTSelect($event)\">\r\n      </ngx-charts-bar-vertical-2d>\r\n      <div class=\"dashboardheader\">\r\n        Originate Category / Status - Revenue\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/show-originate/show-originate.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShowOriginateComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__originatedashdialog_originatedashdialog_component__ = __webpack_require__("./src/app/originatedashdialog/originatedashdialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_typescript_linq__ = __webpack_require__("./node_modules/typescript-linq/TS.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_typescript_linq___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_typescript_linq__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__add_originate_addoriginate__ = __webpack_require__("./src/app/add-originate/addoriginate.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__originateDetails__ = __webpack_require__("./src/app/show-originate/originateDetails.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__utils_excelService__ = __webpack_require__("./src/app/utils/excelService.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









var ShowOriginateComponent = (function () {
    function ShowOriginateComponent(httpConfig, _formBuilder, excelService, originateService, addOriginateService, dialog) {
        this.httpConfig = httpConfig;
        this._formBuilder = _formBuilder;
        this.excelService = excelService;
        this.originateService = originateService;
        this.addOriginateService = addOriginateService;
        this.dialog = dialog;
        this.BAview = [480, 350];
        this.VTTview = [480, 350];
        this.view = [480, 350];
        this.selectDialogData = [];
        this.elCount = 0;
        this.originateTower = [];
        this.originateCategory = [];
        //datasource: object ;
        this.elements = ['CategoryArea', 'CatStatProb', 'CatStatEst', 'CatStatRev'];
        this.pdf = new jsPDF('l', 'mm', 'a4');
        this.originateDetailsData = [];
        this.masterOriginateDetails = [];
        this.towerselected = '';
        this.categoryselected = '';
        this.originateCatData = [];
        this.origCatStatProb = [];
        this.origCatStatEst = [];
        this.origCatStatReve = [];
        this.sample = new __WEBPACK_IMPORTED_MODULE_5_typescript_linq__["TS"].Collections.List(false);
        this.pieOriginCatData = [];
        this.origCatStatEstData = [];
        this.origCatStatReveData = [];
        this.origCatStatProbData = [];
        this.gradient = false;
        this.BAlegendTitle = 'Initiative';
        this.WTlegentTitle = 'Tool';
        this.count = 0;
        this.verStacScheme = {
            domain: ['#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886']
        };
        this.colorscheme = {
            domain: ['#647c8a', '#3f51b5', '#2196f3', '#00b862', '#afdf0a', '#a7b61a', '#f3e562', '#ff9800', '#ff5722', '#ff4514']
        };
        this.originatecategoryscheme = {
            domain: ['#A10A28', '#D3342D', '#EF6D49', '#FAAD67', '#FDDE90', '#DBED91', '#A9D770', '#6CBA67', '#2C9653', '#146738']
        };
        this.WorkToolScheme = {
            domain: ['#55C22D', '#C1F33D', '#3CC099', '#AFFFFF', '#8CFC9D', '#76CFFA', '#BA60FB', '#EE6490', '#C42A1C', '#FC9F32']
        };
    }
    ShowOriginateComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var serverResult, originateEntityData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        serverResult = '';
                        this.searchOriginateForm = this._formBuilder.group({
                            originateTower: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */](),
                            originateCategory: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]()
                        });
                        originateEntityData = '';
                        return [4 /*yield*/, this.FetchOriginateEntity()];
                    case 1:
                        originateEntityData = _a.sent();
                        this.PopulateDropdowns(originateEntityData);
                        return [4 /*yield*/, this.FetchDetailsServer()];
                    case 2:
                        serverResult = _a.sent();
                        this.masterOriginateDetails = serverResult;
                        this.PopulateDashboard(serverResult);
                        // this.pieOriginCatData = {
                        // 'chart': {
                        //   'caption': 'Opportunity Category Split',
                        //   'plottooltext': '<b>$percentValue</b> of opportunity are $label category',
                        //   'showlegend': '0',
                        //   'showpercentvalues': '1',
                        //   'legendposition': 'bottom',
                        //   'usedataplotcolorforlabels': '1',
                        //   'theme': 'fusion'
                        // }, 'data': this.originateCatData};
                        console.log(this.originateCatData);
                        this.pieOriginCatData = this.originateCatData;
                        this.origCatStatEstData = this.origCatStatEst;
                        this.origCatStatProbData = this.origCatStatProb;
                        this.origCatStatReveData = this.origCatStatReve;
                        return [2 /*return*/];
                }
            });
        });
    };
    ShowOriginateComponent.prototype.FetchOriginateEntity = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var originateEntityURL = _this.httpConfig.getOriginateEntity();
            _this.addOriginateService.originateEntity(originateEntityURL).subscribe(function (result) {
                if (result.status) {
                    (result.status ? resolve(result.data) : reject(result));
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    ShowOriginateComponent.prototype.PopulateDropdowns = function (dropdownResults) {
        var _this = this;
        var serverData = [];
        dropdownResults.forEach(function (element) {
            serverData.push({
                item: element.originateentityID,
                value: element.originateentityDesc,
                Type: element.originateentityTitle
            });
        });
        serverData.forEach(function (element) {
            if (element.Type === 'originateTower') {
                _this.originateTower.push(element);
            }
            if (element.Type === 'originateCategoryType') {
                _this.originateCategory.push(element);
            }
        });
    };
    ShowOriginateComponent.prototype.FetchDetailsServer = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var automationDetailsURL = _this.httpConfig.getOriginateDetails();
            _this.originateService.originateDetails(automationDetailsURL).subscribe(function (result) {
                if (result.status) {
                    (result.status ? resolve(result.data) : reject(result));
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    ShowOriginateComponent.prototype.PopulateCategoryData = function (data) {
        var _this = this;
        data.forEach(function (element) {
            if (!_this.originateCatData) {
                _this.originateCatData.push({
                    name: element.category,
                    value: 1
                });
            }
            else {
                var findTower = _this.originateCatData.findIndex(function (ele) { return ele.name === element.category; });
                if (findTower === -1) {
                    _this.originateCatData.push({
                        name: element.category,
                        value: 1
                    });
                }
                else {
                    _this.originateCatData.find(function (ele) { return ele.name === element.category; }).value++;
                }
            }
        });
    };
    ShowOriginateComponent.prototype.PopulateCatStatusEstData = function (data) {
        var _this = this;
        // console.log(data);
        data.forEach(function (element) {
            if (!_this.origCatStatEst) {
                _this.origCatStatEst.push({
                    name: element.status,
                    value: 1
                });
            }
            else {
                var findStatus = _this.origCatStatEst.findIndex(function (ele) { return ele.name === element.status; });
                if (findStatus === -1) {
                    _this.origCatStatEst.push({
                        name: element.status,
                        value: 1
                    });
                }
                else {
                    _this.origCatStatEst.find(function (ele) { return ele.name === element.status; }).value++;
                }
            }
        });
    };
    ShowOriginateComponent.prototype.PopulateCatStatProbData = function (data) {
        var _this = this;
        data.forEach(function (element) {
            if (!_this.origCatStatProb) {
                _this.origCatStatProb.push({
                    name: element.category,
                    series: [{
                            name: element.perprobability,
                            value: 1
                        }]
                });
            }
            else {
                var findorigincat = _this.origCatStatProb.findIndex(function (ele) { return ele.name === element.category; });
                if (findorigincat === -1) {
                    _this.origCatStatProb.push({
                        name: element.category,
                        series: [{
                                name: element.perprobability,
                                value: 1
                            }]
                    });
                }
                else {
                    var findPerProb = (_this.origCatStatProb[findorigincat].series === undefined) ? -1 :
                        _this.origCatStatProb[findorigincat].series.findIndex(function (ele) { return ele.name === element.perprobability; });
                    if (findPerProb === -1) {
                        _this.origCatStatProb[findorigincat].series.push({
                            name: element.perprobability,
                            value: 1
                        });
                    }
                    else {
                        _this.origCatStatProb[findorigincat].series[findPerProb].value++;
                    }
                }
            }
        });
    };
    ShowOriginateComponent.prototype.PopulateorigCatStatReve = function (data) {
        var _this = this;
        // console.log(data);
        data.forEach(function (element) {
            if (!_this.origCatStatReve) {
                _this.origCatStatReve.push({
                    name: element.category,
                    series: [{
                            name: element.status,
                            value: element.revenue
                        }]
                });
            }
            else {
                var findCat = _this.origCatStatReve.findIndex(function (ele) { return ele.name === element.category; });
                if (findCat === -1) {
                    _this.origCatStatReve.push({
                        name: element.category,
                        series: [{
                                name: element.status,
                                value: element.revenue
                            }]
                    });
                }
                else {
                    var findStatus = _this.origCatStatReve[findCat].series.findIndex(function (ele) { return ele.name === element.status; });
                    if (findStatus === -1) {
                        _this.origCatStatReve[findCat].series.push({
                            name: element.status,
                            value: element.revenue
                        });
                    }
                    else {
                        _this.origCatStatReve[findCat].series[findStatus].value += element.revenue;
                    }
                }
            }
        });
    };
    ShowOriginateComponent.prototype.PopulateDashboard = function (data) {
        var _this = this;
        this.originateDetailsData = [];
        // console.log(data);
        data.forEach(function (element) {
            var percentprobability = element.OriginateProbability > 50 ? 'High' : 'Low';
            _this.originateDetailsData.push({
                id: element.OriginateID,
                tower: element.OriginateTower,
                category: element.OriginateCategoryType,
                status: element.OriginateStatusType,
                title: element.OriginateTitle,
                description: element.OriginateDesc,
                stakeholder: element.OriginateStakeholder,
                probability: element.OriginateProbability,
                perprobability: percentprobability,
                estimate: element.OriginateEstimate,
                revenue: element.OriginateRevenue,
            });
        });
        this.PopulateCategoryData(this.originateDetailsData);
        this.PopulateCatStatusEstData(this.originateDetailsData);
        this.PopulateCatStatProbData(this.originateDetailsData);
        this.PopulateorigCatStatReve(this.originateDetailsData);
    };
    ShowOriginateComponent.prototype.onSelectAll = function (event) {
        this.pieOriginCatData = [];
        this.originateCatData = [];
        this.origCatStatEstData = [];
        this.origCatStatEst = [];
        this.origCatStatProbData = [];
        this.origCatStatProb = [];
        this.origCatStatReveData = [];
        this.origCatStatReve = [];
        this.PopulateDashboard(this.masterOriginateDetails);
        this.pieOriginCatData = this.originateCatData;
        this.origCatStatEstData = this.origCatStatEst;
        this.origCatStatProbData = this.origCatStatProb;
        this.origCatStatReveData = this.origCatStatReve;
        this.towerselected = '';
        this.categoryselected = '';
        this.searchOriginateForm.controls['originateTower'].setValue(-1);
        this.searchOriginateForm.controls['originateCategory'].setValue(-1);
    };
    ShowOriginateComponent.prototype.onStatusSelect = function (event) {
        // console.log(event.value);
        // console.log(event.name);
        // console.log(this.originateDetailsData);
        var selectDialogData = [];
        this.originateDetailsData.forEach(function (element) {
            if (element.status === event.name) {
                selectDialogData.push(element);
            }
        });
        this.dialog.open(__WEBPACK_IMPORTED_MODULE_3__originatedashdialog_originatedashdialog_component__["a" /* OriginatedashdialogComponent */], {
            data: { detailsData: selectDialogData, type: 'Status' }, panelClass: 'show-idea'
        });
    };
    ShowOriginateComponent.prototype.onBASelect = function (event) {
        var selectDialogData = [];
        this.originateDetailsData.forEach(function (element) {
            if (element.perprobability === event.name && element.category === event.series) {
                selectDialogData.push(element);
            }
        });
        this.dialog.open(__WEBPACK_IMPORTED_MODULE_3__originatedashdialog_originatedashdialog_component__["a" /* OriginatedashdialogComponent */], {
            data: { detailsData: selectDialogData, type: 'BenefitArea' }, panelClass: 'show-idea'
        });
    };
    ShowOriginateComponent.prototype.onWTSelect = function (event) {
        var selectDialogData = [];
        this.originateDetailsData.forEach(function (element) {
            if (element.status === event.name && element.category === event.series) {
                selectDialogData.push(element);
            }
        });
        this.dialog.open(__WEBPACK_IMPORTED_MODULE_3__originatedashdialog_originatedashdialog_component__["a" /* OriginatedashdialogComponent */], {
            data: { detailsData: selectDialogData, type: 'WorkType' }, panelClass: 'show-idea'
        });
    };
    ShowOriginateComponent.prototype.ClickPDF = function () {
        var _this = this;
        // All units are in the set measurement for the document
        // This can be changed to 'pt' (points), 'mm' (Default), 'cm', 'in'
        this.sample = new __WEBPACK_IMPORTED_MODULE_5_typescript_linq__["TS"].Collections.List(false);
        this.originateDetailsData.forEach(function (element) {
            _this.sample.add(element);
        });
        this.pdf = new jsPDF('l', 'mm', 'a4');
        this.elCount = -1;
        this.pdf.setFontSize(22);
        this.pdf.setFont('times');
        this.pdf.setFontType('bolditalic');
        this.pdf.text('Automation Report', 150, 10, 'center');
        this.recursiveAddHtml();
    };
    ShowOriginateComponent.prototype.recursiveAddHtml = function () {
        var _this = this;
        this.elCount++;
        if (this.elCount < this.elements.length) {
            this.pdf.setFontSize(16);
            this.pdf.setFont('helvetica');
            this.pdf.setFontType('italic');
            switch (this.elements[this.elCount]) {
                case 'CategoryArea':
                    this.pdf.text('Category Wise View', 10, 30);
                    break;
                case 'CatStatProb':
                    this.pdf.text('Category Status Wise View', 10, 30);
                    break;
                case 'CatStatEst':
                    this.pdf.text('Category Status Estimate View', 10, 30);
                    break;
                case 'CatStatRev':
                    this.pdf.text('Category Status Revenue View', 10, 30);
                    break;
            }
            this.pdf.addHTML(document.getElementById(this.elements[this.elCount]), 80, 40, function () {
                switch (_this.elements[_this.elCount]) {
                    case 'CategoryArea':
                        _this.printCategoryData();
                        break;
                    case 'CatStatProb':
                        _this.printCatStatProbData();
                        break;
                    case 'CatStatEst':
                        _this.printCatStatEstData();
                        break;
                    case 'CatStatRev':
                        _this.printCatStatRevData();
                        break;
                }
                _this.pdf.setLineWidth(0.5);
                _this.pdf.line(10, 150, 150, 150);
                _this.pdf.addPage();
                _this.recursiveAddHtml();
            });
        }
        else {
            this.pdf.save('Automation_Dashboard.pdf');
        }
    };
    ShowOriginateComponent.prototype.printCategoryData = function () {
        var _this = this;
        var rowsTower = [];
        var rowsStatus = [];
        var resultTower = new __WEBPACK_IMPORTED_MODULE_5_typescript_linq__["TS"].Collections.List(false);
        resultTower = this.sample.groupBy(function (ele) { return (ele.category); }).toList();
        var columnsTower = [
            { title: 'ID', dataKey: 'id' },
            { title: 'Category', dataKey: 'tower' },
            { title: 'Count', dataKey: 'count' }
        ];
        this.count = 0;
        resultTower.forEach(function (element) {
            rowsTower.push({
                id: ++_this.count,
                tower: element.key,
                count: _this.sample.count(function (ele) { return ele.category === element.key; })
            });
        });
        this.pdf.autoTable(columnsTower, rowsTower, {
            margin: { vertical: 40 },
            styles: { overflow: 'linebreak', columnWidth: 'wrap' },
            columnStyles: { text: { columnWidth: 'auto' } }
        });
    };
    ShowOriginateComponent.prototype.printCatStatProbData = function () {
        var _this = this;
        var rowsStatus = [];
        var resultStatus = new __WEBPACK_IMPORTED_MODULE_5_typescript_linq__["TS"].Collections.List(false);
        resultStatus = this.sample.groupBy(function (ele) { return (ele.status); }).toList();
        var columnsStatus = [
            { title: 'ID', dataKey: 'id' },
            { title: 'Status', dataKey: 'status' },
            { title: 'Count', dataKey: 'count' }
        ];
        this.count = 0;
        resultStatus.forEach(function (element) {
            rowsStatus.push({
                id: ++_this.count,
                status: element.key,
                count: _this.sample.count(function (ele) { return ele.status === element.key; })
            });
        });
        this.pdf.autoTable(columnsStatus, rowsStatus, {
            margin: { vertical: 40 },
            styles: { overflow: 'linebreak', columnWidth: 'wrap' },
            columnStyles: { text: { columnWidth: 'auto' } }
        });
    };
    ShowOriginateComponent.prototype.OnTowerChange = function (event) {
        var _this = this;
        console.log(event);
        console.log(this.masterOriginateDetails);
        if (event !== -1) {
            var selectTowerWisedata = void 0;
            this.towerselected = event;
            if (this.categoryselected === '') {
                selectTowerWisedata = this.masterOriginateDetails.filter(function (element) { return element.OriginateTower === event; });
            }
            else {
                selectTowerWisedata = this.masterOriginateDetails.filter(function (element) { return element.OriginateTower === event
                    && element.OriginateCategoryType === _this.categoryselected; });
            }
            console.log(selectTowerWisedata);
            this.pieOriginCatData = [];
            this.origCatStatProbData = [];
            this.origCatStatEstData = [];
            this.origCatStatReveData = [];
            this.originateCatData = [];
            this.origCatStatEst = [];
            this.origCatStatProb = [];
            this.origCatStatReve = [];
            this.PopulateDashboard(selectTowerWisedata);
            this.pieOriginCatData = this.originateCatData;
            this.origCatStatEstData = this.origCatStatEst;
            this.origCatStatProbData = this.origCatStatProb;
            this.origCatStatReveData = this.origCatStatReve;
        }
    };
    ShowOriginateComponent.prototype.OnCatChange = function (event) {
        var _this = this;
        console.log(event);
        console.log(this.masterOriginateDetails);
        if (event !== -1) {
            var selectTowerWisedata = void 0;
            this.categoryselected = event;
            if (this.towerselected === '') {
                selectTowerWisedata = this.masterOriginateDetails.filter(function (element) { return element.OriginateCategoryType === event; });
            }
            else {
                selectTowerWisedata = this.masterOriginateDetails.filter(function (element) { return element.OriginateCategoryType === event
                    && element.OriginateTower === _this.towerselected; });
            }
            console.log(selectTowerWisedata);
            this.pieOriginCatData = [];
            this.origCatStatProbData = [];
            this.origCatStatEstData = [];
            this.origCatStatReveData = [];
            this.originateCatData = [];
            this.origCatStatEst = [];
            this.origCatStatProb = [];
            this.origCatStatReve = [];
            this.PopulateDashboard(selectTowerWisedata);
            this.pieOriginCatData = this.originateCatData;
            this.origCatStatEstData = this.origCatStatEst;
            this.origCatStatProbData = this.origCatStatProb;
            this.origCatStatReveData = this.origCatStatReve;
        }
    };
    ShowOriginateComponent.prototype.printCatStatEstData = function () {
        // const rowsBArea: any[]  = [];
        // let resultBArea = new TS.Collections.List<any>(false);
        // resultBArea = this.sample.groupBy(ele =>  (ele.BenefitArea)).toList();
        // // console.log(resultBArea);
        // const columnsBArea = [
        //     { title: 'ID', dataKey: 'id' },
        //     { title: 'Benefit Area', dataKey: 'benArea' },
        //     { title: 'Count', dataKey: 'count' }];
        // this.count = 0;
        // resultBArea.forEach(element => {
        //   rowsBArea.push({
        //     id: ++this.count,
        //     benArea: element.key,
        //     count: this.sample.count(ele => ele.BenefitArea === element.key)
        //   });
        // });
        // this.pdf.autoTable(columnsBArea, rowsBArea, {
        //   margin: { vertical : 40 },
        //   styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        //   columnStyles: { text: { columnWidth: 'auto' } }
        // });
    };
    ShowOriginateComponent.prototype.printCatStatRevData = function () {
        // const rowsBArea: any[]  = [];
        // let resultBArea = new TS.Collections.List<any>(false);
        // resultBArea = this.sample.groupBy(ele =>  (ele.Tool)).toList();
        // const columnsBArea = [
        //     { title: 'ID', dataKey: 'id' },
        //     { title: 'Tool', dataKey: 'tool' },
        //     { title: 'Count', dataKey: 'count' }];
        // this.count = 0;
        // resultBArea.forEach(element => {
        //   rowsBArea.push({
        //     id: ++this.count,
        //     tool: element.key,
        //     count: this.sample.count(ele => ele.Tool === element.key)
        //   });
        // });
        // this.pdf.autoTable(columnsBArea, rowsBArea, {
        //   margin: { vertical : 40 },
        //   styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        //   columnStyles: { text: { columnWidth: 'auto' } }
        // });
    };
    ShowOriginateComponent.prototype.ClickExcel = function () {
        var _this = this;
        this.selectDialogData = [];
        this.originateDetailsData.forEach(function (element) {
            _this.selectDialogData.push(element);
        });
        this.excelService.exportAsExcelFile(this.selectDialogData, 'Originate_Dashboard');
    };
    ShowOriginateComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-show-originate',
            template: __webpack_require__("./src/app/show-originate/show-originate.component.html"),
            styles: [__webpack_require__("./src/app/show-originate/show-originate.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__utils_httpConfig__["a" /* HttpCOnfig */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_8__utils_excelService__["a" /* ExcelService */],
            __WEBPACK_IMPORTED_MODULE_7__originateDetails__["a" /* OriginateService */],
            __WEBPACK_IMPORTED_MODULE_6__add_originate_addoriginate__["a" /* AddOriginateService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["f" /* MatDialog */]])
    ], ShowOriginateComponent);
    return ShowOriginateComponent;
}());



/***/ }),

/***/ "./src/app/show-people/show-people.component.css":
/***/ (function(module, exports) {

module.exports = "          .dateselector{\r\n            width:1000px;\r\n            height:1000px;\r\n            padding:5px;\r\n            border-radius:6px;\r\n            border:none;\r\n            color:#0F0D0D;;\r\n            font-weight:bold;\r\n            text-align: center;\r\n            vertical-align: middle;\r\n            background-color:#E2EFF7;\r\n            -webkit-box-shadow:\r\n        0 -2px 2px 0 rgba(199, 199, 199, 0.55),\r\n        0 1px 1px 0 #fff,\r\n        0 2px 2px 1px #fafafa,\r\n        0 2px 4px 0 #b2b2b2 inset,\r\n        0 -1px 1px 0 #f2f2f2 inset,\r\n        0 15px 15px 0 rgba(41, 41, 41, 0.09) inset;\r\n        box-shadow:\r\n        0 -2px 2px 0 rgba(199, 199, 199, 0.55),\r\n        0 1px 1px 0 #fff,\r\n        0 2px 2px 1px #fafafa,\r\n        0 2px 4px 0 #b2b2b2 inset,\r\n        0 -1px 1px 0 #f2f2f2 inset,\r\n        0 15px 15px 0 rgba(41, 41, 41, 0.09) inset;\r\n            }\r\n    .search{\r\n        width:160px;\r\n        height:44px;\r\n        padding:10px;\r\n        min-width: 0px;\r\n        margin: 30px 30px 30px 30px;\r\n        border-radius:6px;\r\n        border:none;\r\n        color:#0F0D0D;;\r\n        font-weight:bold;\r\n        background-color:#E2EFF7;\r\n        -webkit-box-shadow:\r\n    0 -2px 2px 0 rgba(199, 199, 199, 0.55),\r\n    0 1px 1px 0 #fff,\r\n    0 2px 2px 1px #fafafa,\r\n    0 2px 4px 0 #b2b2b2 inset,\r\n    0 -1px 1px 0 #f2f2f2 inset,\r\n    0 15px 15px 0 rgba(41, 41, 41, 0.09) inset;\r\n    box-shadow:\r\n    0 -2px 2px 0 rgba(199, 199, 199, 0.55),\r\n    0 1px 1px 0 #fff,\r\n    0 2px 2px 1px #fafafa,\r\n    0 2px 4px 0 #b2b2b2 inset,\r\n    0 -1px 1px 0 #f2f2f2 inset,\r\n    0 15px 15px 0 rgba(41, 41, 41, 0.09) inset;\r\n        }\r\n    .submit_1{\r\n            width:35px;\r\n            height:35px;\r\n            min-width: 15px;\r\n            border:none;\r\n            cursor:pointer;\r\n            background-color: #609ea0\r\n            }\r\n    .search_2{\r\n                background-color: #609ea0;\r\n                width:120px;\r\n    margin: 50px 30px;\r\n    -webkit-box-sizing:border-box;\r\n    box-sizing:border-box;\r\n    border-radius:6px;\r\n    font-style: Bold;\r\n    font-size: 15px;\r\n\r\n            }\r\n    .dateselect{\r\n                width:150px;\r\n                height:45px;\r\n                padding:5px;\r\n                border-radius:6px;\r\n                margin: 39px -40px 30px 30px;\r\n                text-align: justify;\r\n                border:none;\r\n                color:#0F0D0D;;\r\n                font-weight:bold;\r\n                background-color:#E2EFF7;\r\n                -webkit-box-shadow:\r\n            0 -2px 2px 0 rgba(199, 199, 199, 0.55),\r\n            0 1px 1px 0 #fff,\r\n            0 2px 2px 1px #fafafa,\r\n            0 2px 4px 0 #b2b2b2 inset,\r\n            0 -1px 1px 0 #f2f2f2 inset,\r\n            0 15px 15px 0 rgba(41, 41, 41, 0.09) inset;\r\n            box-shadow:\r\n            0 -2px 2px 0 rgba(199, 199, 199, 0.55),\r\n            0 1px 1px 0 #fff,\r\n            0 2px 2px 1px #fafafa,\r\n            0 2px 4px 0 #b2b2b2 inset,\r\n            0 -1px 1px 0 #f2f2f2 inset,\r\n            0 15px 15px 0 rgba(41, 41, 41, 0.09) inset;\r\n                }\r\n    .check{\r\n                    width: 100px\r\n\r\n                }\r\n    .showall{\r\n                    width:150px;\r\n                    height:45px;\r\n                    padding:5px;\r\n                    border-radius:6px;\r\n                    margin: 30px 30px 30px 30px;\r\n                    border:none;\r\n                    color:#0F0D0D;;\r\n                    font-weight:bold;\r\n                    background-color:#E2EFF7;\r\n                    -webkit-box-shadow:\r\n                0 -2px 2px 0 rgba(199, 199, 199, 0.55),\r\n                0 1px 1px 0 #fff,\r\n                0 2px 2px 1px #fafafa,\r\n                0 2px 4px 0 #b2b2b2 inset,\r\n                0 -1px 1px 0 #f2f2f2 inset,\r\n                0 15px 15px 0 rgba(41, 41, 41, 0.09) inset;\r\n                box-shadow:\r\n                0 -2px 2px 0 rgba(199, 199, 199, 0.55),\r\n                0 1px 1px 0 #fff,\r\n                0 2px 2px 1px #fafafa,\r\n                0 2px 4px 0 #b2b2b2 inset,\r\n                0 -1px 1px 0 #f2f2f2 inset,\r\n                0 15px 15px 0 rgba(41, 41, 41, 0.09) inset;\r\n                    }\r\n    .SearchFont{\r\n                    color: black;\r\n                    font-weight:bolder;\r\n                    font-family: Arial, Helvetica, sans-serif;\r\n                    -ms-flex-line-pack: center;\r\n                        align-content: center;\r\n                    vertical-align: center;\r\n                }\r\n    .topBuffer{\r\n                  color: black;  \r\n                   background: cadetblue;\r\n                   border-radius:4px;                 \r\n                                         \r\n                }\r\n    .mat-card-subtitle{\r\n                    margin-top: 1px;\r\n                }\r\n    .training{                    \r\n                    border:groove;\r\n                    color:black;\r\n                    -webkit-box-flex:1;\r\n                        -ms-flex:1;\r\n                            flex:1;\r\n                    border-radius:4px;\r\n                    padding-top: 10px;\r\n                    padding-bottom: 5px;\r\n                    padding-right: 10px;\r\n                    margin-top: -4px;                                \r\n                      }\r\n    .mat-card-title{\r\n                          font-size: 17px;\r\n                          font-weight: bold;\r\n                          margin-bottom: 35px;\r\n                         \r\n                      }\r\n    .cardclass{\r\n                    text-align:center;\r\n                    font-weight: bolder;\r\n                    font-size: 50;\r\n                    font-family: Arial, Helvetica, sans-serif;\r\n                         margin-left: -9px; \r\n                         margin-top: 8px;            \r\n                         color:rgb(6, 58, 58); \r\n                    width: 175px;\r\n                    \r\n                }\r\n    .subtitle{\r\n                    color:rgb(6, 58, 58);\r\n                    font-weight: 700;\r\n                    margin-top: 5px;\r\n                    margin-left: -45px;\r\n                    padding-left: -5px;\r\n                }\r\n    .example-header-image {\r\n                    background-image: url('https://www.accenture.com/us-en/_acnmedia/Accenture/next-gen-4/oracle-technology-vision-2017/img/Accenture-Oracle-Tech-Vision-2017-Lockup.png');\r\n                    background-size: cover;\r\n                    background-position: left;\r\n                    margin-left: 25px;\r\n                    padding-left: 50px;\r\n                  }\r\n    .buttoncls{\r\n                    -ms-flex-line-pack: right;\r\n                        align-content: right;\r\n                    font-weight: bolder;\r\n                    color: rgb(6, 58, 58);\r\n                    text-decoration: underline;\r\n                    \r\n                    \r\n                    \r\n                    \r\n                }\r\n    .my-icon-button {\r\n                    padding: 7px !important;\r\n                    width: 100px;\r\n                    height: 50px;\r\n                    line-height: normal;\r\n                  }\r\n    .mtselect{\r\n                    color:#0F0D0D;\r\n                    border-radius:4px;\r\n                    border:none;\r\n                    color:#0F0D0D;;\r\n                    font-weight:bold;\r\n                    background-color:#E2EFF7;\r\n                  }\r\n    [hidden] { display: none !important;}"

/***/ }),

/***/ "./src/app/show-people/show-people.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"col-md-12 wrapper\">\r\n  <form [formGroup]=\"PeopleSearchForm\"> \r\n        <input class=\"dateselect\"   matInput [matDatepicker]=\"pickerTo\"  [matDatepickerFilter]=\"myFilter\" placeholder=\"Select Date\" (ngModelChange)=\"onDateChange($event)\" required formControlName=\"searchField\">\r\n        <mat-datepicker  #pickerTo></mat-datepicker>\r\n        <mat-datepicker-toggle matSuffix [for]=\"pickerTo\">\r\n        </mat-datepicker-toggle>\r\n      <mat-select class=\"search\"  placeholder=\"Select Mode\" (ngModelChange)=\"onModeChange($event)\"  formControlName=\"modeSearch\">\r\n        <mat-option class=\"mtselect\"  value=\"Lync\">Lync</mat-option>\r\n        <mat-option  class=\"mtselect\" value=\"ClassRoom\">ClassRoom</mat-option>\r\n        <mat-option class=\"mtselect\"  value=\"External\">External</mat-option>          \r\n      </mat-select>\r\n      <mat-select class=\"search\" placeholder=\"TrainingSkillType\" (ngModelChange)=\"onTypeChange($event)\" formControlName=\"SkillTypeSearch\" >        \r\n         <mat-option class=\"mtselect\" value=\"NewIT\">New IT</mat-option>\r\n        <mat-option  class=\"mtselect\" value=\"Domain\">Domain</mat-option>\r\n        <mat-option class=\"mtselect\" value=\"Technical\">Technical</mat-option>\r\n        <mat-option class=\"mtselect\" value=\"SoftSkill\">SoftSkill</mat-option>\r\n      </mat-select>\r\n     <button class=\"showall SearchFont\" (click)=\"onSelectAll($event)\" mat-button>Showall</button> \r\n  </form>\r\n</div>\r\n\r\n\r\n<div *ngFor=\"let training of trainingList; let i = index\" class=\"col-md-4\" style=\"margin-bottom:5px\">\r\n  <mat-card class=\"topBuffer\">\r\n      <div class=\"training\">\r\n      <mat-card-header>\r\n       <div  mat-card-avatar class=\"example-header-image\">  </div>\r\n       <mat-card-title class=\"cardclass\">{{training.TrainingName}} </mat-card-title>   \r\n       <mat-card-subtitle class=\"subtitle\">\r\n          Mode:  {{training.TrainingMode}}\r\n       </mat-card-subtitle>\r\n       <mat-card-subtitle class=\"subtitle\">\r\n          Training Type: {{training.TrainingSkillType}}\r\n        </mat-card-subtitle>\r\n        <mat-card-subtitle class=\"subtitle\">\r\n          Scheduled on:{{training.TrainingStartDate}} </mat-card-subtitle>\r\n        </mat-card-header>\r\n  <mat-card-actions>\r\n      <button class=\"buttoncls\" style=\"float:right\" [disabled]=\"checkEnrolled(i)\" (click)=\"onRegisterClick(i)\" mat-button>\r\n          Register\r\n      </button>\r\n      <button class=\"buttoncls\"  style=\"float:right\"  (click)=\"onTrainingClick(i)\"  mat-button>\r\n        ViewDetails\r\n      </button>\r\n  </mat-card-actions>\r\n  </div>\r\n  </mat-card>\r\n</div>\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/show-people/show-people.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShowPeopleComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__progress_dialog_progress_dialog_component__ = __webpack_require__("./src/app/progress-dialog/progress-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__utils_checkUserStatus__ = __webpack_require__("./src/app/utils/checkUserStatus.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__showPeopleService__ = __webpack_require__("./src/app/show-people/showPeopleService.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__training_details_training_details_component__ = __webpack_require__("./src/app/training-details/training-details.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ShowPeopleComponent = (function () {
    function ShowPeopleComponent(datepipe, _formBuilder, userinfo, dialog, matDialog, httpConfig, showPeopleService) {
        this.datepipe = datepipe;
        this._formBuilder = _formBuilder;
        this.userinfo = userinfo;
        this.dialog = dialog;
        this.matDialog = matDialog;
        this.httpConfig = httpConfig;
        this.showPeopleService = showPeopleService;
        this.isAdmin = false;
        this.trngMode = '';
        this.trngskill = '';
        this.trngdate = '';
        this.trainingList = [];
        this.RegiteredList = [];
        this.userCnt = [];
        this.myFilter = function (d) {
            var day = d.getDay();
            var year = d.getFullYear();
            var currentDate = new Date();
            var currentyear = currentDate.getFullYear();
            // Prevent Saturday and Sunday from being selected.
            return day !== 0 && day !== 6 && year > currentyear - 2 && year < currentyear + 2;
        };
        this.userid = JSON.parse(this.userinfo.getUserDetails())[0].id;
        this.getContributionList();
        this.getRegisteredTraining();
        this.getUserInfo();
        this.getUsercount();
    }
    ShowPeopleComponent.prototype.ngOnInit = function () {
        this.PeopleSearchForm = this._formBuilder.group({
            searchField: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */](),
            modeSearch: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */](),
            SkillTypeSearch: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormControl */]()
        });
    };
    ShowPeopleComponent.prototype.getUserInfo = function () {
        this.user = JSON.parse(this.userinfo.getUserDetails())[0].username;
        this.userid = JSON.parse(this.userinfo.getUserDetails())[0].userid;
        this.isAdmin = this.userinfo.isUserAdmin();
    };
    ShowPeopleComponent.prototype.getContributionList = function () {
        var _this = this;
        // this.showDialog();
        this.dialogRef = this.matDialog.open(__WEBPACK_IMPORTED_MODULE_4__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: 1 } });
        var getalltrainingList = this.httpConfig.getAllTrainings();
        console.log(getalltrainingList);
        this.showPeopleService.getAlltrainings(getalltrainingList).subscribe(function (result) {
            _this.hideDialog();
            if (result.status) {
                console.log(result);
                _this.trainingList = result.data;
                console.log(_this.trainingList);
            }
        }, function (error) {
            console.log(error);
            _this.hideDialog();
        });
    };
    ShowPeopleComponent.prototype.getList = function () {
        var _this = this;
        this.dialogRef = this.matDialog.open(__WEBPACK_IMPORTED_MODULE_4__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: 1 } });
        var getalltrainingList = this.httpConfig.getAllTrainings();
        console.log(getalltrainingList);
        this.showPeopleService.getAlltrainings(getalltrainingList).subscribe(function (result) {
            _this.hideDialog();
            if (result.status) {
                console.log(result);
                _this.Filterdata = result.data;
                //console.log(this.trainingList);
                return "this.Filterdata";
            }
        }, function (error) {
            console.log(error);
            _this.hideDialog();
        });
    };
    ShowPeopleComponent.prototype.onTrainingClick = function (i) {
        var _this = this;
        this.showDialog();
        var TrainingDetail = this.httpConfig.getTrainingById();
        this.showPeopleService.getPeopleDetail(TrainingDetail, { id: this.trainingList[i].TrainingId }).subscribe(function (result) {
            _this.hideDialog();
            if (result.status) {
                console.log("Test");
                console.log(result.data[0]);
                _this.ShowPeopleDialogRef = _this.matDialog.open(__WEBPACK_IMPORTED_MODULE_8__training_details_training_details_component__["a" /* TrainingDetailsComponent */], { width: '500px', height: '570px',
                    data: result.data[0]
                });
            }
        }, function (error) {
            _this.hideDialog();
            console.log(error);
        });
    };
    ShowPeopleComponent.prototype.showDialog = function () {
        this.dialogRef = this.matDialog.open(__WEBPACK_IMPORTED_MODULE_4__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: 1 } });
    };
    ShowPeopleComponent.prototype.hideDialog = function () {
        this.dialogRef.close();
    };
    ShowPeopleComponent.prototype.onModeChange = function (event) {
        var _this = this;
        if (event !== -1) {
            if (this.trngskill === '' && this.trngdate === '') {
                var getalltrainingList = this.httpConfig.getAllTrainings();
                this.showPeopleService.getAlltrainings(getalltrainingList).subscribe(function (result) {
                    _this.hideDialog();
                    if (result.status) {
                        _this.trainingList = result.data;
                        _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingMode === event; });
                    }
                }, function (error) {
                    console.log(error);
                    _this.hideDialog();
                });
            }
            else {
                if (this.trngskill === '' && this.trngdate !== '') {
                    var getalltrainingList = this.httpConfig.getAllTrainings();
                    this.showPeopleService.getAlltrainings(getalltrainingList).subscribe(function (result) {
                        if (result.status) {
                            _this.trainingList = result.data;
                            _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingStartDate >= _this.trngdate; });
                            _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingMode === event; });
                        }
                    }, function (error) {
                        console.log(error);
                    });
                }
                if (this.trngskill !== '' && this.trngdate == '') {
                    var getalltrainingList = this.httpConfig.getAllTrainings();
                    this.showPeopleService.getAlltrainings(getalltrainingList).subscribe(function (result) {
                        if (result.status) {
                            _this.trainingList = result.data;
                            _this.trainingList = _this.trainingList.filter((function (train) { return train.TrainingSkillType === _this.trngskill; }));
                            _this.trainingList = _this.trainingList.filter((function (train) { return train.TrainingMode === event; }));
                        }
                    }, function (error) {
                        console.log(error);
                    });
                }
                if (this.trngskill !== '' && this.trngdate !== '') {
                    var getalltrainingList = this.httpConfig.getAllTrainings();
                    this.showPeopleService.getAlltrainings(getalltrainingList).subscribe(function (result) {
                        if (result.status) {
                            console.log(result);
                            _this.trainingList = result.data;
                            console.log(event);
                            _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingStartDate > _this.trngdate; });
                            _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingSkillType = _this.trngskill; });
                            _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingMode === event; });
                        }
                    }, function (error) {
                        console.log(error);
                    });
                }
            }
            this.trngMode = event;
        }
    };
    ShowPeopleComponent.prototype.onTypeChange = function (event) {
        var _this = this;
        if (event !== -1) {
            if (this.trngMode === '' && this.trngdate === '') {
                var getalltrainingList = this.httpConfig.getAllTrainings();
                this.showPeopleService.getAlltrainings(getalltrainingList).subscribe(function (result) {
                    _this.hideDialog();
                    if (result.status) {
                        _this.trainingList = result.data;
                        _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingSkillType === event; });
                    }
                }, function (error) {
                    console.log(error);
                    _this.hideDialog();
                });
            }
            else {
                if (this.trngMode === '' && this.trngdate !== '') {
                    var getalltrainingList = this.httpConfig.getAllTrainings();
                    this.showPeopleService.getAlltrainings(getalltrainingList).subscribe(function (result) {
                        if (result.status) {
                            _this.trainingList = result.data;
                            console.log(event);
                            _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingStartDate >= _this.trngdate; });
                            _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingSkillType === event; });
                        }
                    }, function (error) {
                        console.log(error);
                    });
                }
                if (this.trngMode !== '' && this.trngdate == '') {
                    var getalltrainingList = this.httpConfig.getAllTrainings();
                    this.showPeopleService.getAlltrainings(getalltrainingList).subscribe(function (result) {
                        if (result.status) {
                            console.log(result);
                            _this.trainingList = result.data;
                            console.log(event);
                            _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingMode === _this.trngMode; });
                            _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingSkillType === event; });
                        }
                    }, function (error) {
                        console.log(error);
                    });
                }
                if (this.trngMode !== '' && this.trngdate !== '') {
                    var getalltrainingList = this.httpConfig.getAllTrainings();
                    this.showPeopleService.getAlltrainings(getalltrainingList).subscribe(function (result) {
                        if (result.status) {
                            _this.trainingList = result.data;
                            console.log(event);
                            _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingMode === _this.trngMode; });
                            _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingStartDate >= _this.trngdate; });
                            _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingSkillType == event; });
                        }
                    }, function (error) {
                        console.log(error);
                    });
                }
            }
            this.trngskill = event;
        }
    };
    ShowPeopleComponent.prototype.onSearchSubmit = function () {
        this.getContributionList();
    };
    ShowPeopleComponent.prototype.resetfrm = function () {
        this.PeopleSearchForm.controls["searchField"].setValue("");
        this.PeopleSearchForm.controls["modeSearch"].setValue("");
        this.PeopleSearchForm.controls["SkillTypeSearch"].setValue("");
    };
    ShowPeopleComponent.prototype.onSelectAll = function (event) {
        this.resetfrm();
        this.getContributionList();
    };
    ShowPeopleComponent.prototype.onDateChange = function (event) {
        var _this = this;
        event = this.datepipe.transform(event, 'yyyy-MM-dd');
        if (event !== -1) {
            if (this.trngMode === '' && this.trngskill === '') {
                var getalltrainingList = this.httpConfig.getAllTrainings();
                this.showPeopleService.getAlltrainings(getalltrainingList).subscribe(function (result) {
                    _this.hideDialog();
                    if (result.status) {
                        _this.trainingList = result.data;
                        _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingStartDate >= event; });
                    }
                }, function (error) {
                    console.log(error);
                    _this.hideDialog();
                });
            }
            else {
                if (this.trngMode === '' && this.trngskill !== '') {
                    var getalltrainingList = this.httpConfig.getAllTrainings();
                    console.log(getalltrainingList);
                    this.showPeopleService.getAlltrainings(getalltrainingList).subscribe(function (result) {
                        if (result.status) {
                            console.log(result);
                            _this.trainingList = result.data;
                            console.log(event);
                            _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingSkillType === _this.trngskill; });
                            _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingStartDate >= event; });
                        }
                    }, function (error) {
                        console.log(error);
                    });
                }
                if (this.trngMode !== '' && this.trngskill == '') {
                    var getalltrainingList = this.httpConfig.getAllTrainings();
                    this.showPeopleService.getAlltrainings(getalltrainingList).subscribe(function (result) {
                        if (result.status) {
                            console.log(result);
                            _this.trainingList = result.data;
                            console.log(event);
                            _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingMode === _this.trngMode; });
                            _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingStartDate >= event; });
                        }
                    }, function (error) {
                        console.log(error);
                    });
                }
                if (this.trngMode !== '' && this.trngskill !== '') {
                    var getalltrainingList = this.httpConfig.getAllTrainings();
                    this.showPeopleService.getAlltrainings(getalltrainingList).subscribe(function (result) {
                        if (result.status) {
                            _this.trainingList = result.data;
                            _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingMode === _this.trngMode; });
                            _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingSkillType === _this.trngskill; });
                            _this.trainingList = _this.trainingList.filter(function (train) { return train.TrainingStartDate >= event; });
                        }
                    }, function (error) {
                        console.log(error);
                    });
                }
            }
            this.trngdate = event;
        }
    };
    ShowPeopleComponent.prototype.onRegisterClick = function (i) {
        var _this = this;
        var TrainingId;
        var usrCount;
        var status;
        status = "Registered";
        var chkenroll;
        /*gets the User count registered for that Training */
        usrCount = this.userCnt.filter(function (x) { return x.TrainingId === _this.trainingList[i].TrainingId; });
        if (usrCount.length > 0) {
            usrCount = usrCount[0].usrCount;
        }
        else {
            usrCount = 0;
        }
        /*Checks whether the user is already enrolled */
        chkenroll = (this.RegiteredList.filter(function (x) { return x.TrainingId === _this.trainingList[i].TrainingId; }));
        if (chkenroll.length === 0) {
            if (usrCount < this.trainingList[i].TargetAudience) {
                console.log("you are eligible for registration");
                var userEnroll = {
                    TrainingId: this.trainingList[i].TrainingId,
                    EmployeeId: JSON.parse(this.userinfo.getUserDetails())[0].id,
                    EnrollmentStatus: "Registered"
                };
                var enrollmentUrl = this.httpConfig.TrainingEnrollment();
                console.log(userEnroll);
                this.showPeopleService.RegisterUser(enrollmentUrl, { data: userEnroll }).subscribe(function (result) {
                    if (result.status) {
                        // erase form data and show success gif
                        console.log(result.status);
                        _this.successGif();
                    }
                    else {
                        console.log(result.message);
                    }
                }, function (error) {
                    console.log('error', error);
                });
                this.getRegisteredTraining();
                this.getUsercount();
            }
            else {
                console.log("you have been in waiting list");
            }
        }
        else {
            console.log("You have been already registered for this");
        }
    };
    ShowPeopleComponent.prototype.openDIalog = function (code) {
        this.dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_4__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: code } });
    };
    ShowPeopleComponent.prototype.successGif = function () {
        var _this = this;
        // first reset form data
        this.closeDialog();
        // this.resetForm();
        this.openDIalog(2);
        setTimeout(function () {
            _this.closeDialog();
        }, 2000);
    };
    ShowPeopleComponent.prototype.resetForm = function () {
        this.myform.resetForm();
    };
    ShowPeopleComponent.prototype.closeDialog = function () {
        this.dialogRef.close();
    };
    /*get the list of trainings registered by the specific user */
    ShowPeopleComponent.prototype.getRegisteredTraining = function () {
        var _this = this;
        var id;
        this.userid = JSON.parse(this.userinfo.getUserDetails())[0].id;
        id = this.userid;
        var getRegisteredTrainingList = this.httpConfig.getRegisteredTraining();
        this.showPeopleService.getRegisteredTraining(getRegisteredTrainingList, { id: this.userid }).subscribe(function (result) {
            if (result.status) {
                _this.RegiteredList = result.data;
            }
        }, function (error) {
            console.log(error);
            _this.hideDialog();
        });
    };
    /*check and set whether the user is already enrolled*/
    ShowPeopleComponent.prototype.checkEnrolled = function (i) {
        var _this = this;
        var chckEnroll;
        chckEnroll = (this.RegiteredList.filter(function (x) { return x.TrainingId === _this.trainingList[i].TrainingId; }));
        if (chckEnroll.length === 0) {
            return false;
        }
        else {
            return true;
        }
    };
    /*get the  enrolled usercount of all trainings*/
    ShowPeopleComponent.prototype.getUsercount = function () {
        var _this = this;
        var userCountUrl = this.httpConfig.getEnrolledUserCount();
        this.showPeopleService.getEnrolledUserCount(userCountUrl).subscribe(function (result) {
            if (result.status) {
                _this.userCnt = result.data;
            }
            else {
                console.log(result.message);
            }
        }, function (error) {
            console.log('error', error);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_3__angular_forms__["e" /* FormGroupDirective */]),
        __metadata("design:type", Object)
    ], ShowPeopleComponent.prototype, "myform", void 0);
    ShowPeopleComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-show-people',
            template: __webpack_require__("./src/app/show-people/show-people.component.html"),
            styles: [__webpack_require__("./src/app/show-people/show-people.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common__["DatePipe"], __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_6__utils_checkUserStatus__["a" /* CheckUserStatus */], __WEBPACK_IMPORTED_MODULE_2__angular_material__["f" /* MatDialog */], __WEBPACK_IMPORTED_MODULE_2__angular_material__["f" /* MatDialog */], __WEBPACK_IMPORTED_MODULE_5__utils_httpConfig__["a" /* HttpCOnfig */], __WEBPACK_IMPORTED_MODULE_7__showPeopleService__["a" /* ShowPeopleService */]])
    ], ShowPeopleComponent);
    return ShowPeopleComponent;
}());



/***/ }),

/***/ "./src/app/show-people/showPeopleService.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShowPeopleService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
Fetching Automation Details through Http Calls
*/
var ShowPeopleService = (function () {
    function ShowPeopleService(httpClient) {
        this.httpClient = httpClient;
    }
    ShowPeopleService.prototype.getAlltrainings = function (URL) {
        return this.httpClient.get(URL, {});
    };
    ShowPeopleService.prototype.getPeopleDetail = function (URL, data) {
        console.log(URL);
        console.log(data);
        return this.httpClient.post(URL, data);
    };
    ShowPeopleService.prototype.RegisterUser = function (URL, data) {
        return this.httpClient.post(URL, data);
    };
    ShowPeopleService.prototype.getRegisteredTraining = function (URL, data) {
        console.log(URL);
        console.log("data");
        console.log(data);
        console.log(this.httpClient.post(URL, data));
        return this.httpClient.post(URL, data);
    };
    ShowPeopleService.prototype.getEnrolledUserCount = function (URL) {
        console.log(URL);
        return this.httpClient.get(URL);
    };
    ShowPeopleService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], ShowPeopleService);
    return ShowPeopleService;
}());



/***/ }),

/***/ "./src/app/side-nav/side-nav.component.css":
/***/ (function(module, exports) {

module.exports = ".navChild {\r\n  margin-bottom: 5px;\r\n}\r\n\r\n.sidenav-link{\r\n  color: white;\r\n}\r\n\r\n.mat-icon.mat-list-icon.material-icons{\r\n  margin-bottom: 10px;\r\n}\r\n\r\n.active-route{\r\n  color: teal;\r\n  font-weight: bold;\r\n}"

/***/ }),

/***/ "./src/app/side-nav/side-nav.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-nav-list>\r\n  <a mat-list-item class=\"sidenav-link\" [routerLink]=\"['/addIdeas']\" [routerLinkActive]=\"['active-route']\">\r\n    <mat-icon mat-list-icon>compare_arrows</mat-icon>\r\n    <span class=\"title\" mat-line>Add Ideas</span>\r\n  </a>\r\n  <a mat-list-item class=\"sidenav-link\" [routerLink]=\"['/trending']\" [routerLinkActive]=\"['active-route']\">\r\n    <mat-icon mat-list-icon>trending_up</mat-icon>\r\n    <span class=\"title\" mat-line>Trending</span>\r\n  </a>\r\n</mat-nav-list>\r\n"

/***/ }),

/***/ "./src/app/side-nav/side-nav.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SideNavComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SideNavComponent = (function () {
    function SideNavComponent() {
        this.tempArray = [1, 2, 3, 4];
    }
    SideNavComponent.prototype.ngOnInit = function () {
    };
    SideNavComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-side-nav',
            template: __webpack_require__("./src/app/side-nav/side-nav.component.html"),
            styles: [__webpack_require__("./src/app/side-nav/side-nav.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SideNavComponent);
    return SideNavComponent;
}());



/***/ }),

/***/ "./src/app/toolbar/toolbar.component.css":
/***/ (function(module, exports) {

module.exports = "body {\r\n    margin: 0;\r\n    font-family: Roboto, sans-serif;\r\n  }\r\n  \r\n  .toolBar{\r\n    background: white;\r\n    margin-bottom: 2px;\r\n  }\r\n  \r\n  mat-toolbar-row {\r\n    -webkit-box-pack: justify;\r\n        -ms-flex-pack: justify;\r\n            justify-content: space-between;\r\n  }\r\n  \r\n  .example-spacer {\r\n    -webkit-box-flex: 1;\r\n        -ms-flex: 1 1 auto;\r\n            flex: 1 1 auto;\r\n  }\r\n  \r\n  .toolbar-menu{\r\n    color: black;\r\n  }\r\n  \r\n  .logo{\r\n    height: 26px;\r\n    margin: 0 4px 3px 15px;\r\n    vertical-align: middle;\r\n  }\r\n  \r\n  .title{\r\n    vertical-align: middle;\r\n    margin: 0 4px 3px 10px;\r\n    color: black;\r\n  }\r\n  \r\n"

/***/ }),

/***/ "./src/app/toolbar/toolbar.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\r\n  <mat-toolbar color=\"mat-elevation-z*\" class=\"toolBar\">\r\n    <mat-toolbar-row>\r\n      <img  class=\"logo\" src=\"https://media.cdn.gradconnection.com/uploads/01dd7e6c-9977-4abf-b57c-fab080cf7518-image.png\" alt=\"logo\">\r\n      <span class=\"title\"><span>Accenture</span></span>\r\n      <span class=\"example-spacer\"></span>\r\n      <button class=\"toolbar-menu\" mat-icon-button [mat-menu-trigger-for]=\"menu\">\r\n        <mat-icon>more_vert</mat-icon>\r\n      </button>\r\n    </mat-toolbar-row>\r\n    <mat-menu x-position=\"before\" #menu=\"matMenu\">\r\n      <button mat-menu-item>Option 1</button>\r\n      <button mat-menu-item>Option 2</button>\r\n    </mat-menu>\r\n  </mat-toolbar>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/toolbar/toolbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToolbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ToolbarComponent = (function () {
    function ToolbarComponent() {
    }
    ToolbarComponent.prototype.ngOnInit = function () {
    };
    ToolbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-toolbar',
            template: __webpack_require__("./src/app/toolbar/toolbar.component.html"),
            styles: [__webpack_require__("./src/app/toolbar/toolbar.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ToolbarComponent);
    return ToolbarComponent;
}());



/***/ }),

/***/ "./src/app/training-details/training-details.component.css":
/***/ (function(module, exports) {

module.exports = ".formWrapper{\r\n    margin: 150px 0px 0px 0px;\r\n    font-size: 16px;\r\n  \r\n}\r\n\r\n.example-full-width {\r\n    width: 100%;\r\n  }\r\n\r\n.date_wrap {\r\n    width: 49%;\r\n  }\r\n\r\n.formcls{\r\n     width: 400px;\r\n     height: 300px;\r\n }\r\n\r\n.submit{\r\n      margin-top: 10px;\r\n  }\r\n\r\n.mtsize{\r\n      width: 400;\r\n      height: 300;\r\n  }"

/***/ }),

/***/ "./src/app/training-details/training-details.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-6 mtsize \">\r\n\r\n          <form class=\"formcls\" [formGroup]=\"PeopleContributionForm\" (ngSubmit)=\"onupdateTrainingDetails()\">  \r\n              <mat-horizontal-stepper formArrayName=\"formArray\" linear>\r\n         <mat-step class=\"formcls\" formGroupName=\"0\" [stepControl]=\"formArray.get([0])\">\r\n            <ng-template matStepLabel>Training Info</ng-template>\r\n             <mat-form-field class=\"example-full-width\">\r\n                 <input matInput #title placeholder=\"Course Name\"  minLength=\"3\" disabled=true [value]=\"trainingData.TrainingName\" required formControlName=\"TitleControl\">\r\n                 <mat-error *ngIf=\"PeopleContributionForm.hasError('minLength')\">\r\n                  <p>Title should be minimum of 10</p>\r\n                  </mat-error>\r\n                 </mat-form-field >\r\n                 <mat-form-field class=\"example-full-width\">\r\n                 <textarea matInput #message placeholder=\"Course Description\" maxlength=\"256\" [disabled]=\"!cntrlenable\" [value]=\"trainingData.TrainingDescription\" required formControlName=\"Descriptioncntrl\"></textarea>\r\n                 <mat-hint align=\"end\">{{message.value.length}} / 256</mat-hint>\r\n                  </mat-form-field>\r\n                  <mat-form-field class=\"example-full-width\">\r\n                    <textarea matInput #prerequisite placeholder=\"Prerequisite\" maxlength=\"150\" [disabled]=\"!cntrlenable\" [value]=\"trainingData.Prerequisite\"  formControlName=\"Prerequisitecntrl\"></textarea>\r\n                    <mat-hint align=\"end\">{{message.value.length}} / 150</mat-hint>\r\n                     </mat-form-field>\r\n                  <mat-form-field class=\"example-full-width\">\r\n                    <mat-select placeholder=\"SkillType\" [disabled]=\"!cntrlenable\" [(ngModel)]=\"trainingData.TrainingSkillType\" formControlName=\"trngSkillType\">\r\n                      <mat-option value=\"CDCC2\">CDC2</mat-option>\r\n                      <mat-option value=\"NewIT\">New IT</mat-option>\r\n                     <mat-option value=\"Domain\">Domain</mat-option>\r\n                     <mat-option value=\"Technical\">Technical</mat-option>\r\n                     <mat-option value=\"SoftSkill\">SoftSkill</mat-option>\r\n                    </mat-select>\r\n                  </mat-form-field>\r\n                  \r\n                  <mat-form-field class=\"example-full-width\">\r\n                    <mat-select placeholder=\"TrainingType\" [disabled]=\"!cntrlenable\" [(ngModel)]=\"trainingData.TrainingType\" formControlName=\"trngType\">\r\n                      <mat-option value=\"LKM\">LKM</mat-option>\r\n                      <mat-option value=\"Project\">Project</mat-option>\r\n                      <mat-option value=\"External\">External</mat-option> \r\n                    </mat-select>\r\n                  </mat-form-field>\r\n\r\n                      \r\n         </mat-step> \r\n         <mat-step class=\"formcls\" formGroupName=\"1\" [stepControl]=\"formArray.get([1])\"> \r\n             <ng-template matStepLabel>Training Schedule</ng-template>\r\n             \r\n\r\n             <mat-form-field class=\"example-full-width\">\r\n               <textarea matInput #Audience placeholder=\"Target Audience\" [disabled]=\"!cntrlenable\" [value]=\"trainingData.TargetAudience\"   formControlName=\"TargetAudience\"></textarea>\r\n             </mat-form-field>\r\n             \r\n    \r\n   <mat-form-field class=\"example-full-width\">\r\n     <mat-select placeholder=\"Location\" [disabled]=\"!cntrlenable\"  [(ngModel)]=\"trainingData.Location\" formControlName=\"trngLocation\">\r\n       <mat-option value=\"CDCC2\">CDC2</mat-option>\r\n     </mat-select>\r\n   </mat-form-field>\r\n   <mat-form-field class=\"example-full-width\">\r\n    <mat-select placeholder=\"TrainingMode\" [disabled]=\"!cntrlenable\"  [(ngModel)]=\"trainingData.TrainingMode\" formControlName=\"trngMode\">\r\n      <mat-option value=\"Lync\">Lync</mat-option>\r\n         <mat-option value=\"ClassRoom\">ClassRoom</mat-option>\r\n         <mat-option value=\"External\">External</mat-option> \r\n    </mat-select>\r\n  </mat-form-field>\r\n   <mat-form-field class=\"date_wrap\">\r\n     <input matInput  [matDatepickerFilter]=\"myFilter\" [matDatepicker]=\"pickerTo\" placeholder=\"StartDate\" [disabled]=\"!cntrlenable\" [(ngModel)]=\"trainingData.TrainingStartDate\" required formControlName=\"TrainingStartdate\">\r\n     <mat-datepicker-toggle matSuffix [for]=\"pickerTo\"></mat-datepicker-toggle>\r\n     <mat-datepicker #pickerTo></mat-datepicker>\r\n     </mat-form-field> \r\n     <mat-form-field class=\"date_wrap\" >\r\n       <input matInput  [matDatepickerFilter]=\"myFilter\" [matDatepicker]=\"pickerFrom\" placeholder=\"EndDate\" [disabled]=\"!cntrlenable\" [(ngModel)]=\"trainingData.TrainingEndDate\" required formControlName=\"trainingEnddate\">\r\n     <mat-datepicker-toggle matSuffix [for]=\"pickerFrom\"></mat-datepicker-toggle>\r\n     <mat-datepicker   #pickerFrom></mat-datepicker>\r\n     </mat-form-field>\r\n                             \r\n             <mat-form-field class=\"example-full-width\">\r\n      \r\n                 <input type=\"textArea\" placeholder=\"Comments\" disabled=true [disabled]=\"!cntrlenable\"  [(ngModel)]=\"trainingData.TrainingComments\" required matInput formControlName=\"Autocomments\">\r\n                 \r\n               </mat-form-field>\r\n                               \r\n               \r\n             </mat-step> \r\n           \r\n           </mat-horizontal-stepper>\r\n\r\n         <button  *ngIf=\"isAdmin||createUser\" [disabled]=\"!PeopleContributionForm.dirty\" class=\"submit\" type=\"submit\" formcontrolName=\"submit\" [disabled]=\"!PeopleContributionForm.valid\" mat-raised-button color=\"warn\">SUBMIT</button>\r\n </form>\r\n      \r\n    \r\n    \r\n    </div>"

/***/ }),

/***/ "./src/app/training-details/training-details.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrainingDetailsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__progress_dialog_progress_dialog_component__ = __webpack_require__("./src/app/progress-dialog/progress-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__utils_checkUserStatus__ = __webpack_require__("./src/app/utils/checkUserStatus.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__utils_httpConfig__ = __webpack_require__("./src/app/utils/httpConfig.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__trainingDetailService__ = __webpack_require__("./src/app/training-details/trainingDetailService.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};







var TrainingDetailsComponent = (function () {
    function TrainingDetailsComponent(_formBuilder, userinfo, dialog, httpConfig, trainingDetailService, data) {
        this._formBuilder = _formBuilder;
        this.userinfo = userinfo;
        this.dialog = dialog;
        this.httpConfig = httpConfig;
        this.trainingDetailService = trainingDetailService;
        this.data = data;
        this.isAdmin = false;
        this.createUser = false;
        this.cntrlenable = false;
        this.getUserInfo();
        this.trainingData = data;
        console.log(data);
        this.checkuser();
        //this.statval=data.autoStatus;
        console.log("Fetchedval");
        console.log(data);
    }
    TrainingDetailsComponent.prototype.ngOnInit = function () {
        this.PeopleContributionForm = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["d" /* FormGroup */]({
            formArray: this._formBuilder.array([
                this._formBuilder.group({
                    TitleControl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.trainingData.TrainingName, disabled: true }),
                    Descriptioncntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.trainingData.TrainingDescription }),
                    Prerequisitecntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */](''),
                    trngSkillType: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                    trngType: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required)
                }),
                this._formBuilder.group({
                    trngLocation: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                    trngMode: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                    TargetAudience: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */](''),
                    TrainingStartdate: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                    trainingEnddate: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
                    Autocomments: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: '', disabled: false }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required)
                })
            ])
        });
        this.peopleFormGroup1 = this._formBuilder.group({
            TitleControl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.trainingData.TrainingName, disabled: true }),
            Descriptioncntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.trainingData.TrainingDescription }),
            trngSkillType: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
            Prerequisitecntrl: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: '', disabled: false }),
            trngType: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.trainingData.TrainingMode, disabled: !this.cntrlenable }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
        });
        this.peopleFormGroup2 = this._formBuilder.group({
            trngLocation: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: '', disabled: false }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
            TargetAudience: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: '', disabled: false }),
            trngMode: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: this.trainingData.TrainingMode, disabled: !this.cntrlenable }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
            TrainingStartdate: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: '', disabled: false }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
            trainingEnddate: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: '', disabled: false }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required),
            Autocomments: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormControl */]({ value: '', disabled: false }, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* Validators */].required)
        });
    };
    TrainingDetailsComponent.prototype.getUserInfo = function () {
        this.user = JSON.parse(this.userinfo.getUserDetails())[0].username;
    };
    Object.defineProperty(TrainingDetailsComponent.prototype, "formArray", {
        get: function () { return this.PeopleContributionForm.get('formArray'); },
        enumerable: true,
        configurable: true
    });
    TrainingDetailsComponent.prototype.checkuser = function () {
        this.isAdmin = this.userinfo.isUserAdmin();
        if (this.trainingData.Contributor == this.user) {
            this.createUser = true;
        }
        this.cntrlenable = this.isAdmin || this.createUser;
        //this.cntrlenable=true;
        console.log(this.isAdmin);
        console.log(this.createUser);
        console.log(this.user);
        console.log(this.cntrlenable);
    };
    TrainingDetailsComponent.prototype.onupdateTrainingDetails = function () {
        var _this = this;
        this.openDIalog(1);
        var frmArry = this.PeopleContributionForm.get('formArray');
        console.log(frmArry);
        var frmArryValues = frmArry.value;
        console.log(frmArryValues);
        this.dat = this.getDate(frmArryValues[1].TrainingStartdate);
        console.log(this.dat);
        //this. dat = new DatePipe(frmArryValues[1].TrainingStartdate);
        // =this. dat.transform(frmArryValues[1].TrainingStartdate, 'dd/MM/yyyy');
        var updatedTrainingData = {
            TrainingDescription: frmArryValues[0].Descriptioncntrl,
            TrainingMode: frmArryValues[1].trngMode,
            TrainingType: frmArryValues[0].trngType,
            TrainingSkillType: frmArryValues[0].trngSkillType,
            TrainingStartDate: frmArryValues[1].TrainingStartdate,
            TrainingEndDate: frmArryValues[1].trainingEnddate,
            TargetAudience: frmArryValues[1].TargetAudience,
            TrainingLocation: frmArryValues[1].trngLocation,
            TrainingPrequisite: frmArryValues[0].Prerequisitecntrl,
            TrainingComments: frmArryValues[1].Autocomments,
            id: this.trainingData.TrainingId
        };
        console.log("Test6");
        console.log(updatedTrainingData);
        var trainingsubmitUrl = this.httpConfig.updateTrainingById();
        console.log(trainingsubmitUrl);
        this.trainingDetailService.updateTraining(trainingsubmitUrl, { data: updatedTrainingData }).subscribe(function (result) {
            if (result.status) {
                console.log(result.status);
                _this.successGif();
            }
            else {
                console.log(result.message);
            }
        }, function (error) {
            console.log('error', error);
        });
    };
    TrainingDetailsComponent.prototype.openDIalog = function (code) {
        this.dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_3__progress_dialog_progress_dialog_component__["a" /* ProgressDialogComponent */], { data: { code: code } });
    };
    TrainingDetailsComponent.prototype.successGif = function () {
        var _this = this;
        // first reset form data
        this.closeDialog();
        this.openDIalog(2);
        setTimeout(function () {
            _this.closeDialog();
        }, 2000);
    };
    TrainingDetailsComponent.prototype.closeDialog = function () {
        this.dialogRef.close();
    };
    TrainingDetailsComponent.prototype.getDate = function (date) {
        if (date) {
            var dateOld = new Date(date);
            return dateOld.toLocaleDateString();
        }
    };
    TrainingDetailsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-training-details',
            template: __webpack_require__("./src/app/training-details/training-details.component.html"),
            styles: [__webpack_require__("./src/app/training-details/training-details.component.css")]
        }),
        __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_4__utils_checkUserStatus__["a" /* CheckUserStatus */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["f" /* MatDialog */], __WEBPACK_IMPORTED_MODULE_5__utils_httpConfig__["a" /* HttpCOnfig */], __WEBPACK_IMPORTED_MODULE_6__trainingDetailService__["a" /* TrainingDetailService */], Object])
    ], TrainingDetailsComponent);
    return TrainingDetailsComponent;
}());



/***/ }),

/***/ "./src/app/training-details/trainingDetailService.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrainingDetailService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
Fetching Automation Details through Http Calls
*/
var TrainingDetailService = (function () {
    function TrainingDetailService(httpClient) {
        this.httpClient = httpClient;
    }
    TrainingDetailService.prototype.updateTraining = function (URL, data) {
        console.log("Hello", URL);
        console.log("Hello", data);
        return this.httpClient.post(URL, data);
    };
    TrainingDetailService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], TrainingDetailService);
    return TrainingDetailService;
}());



/***/ }),

/***/ "./src/app/updated-tool-bar/updated-tool-bar.component.css":
/***/ (function(module, exports) {

module.exports = "body {\r\n    margin: 0;\r\n    font-family: Roboto, sans-serif;\r\n  }\r\n  \r\n  .toolBar{\r\n    background: darkslategrey;\r\n    top: 0;\r\n    position: fixed;\r\n    z-index:999;\r\n  }\r\n  \r\n  mat-toolbar-row {\r\n    -webkit-box-pack: justify;\r\n        -ms-flex-pack: justify;\r\n            justify-content: space-between;\r\n  }\r\n  \r\n  .example-spacer {\r\n    -webkit-box-flex: 1;\r\n        -ms-flex: 1 1 auto;\r\n            flex: 1 1 auto;\r\n  }\r\n  \r\n  .toolbar-menu{\r\n    color: black;\r\n  }\r\n  \r\n  .logo{\r\n    height: 26px;\r\n    margin: 0 4px 5px 5px;\r\n    vertical-align: middle;\r\n  }\r\n  \r\n  .singtel{\r\n    height: 58px;\r\n    margin: 0 4px 5px 15px;\r\n    vertical-align: middle;\r\n  }\r\n  \r\n  .title{\r\n    vertical-align: middle;\r\n    margin: 0 4px 3px 10px;\r\n    color: white;\r\n  }\r\n  \r\n  .mainText{\r\n      color: white;\r\n      position: relative;\r\n      font-weight: bold;\r\n      top: 40%\r\n  }\r\n  \r\n  .formCard{\r\n      top: 20%;\r\n  }\r\n  \r\n  .titleText{\r\n    color: aqua;\r\n    font-size: x-large;\r\n    padding: 11px;\r\n    border: 1px solid;\r\n  }\r\n  \r\n  .description{\r\n    margin-top: 30px;\r\n    font-style: italic;\r\n  }\r\n  \r\n  .toolbarButton{\r\n    color: aqua;\r\n    font-weight: bold;\r\n  }\r\n  \r\n  >>> .innovateMenu .mat-menu-content{\r\n    background: darkslategrey;\r\n    color: white !important;\r\n  }\r\n  \r\n  .menuButton{\r\n    color: aqua;\r\n  }"

/***/ }),

/***/ "./src/app/updated-tool-bar/updated-tool-bar.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-toolbar color=\"mat-elevation-z*\" class=\"toolBar\">\r\n    <mat-toolbar-row>\r\n      <a href=\"#\">\r\n          <img  class=\"singtel\" src=\"https://www.singtel.com/content/dam/singtel/online-draft/singtel_logo_coloured.png\" alt=\"singtel\">\r\n          <img  class=\"logo\" src=\"https://beta-cdn.optusdigital.com/content/dam/optus/images/about-us/media-centre/multimedia/logos/OPTUS_Teal_sRGB_RELEASE_03_310316.png\" alt=\"optus\">\r\n      </a>\r\n      <span class=\"example-spacer\"></span>\r\n      <!-- <button  class=\"toolbarButton\" [matMenuTriggerFor]=\"PeopleMenu\" mat-button>People</button> -->\r\n      <button  class=\"toolbarButton\" [matMenuTriggerFor]=\"OriginateMenu\" mat-button>Originate</button>\r\n      <button  class=\"toolbarButton\" [matMenuTriggerFor]=\"AutomateMenu\" mat-button>Automate</button>\r\n      <button  class=\"toolbarButton\"  [matMenuTriggerFor]=\"InnovateMenu\" mat-button>Innovate</button>\r\n      <button *ngIf=\"!isLoggedIn\" [routerLink]=\"['./login']\" class=\"toolbarButton\" mat-button>Login</button>\r\n      <button *ngIf=\"isLoggedIn\" (click)=\"onLoggedOut()\" class=\"toolbarButton\" mat-button>Logout</button>\r\n      <img class=\"logo\" src=\"https://www.accenture.com/us-en/_acnmedia/Accenture/next-gen-4/oracle-technology-vision-2017/img/Accenture-Oracle-Tech-Vision-2017-Lockup.png\" alt=\"Accenture Logo\">\r\n    </mat-toolbar-row>\r\n  </mat-toolbar>\r\n  <!-- this is for Innovate menu -->\r\n  <mat-menu #OriginateMenu=\"matMenu\" [overlapTrigger]=\"false\" class=\"innovateMenu\">\r\n    <a routerLink=\"/originateLogging\"><button  class=\"menuButton\" mat-menu-item>Log Opportunity</button></a>\r\n    <a routerLink=\"/originate\"><button class=\"menuButton\" mat-menu-item>Show All</button></a>\r\n    <a routerLink=\"/originateDashboard\"><button class=\"menuButton\" mat-menu-item>Dashboard</button></a>\r\n  </mat-menu>\r\n   <!-- this is for Innovate menu -->\r\n   <mat-menu #InnovateMenu=\"matMenu\" [overlapTrigger]=\"false\" class=\"innovateMenu\">\r\n      <a routerLink=\"/ideaLogging\"><button  class=\"menuButton\" mat-menu-item>Log Ideas</button></a>\r\n      <a routerLink=\"/ideas\"><button class=\"menuButton\" mat-menu-item>Show All</button></a>\r\n      <a routerLink=\"/innovationDashboard\"><button class=\"menuButton\" mat-menu-item>Dashboard</button></a>\r\n    </mat-menu>\r\n    <!-- Automation Menu-->\r\n    <mat-menu #AutomateMenu=\"matMenu\" [overlapTrigger]=\"false\" class=\"innovateMenu\">\r\n      <a routerLink=\"/addAutomation\"><button  class=\"menuButton\" mat-menu-item>Contribute</button></a>\r\n      <a routerLink=\"/showAutomation\"><button  class=\"menuButton\" mat-menu-item>Show All</button></a>\r\n      <a routerLink=\"/automationDashboard\"><button class=\"menuButton\" mat-menu-item>Dashboard</button></a>      \r\n    </mat-menu>\r\n    <!-- <mat-menu #PeopleMenu=\"matMenu\" [overlapTrigger]=\"false\" class=\"innovateMenu\">\r\n      <a routerLink=\"/addPeopleContribution\"><button  class=\"menuButton\" mat-menu-item>Contribute</button></a>\r\n      <a routerLink=\"/showPeopleContribution\"><button  class=\"menuButton\" mat-menu-item>Show All</button></a>\r\n      <a routerLink=\"/peopleDashboard\"><button class=\"menuButton\" mat-menu-item>Dashboard</button></a>      \r\n    </mat-menu> -->\r\n"

/***/ }),

/***/ "./src/app/updated-tool-bar/updated-tool-bar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdatedToolBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__dashboard_dialog_dashboard_dialog_component__ = __webpack_require__("./src/app/dashboard-dialog/dashboard-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils_checkUserStatus__ = __webpack_require__("./src/app/utils/checkUserStatus.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UpdatedToolBarComponent = (function () {
    function UpdatedToolBarComponent(checkUserStatus, router, dialog) {
        this.checkUserStatus = checkUserStatus;
        this.router = router;
        this.dialog = dialog;
        // loggin in status 
        this.isLoggedIn = false;
        // subscribe to logging status 
        this.subScibeToLogin();
    }
    UpdatedToolBarComponent.prototype.ngOnInit = function () {
    };
    // subscribe to logging status 
    UpdatedToolBarComponent.prototype.subScibeToLogin = function () {
        var _this = this;
        this.checkUserStatus.getUserLoggedStatus().subscribe(function (status) {
            _this.isLoggedIn = status;
        });
        // check if user active or not 
        this.checkUserStatus.setUserLoggedStatus(this.checkUserStatus.isUserLoggedIn());
    };
    // whether user clicks on logout button 
    UpdatedToolBarComponent.prototype.onLoggedOut = function () {
        // clear user data 
        this.checkUserStatus.clearUserInfo();
        this.checkUserStatus.setUserLoggedStatus(false);
        this.router.navigateByUrl('/login');
    };
    // open dashboard dialog 
    UpdatedToolBarComponent.prototype.showDashboard = function () {
        var dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_0__dashboard_dialog_dashboard_dialog_component__["a" /* DashboardDialogComponent */], {
            panelClass: 'graphs-dialog',
            data: { name: 'name', animal: 'this.animal' }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log('The dialog was closed');
        });
    };
    UpdatedToolBarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'app-updated-tool-bar',
            template: __webpack_require__("./src/app/updated-tool-bar/updated-tool-bar.component.html"),
            styles: [__webpack_require__("./src/app/updated-tool-bar/updated-tool-bar.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__utils_checkUserStatus__["a" /* CheckUserStatus */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["f" /* MatDialog */]])
    ], UpdatedToolBarComponent);
    return UpdatedToolBarComponent;
}());



/***/ }),

/***/ "./src/app/utils/checkUserStatus.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckUserStatus; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_Subject__ = __webpack_require__("./node_modules/rxjs/_esm5/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
this will check user status such as
1. whether he is admin or not
2. whether user logged in or not
*/
var CheckUserStatus = (function () {
    function CheckUserStatus() {
        this.isLoggedIn = "isLoggedIn";
        this.isAdmin = "isAdmin";
        this.userDetails = "userDetails";
        this.isLoggedBoolean = false;
        // creating a emitter to show/hide login/logout button 
        this.isLoggedInEmitter = new __WEBPACK_IMPORTED_MODULE_0_rxjs_Subject__["a" /* Subject */]();
    }
    ;
    // checking user logged in or not 
    CheckUserStatus.prototype.isUserLoggedIn = function () {
        return sessionStorage.getItem(this.isLoggedIn) != null;
    };
    // checking whther user admin or not here 1 is for admin
    CheckUserStatus.prototype.isUserAdmin = function () {
        if (sessionStorage.getItem(this.userDetails) == null) {
            return false;
        }
        return JSON.parse(sessionStorage.getItem(this.userDetails))[0].user_group_id_fk == 1;
    };
    // saving login information 
    CheckUserStatus.prototype.saveLoginInformation = function (userDetails) {
        sessionStorage.setItem(this.isLoggedIn, "true");
        sessionStorage.setItem(this.userDetails, userDetails);
    };
    // getting user details 
    CheckUserStatus.prototype.getUserDetails = function () {
        return sessionStorage.getItem(this.userDetails);
    };
    // remove localStorage
    CheckUserStatus.prototype.clearUserInfo = function () {
        sessionStorage.clear();
    };
    // check whether user logged in or not, remember this is an emitter 
    CheckUserStatus.prototype.getUserLoggedStatus = function () {
        return this.isLoggedInEmitter.asObservable();
    };
    // save user logged status 
    CheckUserStatus.prototype.setUserLoggedStatus = function (status) {
        this.isLoggedInEmitter.next(status);
    };
    CheckUserStatus = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], CheckUserStatus);
    return CheckUserStatus;
}());



/***/ }),

/***/ "./src/app/utils/excelService.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExcelService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_file_saver__ = __webpack_require__("./node_modules/file-saver/FileSaver.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_file_saver___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_file_saver__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_xlsx__ = __webpack_require__("./node_modules/xlsx/xlsx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_xlsx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_xlsx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
var EXCEL_EXTENSION = '.xlsx';
var ExcelService = (function () {
    function ExcelService() {
    }
    // gen data 
    ExcelService.prototype.exportAsExcelFile = function (json, excelFileName) {
        var worksheet = __WEBPACK_IMPORTED_MODULE_2_xlsx__["utils"].json_to_sheet(json);
        var workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        var excelBuffer = __WEBPACK_IMPORTED_MODULE_2_xlsx__["write"](workbook, { bookType: 'xlsx', type: 'buffer' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    };
    /* saving excel with data */
    ExcelService.prototype.saveAsExcelFile = function (buffer, fileName) {
        var data = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        __WEBPACK_IMPORTED_MODULE_1_file_saver__["saveAs"](data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    };
    ExcelService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], ExcelService);
    return ExcelService;
}());



/***/ }),

/***/ "./src/app/utils/httpConfig.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpCOnfig; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// will be storing all the http URLs here 
var HttpCOnfig = (function () {
    function HttpCOnfig() {
        this.BASE_URL = "http://localhost:3000/";
    }
    // get URL for inserting ideas 
    HttpCOnfig.prototype.getIdeasInsertURL = function () {
        return this.BASE_URL + "ideas/insert";
    };
    // get all the ideas 
    HttpCOnfig.prototype.getIdeas = function () {
        return this.BASE_URL + "ideas/getIdeas";
    };
    // get admin login URL 
    HttpCOnfig.prototype.getAdminURL = function () {
        return this.BASE_URL + "auth/adminLogin";
    };
    // get non-admin uers login URL 
    HttpCOnfig.prototype.getUserLoginURL = function () {
        return this.BASE_URL + "auth/userLogin";
    };
    // update like count 
    HttpCOnfig.prototype.getUpdateLikeURL = function () {
        return this.BASE_URL + "ideas/updateLike";
    };
    // get likes count URL 
    HttpCOnfig.prototype.getLikesCount = function () {
        return this.BASE_URL + "ideas/getLikesCount";
    };
    // get approval history URL 
    HttpCOnfig.prototype.getApprovalHistotyURL = function () {
        return this.BASE_URL + "ideas/getApprovalHistory";
    };
    // update approval data 
    HttpCOnfig.prototype.updateApproveStatus = function () {
        return this.BASE_URL + "ideas/updateApproval";
    };
    // get feasibility history 
    HttpCOnfig.prototype.getFeasiBulityHiURL = function () {
        return this.BASE_URL + "ideas/getFeasibilityHistory";
    };
    // update feas data 
    HttpCOnfig.prototype.getFeasDataURL = function () {
        return this.BASE_URL + "ideas/updateFeasibilityStatus";
    };
    // getPhase data URL
    HttpCOnfig.prototype.getPhasesDataURL = function () {
        return this.BASE_URL + "ideas/getOverdueStatus";
    };
    // getting team data URL 
    HttpCOnfig.prototype.getTeamDataURL = function () {
        return this.BASE_URL + "ideas/getTeamData";
    };
    // adding team data on server 
    HttpCOnfig.prototype.addTeamDataURL = function () {
        return this.BASE_URL + "ideas/insertTeamData";
    };
    // get build data URL 
    HttpCOnfig.prototype.getBuilDataURL = function () {
        return this.BASE_URL + "ideas/getBuildData";
    };
    // isnert build data
    HttpCOnfig.prototype.isnertBuildDataURL = function () {
        return this.BASE_URL + "ideas/insertBuildData";
    };
    // get production data URL
    HttpCOnfig.prototype.getProductionDataURL = function () {
        return this.BASE_URL + "ideas/getProductionData";
    };
    HttpCOnfig.prototype.insertProductionData = function () {
        return this.BASE_URL + "ideas/insertProductionData";
    };
    HttpCOnfig.prototype.getAutomationDetails = function () {
        return this.BASE_URL + "automation/getDetails";
    };
    HttpCOnfig.prototype.getAutomationDetail = function () {
        return this.BASE_URL + "automation/getEditAutomation";
    };
    HttpCOnfig.prototype.getautomationEntity = function () {
        return this.BASE_URL + "automation/getEntity";
    };
    HttpCOnfig.prototype.getAutomationTower = function () {
        return this.BASE_URL + "automation/getTowers";
    };
    HttpCOnfig.prototype.getAutomationStatus = function () {
        return this.BASE_URL + "automation/getStatus";
    };
    HttpCOnfig.prototype.updateAutomation = function () {
        return this.BASE_URL + "automation/updateAutomationDetails";
    };
    HttpCOnfig.prototype.getAutomationAppNames = function () {
        return this.BASE_URL + "automation/getApplications";
    };
    HttpCOnfig.prototype.saveAutomationDetails = function () {
        return this.BASE_URL + "automation/insertAutomation";
    };
    HttpCOnfig.prototype.searchIdeaDetails = function () {
        return this.BASE_URL + "ideas/searchIdeaDetails";
    };
    HttpCOnfig.prototype.getAllAutomationDetails = function () {
        return this.BASE_URL + "automation/getAllAutomation";
    };
    HttpCOnfig.prototype.getSearchAutomation = function () {
        return this.BASE_URL + "automation/getSearchAutomation";
    };
    HttpCOnfig.prototype.getIdeasCountByTower = function () {
        return this.BASE_URL + "ideas/getTowerIdeacount";
    };
    HttpCOnfig.prototype.getIdeasCountByTech = function () {
        return this.BASE_URL + "ideas/getTechIdeacount";
    };
    HttpCOnfig.prototype.getIdeasCountByQuarter = function () {
        return this.BASE_URL + "ideas/getQuarterIdeacount";
    };
    HttpCOnfig.prototype.getIdeasCountByStatus = function () {
        return this.BASE_URL + "ideas/getStatusIdeacount";
    };
    HttpCOnfig.prototype.getBusinessBenefitData = function () {
        return this.BASE_URL + "ideas/getBenefitsdata";
    };
    HttpCOnfig.prototype.saveBusinessBenefitsData = function () {
        return this.BASE_URL + "ideas/saveBenefitsdata";
    };
    //getquarterdata
    HttpCOnfig.prototype.getQuarterData = function () {
        return this.BASE_URL + "ideas/getQuarter";
    };
    HttpCOnfig.prototype.getquarthours = function () {
        return this.BASE_URL + "ideas/gethours";
    };
    HttpCOnfig.prototype.savePeopleContributionDetails = function () {
        return this.BASE_URL + "peopleContribution/insertPeopleContribution";
    };
    HttpCOnfig.prototype.TrainingEnrollment = function () {
        return this.BASE_URL + "peopleContribution/insertPeopleTrainingEnrollment";
    };
    HttpCOnfig.prototype.getAllTrainings = function () {
        return this.BASE_URL + "peopleContribution/getAllTrainings";
    };
    HttpCOnfig.prototype.getTrainingsUserCount = function () {
        return this.BASE_URL + "peopleContribution/userEnrolledcountbyType";
    };
    HttpCOnfig.prototype.getRegisteredTraining = function () {
        return this.BASE_URL + "peopleContribution/getregisteredTraining";
    };
    HttpCOnfig.prototype.getTrainingById = function () {
        return this.BASE_URL + "peopleContribution/showTrainingById";
    };
    HttpCOnfig.prototype.updateTrainingById = function () {
        return this.BASE_URL + "peopleContribution/updateTrainingDetails";
    };
    HttpCOnfig.prototype.getEnrolledUserCount = function () {
        return this.BASE_URL + "peopleContribution/getTrainingCount";
    };
    HttpCOnfig.prototype.getEnrolledUserCountByMonth = function () {
        return this.BASE_URL + "peopleContribution/getEnrolledUserCntbyMonth";
    };
    HttpCOnfig.prototype.getOriginateEntity = function () {
        return this.BASE_URL + "originate/getOriginateEntity";
    };
    HttpCOnfig.prototype.getOriginateDetails = function () {
        return this.BASE_URL + "originate/getOriginateDetails";
    };
    HttpCOnfig.prototype.saveOriginateDetails = function () {
        return this.BASE_URL + "originate/saveOriginateDetails";
    };
    HttpCOnfig.prototype.getSearchOriginate = function () {
        return this.BASE_URL + "originate/getSearchOriginate";
    };
    HttpCOnfig.prototype.updateoriginate = function () {
        return this.BASE_URL + "originate/editOriginateDetails";
    };
    HttpCOnfig.prototype.ViewAutomationCommentsHistor = function () {
        return this.BASE_URL + "automation/getCommentsHistory";
    };
    HttpCOnfig.prototype.commentsOriginate = function () {
        return this.BASE_URL + "originate/getCommentsOriginate";
    };
    HttpCOnfig = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], HttpCOnfig);
    return HttpCOnfig;
}());



/***/ }),

/***/ "./src/app/utils/navServices.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__("./node_modules/rxjs/_esm5/Subject.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


// this class helps us in hide/show navgation 
var NavService = (function () {
    function NavService() {
        this.navStatus = false;
        this.subject = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["a" /* Subject */]();
    }
    // this is for showing nav status 
    NavService.prototype.showNavStatus = function () {
        this.navStatus = true;
        this.subject.next(this.navStatus);
    };
    // this is for hiding nav status 
    NavService.prototype.hideNavStatsu = function () {
        this.navStatus = false;
        this.subject.next(this.navStatus);
    };
    // getting nav status 
    NavService.prototype.getNavStatus = function () {
        return this.subject;
    };
    NavService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
    ], NavService);
    return NavService;
}());



/***/ }),

/***/ "./src/app/welcome-component/welcome-component.component.css":
/***/ (function(module, exports) {

module.exports = "body {\r\n    margin: 0;\r\n    font-family: Roboto, sans-serif;\r\n  }\r\n  \r\n  .toolBar{\r\n    background: darkslategrey;\r\n    top: 0;\r\n    position: fixed;\r\n    z-index:999;\r\n  }\r\n  \r\n  mat-toolbar-row {\r\n    -webkit-box-pack: justify;\r\n        -ms-flex-pack: justify;\r\n            justify-content: space-between;\r\n  }\r\n  \r\n  .example-spacer {\r\n    -webkit-box-flex: 1;\r\n        -ms-flex: 1 1 auto;\r\n            flex: 1 1 auto;\r\n  }\r\n  \r\n  .toolbar-menu{\r\n    color: black;\r\n  }\r\n  \r\n  .logo{\r\n    height: 26px;\r\n    margin: 0 4px 5px 15px;\r\n    vertical-align: middle;\r\n  }\r\n  \r\n  .title{\r\n    vertical-align: middle;\r\n    margin: 0 4px 3px 10px;\r\n    color: white;\r\n  }\r\n  \r\n  .mainContent{\r\n      height: 100vh;\r\n      background-image: url(\"https://brandspace.accenture.com/Pages/downloads/images/images/header_img.jpg\")\r\n  }\r\n  \r\n  .mainText{\r\n      color: white;\r\n      position: relative;\r\n      font-weight: bold;\r\n      margin-top: 140px;\r\n  }\r\n  \r\n  .formCard{\r\n      margin-top: 33px;\r\n  }\r\n  \r\n  .titleText{\r\n    color: aqua;\r\n    font-size: x-large;\r\n    padding: 11px;\r\n    border: 1px solid;\r\n  }\r\n  \r\n  .description{\r\n    margin-top: 30px;\r\n    font-style: italic;\r\n  }\r\n"

/***/ }),

/***/ "./src/app/welcome-component/welcome-component.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-8 col-sm-8 col-xs-8  text-center\">\r\n  <app-image-slider></app-image-slider>\r\n</div>\r\n<div class=\"col-md-4 col-sm-4 col-xs-4  formCard\">\r\n  <app-idea-logging></app-idea-logging>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/welcome-component/welcome-component.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WelcomeComponentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WelcomeComponentComponent = (function () {
    function WelcomeComponentComponent() {
    }
    WelcomeComponentComponent.prototype.ngOnInit = function () {
    };
    WelcomeComponentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-welcome-component',
            template: __webpack_require__("./src/app/welcome-component/welcome-component.component.html"),
            styles: [__webpack_require__("./src/app/welcome-component/welcome-component.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], WelcomeComponentComponent);
    return WelcomeComponentComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_hammerjs__ = __webpack_require__("./node_modules/hammerjs/hammer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_hammerjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_hammerjs__);





if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ }),

/***/ 2:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[1]);
//# sourceMappingURL=main.bundle.js.map